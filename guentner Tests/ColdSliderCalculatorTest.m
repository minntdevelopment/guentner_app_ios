//
//  ColdSliderCalculatorTest.m
//  guentner
//
//  Created by Julian Dawo on 13.02.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "ColdSliderCalculator.h"

@interface ColdSliderCalculatorTest : XCTestCase

@end

@implementation ColdSliderCalculatorTest {
    ColdSliderCalculator *_coldSliderCalculator;
}

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _coldSliderCalculator = [[ColdSliderCalculator alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    
    // mbar is the value in te reference table of guentner and the rest is according to the code
    
    
    NSString *pressure = @"bar (a)";
   
    NSString *tempUnit = @"Celsius";
    NSString *refrigarant = @"R22";
 
    // t = -50 und p = 645
    double result1 = [_coldSliderCalculator getPressure:pressure forTemperature:-50.0 temperatureUnit:tempUnit andRefrigerant:refrigarant];
    
    NSLog(@"The result: %f",result1);
    NSString* stringResult1 = [self helperForSiginficants:result1];
    
    
    
    XCTAssertTrue([stringResult1 isEqualToString:@"0.645"], @"The Value is correct.");
    
    
    NSString *pressure2 = @"bar (a)";
    double result2 = [_coldSliderCalculator getPressure:pressure2 forTemperature:35 temperatureUnit:tempUnit andRefrigerant:refrigarant];
    
    NSLog(@"The result: %f",result2);
    NSString* stringResult2 = [self helperForSiginficants:result2];

    
    
    XCTAssertTrue([stringResult2 isEqualToString:@"13.547"], @"35 °C are 13.547 bar.");
    
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

-(NSString *)helperForSiginficants:(double) input {
    NSNumber *number = [NSNumber numberWithDouble:input];
    NSNumberFormatter *numFormatter = [[NSNumberFormatter alloc] init];
    [numFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numFormatter setMaximumFractionDigits:3];
    [numFormatter setRoundingMode: NSNumberFormatterRoundHalfEven];
    
    NSString* stringNumber = [numFormatter stringFromNumber:number];
    NSLog(@"With 4 digits: %@",stringNumber);
    return stringNumber;
    
}

@end
