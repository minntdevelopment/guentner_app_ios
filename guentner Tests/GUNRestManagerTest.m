//
//  GUNRestManagerTest.m
//  guentner
//
//  Created by Julian Dawo on 03.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "GUNRestmanager.h"
#import "GUNVersionControlResponse.h"
#import "GUNLanguagesResponse.h"
#import "GUNDynamicMenuResponse.h"

@interface GUNRestManagerTest : XCTestCase

@end

@implementation GUNRestManagerTest {
    GUNRestmanager *_restManager;
}

- (void)setUp {

    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _restManager = [GUNRestmanager new];

}

- (void)tearDown {
    _restManager = nil;

    [super tearDown];
}

- (void)testGetVersionControlWebService {
    [_restManager getVersionControlOverWebService:@"" completion:^(GUNVersionControlResponse *response , NSError *error) {
        NSLog(@"%@",response.content_version);
        [[NSUserDefaults standardUserDefaults] synchronize];
    }];
}
- (void)testGetLanguageWebService {
    [_restManager getOneTranslationListOverWebService:@"" andLanguageKey: @"DE" completion:^(GUNLanguagesResponse *response, NSError *error) {
        NSLog(@"%@",response.languageKey);
        NSLog(@"%@", response.translations);
        [[NSUserDefaults standardUserDefaults] synchronize];
    }];
}

- (void)testGetDynamicMenuWebService {
    [_restManager getDynamicMenuOverWebService:@"" completion:^(GUNDynamicMenuResponse *response, NSError *error) {
        NSLog(@"%@",response.productsAsia);
        NSLog(@"%@",response.productsEurope);
        NSLog(@"%@",response.productsAmerica);
        NSLog(@"%@",response.products[0]);
        [[NSUserDefaults standardUserDefaults] synchronize];
    }];
}



@end
