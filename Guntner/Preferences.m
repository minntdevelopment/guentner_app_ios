//
//  Preferences.m
//  guentner
//
//  Created by Wassim Ben Hamadou on 20.05.14.
//  Copyright (c) 2014 Brandixi3. All rights reserved.
//

#import "Preferences.h"

@implementation Preferences

+(id)getUserPreference:(NSString *)forKey
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults valueForKey:forKey];
}

+(void)setUserPreference:(id)value forKey:(NSString *)key
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:value forKey:key];
    [defaults synchronize];
}

+(void)setAppOpenCount:(int)value
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:value forKey:PREF_APP_USE_COUNT];
    [defaults synchronize];
}

+(void)setPopupViewedFlag:(bool)value
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:value forKey:APP_RATING_POPUP_VIEWED];
    [defaults synchronize];
}

+(bool)getPopupViewedFlag
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    bool result = [defaults integerForKey:APP_RATING_POPUP_VIEWED];
    return result;
}

+(int)getAppOpenCount
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int result = [defaults integerForKey:PREF_APP_USE_COUNT];
    return result;
}



@end
