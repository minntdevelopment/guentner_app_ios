//
//  GUNInsightTutorialHintViewController.m
//  guentner
//
//  Created by Julian Dawo on 06.09.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "GUNInsightTutorialHintViewController.h"
#import "GUNTutorialManager.h"
#import "GUNRootManager.h"
@interface GUNInsightTutorialHintViewController ()

@end

@implementation GUNInsightTutorialHintViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _v_textBox.layer.shadowColor = color_black_015.CGColor;
    _v_textBox.layer.shadowOpacity = 1.0;
    _v_textBox.layer.shadowRadius = 3.0;
    _v_textBox.layer.shadowOffset = CGSizeMake(0, 3);
    _v_textBox.layer.masksToBounds = YES;
    _v_textBox.clipsToBounds = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    _txt_v_hintText.text = [[[GUNRootManager instance] tutorialManager] translationForKeyInActiveLanguage:_hintText];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
