//
//  GUNPopupInfoViewController.m
//  guentner
//
//  Created by Julian Dawo on 07.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "GUNPopupInfoViewController.h"
#import "AppDelegate.h"
#import "RootViewController.h"
#import "Configuration.h"

@interface GUNPopupInfoViewController ()

@end

@implementation GUNPopupInfoViewController {
    
    void (^_okButtonBlock)();
    NSString *_title;
    NSString *_messageText;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    _lbl_title.text = _title;
    _lbl_message.text = _messageText;
   
    
    //style
    _v_popupBox.layer.cornerRadius = 10.0f;
    _btn_ok.layer.cornerRadius = 10.0f;
    
    _v_popupBox.layer.shadowColor = color_black_015.CGColor;
    _v_popupBox.layer.shadowOpacity = 1.0;
    _v_popupBox.layer.shadowRadius = 3.0;
    _v_popupBox.layer.shadowOffset = CGSizeMake(0, 3);
    _v_popupBox.layer.masksToBounds = YES;
    _v_popupBox.clipsToBounds = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setNoButtonStyleWithTitle:(NSString*)title text:(NSString*)text okButtonBlock:(void (^)())okButtonBlock
{
    _title = title;
    _messageText = text;
    
    _okButtonBlock = okButtonBlock;
    
}


- (IBAction)tapOkBtn:(id)sender {
    
    [self close];
    if(_okButtonBlock) {
        _okButtonBlock();
    }
}

- (void)close {
    AppDelegate* appDelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    RootViewController* rootViewController = appDelegate.rootViewController;
    [rootViewController dismissPopupViewController:self];
}
@end
