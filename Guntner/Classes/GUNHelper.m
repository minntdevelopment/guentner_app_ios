//
//  GUNHelper.m
//  guentner
//
//  Created by Julian Dawo on 06.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "GUNHelper.h"

@implementation GUNHelper


+(NSString*)generateUUID {
    CFUUIDRef uuid = CFUUIDCreate(NULL);
    NSString* uuidStr = (__bridge_transfer NSString*) CFUUIDCreateString(NULL, uuid);
    CFRelease(uuid);
    return [[uuidStr stringByReplacingOccurrencesOfString:@"-" withString:@""] lowercaseString];
}

@end
