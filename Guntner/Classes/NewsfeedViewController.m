//
//  GECLiteViewController.m
//  Guntner
//
//  Created by Shiraz Omar on 8/6/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "NewsfeedViewController.h"

#import "Configuration.h"
#import "LoadingViewController.h"
#import "CommunicationManager.h"
#import "NewsObject.h"
#import "NewsFeedCell.h"
#import "NewsDetailViewController.h"

@implementation NewsfeedViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        // Uncomment to override the CDVCommandDelegateImpl used
        // _commandDelegate = [[MainCommandDelegate alloc] initWithViewController:self];
        // Uncomment to override the CDVCommandQueue used
        // _commandQueue = [[MainCommandQueue alloc] initWithViewController:self];
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NSLocale *currentLocale = [NSLocale currentLocale];
    NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    
    NSMutableArray* objectArray = [NSMutableArray array];
    [[CommunicationManager instance] getNews:[[NSLocale preferredLanguages] firstObject] withContry:countryCode andCompletionHandler:^(NSArray *result) {
        NSMutableArray* ids = [NSMutableArray array];
        for (NSDictionary* item in result) {
            [ids addObject:[item objectForKey:@"id"]];
            NewsObject* newsObject = [[NewsObject alloc] initWithId:[item objectForKey:@"id"] imageLink:[item objectForKey:@"imageLink"] text:[item objectForKey:@"text"] title:[item objectForKey:@"title"] createdDate:[item objectForKey:@"publishingDate"]];
            newsObject.isLinkedin = false;
            [objectArray addObject:newsObject];
            
        }
        
        self.newsArray = [objectArray sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSNumber *first = [(NewsObject*)a createdDate];
            NSNumber *second = [(NewsObject*)b createdDate];
            return [first compare:second];
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tv_news reloadData];
        });
        
        
        /*
        [[CommunicationManager instance] getLinkedinNewsWithCompletionHandler:^(NSArray *result) {
            
            for (NSDictionary* item in result) {
                NSNumber * generatedId = [self generatedIdExcept:ids];
                NewsObject* newsObject = [[NewsObject alloc] initWithId:generatedId imageLink:[item objectForKey:@"imageLink"] text:[item objectForKey:@"text"] title:[item objectForKey:@"title"] createdDate:[item objectForKey:@"publishingDate"]];
                newsObject.isLinkedin = true;
                [objectArray addObject:newsObject];
            }
            
            self.newsArray = [objectArray sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
                NSNumber *first = [(NewsObject*)a createdDate];
                NSNumber *second = [(NewsObject*)b createdDate];
                return [first compare:second];
            }];
            
            [self.tv_news reloadData];
        }];*/
        
    }];
   
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(NSNumber*) generatedIdExcept:(NSArray*) ids {
    long generatedNr = arc4random() % 1000;
    while ([ids containsObject:[NSNumber numberWithLong:generatedNr]]) {
        generatedNr = arc4random() % 1000;
    }
    return [NSNumber numberWithLong:generatedNr];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}


#pragma mark Auto Rotation Methods

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


#pragma mark Cleanup

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}


- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kShowLoadingViewIdentifier object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kDismissLoadingViewIdentifier object:nil];
    
}

#pragma mark NewsFeed DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Data Source Count
    return [self.newsArray count];
    
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        
    static NSString *CellIdentifier = @"NewsFeedCell";
    NewsFeedCell *cell = (NewsFeedCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    NewsObject* newsObject = [self.newsArray objectAtIndex:indexPath.row];
    [cell setupCellWithTitle:newsObject.title text:newsObject.text createdDate:newsObject.createdDate rowNr:indexPath.row];
    [cell setLinkedinNews:newsObject.isLinkedin];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NewsObject* selectedNewsObject = [self.newsArray objectAtIndex:indexPath.row];
    
    NewsDetailViewController *newsDetailViewcontroller = (NewsDetailViewController *)[[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"NewsDetailViewController"];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController pushViewController:newsDetailViewcontroller animated:YES];
    newsDetailViewcontroller.selectedNewsObject = selectedNewsObject;
}

@end
