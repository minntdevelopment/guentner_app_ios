//
//  TutorialProductItemCell.m
//  guentner
//
//  Created by Julian Dawo on 04.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "TutorialProductItemCell.h"

@implementation TutorialProductItemCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
