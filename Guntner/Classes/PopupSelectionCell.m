//
//  PopupSelectionCell.m
//  guentner
//
//  Created by Julian Dawo on 08.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "PopupSelectionCell.h"
#import "Configuration.h"

@implementation PopupSelectionCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    if (selected) {
        _iv_selectionIcon.highlighted = NO;
        self.contentView.backgroundColor = color_light_blue;
    } else {
        
        _iv_selectionIcon.highlighted = NO;
        self.contentView.backgroundColor = [UIColor whiteColor];
    }
}

@end
