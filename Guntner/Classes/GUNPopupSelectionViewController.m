//
//  GUNPopupSelectionViewController.m
//  guentner
//
//  Created by Julian Dawo on 08.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "GUNPopupSelectionViewController.h"
#import "AppDelegate.h"
#import "RootViewController.h"
#import "PopupSelectionCell.h"
#import "SelectionItemTO.h"
#import "Configuration.h"

#import <QuartzCore/QuartzCore.h>

@interface GUNPopupSelectionViewController () {
    
    void (^_okButtonBlock)(NSArray*) ;
    NSString *_titleText;
    BOOL _isMultiselection;
    NSMutableArray *_selectedItems;
    NSArray *_selectableItems;
}

@end

@implementation GUNPopupSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    //style
    _v_popupBox.layer.cornerRadius = 10.0f;
    _btn_ok.layer.cornerRadius = 10.0f;
    
    _v_popupBox.layer.shadowColor = color_black_015.CGColor;
    _v_popupBox.layer.shadowOpacity = 1.0;
    _v_popupBox.layer.shadowRadius = 3.0;
    _v_popupBox.layer.shadowOffset = CGSizeMake(0, 3);
    _v_popupBox.layer.masksToBounds = YES;
    _v_popupBox.clipsToBounds = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    _lbl_title.text = _titleText;
    _selectedItems = [[NSMutableArray alloc] init];
    
    if (_isMultiselection) {
        _tv_selectionTableView.allowsMultipleSelection = YES;
    } else {
        _tv_selectionTableView.allowsMultipleSelection = NO;
        
    }
    
    [_tv_selectionTableView reloadData];
    
    NSInteger i = 0;
    for (id item in _selectableItems) {
        if (((SelectionItemTO *) item).isSelected) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            [_tv_selectionTableView selectRowAtIndexPath:indexPath
                                   animated:NO
                             scrollPosition:UITableViewScrollPositionNone];
            [_selectedItems addObject:item];
        }
        i++;
    }
    [self handleOkButtonStatus];
    
   
}

- (IBAction)tapOkBtn:(id)sender {
    
    [self close];
    if(_okButtonBlock) {
        NSArray* result = [_selectedItems copy];
        _okButtonBlock(result);
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_selectableItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.row < _selectableItems.count) {
        static NSString *selectionCell = @"PopupSelectionCell";

        PopupSelectionCell *cell = (PopupSelectionCell *)[_tv_selectionTableView dequeueReusableCellWithIdentifier:selectionCell];



        SelectionItemTO *currentItem = (SelectionItemTO *)[_selectableItems objectAtIndex:indexPath.row];

        cell.lbl_selectableText.text = currentItem.translatedValue;
      

        return cell;
    }

    return nil;
}

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    SelectionItemTO *currentItem = (SelectionItemTO *)[_selectableItems objectAtIndex:indexPath.row];
//    if (currentItem.isSelected) {
//        [cell setSelected:YES animated:NO];
//    }
//}


-(void)setSelectionWithTitle:(NSString*)title multiselection:(BOOL)multiselection selectableItems:(NSArray *)selectableItems okButtonBlock:(void (^)(NSArray*)) okblock {
    _titleText = title;
    _isMultiselection = multiselection;
    _okButtonBlock = okblock;
    _selectableItems = selectableItems;
}


- (void)close {
    AppDelegate* appDelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    RootViewController* rootViewController = appDelegate.rootViewController;
    [rootViewController dismissPopupViewController:self];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SelectionItemTO *currentItem = (SelectionItemTO *)[_selectableItems objectAtIndex:indexPath.row];
    currentItem.isSelected = YES;
    [_selectedItems addObject:currentItem];
    [self handleOkButtonStatus];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SelectionItemTO *currentItem = (SelectionItemTO *)[_selectableItems objectAtIndex:indexPath.row];
    currentItem.isSelected = NO;
    [_selectedItems removeObject:currentItem];
    [self handleOkButtonStatus];
}


-(void) handleOkButtonStatus {
    if (_selectedItems.count > 0) {
        _btn_ok.enabled = YES;
    } else {
        _btn_ok.enabled = NO;
    }
}

@end
