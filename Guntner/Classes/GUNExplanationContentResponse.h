//
// Created by Julian Dawo on 03.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GUNProductContentResponse.h"


@interface GUNExplanationContentResponse : GUNProductContentResponse

@property (nonatomic, strong) NSString *explanationContentUrl;

@end