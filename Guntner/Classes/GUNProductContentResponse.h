//
// Created by Julian Dawo on 03.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>



@class GUNContentGroupResponse;
@interface GUNProductContentResponse : NSObject


@property (nonatomic, strong) NSNumber *contentType;
@property (nonatomic, strong) NSString *contentName;
@property (nonatomic, strong) NSArray *availableLanguages;
@property (nonatomic, strong) GUNContentGroupResponse *relatedGroup;

@end