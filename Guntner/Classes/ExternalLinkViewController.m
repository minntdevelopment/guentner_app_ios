//
//  ExternalLinkViewController.m
//  guentner
//
//  Created by Julian Dawo on 05.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "ExternalLinkViewController.h"
#import "GUNLoadingViewController.h"
#import "Configuration.h"
#import "GUNTutorialManager.h"
#import "GUNRootManager.h"
#import <QuartzCore/QuartzCore.h>

@interface ExternalLinkViewController ()

@end

@implementation ExternalLinkViewController {
    NSString* _loadingHandler;
    GUNTutorialManager *tutorialManager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _lbl_noConnection.text = @"There is no Connection to the internet.";
    _lbl_start.text = @"Swipe right to start the tutorial";
    _v_errorContainer.hidden = YES;
    
    _v_textbox.layer.shadowColor = color_black_015.CGColor;
    _v_textbox.layer.shadowOpacity = 1.0;
    _v_textbox.layer.shadowRadius = 3.0;
    _v_textbox.layer.shadowOffset = CGSizeMake(0, 3);
    _v_textbox.layer.masksToBounds = YES;
    _v_textbox.clipsToBounds = NO;
    
    tutorialManager = [[GUNRootManager instance]tutorialManager];
    _wv_externalLinkViewer.scrollView.bounces = NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [_wv_externalLinkViewer loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_contentUrl]]];
    //_v_errorContainer.hidden = NO;
    _wv_externalLinkViewer.hidden = NO;
}

-(void)webViewDidStartLoad:(UIWebView *)webView {
    
    if (!_loadingHandler) {
        _loadingHandler = [GUNLoadingViewController pushLoadingViewControllerWithText:[tutorialManager translationForKeyInActiveLanguage:@"Please wait. Loading tutorials..."]];
        NSLog(@"Start loading Content");
    }
}

 -(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {

     [GUNLoadingViewController popLoadingViewController:_loadingHandler];
     _v_errorContainer.hidden = NO;
     _wv_externalLinkViewer.hidden = YES;
 }

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"Stop loading Content");
    [_v_errorContainer setHidden:YES];
    [GUNLoadingViewController popLoadingViewController:_loadingHandler];
}


#pragma mark Auto Rotation Methods

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

@end
