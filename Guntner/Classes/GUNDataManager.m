//
// Created by Julian Dawo on 03.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "GUNDataManager.h"
#import "GUNVersionControlResponse.h"
#import "GUNLanguagesResponse.h"
#import "GUNDynamicMenuResponse.h"
#import "GUNLanguageItemResponse.h"
#import "GUNProductResponse.h"
#import "GUNProductContentResponse.h"
#import "Configuration.h"
#import "GUNContentTutorialResponse.h"
#import "GUNExplanationContentResponse.h"
#import "GUNContentGroupResponse.h"
#import "GUNTutorialDetailResponse.h"
#import "GUNTutorialDetailPageResponse.h"
#import "GUNTutorialPageStepResponse.h"
#import "GUNTutorialStepActionResponse.h"


@implementation GUNDataManager {
}


-(GUNVersionControlResponse *) getVersionControlFromDictionary:(NSDictionary *) jsonDictionary {

    GUNVersionControlResponse *result = [GUNVersionControlResponse new];
    result.content_version = (NSNumber *)[jsonDictionary valueForKey:@"content_version"];
    return result;
}


-(GUNLanguagesResponse *) getLanguageFromDictionary:(NSDictionary *) jsonDictionary {

    GUNLanguagesResponse *resultResponse = [GUNLanguagesResponse new];
    NSMutableArray*translationList = [[NSMutableArray alloc] init];
    NSArray *allTranslationItems = jsonDictionary[@"translations"];
    if ([allTranslationItems isKindOfClass:[NSArray class]]) {

        for (NSDictionary *tmpDictionary in allTranslationItems) {
            GUNLanguageItemResponse *languageItem = [[GUNLanguageItemResponse alloc] init];

            languageItem.key = [tmpDictionary objectForKey:@"key"];
            languageItem.value = [tmpDictionary objectForKey:@"value"];


            [translationList addObject:languageItem];
        }
    }

    resultResponse.translations = translationList;
    resultResponse.languageKey = [jsonDictionary valueForKey:@"language_key"];

    return resultResponse;
}


-(GUNDynamicMenuResponse *) getDynamicMenuFromDictionary:(NSDictionary *) jsonDictionary {

    GUNDynamicMenuResponse *resultResponse = [GUNDynamicMenuResponse new];

    resultResponse.productsAsia = jsonDictionary[@"region_asia"];
    resultResponse.productsEurope = jsonDictionary[@"region_europe"];
    resultResponse.productsAmerica = jsonDictionary[@"region_america"];

    NSArray *allProducts = jsonDictionary[@"products"];

    NSMutableArray *productList = [[NSMutableArray alloc] init];

    for (NSDictionary *tmpProduct in allProducts) {
        GUNProductResponse *productResponse = [GUNProductResponse new];
        productResponse.productID = (NSNumber *)[tmpProduct objectForKey:@"id"];
        productResponse.productName = (NSString *)[tmpProduct objectForKey:@"product_name"];

        NSArray *totalContent = tmpProduct[@"content"];
        NSMutableArray *contentList = [[NSMutableArray alloc] init];

        for (NSDictionary *tmpContent in totalContent) {

            NSNumber *tmpContentType = (NSNumber *)[tmpContent valueForKey:@"content_type"];
            GUNProductContentResponse *contentResponse;

           if (tmpContentType.integerValue == interactiveTutorialContentType) {
                contentResponse = [GUNContentTutorialResponse new];
                contentResponse.availableLanguages = tmpContent[@"available_languages"];
               ((GUNContentTutorialResponse *) contentResponse).tutorialId = tmpContent[@"tutorial_id"];

           } else if (tmpContentType.integerValue == externalExplanationContentType) {
               contentResponse = [GUNExplanationContentResponse new];
               contentResponse.availableLanguages = tmpContent[@"available_languages"];
               ((GUNExplanationContentResponse *) contentResponse).explanationContentUrl = tmpContent[@"explanation_content_url"];


           } else if (tmpContentType.integerValue == contentGroupContentType) {
               contentResponse = [GUNContentGroupResponse new];

               NSArray *groupContent = tmpContent[@"group_items"];
               NSMutableArray *groupContentList = [[NSMutableArray alloc] init];

               for (NSDictionary *tmpGroupItem in groupContent) {

                   NSNumber *tmpGroupItemContentType = (NSNumber *)[tmpGroupItem valueForKey:@"content_type"];

                   GUNProductContentResponse *groupItemContentResponse;

                   if (tmpGroupItemContentType.integerValue == interactiveTutorialContentType) {
                       groupItemContentResponse = [GUNContentTutorialResponse new];
                       groupItemContentResponse.availableLanguages = tmpGroupItem[@"available_languages"];
                       ((GUNContentTutorialResponse *) groupItemContentResponse).tutorialId = tmpGroupItem[@"tutorial_id"];


                       groupItemContentResponse.relatedGroup = ((GUNContentGroupResponse *) contentResponse);

                   } else if (tmpGroupItemContentType.integerValue == externalExplanationContentType) {
                       groupItemContentResponse = [GUNExplanationContentResponse new];
                       groupItemContentResponse.availableLanguages = tmpGroupItem[@"available_languages"];
                       ((GUNExplanationContentResponse *) groupItemContentResponse).explanationContentUrl = tmpGroupItem[@"explanation_content_url"];


                       groupItemContentResponse.relatedGroup = ((GUNContentGroupResponse *) contentResponse);


                   } else {
                       NSLog(@"Error no content type found in group content Item.");
                   }
                   groupItemContentResponse.contentType = tmpGroupItemContentType;
                   groupItemContentResponse.contentName = (NSString *)[tmpGroupItem valueForKey:@"content_name"];
                   [groupContentList addObject:groupItemContentResponse];
               }
               ((GUNContentGroupResponse *) contentResponse).extended = NO;
               ((GUNContentGroupResponse *) contentResponse).groupItems = groupContentList;

            } else {
               NSLog(@"Error no content type found.");
           }
            contentResponse.contentType = tmpContentType;
            contentResponse.contentName = (NSString *)[tmpContent valueForKey:@"content_name"];

            [contentList addObject:contentResponse];

        }
        
        productResponse.content = contentList;
        [productList addObject:productResponse];

    }
    resultResponse.products = productList;

    return resultResponse;
}


-(void)saveVersionControlPersistent:(NSNumber *) versionControlAsNumber {


    [[NSUserDefaults standardUserDefaults] setObject:versionControlAsNumber forKey:@"activeVersion"];

    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(NSNumber *) getVersionControlNumber {

    NSNumber *savedVersion = [[NSUserDefaults standardUserDefaults] objectForKey:@"activeVersion"];


    return savedVersion;
}

-(void)saveLanguagePersistent:(NSDictionary *) languageResponse languageKey: (NSString *)languageKey {

    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:languageResponse];
    NSString *userDefaultsKey = [NSString localizedStringWithFormat:@"language_%@",[languageKey lowercaseString]];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:userDefaultsKey];

    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSDictionary *) getLanguageDictionaryForLanguageKey: (NSString *)languageKey {

    NSString *userDefaultsKey = [NSString localizedStringWithFormat:@"language_%@",[languageKey lowercaseString]];
    NSData *dictionaryData = [[NSUserDefaults standardUserDefaults] objectForKey:userDefaultsKey];
    NSDictionary *dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData];

    return dictionary;
}

-(void)saveDynamicMenuPersistent:(NSDictionary *) dynamicMenuResponse {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dynamicMenuResponse];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"currentMenu"];

    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSDictionary *) getDynamicMenuDictionary {

    NSData *dictionaryData = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentMenu"];
    NSDictionary *dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData];

    return dictionary;
}

- (void)saveSelectedLanguagesForTutorialPersistent:(NSArray *)selectedLanguages {
    [[NSUserDefaults standardUserDefaults] setObject:selectedLanguages forKey:@"selectedLanguages"];

    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSArray *)getSelectedLanguagesForTutorials {
    NSArray *selectedLanguages = [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedLanguages"];


    return selectedLanguages;

}

- (void)saveSelectedRegionForTutorialsPersistent:(ContenRegion)selectedRegion {

    NSNumber *numberToStore = [NSNumber numberWithInteger:selectedRegion];

    [[NSUserDefaults standardUserDefaults] setObject:numberToStore forKey:@"selectedRegion"];

    [[NSUserDefaults standardUserDefaults] synchronize];

}

- (ContenRegion)getSelectedRegionForTutorials {

    NSNumber *selectedRegion = [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedRegion"];


    return (ContenRegion)selectedRegion.integerValue;

}


- (NSDictionary *)getTutorialByID:(NSInteger) tutorialID {

    NSString *keyString = [NSString stringWithFormat:@"tutorialDetails_%ld", (long) tutorialID ];
    NSData *dictionaryData = [[NSUserDefaults standardUserDefaults] objectForKey:keyString];
    
    if (dictionaryData) {
        NSDictionary *dictionary = [NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData];
        return dictionary;
    }
    
    return nil;
    
}

- (void)saveTutorialDetailsPersistent:(NSDictionary *) tutorialDetailDictonary withID:(NSInteger) tutorialID {
    NSLog(@"not yet implemented.");

    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:tutorialDetailDictonary];

    NSString *keyString = [NSString stringWithFormat:@"tutorialDetails_%ld", (long) tutorialID];

    [[NSUserDefaults standardUserDefaults] setObject:data forKey:keyString];

    [[NSUserDefaults standardUserDefaults] synchronize];


}

- (GUNTutorialDetailResponse *)getTutorialDetailsFromDictionary:(NSDictionary *)dictionary {

    if (dictionary == nil) {
        return nil;
    }
    
    GUNTutorialDetailResponse *result = [GUNTutorialDetailResponse new];
    result.explainationURL = dictionary[@"explanation_url"];
    result.feedbackText = dictionary[@"feedback_text"];
    result.tutorialID = dictionary[@"tutorialID"];

    NSArray *allPages = dictionary[@"pages"];

    NSMutableArray *pageList = [[NSMutableArray alloc] init];

    for (NSDictionary *tmpDicPage in allPages) {

        GUNTutorialDetailPageResponse *tmpPageResponse = [GUNTutorialDetailPageResponse new];

        tmpPageResponse.pageType = tmpDicPage[@"page_type"];

        NSArray *allSteps = tmpDicPage[@"steps"];

        NSMutableArray *stepList = [[NSMutableArray alloc] init];

        for (NSDictionary *tmpDicStep in allSteps) {

            GUNTutorialPageStepResponse *tmpStepResponse = [GUNTutorialPageStepResponse new];
            tmpStepResponse.stepDescriptionLine1 = tmpDicStep[@"step_description_line1"];
            tmpStepResponse.stepDescriptionLine2 = tmpDicStep[@"step_description_line2"];
            tmpStepResponse.showEqualActionCount = [tmpDicStep[@"show_equal_action_count"] boolValue];
            tmpStepResponse.actionsButtonType = tmpDicStep[@"button_type"];
            NSArray *allActions = tmpDicStep[@"actions"];

            NSMutableArray *actionList = [[NSMutableArray alloc] init];

            for (NSDictionary *tmpDicAction in allActions) {

                GUNTutorialStepActionResponse *tmpActionResponse = [GUNTutorialStepActionResponse new];

                tmpActionResponse.displayStartLine1Text = tmpDicAction[@"start_line1_text"];
                tmpActionResponse.displayStartLine2Text = tmpDicAction[@"start_line2_text"];
                tmpActionResponse.displayEndLine1Text = tmpDicAction[@"end_line1_text"];
                tmpActionResponse.displayEndLine2Text = tmpDicAction[@"end_line2_text"];

                tmpActionResponse.displayStartLine1Icons = tmpDicAction[@"start_line1_icons"];
                tmpActionResponse.displayStartLine2Icons = tmpDicAction[@"start_line2_icons"];
                tmpActionResponse.displayEndLine1Icons = tmpDicAction[@"end_line1_icons"];
                tmpActionResponse.displayEndLine2Icons = tmpDicAction[@"end_line2_icons"];

                [actionList addObject:tmpActionResponse];


            }

            tmpStepResponse.stepActions = [actionList copy];

            [stepList addObject:tmpStepResponse];


        }
        tmpPageResponse.hintPageExplanation = tmpDicPage[@"hint_text"];
        tmpPageResponse.steps = [stepList copy];
        [pageList addObject:tmpPageResponse];

    }

    result.tutorialPages = [pageList copy];

    return result;
}

- (NSArray *)getAllTutorialsWithFeedback {

    NSArray *tutorialsWithFeedback = [[NSUserDefaults standardUserDefaults] objectForKey:@"tutorialsWithFeedback"];

    if (tutorialsWithFeedback) {
        return tutorialsWithFeedback;
    }

    return [NSArray new];
}

- (void)addTutorialToFeedbackList:(NSNumber *) tutorialID {
    NSMutableArray *tutorialsWithFeedback = [NSMutableArray arrayWithArray:[self getAllTutorialsWithFeedback]];

    [tutorialsWithFeedback addObject:tutorialID];
    [[NSUserDefaults standardUserDefaults] setObject:tutorialsWithFeedback forKey:@"tutorialsWithFeedback"];

    [[NSUserDefaults standardUserDefaults] synchronize];

}
@end