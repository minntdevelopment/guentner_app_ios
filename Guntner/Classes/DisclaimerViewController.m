//
//  DisclaimerViewController.m
//  guentner
//
//  Created by Daniela Becker on 19.06.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "DisclaimerViewController.h"

@interface DisclaimerViewController () <UIScrollViewDelegate>{

    
    IBOutlet UITextView *contentView;
    IBOutlet UIScrollView *scrollView;
    
}

@property (nonatomic, weak) IBOutlet UITextView *contentView;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;

@end


@implementation DisclaimerViewController


@synthesize scrollView = _scrollView;
@synthesize contentView = _contentView;


#pragma mark Setup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    // Show Navigation Bar
    self.navigationController.navigationBarHidden = NO;
    
    
    // Setup ScrollView and ContentView
    self.scrollView.delegate = self;
//    CGRect contentViewFrame = self.contentView.frame;
//    if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqual:kGermanLocale]) {
//        self.scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, 500.0);
//        //contentViewFrame.size.height = 1054.0;
//    } else {
//        self.scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, 500.0);
//        //contentViewFrame.size.height = 844.0;
//    }
    
    
    // Round ContentView's Corners
    [self roundViewsCorners:self.contentView];
    
    
    // Create And Show Back Button
    [self createAndShowBackButton];

    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    self.scrollView.contentSize = CGSizeMake(self.contentView.contentSize.width, self.contentView.contentSize.height + 10.0);
}

- (void)roundViewsCorners:(UIView *)view {
    
    // Set View's Corner Radius To 5px To Round The Corners
    view.layer.cornerRadius = 5.0;
    view.layer.masksToBounds = YES;
    
}


- (void)createAndShowBackButton {
    
    // Create and Show Back Button
    UIImage *buttonImage = [UIImage imageNamed:kBackButtonImage];
    CGRect buttonFrame = kBackButtonFrame;
    UIButton *backButton = [[UIButton alloc] initWithFrame:buttonFrame];
    [backButton setTitle:kBackButtonTitle forState:UIControlStateNormal];
    [backButton setTitleColor:kBackButtonTitleColor forState:UIControlStateNormal];
    [[backButton titleLabel] setFont:kBackButtonTitleFont];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(goToPreviousViewController) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationController.topViewController.navigationItem.leftBarButtonItem = backBarButton;
    backBarButton = nil;
    buttonImage = nil;
    
}


- (void)goToPreviousViewController {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark Auto Rotation Methods

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

@end
