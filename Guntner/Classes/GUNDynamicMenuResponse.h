//
// Created by Julian Dawo on 01.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GUNDynamicMenuResponse : NSObject

@property (nonatomic, strong) NSArray *productsAsia;
@property (nonatomic, strong) NSArray *productsEurope;
@property (nonatomic, strong) NSArray *productsAmerica;

@property (nonatomic, strong) NSArray *products;
@end