//
// Created by Julian Dawo on 01.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GUNVersionControlResponse : NSObject

@property (nonatomic, strong) NSNumber *content_version;

//- (id)initWithDictionary:(NSDictionary *)dictionary;
@end