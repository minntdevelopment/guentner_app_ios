//
// Created by Julian Dawo on 03.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Configuration.h"

@class GUNVersionControlResponse;
@class GUNLanguagesResponse;
@class GUNDynamicMenuResponse;
@class GUNTutorialDetailResponse;


@interface GUNDataManager : NSObject

-(GUNVersionControlResponse *) getVersionControlFromDictionary:(NSDictionary *) jsonDictionary;
-(GUNLanguagesResponse *) getLanguageFromDictionary:(NSDictionary *) jsonDictionary;
-(GUNDynamicMenuResponse *) getDynamicMenuFromDictionary:(NSDictionary *) jsonDictionary;

-(void)saveVersionControlPersistent:(NSNumber *) versionControlAsNumber;
-(NSNumber *) getVersionControlNumber;
-(void)saveLanguagePersistent:(NSDictionary *) languageResponse languageKey: (NSString *)languageKey;
-(NSDictionary *) getLanguageDictionaryForLanguageKey: (NSString *)languageKey;
-(void)saveDynamicMenuPersistent:(NSDictionary *) dynamicMenuResponse;
-(NSDictionary *) getDynamicMenuDictionary;

-(void)saveSelectedLanguagesForTutorialPersistent: (NSArray *) selectedLanguages;
-(NSArray *)getSelectedLanguagesForTutorials;

-(void)saveSelectedRegionForTutorialsPersistent:(ContenRegion) selectedRegion;
-(ContenRegion)getSelectedRegionForTutorials;

- (NSDictionary *)getTutorialByID:(NSInteger) tutorialID;
- (void)saveTutorialDetailsPersistent:(NSDictionary *) tutorialDetailDictonary withID:(NSInteger) tutorialID;

- (GUNTutorialDetailResponse *)getTutorialDetailsFromDictionary:(NSDictionary *)dictionary;

- (NSArray *)getAllTutorialsWithFeedback;

- (void)addTutorialToFeedbackList:(NSNumber *) tutorialID;
@end