//
// Created by Julian Dawo on 17.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "GUNInsideTutorialExplanationViewController.h"
#import "GUNLoadingViewController.h"
#import "Configuration.h"
#import "GUNRootManager.h"
#import "GUNTutorialManager.h"

#import <QuartzCore/QuartzCore.h>
@implementation GUNInsideTutorialExplanationViewController {
    NSString* _loadingHandler;
    GUNTutorialManager *_tutorialManager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _lbl_noConnection.text = @"There is no Connection to the internet.";
    _lbl_start.text = @"Swipe right to start the tutorial.";
    _v_errorBox.hidden = YES;
    
    _v_textBox.layer.shadowColor = color_black_015.CGColor;
    _v_textBox.layer.shadowOpacity = 1.0;
    _v_textBox.layer.shadowRadius = 3.0;
    _v_textBox.layer.shadowOffset = CGSizeMake(0, 3);
    _v_textBox.layer.masksToBounds = YES;
    _v_textBox.clipsToBounds = NO;
    _wv_explanation.clipsToBounds = YES;
    _tutorialManager = [[GUNRootManager instance]tutorialManager];
    
    if (!_loadingHandler) {
        _loadingHandler = [GUNLoadingViewController pushLoadingViewControllerWithText:[_tutorialManager translationForKeyInActiveLanguage:@"Please wait. Loading tutorials..."]];
        NSLog(@"Start loading Content");
        
    }
    [_wv_explanation loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_contentUrl]]];

    _wv_explanation.hidden = NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //[_wv_explanation loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_contentUrl]]];
    _v_errorBox.hidden = YES;
    _wv_explanation.hidden = NO;
}

-(void)webViewDidStartLoad:(UIWebView *)webView {

    if (!_loadingHandler) {
        _loadingHandler = [GUNLoadingViewController pushLoadingViewControllerWithText:[_tutorialManager translationForKeyInActiveLanguage:@"Please wait. Loading tutorials..."]];
        NSLog(@"Start loading Content");

    }


}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {

    [GUNLoadingViewController popLoadingViewController:_loadingHandler];
    _v_errorBox.hidden = NO;
    _wv_explanation.hidden = YES;
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"Stop loading Content");

    [GUNLoadingViewController popLoadingViewController:_loadingHandler];
    _v_errorBox.hidden = YES;
    _wv_explanation.hidden = NO;
}


#pragma mark Auto Rotation Methods

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


@end