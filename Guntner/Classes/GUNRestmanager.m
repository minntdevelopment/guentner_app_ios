//
//  GUNRestmanager.m
//  guentner
//
//  Created by Julian Dawo on 01.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "GUNRestmanager.h"
#import "GUNVersionControlResponse.h"
#import "Configuration.h"
#import "GUNLanguagesResponse.h"
#import "GUNDataManager.h"
#import "GUNDynamicMenuResponse.h"
#import "GUNRootManager.h"
#import "GUNTutorialDetailResponse.h"


#define wsTimeoutInterval 15.0f
#define wsBaseURL @"http://www.guentner.de/mobile/" //Live

//#define wsBaseURL @"http://clients.everywhere-m.com/guentner/app/tutorials/"  // Test

@implementation GUNRestmanager {
    GUNDataManager *_dataManager;
    NSString *_baseURL;
}

-(instancetype)init {

    if(self = [super init])
    {
        _dataManager = [GUNRootManager instance].dataManager;
        _baseURL = wsBaseURL;
    }
    return self;
}


- (id)initWithDataManager:(GUNDataManager *)dataManager {
    self = [super init];

    if (self) {

        _dataManager = dataManager;
        _baseURL = wsBaseURL;


    }
    return self;
}



-(void) getVersionControlOverWebService:(NSString*)webServiceURL completion:(void (^)(GUNVersionControlResponse*, NSError*))block {

    NSLog(@"Call Web Serice for Version - URL: %@",webServiceURL);
#if 1 == USE_FIXTURES
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"versionControl" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

    GUNVersionControlResponse* response = [_dataManager getVersionControlFromDictionary:result];
    block(response,nil);

#else
    NSString *fullURLString = [NSString stringWithFormat:@"%@/%@",_baseURL,webServiceURL];
    
    NSURL *url = [NSURL URLWithString:fullURLString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:wsTimeoutInterval];

    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                   NSData *data, NSError *connectionError)
                           {
                               if (data.length > 0 && connectionError == nil)
                               {
                                   NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                                        options:0
                                                                                          error:NULL];

                                   GUNVersionControlResponse* response = [_dataManager getVersionControlFromDictionary:result];
                                   
                                   if (response) {
                                       block(response,nil);
                                   } else {
                                       
                                       block(nil,[self offlineErrorNoData]);
                                   }

                               } else {
                                   NSLog(@"Error while web service call. %@", connectionError.localizedDescription);
                                   block(nil,connectionError);
                               }
                           }];
#endif

}


-(void) getOneTranslationListOverWebService:(NSString*) webServiceURL andLanguageKey:(NSString *) languageKey completion:(void (^)(GUNLanguagesResponse*, NSError*))block {


#if 1 == USE_FIXTURES
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"languageDE" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

    GUNLanguagesResponse* response = [_dataManager getLanguageFromDictionary:result];
    
    [_dataManager saveLanguagePersistent:result languageKey:languageKey];
    block(response,nil);

#else
    NSString *fullURLString = [NSString stringWithFormat:@"%@/%@%@.json",_baseURL,webServiceURL,[languageKey uppercaseString]];
    
    NSURL *url = [NSURL URLWithString:fullURLString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:wsTimeoutInterval];

    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                   NSData *data, NSError *connectionError)
                           {
                               if (data.length > 0 && connectionError == nil)
                               {
                            
                                   NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                                          options:0
                                                                                            error:NULL];

                                   GUNLanguagesResponse* response = [_dataManager getLanguageFromDictionary:result];
                                   [_dataManager saveLanguagePersistent:result languageKey:languageKey];
                                   if (response) {
                                       block(response,nil);
                                   } else {
                                       
                                       block(nil,[self offlineErrorNoData]);
                                   }
                               } else {
                                   NSLog(@"Error while web service call. %@", connectionError.localizedDescription);
                                   block(nil,connectionError);
                               }
                           }];
#endif

}

-(void) getDynamicMenuOverWebService:(NSString*) webServiceURL completion:(void (^)(GUNDynamicMenuResponse*, NSError*))block {


#if 1 == USE_FIXTURES
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"dynamicTutorialMenu" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

    GUNDynamicMenuResponse* response = [_dataManager getDynamicMenuFromDictionary:result];
    [_dataManager saveDynamicMenuPersistent:result];
    block(response,nil);

#else
    NSString *fullURLString = [NSString stringWithFormat:@"%@/%@",_baseURL,webServiceURL];
    
    NSURL *url = [NSURL URLWithString:fullURLString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:wsTimeoutInterval];

    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                   NSData *data, NSError *connectionError)
                           {
                               if (data.length > 0 && connectionError == nil)
                               {
                                   NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                                          options:0
                                                                                            error:NULL];

                                   GUNDynamicMenuResponse* response = [_dataManager getDynamicMenuFromDictionary:result];
                                   [_dataManager saveDynamicMenuPersistent:result];
                                   if (response) {
                                       block(response,nil);
                                   } else {
                                       
                                       block(nil,[self offlineErrorNoData]);
                                   }

                               } else {
                                   NSLog(@"Error while web service call. %@", connectionError.localizedDescription);
                                   block(nil,connectionError);
                               }
                           }];
#endif

}

- (void)getTutorialDetailsOverWebService:(NSString *)webServiceURL withID:(NSInteger) tutorialID completion:(void (^)(GUNTutorialDetailResponse *, NSError *))block {

#if 1 == USE_FIXTURES
//    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tutorialDetails2" ofType:@"json"];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"tutorialDetail1" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

    GUNTutorialDetailResponse* response = [_dataManager getTutorialDetailsFromDictionary:result];
    [_dataManager saveTutorialDetailsPersistent:result withID:tutorialID];
    block(response,nil);
#else
    
    
    NSString *fullURLString = [NSString stringWithFormat:@"%@/%@/%ld.json",_baseURL,webServiceURL,(long) tutorialID];
    
    NSURL *url = [NSURL URLWithString:fullURLString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:wsTimeoutInterval];

    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response,
                                   NSData *data, NSError *connectionError)
                           {
                               if (data.length > 0 && connectionError == nil)
                               {
                                   NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                                          options:0
                                                                                            error:NULL];

                                   GUNTutorialDetailResponse* response = [_dataManager getTutorialDetailsFromDictionary:result];
                                   [_dataManager saveTutorialDetailsPersistent:result withID:tutorialID];
                                   
                                   if (response) {
                                       block(response,nil);
                                   } else {
                                       
                                       block(nil,[self offlineErrorNoData]);
                                   }
                                   

                               } else {
                                   NSLog(@"Error while web service call. %@", connectionError.localizedDescription);
                                   block(nil,connectionError);
                               }
                           }];
#endif

}

-(NSError *)offlineErrorNoData {
    NSString *errorDescription = @"The selected Link is not existing.";
    NSMutableDictionary* details = [NSMutableDictionary dictionary];
    [details setValue:errorDescription forKey:NSLocalizedDescriptionKey];
    
    return [NSError errorWithDomain:@"com.everywhere.guentner" code:101 userInfo:details];
}

@end
