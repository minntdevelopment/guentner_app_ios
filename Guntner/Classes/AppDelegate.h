//
//  AppDelegate.h
//  Guntner
//
//  Created by Shiraz Omar on 8/5/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <Cordova/CDVCommandQueue.h>
#import <Cordova/CDVViewController.h>
#import <Cordova/CDVCommandDelegateImpl.h>

@class ViewDeckController;
@class RootViewController;


@interface AppDelegate : UIResponder <UIApplicationDelegate> {

}


@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) RootViewController *rootViewController;

-(ViewDeckController *)getViewDeckController;

@end


@interface MainCommandDelegate : CDVCommandDelegateImpl
@end


@interface MainCommandQueue : CDVCommandQueue
@end
