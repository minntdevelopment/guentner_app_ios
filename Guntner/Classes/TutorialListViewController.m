//
//  TutorialListViewController.m
//  guentner
//
//  Created by Julian Dawo on 05.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "TutorialListViewController.h"
#import "GUNProductResponse.h"
#import "GUNProductContentResponse.h"
#import "Configuration.h"
#import "TutorialItemCell.h"
#import "ExternalLinkItemCell.h"
#import "TutorialGroupItemCell.h"
#import "GUNContentGroupResponse.h"
#import "GUNTutorialManager.h"
#import "GUNRootManager.h"
#import "ExternalLinkViewController.h"
#import "GUNExplanationContentResponse.h"
#import "GUNPopupHelper.h"
#import "LoadingViewController.h"
#import "GUNLoadingViewController.h"
#import "SelectionItemTO.h"
#import "GUNLanguageTO.h"
#import "GUNContentTutorialResponse.h"
#import "GUNTutorialDetailViewController.h"

@interface TutorialListViewController ()

@end

@implementation TutorialListViewController {
    
    GUNTutorialManager *_tutorialManager;
    NSMutableArray *_contentDataSource;
    NSString * _activeLanguage;
    BOOL _onceAppeared;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    _tutorialManager = [GUNRootManager instance].tutorialManager;
    _contentDataSource = [[NSMutableArray alloc] initWithArray:_product.content];
    
    //all content needs to be shrinked.
    for (GUNProductContentResponse *currentContent in _contentDataSource) {
        if ([currentContent isKindOfClass:[GUNContentGroupResponse class]]) {
            GUNContentGroupResponse *groupContent = (GUNContentGroupResponse *) currentContent;
            groupContent.extended = NO;
        }
        
        

    }
    
    _tv_tutorialList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _tv_tutorialList.alwaysBounceVertical = NO;
    
    if (_tutorialManager.getActiveLangues.count > 0) {
        // user selected a language
        NSLog(@"User selected a language");
        _activeLanguage = [_tutorialManager.getActiveLangues objectAtIndex:0];
        
    } else {
        NSLog(@"User did not select a language");
        _activeLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
    }
    NSLog(@"Active Language: %@",_activeLanguage);
    
    if (_product.content.count > 0) {
        [self showContent];
    } else {
        [self showNoContentError];
    }
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!_onceAppeared) {
        _onceAppeared = YES;
    } else {
        
        [_tv_tutorialList reloadData];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showNoContentError {
    NSString *translatedErrorText = [_tutorialManager translationForKeyInActiveLanguage:@"No tutorial available for this unit type."];
    _lbl_errorNoContent.text = translatedErrorText;
    _tv_tutorialList.hidden = YES;
    _v_noContent.hidden = NO;
}

- (void)showContent {
    [_tv_tutorialList reloadData];
    _tv_tutorialList.hidden = NO;
    _v_noContent.hidden = YES;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat result = 60.0f;
    
 

    return result;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_contentDataSource count];
}





- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < [_contentDataSource count]) {

        GUNProductContentResponse *currentContent = (GUNProductContentResponse *) [_contentDataSource objectAtIndex:indexPath.row];

        if (currentContent.contentType.integerValue == interactiveTutorialContentType) {

            TutorialItemCell *cell = (TutorialItemCell *)[_tv_tutorialList dequeueReusableCellWithIdentifier:@"TutorialItemCell"];
            cell.availableLanguages = currentContent.availableLanguages;
            [cell setupViewForAvailableLanguages];
            cell.lbl_tutorialName.text = [_tutorialManager translationForKeyInActiveLanguage:currentContent.contentName];
            
            if (currentContent.relatedGroup) {
                cell.backgroundColor = [UIColor whiteColor];
            } else {
                cell.backgroundColor = color_light_blue; 
            }

            //check if menu icons needs to be shown to the user
            

            if ([currentContent.availableLanguages containsObject:[_activeLanguage uppercaseString]]) {
                //selected language exists in Content
                NSLog(@"active language exists in Content");
                cell.v_availableLanguagesContainer.hidden = YES;
                [cell updateNamePosition];
            } else {
                //selected language does not exist in Content
                NSLog(@"active language does not exist in Content");
                cell.v_availableLanguagesContainer.hidden = NO;
            }
            
            return cell;

        } else if (currentContent.contentType.integerValue == externalExplanationContentType) {

            ExternalLinkItemCell *cell = (ExternalLinkItemCell *)[_tv_tutorialList dequeueReusableCellWithIdentifier:@"ExternalLinkItemCell"];
            cell.lbl_linkName.text = [_tutorialManager translationForKeyInActiveLanguage:currentContent.contentName];
            
            if (currentContent.relatedGroup) {
                cell.backgroundColor = [UIColor whiteColor];
            } else {
                cell.backgroundColor = color_light_blue;
            }
            
            return cell;

        } else if (currentContent.contentType.integerValue == contentGroupContentType) {

            TutorialGroupItemCell *cell = (TutorialGroupItemCell *)[_tv_tutorialList dequeueReusableCellWithIdentifier:@"TutorialGroupItemCell"];
            cell.lbl_groupName.text = [_tutorialManager translationForKeyInActiveLanguage:currentContent.contentName];
            cell.backgroundColor = color_light_blue;
            GUNContentGroupResponse *groupContent = (GUNContentGroupResponse *) currentContent;
           if (groupContent.extended) {
               
               [cell expandGroup];
            } else {
               
                [cell shrinkGroup];
            }
            return cell;

        }


    }

    return nil;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    GUNProductContentResponse *currentContent = (GUNProductContentResponse *) [_contentDataSource objectAtIndex:indexPath.row];
    
    if (currentContent.contentType.integerValue == interactiveTutorialContentType) {
        
        //load tutorial
        NSLog(@"Tutorial Tapped");
        
        // check if languagePopup is needed

        if ([currentContent.availableLanguages containsObject:[_activeLanguage uppercaseString]]) {
            //selected language exists in Content
            NSLog(@"active language exists in Content, show tutorial detail screen");
            
            // setup details screen
            // jump to detail screen
            
           // NSArray *activeLanguages = _tutorialManager.getActiveLangues;

            //added Load language if not exists
            NSString *loadingHandler = [GUNLoadingViewController pushLoadingViewControllerWithText:[_tutorialManager translationForKeyInActiveLanguage:@"Please wait. Loading tutorials..."]];
            
            [_tutorialManager downloadLangugeIfNotExistsWithLanguageKey:_activeLanguage complition:^(NSError *error) {
                 [GUNLoadingViewController popLoadingViewController:loadingHandler];
                
                if (error) {
                    
                    // Show error loading problem
                    NSString *titleText = [_tutorialManager translationForKeyInActiveLanguage:@"Connection error"];
                    NSString *messageText = [_tutorialManager translationForKeyInActiveLanguage:@"An error occurred while downloading the new menu content"];
                    [GUNPopupHelper showOneButtonStyleAlertViewWithTitle:titleText messageText:messageText okButtonBlock:^{
                        NSLog(@"error while language selection");
                    }];
                    
                } else {
                    if ([currentContent isKindOfClass:[GUNContentTutorialResponse class]]) {
                        
                        [self loadTutorialDetail:(GUNContentTutorialResponse *) currentContent];
                    }
                }

            }];
            
            
        } else {
            //selected language does not exist in Content
            NSLog(@"active language does not exist in Content");
            NSString *title = [_tutorialManager translationForKeyInActiveLanguage:@"Select Language"];
            NSMutableArray *selectableLanguages = [[NSMutableArray alloc] init];

            for ( NSString *item in currentContent.availableLanguages ) {

                BOOL preSelect = NO;

                NSString *translatedValue = [_tutorialManager translationForKeyInActiveLanguage:[_tutorialManager nameForLanaguageOfLanguageCode:item]];
                [selectableLanguages addObject:[[SelectionItemTO alloc] initWithKey:item andTranslatedValue:translatedValue selectionState:preSelect]];

            }

            [GUNPopupHelper showSelectionWithTitle:title multiselection:NO selectableItems:selectableLanguages okButtonBlock:^(NSArray *array) {
                
                SelectionItemTO* selectedItem = ((SelectionItemTO *) [array objectAtIndex:0]);
                
                NSString *languageKey = (NSString *)selectedItem.key;
                [_tutorialManager setActiveLangues:@[languageKey]];
                NSString *loadingHandler = [GUNLoadingViewController pushLoadingViewControllerWithText:[_tutorialManager translationForKeyInActiveLanguage:@"Please wait. Loading tutorials..."]];
                
                [_tutorialManager downloadLangugeIfNotExistsWithLanguageKey:languageKey complition:^(NSError *error) {

                    [GUNLoadingViewController popLoadingViewController:loadingHandler];
                    if (error) {

                        // Show error loading problem
                        NSString *titleText = [_tutorialManager translationForKeyInActiveLanguage:@"Connection error"];
                        NSString *messageText = [_tutorialManager translationForKeyInActiveLanguage:@"An error occurred while downloading the new menu content"];
                        [GUNPopupHelper showOneButtonStyleAlertViewWithTitle:titleText messageText:messageText okButtonBlock:^{
                            NSLog(@"error while language selection");
                            
                            if ([currentContent isKindOfClass:[GUNContentTutorialResponse class]]) {
                                
                                [self loadTutorialDetail:(GUNContentTutorialResponse *) currentContent];
                            }
                        }];

                    } else {
                        
                        NSLog(@"language selected, show tutorial detail screen");
                        
                        // setup details screen
                        // jump to detail screen
                        if ([currentContent isKindOfClass:[GUNContentTutorialResponse class]]) {

                            [self loadTutorialDetail:(GUNContentTutorialResponse *) currentContent];
                        }

                    }
                }];


            }];
        }
        
        
        
        
    } else if (currentContent.contentType.integerValue == externalExplanationContentType) {
        
        // load external Link
        NSLog(@"Extra Link Tapped");

        ExternalLinkViewController *externalLinkViewController = (ExternalLinkViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ExternalLinkViewController"];

        if (externalLinkViewController != nil) {

            NSString* translatedExplanationName = [_tutorialManager translationForKeyInActiveLanguage:currentContent.contentName];
            if ([currentContent isKindOfClass:[GUNExplanationContentResponse class] ]) {
                GUNExplanationContentResponse *tmpExplanationResponse = (GUNExplanationContentResponse *) currentContent;

                externalLinkViewController.contentUrl = tmpExplanationResponse.explanationContentUrl;
                externalLinkViewController.title = translatedExplanationName;
                self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
                [self.navigationController pushViewController:externalLinkViewController animated:YES];
            }

        }

        
    } else if (currentContent.contentType.integerValue == contentGroupContentType) {

        NSLog(@"Group Tapped");
        if ([currentContent isKindOfClass:[GUNContentGroupResponse class] ]) {
            GUNContentGroupResponse *tmpGroupResponse = (GUNContentGroupResponse *)currentContent;
            TutorialGroupItemCell *cell = (TutorialGroupItemCell*) [_tv_tutorialList cellForRowAtIndexPath:indexPath];
            if (tmpGroupResponse.extended == NO) {
                NSLog(@"extend the group");
      //           cell.img_v_iconArrow.image = [UIImage imageNamed:@"G_Pfeil_rauf.png"];
                [self extendGroupAtIndex:indexPath.row];
                [cell expandGroup];
                
            } else {
                NSLog(@"shrink it.");
          //      cell.img_v_iconArrow.image = [UIImage imageNamed:@"G_Pfeil_runter.png"];
                [self shrinkGroupAtIndex:indexPath.row];
                [cell shrinkGroup];
            }
        }

        
    }

}


- (void) extendGroupAtIndex:(NSInteger) index {

    GUNContentGroupResponse *groupToExtend = [_contentDataSource objectAtIndex:index];
    if ( groupToExtend.extended == NO ) {
        groupToExtend.extended = YES;
        NSInteger nextIndex = index + 1;
        NSMutableArray* newIndexPath = [[NSMutableArray alloc] init];
        // add the groups children to the data source
        for ( id item in groupToExtend.groupItems ) {
            if ([item isKindOfClass:[GUNProductContentResponse class]]) {
                GUNProductContentResponse *groupItem = (GUNProductContentResponse *) item;

                [_contentDataSource insertObject:groupItem atIndex:nextIndex];
                
                [newIndexPath addObject:[NSIndexPath indexPathForRow:nextIndex inSection:0]];
                nextIndex++;
                
            }
        }
        [_tv_tutorialList beginUpdates];
        [_tv_tutorialList insertRowsAtIndexPaths:newIndexPath withRowAnimation:UITableViewRowAnimationFade];
        [_tv_tutorialList endUpdates];
    }


}

- (void) shrinkGroupAtIndex:(NSInteger) index {
    GUNContentGroupResponse *groupToExtend = [_contentDataSource objectAtIndex:index];
    if ( groupToExtend.extended == YES ) {
        groupToExtend.extended = NO;
        
        NSMutableArray* newIndexPath = [[NSMutableArray alloc] init];
        NSMutableArray* objectsToDelete = [[NSMutableArray alloc] init];
        
        for (GUNProductContentResponse* groupItem in _contentDataSource) {
            
            if (groupItem.relatedGroup == groupToExtend) {
                NSInteger indexToDelete = [_contentDataSource indexOfObject:groupItem];
                [objectsToDelete addObject:groupItem];
                [newIndexPath addObject:[NSIndexPath indexPathForRow:indexToDelete inSection:0]];
            }
            
        }
        [_contentDataSource removeObjectsInArray:objectsToDelete];
        
        
        [_tv_tutorialList beginUpdates];
        [_tv_tutorialList deleteRowsAtIndexPaths:newIndexPath withRowAnimation:UITableViewRowAnimationFade];
        [_tv_tutorialList endUpdates];
    }
}

-(void) loadTutorialDetail:(GUNContentTutorialResponse *)contentTutorial {

    GUNTutorialDetailViewController *tutorialDetailViewController = (GUNTutorialDetailViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"GUNTutorialDetailViewController"];

    if (tutorialDetailViewController != nil) {

        NSString* translatedTutorialName = [_tutorialManager translationForKeyInActiveLanguage:contentTutorial.contentName];

        tutorialDetailViewController.tutorialID = [contentTutorial.tutorialId integerValue];
        tutorialDetailViewController.title = translatedTutorialName;
        tutorialDetailViewController.productID = _product.productID.integerValue;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
            [self.navigationController pushViewController:tutorialDetailViewController animated:YES];
    }




}

#pragma mark Auto Rotation Methods

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

@end
