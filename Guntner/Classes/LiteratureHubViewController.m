//
//  LiteratureHubViewController.m
//  Guntner
//
//  Created by Shiraz Omar on 8/6/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "LiteratureHubViewController.h"

#import <Cordova/CDVViewController.h>
#import "Firebase.h"
#import "Configuration.h"
#import "LoadingViewController.h"


@interface LiteratureHubViewController () {
    
    LoadingViewController *loadingView;
    CDVViewController* phoneGapViewController;
    
}

@property (nonatomic, strong) LoadingViewController *loadingView;
@property (nonatomic, strong) CDVViewController* phoneGapViewController;

@end


@implementation LiteratureHubViewController


@synthesize loadingView = _loadingView;
@synthesize phoneGapViewController = _phoneGapViewController;


#pragma mark Setup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        // Uncomment to override the CDVCommandDelegateImpl used
        // _commandDelegate = [[MainCommandDelegate alloc] initWithViewController:self];
        // Uncomment to override the CDVCommandQueue used
        // _commandQueue = [[MainCommandQueue alloc] initWithViewController:self];
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    // Show Navigation Bar
    self.navigationController.navigationBarHidden = NO;
    
    
    // Setup Notification Listeners
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showLoadingIndicator) name:kShowLoadingViewIdentifier object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeLoadingIndicator) name:kDismissLoadingViewIdentifier object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showSharePDFButton) name:kShowSharePDFButtonIdentifier object:nil];
    
    
    // Delete Stored PDF's If Total PDF Storage Is Greater Than 50MB
    [self deletePDFsIfTotalSavedFilesIsMoreThan50MB];
    
    
    // Setup PhoneGap/Cordova
    [self setupPhoneGap];
    
    
    // Show Loading Indicator
    [self showLoadingIndicator];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    // Send Google Analytics Information
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:kLiteratureHubViewControllerIdentifier];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    [FIRAnalytics logEventWithName:kGAIScreenName
                        parameters:@{
                                     kFIRParameterItemName:kLiteratureHubViewControllerIdentifier,
                                     kFIRParameterContentType:@"screen"
                                     }];
}


#pragma mark PhoneGap/Cordova Methods

- (void)setupPhoneGap {
    
    @autoreleasepool {
        
        // Setup PhoneGap/Cordova
        NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        
        [cookieStorage setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
        
        int cacheSizeDisk = kCacheSizeDisk; // 32MB
        int cacheSizeMemory = kCacheSizeMemory; // 8MB
        NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:kCacheDiskPath];
        [NSURLCache setSharedURLCache:sharedCache];
        
        
        self.phoneGapViewController = [CDVViewController new];
        self.phoneGapViewController.wwwFolderName = kHTML5Folder;
        self.phoneGapViewController.startPage = kHTML5LiteratureHubPage;
        
        self.phoneGapViewController.view.frame = self.view.bounds;
        [self.view addSubview:self.phoneGapViewController.view];
    }
    
}


- (void)goBackToPreviousPageOnPhoneGapView {
    
    // Remove Share PDF Button From Navigation Bar
    self.navigationController.topViewController.navigationItem.rightBarButtonItem = nil;
    
    // Call Javascript Function To Go To Previous Page
    [self.phoneGapViewController.webView stringByEvaluatingJavaScriptFromString:kGoBackJavaScriptFunctionForPDF];
    
    // Replace Back Button With Menu Button
    [[NSNotificationCenter defaultCenter] postNotificationName:kSwapMenuButtonWithBackButtonIdentifier object:nil];
    
    // Disable Bounce After Coming Back From PDF
    ((UIScrollView *)[self.phoneGapViewController.webView scrollView]).bounces = NO;
    
}


- (void)deletePDFsIfTotalSavedFilesIsMoreThan50MB {
    
    NSArray *pdfs = [self findFiles:@"pdf"];
    
    NSMutableArray *sortedFiles = [[NSMutableArray alloc] init];
    int totalStoredPDFFileSize = 0;
    
    for (NSString *pdfFile in pdfs) {
        
        // Check If File Exists
        if([[NSFileManager defaultManager] fileExistsAtPath:pdfFile]){
            
            // Get File Attributes
            NSMutableDictionary *pdfAttributes = [[[NSFileManager defaultManager] attributesOfItemAtPath:pdfFile error:nil] mutableCopy];
            [pdfAttributes setObject:pdfFile forKey:@"FilePath"];
            
            // Add PDF Size To TotalSize Counter
            totalStoredPDFFileSize += [[pdfAttributes objectForKey:NSFileSize] integerValue];
            
            // Populate Array
            [sortedFiles addObject:pdfAttributes];
            
            // Release Objects
            pdfAttributes = nil;
        }
    }
    
    // Sort Array According To NSFileModificationDate
    NSSortDescriptor *sortByDate = [NSSortDescriptor sortDescriptorWithKey:@"NSFileModificationDate" ascending:NO];
    [sortedFiles sortUsingDescriptors:@[sortByDate]];
    sortByDate = nil;
    
    while (totalStoredPDFFileSize > (50*1024*1024)) {
        // Delete Oldest File If Total Stored Capacity Is More Than 50MB
        totalStoredPDFFileSize -= [[[sortedFiles lastObject] valueForKey:NSFileSize] integerValue];
        [[NSFileManager defaultManager] removeItemAtPath:[[sortedFiles lastObject] valueForKey:@"FilePath"] error:nil];
        [sortedFiles removeLastObject];
    }
    
    
    // Release Objects
    sortedFiles = nil;
    pdfs = nil;
    
}

- (NSArray *)findFiles:(NSString *)extension {
    
    NSMutableArray *matches = [[NSMutableArray alloc] init];
    NSFileManager *manager = [NSFileManager defaultManager];
    
    NSArray *contents = [manager contentsOfDirectoryAtPath:NSTemporaryDirectory() error:nil];
    
    for (NSString *item in contents) {
        if ([[item pathExtension] isEqualToString:extension]) {
            [matches addObject:[NSTemporaryDirectory() stringByAppendingPathComponent:item]];
        }
    }
    
    contents = nil;
    manager = nil;
    
    return matches;
}


- (void)showLoadingIndicator {
    
    // Disable User Interaction On PhoneGap View
    [self.phoneGapViewController.view setUserInteractionEnabled:NO];
    
    // Show Loading Indicator
    if (self.loadingView == nil) {
        self.loadingView = [[LoadingViewController alloc] initWithNibName:kLoadingViewControllerXibName bundle:nil];
    }
    [self.loadingView.loadingIndicator startAnimating];
    [self.view addSubview:self.loadingView.view];
    self.loadingView.view.center = CGPointMake(self.view.center.x, (self.view.center.y - kLoadingViewVerticalOffset));
    
}

- (void)removeLoadingIndicator {
    
    // Enable User Interaction On PhoneGap View
    [self.phoneGapViewController.view setUserInteractionEnabled:YES];
    
    // Remove Loading Indicator
    if (self.loadingView != nil) {
        [self.loadingView.loadingIndicator stopAnimating];
        [self.loadingView.view removeFromSuperview];
    }
}


- (void)showSharePDFButton {
    
    // Add Share PDF Button To Navigation Bar
    if (!self.navigationController.topViewController.navigationItem.rightBarButtonItem) {
        // Create and Show Share Button
        UIImage *shareButtonImage = [UIImage imageNamed:kShowSharePDFButtonImage];
        UIButton *shareButton = [[UIButton alloc] initWithFrame:kMenuButtonFrame];
        [shareButton setImage:shareButtonImage forState:UIControlStateNormal];
        [shareButton setImageEdgeInsets:UIEdgeInsetsMake(14, 9.5, 14, 9.5)];
        [shareButton addTarget:self action:@selector(triggerSharePDFMethod) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *shareBarButton = [[UIBarButtonItem alloc] initWithCustomView:shareButton];
        self.navigationController.topViewController.navigationItem.rightBarButtonItem = shareBarButton;
        shareButtonImage = nil;
        shareBarButton = nil;
        shareButton = nil;
    }
    
}

- (void)triggerSharePDFMethod {
    
    // Go To Previous Page
    [self goBackToPreviousPageOnPhoneGapView];
    
    // Call Javascript Function Trigger Share Method
    [self.phoneGapViewController.webView performSelector:@selector(stringByEvaluatingJavaScriptFromString:) withObject:kSharePDFJavaScriptFunction afterDelay:0.5];
    
    
}


#pragma mark UIWebDelegate implementation

- (void)webViewDidFinishLoad:(UIWebView*)theWebView
{
    // White base color for background matches the native apps
    theWebView.backgroundColor = [UIColor whiteColor];
    
    
    return [self.phoneGapViewController webViewDidFinishLoad:theWebView];
}

/* Comment out the block below to over-ride */

/*
 
 - (void) webViewDidStartLoad:(UIWebView*)theWebView
 {
 return [super webViewDidStartLoad:theWebView];
 }
 
 - (void) webView:(UIWebView*)theWebView didFailLoadWithError:(NSError*)error
 {
 return [super webView:theWebView didFailLoadWithError:error];
 }
 
 - (BOOL) webView:(UIWebView*)theWebView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType
 {
 return [super webView:theWebView shouldStartLoadWithRequest:request navigationType:navigationType];
 }
 */


#pragma mark Auto Rotation Methods

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


#pragma mark Cleanup

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}


- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kShowLoadingViewIdentifier object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kDismissLoadingViewIdentifier object:nil];
    
    self.loadingView = nil;
    self.phoneGapViewController = nil;
    
}


@end
