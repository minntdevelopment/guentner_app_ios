//
// Created by Julian Dawo on 16.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Configuration.h"

@interface GUNTutorialStepActionResponse : NSObject

@property (nonatomic, strong) NSString *displayStartLine1Text;
@property (nonatomic, strong) NSString *displayStartLine2Text;
@property (nonatomic, strong) NSString *displayEndLine1Text;
@property (nonatomic, strong) NSString *displayEndLine2Text;
@property (nonatomic, strong) NSArray *displayStartLine1Icons;
@property (nonatomic, strong) NSArray *displayStartLine2Icons;
@property (nonatomic, strong) NSArray *displayEndLine1Icons;
@property (nonatomic, strong) NSArray *displayEndLine2Icons;
@property (nonatomic) ControllerButtonType displayButtonType;
@property (nonatomic) NSUInteger activeStepIndex;
@property (nonatomic, strong) NSString *actionCountText;

@end