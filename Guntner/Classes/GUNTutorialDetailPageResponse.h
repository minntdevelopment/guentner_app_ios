//
// Created by Julian Dawo on 16.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GUNTutorialDetailPageResponse : NSObject

@property (nonatomic, strong) NSNumber *pageType;
@property (nonatomic, strong) NSArray *steps;
@property (nonatomic, strong) NSString *hintPageExplanation;

@end