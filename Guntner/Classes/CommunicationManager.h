//
//  CommunicationManager.h
//  guentner
//
//  Created by Balazs Auth on 30.11.18.
//  Copyright © 2018 Brandixi3. All rights reserved.
//

#ifndef CommunicationManager_h
#define CommunicationManager_h

@interface CommunicationManager : NSObject

+ (CommunicationManager* ) instance;

-(NSString*)urlEscapeString:(NSString *)unencodedString;
-(void)getNews:(NSString*)language withContry:(NSString*)country andCompletionHandler:(void (^)(NSArray* result))completionHandler;
-(void)getLinkedinNewsWithCompletionHandler:(void (^)(NSArray* result))completionHandler;

@end
#endif /* CommunicationManager_h */
