//
//  MenuCell.h
//  guentner
//
//  Created by Balazs Auth on 28/09/15.
//  Copyright © 2015 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *menuicon;
@property (weak, nonatomic) IBOutlet UIButton *menubutton;
@property (weak, nonatomic) IBOutlet UIImageView *menuNewLbl;

@end
