//
//  ImpressumViewController.m
//  Guntner
//
//  Created by Shiraz Omar on 8/6/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "ImpressumViewController.h"
#import "Firebase.h"
#import "Configuration.h"


@interface ImpressumViewController () {
    
    IBOutlet UITextView *contentView;
    
}

@property (nonatomic, weak) IBOutlet UITextView *contentView;

@end


@implementation ImpressumViewController


@synthesize contentView = _contentView;


#pragma mark Setup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    // Show Navigation Bar
    self.navigationController.navigationBarHidden = NO;
    
    // Round ContentView's Corners
    [self roundViewsCorners:self.contentView];
    
    
    // Create And Show Back Button
    [self createAndShowBackButton];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.contentView.editable = NO;
    self.contentView.dataDetectorTypes = UIDataDetectorTypeAll;
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    // Send Google Analytics Information
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:kImpressumViewControllerIdentifier];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    [FIRAnalytics logEventWithName:kGAIScreenName
                        parameters:@{
                                     kFIRParameterItemName:kImpressumViewControllerIdentifier,
                                     kFIRParameterContentType:@"screen"
                                     }];
}


- (void)roundViewsCorners:(UIView *)view {
    
    // Set View's Corner Radius To 5px To Round The Corners
    view.layer.cornerRadius = 5.0;
    view.layer.masksToBounds = YES;
    
}


- (void)createAndShowBackButton {
    
    // Create and Show Back Button
    UIImage *buttonImage = [UIImage imageNamed:kBackButtonImage];
    CGRect buttonFrame = kBackButtonFrame;
    UIButton *backButton = [[UIButton alloc] initWithFrame:buttonFrame];
    [backButton setTitle:kBackButtonTitle forState:UIControlStateNormal];
    [backButton setTitleColor:kBackButtonTitleColor forState:UIControlStateNormal];
    [[backButton titleLabel] setFont:kBackButtonTitleFont];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(goToPreviousViewController) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationController.topViewController.navigationItem.leftBarButtonItem = backBarButton;
    backBarButton = nil;
    buttonImage = nil;
    
}


- (void)goToPreviousViewController {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark Auto Rotation Methods

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


#pragma mark Cleanup

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc {
    
    //
    
}


@end
