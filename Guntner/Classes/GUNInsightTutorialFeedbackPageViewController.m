//
// Created by Julian Dawo on 17.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "GUNInsightTutorialFeedbackPageViewController.h"
#import "GUNTutorialManager.h"
#import "GUNRootManager.h"
#import <QuartzCore/QuartzCore.h>

#import "Configuration.h"

@implementation GUNInsightTutorialFeedbackPageViewController {

    NSNumber *_currentRating;
    GUNTutorialManager *_tutorialManager;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _btn_sendRating.userInteractionEnabled = !_feedbackSent;
    _btn_sendRating.enabled = !_feedbackSent;
    _tutorialManager = [[GUNRootManager instance] tutorialManager];

    NSString *ratingIntroText = [_tutorialManager translationForKeyInActiveLanguage:@"HOW USEFUL WAS THIS TUTORIAL FOR YOU?"];
    _lbl_feddbackQuestion.text = ratingIntroText.uppercaseString;

    NSString *ratingDoneText = [_tutorialManager translationForKeyInActiveLanguage:@"Thank you. Your rating has been saved."];
    _lbl_feedbackSuccess.text = ratingDoneText;

    NSString *ratingBestText = [_tutorialManager translationForKeyInActiveLanguage:@"very useful"];
    _lbl_ratingBest.text = ratingBestText;

    NSString *ratingWorstText = [_tutorialManager translationForKeyInActiveLanguage:@"not useful"];
    _lbl_ratingWorst.text = ratingWorstText;

    NSString *nextTutorialHeaderText = [_tutorialManager translationForKeyInActiveLanguage:@"Next Tutorial"];
    _lbl_titleNextTutorial.text = nextTutorialHeaderText;

    [_btn_sendRating setBackgroundImage:[self imageWithColor:color_dark_blue] forState:UIControlStateNormal];
    [_btn_sendRating setBackgroundImage:[self imageWithColor:color_dark_blue_020] forState:UIControlStateDisabled];
    [_btn_sendRating setBackgroundImage:[self imageWithColor:color_dark_blue_020] forState:UIControlStateHighlighted];

    _lbl_feddbackMessage.text = [[[GUNRootManager instance] tutorialManager] translationForKeyInActiveLanguage:_feedbackText];

    [self adapteDesignForFeedbackSendState:_feedbackSent];
    
    _btn_sendRating.userInteractionEnabled = !_feedbackSent;
    _btn_jumpToNextTutorial.titleLabel.text = _nextTutorial;
    _btn_sendRating.enabled = !_feedbackSent;
    
    _lbl_feddbackMessage.text = [[[GUNRootManager instance] tutorialManager] translationForKeyInActiveLanguage:_feedbackText];
    [self adapteDesignForFeedbackSendState:_feedbackSent];
    
    
    _lbl_titleJumpToNextTutorial.text = [[[[GUNRootManager instance] tutorialManager] translationForKeyInActiveLanguage:_nextTutorial] uppercaseString];
    _lbl_titleJumpToNextTutorial.minimumScaleFactor = 0.8;
    _lbl_titleJumpToNextTutorial.adjustsFontSizeToFitWidth = YES;
    
    //style
    _btn_sendRating.layer.cornerRadius = 10.0f;
    _btn_sendRating.layer.masksToBounds = YES;
    
    _v_ratingContainer.layer.shadowColor = color_black_015.CGColor;
    _v_ratingContainer.layer.shadowOpacity = 1.0;
    _v_ratingContainer.layer.shadowRadius = 3.0;
    _v_ratingContainer.layer.shadowOffset = CGSizeMake(0, 3);
    _v_ratingContainer.layer.masksToBounds = YES;
    _v_ratingContainer.clipsToBounds = NO;
    
    _btn_jumpToNextTutorial.layer.shadowColor = color_black_015.CGColor;
    _btn_jumpToNextTutorial.layer.shadowOpacity = 1.0;
    _btn_jumpToNextTutorial.layer.shadowRadius = 3.0;
    _btn_jumpToNextTutorial.layer.shadowOffset = CGSizeMake(0, 3);
    _btn_jumpToNextTutorial.layer.masksToBounds = YES;
    _btn_jumpToNextTutorial.clipsToBounds = NO;
    
    if (_currentRating == nil || [_currentRating integerValue] < 1) {
        _btn_sendRating.enabled = NO;
        _btn_sendRating.userInteractionEnabled = NO;
    }
    
    // check if there is the next tutorial exists
    if (_nextTutorial == nil || !(_nextTutorial.length > 0)) {
        _lbl_titleNextTutorial.hidden = YES;
        _btn_jumpToNextTutorial.hidden = YES;
        _lbl_titleJumpToNextTutorial.hidden = YES;
        _iv_nextBtnIcon.hidden = YES;
        
    } else {
        _lbl_titleNextTutorial.hidden = NO;
        _btn_jumpToNextTutorial.hidden = NO;
        _lbl_titleJumpToNextTutorial.hidden = NO;
        _iv_nextBtnIcon.hidden = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    /*
    _btn_sendRating.userInteractionEnabled = !_feedbackSent;
    _btn_jumpToNextTutorial.titleLabel.text = _nextTutorial;
    _btn_sendRating.enabled = !_feedbackSent;

    _lbl_feddbackMessage.text = [[[GUNRootManager instance] tutorialManager] translationForKeyInActiveLanguage:_feedbackText];
    [self adapteDesignForFeedbackSendState:_feedbackSent];
    
    
    _lbl_titleJumpToNextTutorial.text = [[[[GUNRootManager instance] tutorialManager] translationForKeyInActiveLanguage:_nextTutorial] uppercaseString];
    
    //style
    _btn_sendRating.layer.cornerRadius = 10.0f;
    _btn_sendRating.layer.masksToBounds = YES;
    
    _v_ratingContainer.layer.shadowColor = color_black_015.CGColor;
    _v_ratingContainer.layer.shadowOpacity = 1.0;
    _v_ratingContainer.layer.shadowRadius = 3.0;
    _v_ratingContainer.layer.shadowOffset = CGSizeMake(0, 3);
    _v_ratingContainer.layer.masksToBounds = YES;
    _v_ratingContainer.clipsToBounds = NO;
    
    _btn_jumpToNextTutorial.layer.shadowColor = color_black_015.CGColor;
    _btn_jumpToNextTutorial.layer.shadowOpacity = 1.0;
    _btn_jumpToNextTutorial.layer.shadowRadius = 3.0;
    _btn_jumpToNextTutorial.layer.shadowOffset = CGSizeMake(0, 3);
    _btn_jumpToNextTutorial.layer.masksToBounds = YES;
    _btn_jumpToNextTutorial.clipsToBounds = NO;
    
    if (_currentRating == nil || [_currentRating integerValue] < 1) {
        _btn_sendRating.enabled = NO;
        _btn_sendRating.userInteractionEnabled = NO;
    }
    */
    
}

- (IBAction)rating1ButtonPressed:(id)sender {

    [self userTappedRatingButton:1];

}

- (IBAction)rating2ButtonPressed:(id)sender {

    [self userTappedRatingButton:2];
}

- (IBAction)rating3ButtonPressed:(id)sender {

    [self userTappedRatingButton:3];
}

- (IBAction)rating4ButtonPressed:(id)sender {

    [self userTappedRatingButton:4];

}

- (IBAction)rating5ButtonPressed:(id)sender {

    [self userTappedRatingButton:5];

}

- (IBAction)sendRatingButtonPressed:(id)sender {
    
    _btn_sendRating.userInteractionEnabled = NO;
    _btn_sendRating.enabled = NO;
    _feedbackSent = YES;
    [self.delegate userSendFeedback:_currentRating];

    [self adapteDesignForFeedbackSendState:_feedbackSent];
    _btn_sendRating.enabled = NO;
}


- (IBAction)jumpToNextButtonPressed:(id)sender {

    [self.delegate userWantsToStartTheNextTutorialInRow];
}


-(void) userTappedRatingButton: (int)buttonNumber {


    switch (buttonNumber) {

        case 1:
            _btn_rating1.selected = YES;
            _btn_rating2.selected = NO;
            _btn_rating3.selected = NO;
            _btn_rating4.selected = NO;
            _btn_rating5.selected = NO;
            _currentRating = @1;
            break;

        case 2:
            _btn_rating1.selected = YES;
            _btn_rating2.selected = YES;
            _btn_rating3.selected = NO;
            _btn_rating4.selected = NO;
            _btn_rating5.selected = NO;
            _currentRating = @2;
            break;

        case 3:
            _btn_rating1.selected = YES;
            _btn_rating2.selected = YES;
            _btn_rating3.selected = YES;
            _btn_rating4.selected = NO;
            _btn_rating5.selected = NO;
            _currentRating = @3;
            break;

        case 4:
            _btn_rating1.selected = YES;
            _btn_rating2.selected = YES;
            _btn_rating3.selected = YES;
            _btn_rating4.selected = YES;
            _btn_rating5.selected = NO;
            _currentRating = @4;
            break;

        case 5:
            _btn_rating1.selected = YES;
            _btn_rating2.selected = YES;
            _btn_rating3.selected = YES;
            _btn_rating4.selected = YES;
            _btn_rating5.selected = YES;
            _currentRating = @5;
            break;
    }
    
    if (!_feedbackSent) {
        _btn_sendRating.userInteractionEnabled = YES;
        _btn_sendRating.enabled = YES;
    }


    [self adapteDesignForFeedbackSendState:_feedbackSent];
}



-(void) adapteDesignForFeedbackSendState: (BOOL) isSent {


    if (isSent) {
        // hide rating Buttons and labels
        _btn_rating1.hidden = YES;
        _btn_rating2.hidden = YES;
        _btn_rating3.hidden = YES;
        _btn_rating4.hidden = YES;
        _btn_rating5.hidden = YES;

        _lbl_ratingBest.hidden = YES;
        _lbl_ratingWorst.hidden = YES;
        // show successful sent

        _lbl_feedbackSuccess.hidden = NO;
    } else {
        {
            // hide rating Buttons and labels
            _btn_rating1.hidden = NO;
            _btn_rating2.hidden = NO;
            _btn_rating3.hidden = NO;
            _btn_rating4.hidden = NO;
            _btn_rating5.hidden = NO;

            _lbl_ratingBest.hidden = NO;
            _lbl_ratingWorst.hidden = NO;
            // show successful sent

            _lbl_feedbackSuccess.hidden = YES;
        }
    }
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end