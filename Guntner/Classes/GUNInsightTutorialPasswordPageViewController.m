//
// Created by Julian Dawo on 17.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "GUNInsightTutorialPasswordPageViewController.h"
#import "GUNTutorialManager.h"
#import "GUNRootManager.h"

@implementation GUNInsightTutorialPasswordPageViewController {
    GUNTutorialManager *_tutorialManager;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _tutorialManager = [[GUNRootManager instance] tutorialManager];
    
    NSString* translatedPasswordHeaderText = [_tutorialManager translationForKeyInActiveLanguage:@"Password:"];
    _lbl_passwordHeader.text = translatedPasswordHeaderText.uppercaseString;
    NSString* translatedPasswordHintText = [_tutorialManager translationForKeyInActiveLanguage:@"The following steps explain how to enter this password."];
    _lbl_passwordHint.text = translatedPasswordHintText;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];


}
@end