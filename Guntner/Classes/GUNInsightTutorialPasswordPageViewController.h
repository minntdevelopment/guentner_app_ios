//
// Created by Julian Dawo on 17.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>
#import "PageItemController.h"

@interface GUNInsightTutorialPasswordPageViewController : PageItemController

@property (nonatomic,weak) IBOutlet UILabel *lbl_passwordHeader;
@property (nonatomic,weak) IBOutlet UILabel *lbl_passwordHint;

@end