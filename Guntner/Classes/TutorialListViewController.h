//
//  TutorialListViewController.h
//  guentner
//
//  Created by Julian Dawo on 05.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "GAITrackedViewController.h"

@class GUNProductResponse;

@interface TutorialListViewController : GAITrackedViewController <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tv_tutorialList;
@property (nonatomic, weak) IBOutlet UIView *v_noContent;
@property (nonatomic, strong) GUNProductResponse *product;
@property (weak, nonatomic) IBOutlet UILabel *lbl_errorNoContent;

@end
