//
//  TutorialsViewController.h
//  guentner
//
//  Created by Julian Dawo on 03.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "GAITrackedViewController.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

@interface TutorialsViewController : GAITrackedViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btn_selectRegion;
@property (weak, nonatomic) IBOutlet UIButton *btn_selectLanguages;
@property (weak, nonatomic) IBOutlet UITextField *txt_search;
@property (weak, nonatomic) IBOutlet UITableView *tv_productList;
@property (weak, nonatomic) IBOutlet UIView *v_error;
@property (weak, nonatomic) IBOutlet UIView *v_selectRegionFirst;
@property (weak, nonatomic) IBOutlet UIView *v_noResult;
@property (weak, nonatomic) IBOutlet UILabel *lbl_error_connection;
@property (weak, nonatomic) IBOutlet UILabel *lbl_error_region;
@property (weak, nonatomic) IBOutlet UILabel *lbl_error_search;
@property (weak, nonatomic) IBOutlet UILabel *lbl_languageButtonTitle;

- (IBAction)tapBtnSelectRegion:(id)sender;
- (IBAction)tapBtnSelectLanguage:(id)sender;
- (IBAction)editingCangedTxtfSerarch:(id)sender;

@end
