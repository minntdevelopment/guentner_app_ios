//
//  GUNLoadingViewController.h
//  guentner
//
//  Created by Julian Dawo on 06.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "GAITrackedViewController.h"

static NSString* const defaultLabelText = @"Please wait...";

@interface GUNLoadingViewController : GAITrackedViewController

@property (weak, nonatomic) IBOutlet UIImageView *iv_logo;
@property (weak, nonatomic) IBOutlet UILabel *lbl_text;
@property (weak, nonatomic) IBOutlet UILabel *lbl_staticLoadingText;

+ (NSString*)pushLoadingViewController;
+ (NSString*)pushLoadingViewControllerWithText:(NSString*)loadingText;
+ (void)popLoadingViewController:(NSString*)handle;

@end
