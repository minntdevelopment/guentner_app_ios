//
//  ColdSliderViewController.m
//  Guntner
//
//  Created by Shiraz Omar on 8/6/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "ColdSliderViewController.h"

#import "Configuration.h"
#import "CustomKeyboard.h"
#import "ColdSliderScale.h"
#import "SuffixTextField.h"
#import "ColdSliderComponent.h"
#import "ColdSliderCalculator.h"
#import "InfoPageViewController.h"
#import "ColdSliderRefrigerantSelector.h"
#import "Firebase.h"

#import "AppDelegate.h"
#import "ViewDeckController.h"


@interface ColdSliderViewController () <UITextFieldDelegate, UIScrollViewDelegate, UIAlertViewDelegate, ColdSliderComponentDelegate, ColdSliderRefrigerantSelectorDelegate, CustomKeyboardDelegate> {
    
    NSArray *unitList;
    
    double minTemp;
    double maxTemp;
    double minPressure;
    double maxPressure;
    
    UIView *infoPageView;
    
    CustomKeyboard *numPad;
    
    CGPoint originalOffset;
    
    NSNumberFormatter *numberFormatter;
    
    UIAlertView *refrigerantNotSelectedAlert;
    
    IBOutlet UIButton *odp;
    IBOutlet UIButton *gwp;
    IBOutlet UIButton *oil;
    IBOutlet UIButton *tMin;
    IBOutlet UIButton *tMax;
    IBOutlet UIButton *tCrit;
    IBOutlet UILabel *odpLabel;
    IBOutlet UILabel *gwpLabel;
    IBOutlet UILabel *oilLabel;
    IBOutlet UILabel *tMinLabel;
    IBOutlet UILabel *tMaxLabel;
    IBOutlet UILabel *tCritLabel;
    IBOutlet UIButton *unitButton1;
    IBOutlet UIButton *unitButton2;
    IBOutlet UIScrollView *sliderView;
    IBOutlet UIImageView *sliderImage;
    IBOutlet UIView *sliderVerticalLine;
    IBOutlet UIButton *refrigerantButton;
    IBOutlet UIButton *csComponentButton;
    IBOutlet UIView *sliderHorizontalLine;
    IBOutlet UIImageView *sliderArrowImage;
    IBOutlet SuffixTextField *pressureTextField;
    IBOutlet SuffixTextField *temperatureTextField;
    IBOutlet UIView *coldSliderSliderBackgroundView;
    
    NSUserDefaults *prefs;
    ColdSliderCalculator *csCalculator;
    ColdSliderComponent *coldSliderComponent;
    ColdSliderRefrigerantSelector *coldSliderRSelector;
        
}


@property (nonatomic, strong) NSArray *unitList;

@property (nonatomic, strong) CustomKeyboard *numPad;

@property (nonatomic, strong) NSNumberFormatter *numberFormatter;

@property (nonatomic, strong) UIAlertView *refrigerantNotSelectedAlert;

@property (nonatomic, weak) IBOutlet UIButton *odp;
@property (nonatomic, weak) IBOutlet UIButton *gwp;
@property (nonatomic, weak) IBOutlet UIButton *oil;
@property (nonatomic, weak) IBOutlet UIButton *tMin;
@property (nonatomic, weak) IBOutlet UIButton *tMax;
@property (nonatomic, weak) IBOutlet UIButton *tCrit;
@property (nonatomic, weak) IBOutlet UILabel *odpLabel;
@property (nonatomic, weak) IBOutlet UILabel *gwpLabel;
@property (nonatomic, weak) IBOutlet UILabel *oilLabel;
@property (nonatomic, weak) IBOutlet UILabel *tMinLabel;
@property (nonatomic, weak) IBOutlet UILabel *tMaxLabel;
@property (nonatomic, weak) IBOutlet UILabel *tCritLabel;
@property (nonatomic, weak) IBOutlet UIButton *unitButton1;
@property (nonatomic, weak) IBOutlet UIButton *unitButton2;
@property (nonatomic, weak) IBOutlet UIImageView *sliderImage;
@property (nonatomic, weak) IBOutlet UIScrollView *sliderView;
@property (nonatomic, weak) IBOutlet UIView *sliderVerticalLine;
@property (nonatomic, weak) IBOutlet UIButton *refrigerantButton;
@property (nonatomic, weak) IBOutlet UIButton *csComponentButton;
@property (nonatomic, weak) IBOutlet UIView *sliderHorizontalLine;
@property (nonatomic, weak) IBOutlet UIImageView *sliderArrowImage;
@property (nonatomic, weak) IBOutlet SuffixTextField *pressureTextField;
@property (nonatomic, weak) IBOutlet SuffixTextField *temperatureTextField;
@property (nonatomic, weak) IBOutlet UIView *coldSliderSliderBackgroundView;

@property (nonatomic, strong) ColdSliderCalculator *csCalculator;
@property (nonatomic, strong) ColdSliderComponent *coldSliderComponent;
@property (nonatomic, strong) ColdSliderRefrigerantSelector *coldSliderRSelector;


- (double)getPressureValue;
- (double)getTemperatureValue;
- (NSString *)getSelectedRefrigerant;
- (IBAction)showHideColdSliderComponent;
- (IBAction)unitTypeButtonPressed:(id)sender;
- (IBAction)refrigerantButtonPressed:(id)sender;
- (IBAction)hideColdSliderRefrigerantSelectorWithRefrigerant:(NSString *)refrigerant;
-(void)enablePressureAndTemperatureTextfields;


@end


@implementation ColdSliderViewController


@synthesize numPad = _numPad;

@synthesize unitList = _unitList;

@synthesize numberFormatter = _numberFormatter;

@synthesize refrigerantNotSelectedAlert = _refrigerantNotSelectedAlert;

@synthesize odp = _odp;
@synthesize gwp = _gwp;
@synthesize oil = _oil;
@synthesize tMin = _tMin;
@synthesize tMax = _tMax;
@synthesize tCrit = _tCrit;
@synthesize odpLabel = _odpLabel;
@synthesize gwpLabel = _gwpLabel;
@synthesize oilLabel = _oilLabel;
@synthesize tMinLabel = _tMinLabel;
@synthesize tMaxLabel = _tMaxLabel;
@synthesize tCritLabel = _tCritLabel;
@synthesize sliderView = _sliderView;
@synthesize unitButton1 = _unitButton1;
@synthesize unitButton2 = _unitButton2;
@synthesize sliderImage = _sliderImage;
@synthesize sliderArrowImage = _sliderArrowImage;
@synthesize refrigerantButton = _refrigerantButton;
@synthesize csComponentButton = _csComponentButton;
@synthesize pressureTextField = _pressureTextField;
@synthesize sliderVerticalLine = _sliderVerticalLine;
@synthesize sliderHorizontalLine = _sliderHorizontalLine;
@synthesize temperatureTextField = _temperatureTextField;
@synthesize coldSliderSliderBackgroundView = _coldSliderSliderBackgroundView;

@synthesize csCalculator = _csCalculator;
@synthesize coldSliderComponent = _coldSliderComponent;
@synthesize coldSliderRSelector = _coldSliderRSelector;



#pragma mark Setup


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Show Navigation Bar
    self.navigationController.navigationBarHidden = NO;
    
    
    // Load Cold Slider Data
    [self loadColdSliderData];
    
    
    // Setup Number Formatter
    [self setupNumberFormatter];
    
    
    // Setup TextFeilds
    [self setupTextFields];
    
    
    // Setup Cold Slider Refrigerant Selector and Button
    [self setupColdSliderRefrigerantSelectorAndButton];
    
    
    // Setup Refrigerant Data Labels
    //[self setupRefrigerantDataLabels];
    
    
    // Setup Cold Slider Component and Button
    [self setupColdSliderComponentAndButton];
    
    
    // Setup Unit Type Buttons
    [self setupUnitTypeButtons];
    
    
    // Setup SliderView
    //[self setupSliderView];
    
    
    // Setup Info Page Button
    [self setupInfoPageButton];
    
//    self.navigationController.navigationBar.translucent = NO;
}


- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    // Setup UI According to Previoues State
    [self updateUIFromPreviousState];
    
    
    // Refresh TextField Suffix
    [self updateTextFieldWithNewSuffix];
    
    // Update UI Layout
    [self willAnimateRotationToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0];

}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    
    // Send Google Analytics Information
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:kColdSliderViewControllerIdentifier];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    [FIRAnalytics logEventWithName:kGAIScreenName
                        parameters:@{
                                     kFIRParameterItemName:kColdSliderViewControllerIdentifier,
                                     kFIRParameterContentType:@"screen"
                                     }];
    
}


#pragma mark InfoPage Methods

- (void)setupInfoPageButton {
    
    if (!self.navigationController.topViewController.navigationItem.rightBarButtonItem) {
        // Create and Show Share Button
        //UIImage *buttonImage = [UIImage imageNamed:kContactsFilterIconImage];
        UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
        infoButton.frame = kMenuButtonFrame;
        infoButton.tintColor =   [UIColor whiteColor];

        [infoButton setImage:[UIImage imageNamed:@"icn_impressum.png"] forState:UIControlStateNormal];
        [infoButton addTarget:self action:@selector(presentInfoViewController) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *infoBarButton = [[UIBarButtonItem alloc] initWithCustomView:infoButton];
        self.navigationController.topViewController.navigationItem.rightBarButtonItem = infoBarButton;
        //self.navigationController.topViewController.navigationItem.rightBarButtonItem.image =  [UIImage imageNamed:@"icn_impressum.png"];
        
       // self.navigationController.topViewController.navigationItem.rightBarButtonItem.tintColor =  [UIColor whiteColor];
        infoBarButton = nil;
        infoButton = nil;
        //buttonImage = nil;
    }
    
}


- (void)presentInfoViewController {
    
    InfoPageViewController *infoPage = [self.storyboard instantiateViewControllerWithIdentifier:kInfoPageViewControllerIdentifier];
    [self presentViewController:infoPage animated:YES completion:^{
        //
    }];

    
}


#pragma mark UserDefaults Methods

- (void)updateUIFromPreviousState
{
    
    prefs = [NSUserDefaults standardUserDefaults];
    
    if(NULL != [prefs objectForKey:kColdSliderSelectedRefrigirant])
    {
        self.refrigerantButton.titleLabel.text = [prefs objectForKey:kColdSliderSelectedRefrigirant];
        [self.coldSliderComponent updateSelectedTemperature:[prefs objectForKey:kColdSliderSelectedTempratureUnit]];
        [self updateSelectedRefrigerant:[prefs objectForKey:kColdSliderSelectedRefrigirant]];
        
        if( 0 != [prefs doubleForKey:kColdSliderSelectedTempratureValue])
        {
            self.temperatureTextField.text = [NSString stringWithFormat:@"%f", [prefs doubleForKey:kColdSliderSelectedTempratureValue] ];
            
            
            
            self.temperatureTextField.suffix = [prefs objectForKey:kColdSliderSelectedTempratureUnit];
            self.pressureTextField.suffix = [prefs objectForKey:kColdSliderSelectedPressureType];
            [self updateSliderPositionForTemperatureValue:[prefs doubleForKey:kColdSliderSelectedTempratureValue]];
            [self updatePressureUnit:[[prefs objectForKey:kColdSliderSelectedPressureType] componentsSeparatedByString:@" "][0]];
            [self updateMinMaxTempAndPressure];
            if([[[prefs objectForKey:kColdSliderSelectedPressureType] componentsSeparatedByString:@" "][1] isEqualToString:@"(g)"])
            {
                self.unitButton2.selected = YES;
                self.unitButton1.selected = NO;
            }
            else
            {
                self.unitButton1.selected = YES;
                self.unitButton2.selected = NO;
            }
            
            [self updateColdSliderImage];
        } else {
            [self updateSliderPositionForTemperatureValue:[prefs doubleForKey:kColdSliderSelectedTempratureValue]];
        }
        
        
        // Figure Out Position Of Image and Adjust Values Accordingly
        double position = (((self.sliderView.contentOffset.x - (self.sliderView.bounds.size.width/2.0)) / (self.sliderView.bounds.size.width/2.0)) * -1.0);
        
        // Make Sure Values Don't Exceed Limits
        if ((position >= -1.0) && (position <= 1.0)) {
            
            // Update TextField Values
            [self updatePressureAndTemperatureValuesForSliderPosition:position];
        }
    }
    
}



#pragma mark Unit List Methods

- (void)loadColdSliderData {
    
    // Initialize Cold Slider Calculator
    if (!self.csCalculator) {
        self.csCalculator = [[ColdSliderCalculator alloc] init];
    }
    
    
    // Set Default Min and Max Temp
    minTemp = 0.0;
    maxTemp = 0.0;
    minPressure = 0.0;
    maxPressure = 0.0;
    
    
    // Set Refrigerant Not Selected Alert To nil
    self.refrigerantNotSelectedAlert = nil;
    
    
    // Load Unit List
    self.unitList = [self.csCalculator pressureUnitList];
}


#pragma mark TextField Methods

- (void)setupTextFields {
    
    // Set Self As TextField Delegate
    self.pressureTextField.delegate = self;
    self.temperatureTextField.delegate = self;
    
    
    // Set Textfield Background Image
    self.pressureTextField.background = [[UIImage imageNamed:kColdSliderPressureTextFieldBackgroundImage] resizableImageWithCapInsets:kImageSizingEdgeInsets];
    self.temperatureTextField.background = [[UIImage imageNamed:kColdSliderTemperatureTextFieldBackgroundImage] resizableImageWithCapInsets:kImageSizingEdgeInsets];
    
    
    // Disable Interaction With TextFields until refrigerant got selected
    self.pressureTextField.userInteractionEnabled = NO;
    self.temperatureTextField.userInteractionEnabled = NO;
    
    // Replace Keyboard With Placeholder
    self.pressureTextField.inputView = [[UIView alloc] initWithFrame:CGRectZero];
    self.temperatureTextField.inputView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    // GestureRecognizer To Dismiss Keyboard
    UITapGestureRecognizer *outsideTapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboardForOutsideTouch)];
    [self.view addGestureRecognizer:outsideTapGR];
    outsideTapGR = nil;
    
}


#pragma mark UITextField Delegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    // Calculate Value If Switching From One TextField To Another
    if ((self.numPad != nil) && (self.numPad.targetTextField != textField)) {
        [self textFieldFinishedEditing];
    }
    
    
    // Show Custom Keyboard
    if (self.numPad == nil) {
        self.numPad = [[CustomKeyboard alloc] init];//initWithOrienation:lastOrientation];
        CGPoint keyboardCenter = CGPointMake((self.view.bounds.size.width / 2.0), (self.view.bounds.size.height + (self.numPad.bounds.size.height / 2.0)));
        self.numPad.targetTextField = textField;
        self.numPad.delegate = self;
        [self.view addSubview:self.numPad];
        self.numPad.center = keyboardCenter;
        
        
        // Clear TextField Text
        textField.text = @"";
        
        
        // Update Center Point To End Point
        keyboardCenter.y -= self.numPad.bounds.size.height;
        
        [self updateUIToHandleKeyboard:YES]; // Update UI
        
    } else {
        
        // Keyboard Already Exists So Change Target TextField Without Dismissing And Recreating
        self.numPad.targetTextField = textField;
        
        // Clear TextField Text
        textField.text = @"";
        
    }
    
    
    // Enable Editing
    return YES;
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:kCalculationInputCharacterList] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return (([string isEqualToString:filtered])&&(newLength <= kMaxCalculationInputLength));
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self dismissKeyboardForOutsideTouch];
    return YES;
    
}

-(void)enablePressureAndTemperatureTextfields{
    
    // Enable Interaction With TextFields
    self.pressureTextField.userInteractionEnabled = YES;
    self.temperatureTextField.userInteractionEnabled = YES;
}

- (void)dismissKeyboardForOutsideTouch {
    
    // Remove NumPad
    if (self.numPad) {
        [self textFieldFinishedEditing]; // Compute Entered Value
        [self updateUIToHandleKeyboard:NO]; // Update UI
    }
}

- (void)updateUIToHandleKeyboard:(BOOL)show {
    
    // Resign First Responder For TextFields When Dismissing Keyboard
    if (!show) {
        if ([self.pressureTextField isFirstResponder]) {
            [self.pressureTextField resignFirstResponder];
        }
        if ([self.temperatureTextField isFirstResponder]) {
            [self.temperatureTextField resignFirstResponder];
        }
        
        // Remove Dismiss Keyboard Button and Add Info Page Button
        self.navigationController.topViewController.navigationItem.rightBarButtonItem = nil;
        [self setupInfoPageButton];
        
    } else {
        // Add Dismiss Keyboard Bar Button
        // Create and Add Dismiss Button
        self.navigationController.topViewController.navigationItem.rightBarButtonItem = nil;
        UIButton *dismissButton = [[UIButton alloc] initWithFrame:kBackButtonFrame];
        [dismissButton setTitle:kColdSliderDismissKeyboardButtonTitle forState:UIControlStateNormal];
        [dismissButton setTitleColor:kBackButtonTitleColor forState:UIControlStateNormal];
        [[dismissButton titleLabel] setFont:kBackButtonTitleFont];
        [dismissButton addTarget:self action:@selector(dismissKeyboardForOutsideTouch) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *dismissBarButton = [[UIBarButtonItem alloc] initWithCustomView:dismissButton];
        self.navigationController.topViewController.navigationItem.rightBarButtonItem = dismissBarButton;
        dismissBarButton = nil;
        dismissButton = nil;
    }
    
    // Do Not Adjust View If In Landscape Mode
    if (self.view.frame.size.width < self.view.frame.size.height) {
        
        @autoreleasepool {
            // Get Size of Keyboard
            CGSize keyboardSize = [self.numPad bounds].size;
            
            // Shift UI Vertically To Show TextField When Keyboard Is Up
            if (show) {
                
                // Store Original Content Offset
                originalOffset = ((UIScrollView *)self.view).contentOffset;
                
                // Scroll BackgroundView To Unhide Elements Covered By Keyboard
                CGPoint scrollPoint;
                if (IS_IPHONE_5) {
                    scrollPoint = CGPointMake(0, (self.view.bounds.size.height - (keyboardSize.height - 64.0)));
                } else {
                    scrollPoint = CGPointMake(0, (self.view.bounds.size.height - (keyboardSize.height - 10.0)));
                }
                [(UIScrollView *)self.view setContentOffset:scrollPoint animated:YES];
                
            } else {
                
                // Reset Content Offset to Return To Orginial Position
                [(UIScrollView *)self.view setContentOffset:originalOffset animated:YES];
                
                
                // Remove NumPad
                [self.numPad performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:0.3];
                self.numPad = nil;
                
            }
        }
    } else {
        
        if (show) {
            // Animate Keyboard To Slide Up
            CGPoint visibleCenter = self.numPad.center;
            visibleCenter.y -= self.numPad.bounds.size.height;
            [UIView animateWithDuration:0.3 animations:^{
                self.numPad.center = visibleCenter;
            } completion: ^(BOOL finished) {
            }];
        } else {
            // Animate Keyboard To Slide Down
            CGPoint hiddenCenter = self.numPad.center;
            hiddenCenter.y += self.numPad.bounds.size.height;
            [UIView animateWithDuration:0.3 animations:^{
                self.numPad.center = hiddenCenter;
            } completion: ^(BOOL finished) {
                // Remove NumPad
                [self.numPad removeFromSuperview];
                self.numPad = nil;
            }];
        }
        
    }
    
}


- (void)textFieldFinishedEditing {
    
    // Resign First Reposnder For TextField
    //[self.numPad.targetTextField resignFirstResponder];
    
    // Determine TextField
    if (self.numPad.targetTextField == self.temperatureTextField) {
        
        // Don't Do Anything If It Is Left Blank
        if (![self.temperatureTextField.text isEqualToString:@""]) {
            
            // Validate Entered Values
            if (([self.temperatureTextField.text doubleValue] > maxTemp) || ([self.temperatureTextField.text doubleValue] < minTemp)) {
                
                // Show Invalid Temp Alert
                UIAlertView *invalidTemp = [[UIAlertView alloc] initWithTitle:kColdSliderInvalidTemperatureAlertTitle message:kColdSliderInvalidTemperatureAlertMessage delegate:self cancelButtonTitle:kAlertViewCancelButtonTitle otherButtonTitles:nil];
                [invalidTemp show];
                invalidTemp = nil;
                
                // Reset Entered Value
                self.temperatureTextField.text = @"";
                
            } else {
                
                @autoreleasepool {
                    
                    double temperatureValue = [self getTemperatureValue];
                    
                    // Update Slider
                    [self updateSliderPositionForTemperatureValue:temperatureValue];
                    
                    // Format Entered Value
                    [self setTemperatureValue:temperatureValue];
                }
            }
        }
        
    } else if (self.numPad.targetTextField == self.pressureTextField) {
        
        // Don't Do Anything If It Is Left Blank
        if (![self.pressureTextField.text isEqualToString:@""]) {
            
            // Validate Entered Values
            if (([self.pressureTextField.text doubleValue] > maxPressure) || ([self.pressureTextField.text doubleValue] < minPressure)) {
                
                // Show Invalid Pressure Alert
                UIAlertView *invalidTemp = [[UIAlertView alloc] initWithTitle:kColdSliderInvalidPressureAlertTitle message:kColdSliderInvalidPressureAlertMessage delegate:self cancelButtonTitle:kAlertViewCancelButtonTitle otherButtonTitles:nil];
                [invalidTemp show];
                invalidTemp = nil;
                
                // Reset Entered Value
                self.pressureTextField.text = @"";
                
            } else {
                
                @autoreleasepool {
                    
                    double pressureValue = [self getPressureValue];
                    
                    // Update Slider
                    [self updateSliderPositionForPressureValue:pressureValue];
                    
                    // Format Entered Value
                    [self setPressureValue:pressureValue];
                }
            }
        }
    }
}


#pragma mark ColdSliderRefrigerant Methods

- (void)setupColdSliderRefrigerantSelectorAndButton {
    
    @autoreleasepool {
        // Setup Cold Slider Refrigerant Selector Button
        
        // Set Button Default Title
        [self.refrigerantButton setTitle:kColdSliderRefrigerantSelectorButtonTitle forState:UIControlStateNormal];
        [self.refrigerantButton setTitleEdgeInsets:kColdSliderRefrigerantButtonInsets];
        
        
        // Set Button Image
        [self.refrigerantButton setBackgroundImage:[[UIImage imageNamed:kGreyButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
        
        
        // Add Cold Slider Refrigerant Selector
        if (!self.coldSliderRSelector) {
            self.coldSliderRSelector = [[ColdSliderRefrigerantSelector alloc] initWithNibName:kColdSliderRefrigerantSelectorXibName bundle:nil];
            self.coldSliderRSelector.delegate = self;
            self.coldSliderRSelector.csCalculator = self.csCalculator;
            self.coldSliderRSelector.numberFormatter = self.numberFormatter;
            self.coldSliderRSelector.selectedTemperatureUnit = [self.coldSliderComponent selectedTemperatureUnit];
            
            self.coldSliderRSelector.view.hidden = YES;
            
            [[[[UIApplication sharedApplication] delegate] window] addSubview:self.coldSliderRSelector.view];
        }
    }
}

- (void)hideColdSliderRefrigerantSelectorWithRefrigerant:(NSString *)refrigerant {
    
    @autoreleasepool {
        // Hide Cold Slider Refrigerant Selector
        // Update Variables With Selected Options
        [UIView animateWithDuration:0.3 animations:^{
            self.coldSliderRSelector.view.alpha = 0.0;  // Animate Fade By Setting Alpha Over Time
        } completion: ^(BOOL finished) {
            self.coldSliderRSelector.view.hidden = YES; // Hide View
        }];
        
        NSLog(@"here");
        // Update Selected Refrigerant
        if (refrigerant != nil) {
            
            [self updateSelectedRefrigerant:refrigerant];
            [self enablePressureAndTemperatureTextfields];
        }
        
        [prefs setObject:refrigerant forKey:kColdSliderSelectedRefrigirant];
    }
}

- (IBAction)refrigerantButtonPressed:(id)sender {
    
    // Show Cold Slider Refrigerant Selector
    @autoreleasepool {
        self.coldSliderRSelector.view.hidden = NO; // Unhide View
        
        // Update Selected Temperature Unit and Call Method To Update Labels For New Unit
        self.coldSliderRSelector.selectedTemperatureUnit = [self.coldSliderComponent selectedTemperatureUnit];
        [self.coldSliderRSelector updateDataLabelsForSelectedTemperature];
        
        [UIView animateWithDuration:0.3 animations:^{
            self.coldSliderRSelector.view.alpha = 1.0; // Animate Fade By Setting Alpha Over Time
        } completion: ^(BOOL finished) {
            // Do Nothing Here
        }];
    }
    
}

- (void)updateSelectedRefrigerant:(NSString *)refrigerant {
    
    // Update Button Title With Selected Item Title
    [self.refrigerantButton setTitle:refrigerant forState:UIControlStateNormal];
    
    
    // Update Min and Max Temperature/Pressure Values For Selected Refrigerant
    [self updateMinMaxTempAndPressure];
    
    
    // Reset Slider Position and Update Textfields Accordingly
    [self updatePressureAndTemperatureValuesForSliderPosition:kColdSliderSliderPositionLeft];
    [self moveSliderToPositionWithoutTriggeringDelegateMethods:(self.sliderView.bounds.size.width/2.0)];
    
    
    // Update Slider Image
    [self updateColdSliderImage];
    
    // Update Refrigerant Data Values
    [self updateRefrigerantDataValues];
    
}


#pragma mark Refrigerant Data Methods

- (void)setupRefrigerantDataLabels {

    self.oil.hidden = YES;
    self.oilLabel.hidden = YES;
    
    if (IS_IPHONE_5) {
        
        // Show Data Labels
        self.odp.hidden = NO;
        self.gwp.hidden = NO;
        
        self.tMin.hidden = NO;
        self.tMax.hidden = NO;
        self.tCrit.hidden = NO;
        self.odpLabel.hidden = NO;
        self.gwpLabel.hidden = NO;
        
        self.tMinLabel.hidden = NO;
        self.tMaxLabel.hidden = NO;
        self.tCritLabel.hidden = NO;
        
        // Setup Refrigerant Data Buttons/Labels
        [self.odp setBackgroundImage:[[UIImage imageNamed:kGreyButtonFlatImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
        [self.gwp setBackgroundImage:[[UIImage imageNamed:kGreyButtonFlatImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
        [self.oil setBackgroundImage:[[UIImage imageNamed:kGreyButtonFlatImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
        [self.tMin setBackgroundImage:[[UIImage imageNamed:kGreyButtonFlatImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
        [self.tMax setBackgroundImage:[[UIImage imageNamed:kGreyButtonFlatImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
        [self.tCrit setBackgroundImage:[[UIImage imageNamed:kGreyButtonFlatImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
        
    } else {
        
        // Hide Data Labels
        self.odp.hidden = YES;
        self.gwp.hidden = YES;
        self.oil.hidden = YES;
        self.tMin.hidden = YES;
        self.tMax.hidden = YES;
        self.tCrit.hidden = YES;
        self.odpLabel.hidden = YES;
        self.gwpLabel.hidden = YES;
        self.oilLabel.hidden = YES;
        self.tMinLabel.hidden = YES;
        self.tMaxLabel.hidden = YES;
        self.tCritLabel.hidden = YES;
        
    }
    
}

- (void)updateRefrigerantDataValues {
    
   // if (IS_IPHONE_5) {
        
        // Update Refrigerant Data Button/Label Values for Selected Refrigerant
        NSString *selectedRefrigerant = [self getSelectedRefrigerant];
        
        if ([[self.coldSliderComponent selectedTemperatureUnit] isEqualToString:kCelsiusCharacterString]) {
            self.tMinLabel.text = [NSString stringWithFormat:@"%@:", kColdSliderDataTempMin];
            self.tMaxLabel.text = [NSString stringWithFormat:@"%@:", kColdSliderDataTempMax];
            self.tCritLabel.text = [NSString stringWithFormat:@"%@:", kColdSliderDataTempCritical];
        } else {
            self.tMinLabel.text = [NSString stringWithFormat:@"%@:", kColdSliderDataTempMinF];
            self.tMaxLabel.text = [NSString stringWithFormat:@"%@:", kColdSliderDataTempMaxF];
            self.tCritLabel.text = [NSString stringWithFormat:@"%@:", kColdSliderDataTempCriticalF];
        }
        
        if (![selectedRefrigerant isEqualToString:kColdSliderRefrigerantSelectorButtonTitle]) {
            
            [self.odp setTitle:[self getDecimalFormattedStringForDouble:[[self.csCalculator odpForRefrigerant:selectedRefrigerant] doubleValue]] forState:UIControlStateNormal];
            [self.gwp setTitle:[self getDecimalFormattedStringForDouble:[[self.csCalculator gwpForRefrigerant:selectedRefrigerant] doubleValue]] forState:UIControlStateNormal];
            [self.oil setTitle:[self.csCalculator oilForRefrigerant:selectedRefrigerant] forState:UIControlStateNormal];
            [self.tMin setTitle:[self getDecimalFormattedStringForDouble:[self.csCalculator minTemperature:[self.coldSliderComponent selectedTemperatureUnit] forRefrigerant:[self getSelectedRefrigerant]]] forState:UIControlStateNormal];
            [self.tMax setTitle:[self getDecimalFormattedStringForDouble:[self.csCalculator maxTemperature:[self.coldSliderComponent selectedTemperatureUnit] forRefrigerant:[self getSelectedRefrigerant]]] forState:UIControlStateNormal];
            [self.tCrit setTitle:[self getDecimalFormattedStringForDouble:[self.csCalculator critTemperature:[self.coldSliderComponent selectedTemperatureUnit] forRefrigerant:[self getSelectedRefrigerant]]] forState:UIControlStateNormal];
            [self enablePressureAndTemperatureTextfields];
        } else {
            
            [self.odp setTitle:@"" forState:UIControlStateNormal];
            [self.gwp setTitle:@"" forState:UIControlStateNormal];
            [self.oil setTitle:@"" forState:UIControlStateNormal];
            [self.tMin setTitle:@"" forState:UIControlStateNormal];
            [self.tMax setTitle:@"" forState:UIControlStateNormal];
            [self.tCrit setTitle:@"" forState:UIControlStateNormal];
            
        }
        
        // Release Objects
        selectedRefrigerant = nil;
        
  //  }
    
}


#pragma mark ColdSliderComponent Methods

- (void)setupColdSliderComponentAndButton {
    
    @autoreleasepool {
        // Setup Cold Slider Component Button
        
        // Set Button Default Title
        [self.csComponentButton setTitle:kColdSliderComponentButtonTitle forState:UIControlStateNormal];
        [self.csComponentButton setTitleEdgeInsets:kColdSliderComponentButtonInsets];
        
        
        // Set Button Image
        [self.csComponentButton setBackgroundImage:[[UIImage imageNamed:kGreyButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
        
        
        // Add Cold Slider Component
        if (!self.coldSliderComponent) {
            self.coldSliderComponent = [[ColdSliderComponent alloc] initWithNibName:kColdSliderComponentXibName bundle:nil];
            self.coldSliderComponent.delegate = self;
            self.coldSliderComponent.unitList = self.unitList;
            NSLog(@"unilist :%@",self.unitList);
            self.coldSliderComponent.view.hidden = YES;
            
            [[[[UIApplication sharedApplication] delegate] window] addSubview:self.coldSliderComponent.view];
        }
    }
}

- (IBAction)showHideColdSliderComponent {
    
    @autoreleasepool {
        // Show/Hide Cold Slider Component
        if (self.coldSliderComponent.view.hidden) {
            self.coldSliderComponent.view.hidden = NO;
            [UIView animateWithDuration:0.3 animations:^{
                self.coldSliderComponent.view.alpha = 1.0;
            } completion: ^(BOOL finished) {
                // Do Nothing Here
            }];

        } else {
            
            // Update Variables With Selected Options
            [UIView animateWithDuration:0.3 animations:^{
                self.coldSliderComponent.view.alpha = 0.0;
            } completion: ^(BOOL finished) {
                self.coldSliderComponent.view.hidden = YES;
            }];
            NSLog(@"here2");

        }
    }
}


#pragma mark ColdSliderComponent Delegate Methods

- (void)updateTextFieldWithNewSuffix {
    
    // Update TextField Suffix When Changed On ColdSliderComponent
    [self updatePressureSuffixWithColor:nil];
    [self updateTemperatureSuffixWithColor:nil];
    
    
    
    // Update Min and Max Temperature/Pressure Values For Selected Refrigerant
    [self updateMinMaxTempAndPressure];
    
    
    // Calculate and Update New Pressure Value
    [self setPressureValue:[self.csCalculator getPressure:[self selectedPressureUnit] forTemperature:[self getTemperatureValue] temperatureUnit:[self.coldSliderComponent selectedTemperatureUnit] andRefrigerant:[self getSelectedRefrigerant]]];
    
    
    // Update Slider Image
    [self updateColdSliderImage];
    
}


#pragma mark Unit Type Selection Methods

- (void)setupUnitTypeButtons {
    

    
    // Set Button Background Image
    [self.unitButton1 setBackgroundImage:[[UIImage imageNamed:kBlueButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    [self.unitButton2 setBackgroundImage:[[UIImage imageNamed:kBlueButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    
    
    // Set Image Insets To Display It On Right Side Of Title
    self.unitButton1.imageEdgeInsets = kColdSliderUnitButtonImageInsets;
    self.unitButton2.imageEdgeInsets = kColdSliderUnitButtonImageInsets;
    
    
    // Setup Buttons If They Have Not Already Been Setup
    if (!unitButton1.selected || !unitButton2.selected) {
        [self.unitButton1 setSelected:YES];
        [self.unitButton2 setSelected:NO];
    }
    
}

- (IBAction)unitTypeButtonPressed:(UIButton *)sender {
    
    // Set Clicked Button As Selected
    if (sender == self.unitButton1) {
        [self.unitButton1 setSelected:YES];

        [self.unitButton2 setSelected:NO];


    } else {
        [self.unitButton1 setSelected:NO];
        [self.unitButton2 setSelected:YES];
        

    }
    
    
    // Update TextField With New Suffix
    [self updateTextFieldWithNewSuffix];
    
}

// Getter For Selected Pressure Unit
- (NSString *)selectedPressureUnit {
    
    if (self.unitButton1.selected) {
        return [self.unitButton1 titleForState:UIControlStateNormal];
    } else {
        return [self.unitButton2 titleForState:UIControlStateNormal];
    }
    
}

// Setter For Pressure Unit
- (void)updatePressureUnit:(NSString *)unitName {
    
    NSLog(@"Button Title %@",unitName);
    [self.csComponentButton setTitle:unitName forState:UIControlStateNormal];
    @autoreleasepool {
        NSLog(@"unitButton1 Title %@",unitName);
       
        [self.unitButton1 setTitle:[NSString stringWithFormat:@"%@ %@", unitName, kAtmosphericPressureSymbol] forState:UIControlStateNormal];
         [self.unitButton1 setTitle:[NSString stringWithFormat:@"%@ %@", unitName, kAtmosphericPressureSymbol] forState:UIControlStateSelected];
         [self.unitButton1 setTitle:[NSString stringWithFormat:@"%@ %@", unitName, kAtmosphericPressureSymbol] forState:UIControlStateHighlighted];

        [self.unitButton2 setTitle:[NSString stringWithFormat:@"%@ %@", unitName, kGaugePressureSymbol] forState:UIControlStateNormal];
        [self.unitButton2 setTitle:[NSString stringWithFormat:@"%@ %@", unitName, kGaugePressureSymbol] forState:UIControlStateSelected];
        [self.unitButton2 setTitle:[NSString stringWithFormat:@"%@ %@", unitName, kGaugePressureSymbol] forState:UIControlStateHighlighted];
    }
    
}


#pragma mark ColdSliderScale Methods

- (void)updateColdSliderImage {
    
    @autoreleasepool {
        
        if (![[self getSelectedRefrigerant] isEqualToString:kColdSliderRefrigerantSelectorButtonTitle]) {
            // Determine Scale Data and Populate Array With Information
            NSMutableArray *csScaleData = [NSMutableArray array];
            double tempDelta = (maxTemp - minTemp);
            for (int i = 0; i < 12; i += 2) {
                
                double tempValue = (((tempDelta / 10.0) * i) + minTemp);
                
                double pressureValue = [self.csCalculator getPressure:[self selectedPressureUnit] forTemperature:tempValue temperatureUnit:[self.coldSliderComponent selectedTemperatureUnit] andRefrigerant:[self getSelectedRefrigerant]];
                
                [csScaleData addObject:@[[NSString stringWithFormat:@"%f", tempValue], [NSString stringWithFormat:@"%f", pressureValue]]];
            }
            
            
            CGRect imageFrame = self.sliderView.frame;
            imageFrame.size.width *= 4.0;
            imageFrame.size.height *= 4.0;
            
            ColdSliderScale *scale = [[ColdSliderScale alloc] initWithScaleData:csScaleData tempUnit:[self.coldSliderComponent selectedTemperatureUnit] pressureUnit:[self selectedPressureUnit] refrigerant:[self getSelectedRefrigerant] orientation:[[UIApplication sharedApplication] statusBarOrientation] andFrame:imageFrame];
            self.sliderImage.image = [scale coldSliderScaleImage];
            csScaleData = nil;
            scale = nil;
        } else {
            // Return Null Image
            self.sliderImage.image = nil;
        }
    }
}


#pragma mark Getter Methods

- (double)getPressureValue {
    
    // Return Double Value From Formatted String
    return [self getDoubleFromFormattedString:self.pressureTextField.text];
    
}

- (double)getTemperatureValue {
    
    // Return Double Value From Formatted String
    return [self getDoubleFromFormattedString:self.temperatureTextField.text];
    
}

- (NSString *)getSelectedRefrigerant {
    
    return [self.refrigerantButton titleForState:UIControlStateNormal];
    
}


#pragma mark Setter Methods

- (void)setPressureValue:(double)value {
    
    @autoreleasepool {
        // Set Formatted Value
        self.pressureTextField.text = [NSString stringWithFormat:@"%@", [self getDecimalFormattedStringForDouble:value]];
    }
    
}

- (void)setTemperatureValue:(double)value {
    
    @autoreleasepool {
        // Set Formatted Value
        self.temperatureTextField.text = [NSString stringWithFormat:@"%@", [self getDecimalFormattedStringForDouble:value]];
    }
    
}


- (void)updateMinMaxTempAndPressure {
    
    // Update Min and Max Temperature/Pressure Values For Selected Refrigerant
    minTemp = [self.csCalculator minTemperature:[self.coldSliderComponent selectedTemperatureUnit] forRefrigerant:[self getSelectedRefrigerant]];
    maxTemp = [self.csCalculator maxTemperature:[self.coldSliderComponent selectedTemperatureUnit] forRefrigerant:[self getSelectedRefrigerant]];
    minPressure = [self.csCalculator getPressure:[self selectedPressureUnit] forTemperature:minTemp temperatureUnit:[self.coldSliderComponent selectedTemperatureUnit] andRefrigerant:[self getSelectedRefrigerant]];
    maxPressure = [self.csCalculator getPressure:[self selectedPressureUnit] forTemperature:maxTemp temperatureUnit:[self.coldSliderComponent selectedTemperatureUnit] andRefrigerant:[self getSelectedRefrigerant]];
    
}


- (void)updatePressureSuffixWithColor:(UIColor *)color {
    
    @autoreleasepool {
        
        // Update Pressure TextField Suffix
        self.pressureTextField.suffix = [self selectedPressureUnit];
        
        if (color) {
            self.pressureTextField.suffixColor = color;
        } else {
            self.pressureTextField.suffixColor = self.pressureTextField.textColor;
        }
    }
    
}


- (void)updateTemperatureSuffixWithColor:(UIColor *)color {
    
    @autoreleasepool {
        // Convert Temperature Value To Match New Suffix
        [self setTemperatureValue:[self.csCalculator convertTemperature:[self getTemperatureValue] fromUnit:self.temperatureTextField.suffix toUnit:[self.coldSliderComponent selectedTemperatureUnit]]];
        
        
        // Update Data Label Values
        [self updateRefrigerantDataValues];
        
        
        // Update Temperature TextField Suffix
        
        // Create Appropriate Labels For Temperature Units
        NSString *suffix;
        if ([[self.coldSliderComponent selectedTemperatureUnit] isEqualToString:kCelsiusCharacterString]) {
            suffix = kCelsiusCharacterString;
        } else {
            suffix = kFahrenheitCharacterString;
        }
        self.temperatureTextField.suffix = suffix;
        suffix = nil;
        
        if (color) {
            self.temperatureTextField.suffixColor = color;
        } else {
            self.temperatureTextField.suffixColor = self.temperatureTextField.textColor;
        }
    }
    
}


#pragma mark Number Formatter Methods

- (void)setupNumberFormatter {
    
    if (!self.numberFormatter) {
        self.numberFormatter = [[NSNumberFormatter alloc] init];
        [self.numberFormatter setUsesSignificantDigits:YES];
        [self.numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [self.numberFormatter setMaximumSignificantDigits:kMaxSignificantDigitsColdSlider];
        
        if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqual:kGermanLocale]) {
            [self.numberFormatter setDecimalSeparator:kGermanDecimalSeperator];
            [self.numberFormatter setGroupingSeparator:kGermanGroupingSeperator];
        } else {
            [self.numberFormatter setDecimalSeparator:kEnglishDecimalSeperator];
            [self.numberFormatter setGroupingSeparator:kEnglishGroupingSeperator];
        }
    }
    
}


- (NSString *)getDecimalFormattedStringForDouble:(double)doubleValue {
    
    // Format Double and Return As String
    return [self.numberFormatter stringFromNumber:[NSNumber numberWithDouble:[[NSString stringWithFormat:@"%.2f", doubleValue] doubleValue]]];
    
}


- (double)getDoubleFromFormattedString:(NSString *)string {
    
    // Return Double From Formatted String
    return [[self.numberFormatter numberFromString:string] doubleValue];
    
}


#pragma mark Slider View Methods

- (void)setupSliderView {
    
    // Adjust Content Size To Enable Scrolling
    CGSize sliderViewContentSize = self.sliderView.frame.size;
    sliderViewContentSize.width *= 2;
    self.sliderView.contentSize = sliderViewContentSize;
    
    // Update ImageView Position To Center It In Slider View
    self.sliderImage.center = CGPointMake(self.sliderView.frame.size.width, (self.sliderView.frame.size.height / 2.0));
    [self.sliderView scrollRectToVisible:self.sliderImage.frame animated:NO];
    
    // Set Initial Last Position and Fixed Unit and Temp Values
    [self setPressureValue:0.0];
    [self setTemperatureValue:0.0];
    
    // Set Self As SliderView Delegate
    self.sliderView.delegate = self;
  
    [self.view layoutIfNeeded];
    [self.view updateConstraintsIfNeeded];
    
}

- (void)updatePressureAndTemperatureValuesForSliderPosition:(double)position {
    
    // Calculate New Unit and Temp Values
    double delta = (maxTemp + (minTemp * -1.0)); // Calculate Total Temp Range
    double newTemperature = (((((position * -1.0) + 1.0)/2.0) * delta) + minTemp);
    double newPressure = [self.csCalculator getPressure:[self selectedPressureUnit] forTemperature:newTemperature temperatureUnit:[self.coldSliderComponent selectedTemperatureUnit] andRefrigerant:[self getSelectedRefrigerant]];
    
    // Set New Pressure and Temperature Values
    [self setPressureValue:newPressure];
    [self setTemperatureValue:newTemperature];
}

- (void)updateSliderPositionForPressureValue:(double)pressure {
    
    // Calculate and Update Temperature Value
    double newTemp = [self.csCalculator getTemperature:[self.coldSliderComponent selectedTemperatureUnit] forPressure:pressure pressureUnit:[self selectedPressureUnit] andRefrigerant:[self getSelectedRefrigerant]];
    
    [self setTemperatureValue:newTemp];
    
    // Move Slider Position To Match Value
    [self updateSliderPositionForTemperatureValue:newTemp];
    
}

- (void)updateSliderPositionForTemperatureValue:(double)temperature {
    
    // Calculate and Update Pressure Value
    [self setPressureValue:[self.csCalculator getPressure:[self selectedPressureUnit] forTemperature:temperature temperatureUnit:[self.coldSliderComponent selectedTemperatureUnit] andRefrigerant:[self getSelectedRefrigerant]]];
    
    // Check If Slider's Position Is Different To Calculated Position and Scroll To Point Accordingly
    double delta = (maxTemp - minTemp); // Calculate Total Temp Range
    temperature -= minTemp; // Add Negative Component Of Temp Range To Offset Negative Range
    float position = ((temperature/delta) * self.sliderView.bounds.size.width); // Calculate Position
    
    // Check If Answer Is Within Bounds and Update Offset
    if ((position >= 0) && (position <= self.sliderView.bounds.size.width)) {
        [self moveSliderToPositionWithoutTriggeringDelegateMethods:position];
    }
    
}

- (void)moveSliderToPositionWithoutTriggeringDelegateMethods:(CGFloat)position {
    
    [UIView animateWithDuration:0.3 animations:^{
        
        // Workaround To Prevent ScrollViewDidScroll From Triggering
        CGRect scrollBounds = self.sliderView.bounds;
        scrollBounds.origin = CGPointMake(position, self.sliderView.contentOffset.y);
        self.sliderView.bounds = scrollBounds;
        
    } completion: ^(BOOL finished) {
        // Do Nothing Here
    }];
    
}


#pragma mark ScrollView (Slider) Delegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat halfSliderWidth = (scrollView.bounds.size.width/2.0);
    
    // Check If Refrigerant Is Selected
    if (![[self getSelectedRefrigerant] isEqualToString:kColdSliderRefrigerantSelectorButtonTitle]) {
        
        // Figure Out Position Of Image and Adjust Values Accordingly
        double position = (((scrollView.contentOffset.x - halfSliderWidth) / halfSliderWidth) * -1.0);
        
        // Make Sure Values Don't Exceed Limits
        if ((position >= -1.0) && (position <= 1.0)) {
            
            // Update TextField Values
            [self updatePressureAndTemperatureValuesForSliderPosition:position];
        }
        
    } else {
        
        // Don't Let User Scroll
        [scrollView setContentOffset:CGPointMake((scrollView.bounds.size.width/2.0), scrollView.contentOffset.y)];
        
        // Show Alert If Not Already Displayed
        if (!self.refrigerantNotSelectedAlert) {
            self.refrigerantNotSelectedAlert = [[UIAlertView alloc] initWithTitle:kColdSliderRefrigerantNotSelectedAlertTitle message:kColdSliderRefrigerantNotSelectedAlertMessage delegate:self cancelButtonTitle:kAlertViewCancelButtonTitle otherButtonTitles:nil];
        }
        if (![self.refrigerantNotSelectedAlert isVisible]) {
            [self.refrigerantNotSelectedAlert show];
        }
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    
    // Snap To Points
    CGFloat totalNumberOfTicks = 100.00;
    double snapDelta = (scrollView.bounds.size.width / totalNumberOfTicks);
    double snapPosition = (round((targetContentOffset->x / snapDelta)) * snapDelta);
    targetContentOffset->x = snapPosition;
    //[scrollView setContentOffset:CGPointMake(snapPosition, scrollView.contentOffset.y) animated:YES];
    
}


#pragma mark Auto Rotation Methods

- (BOOL)shouldAutorotate
{
    AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    ViewDeckController *viewDeckController = [appdelegate getViewDeckController];
    
    if([viewDeckController isAnySideOpen])
    {
        return NO;
    }
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration {
    
    // Dismiss Keyboard If Visible During Rotation
    if (self.numPad) {
        [self dismissKeyboardForOutsideTouch];
    }
    
    if (interfaceOrientation == UIInterfaceOrientationPortrait) {
        
        // Call Method To Update UI For Portrait Orientation
        
        // Show Navigation Bar
        self.navigationController.navigationBarHidden = NO;
        
        
        // Enable ViewDeck
        [[NSNotificationCenter defaultCenter] postNotificationName:kEnableDisableViewDeckToggleNotificationIdentifier object:@"YES"];
      
        c_backgroundTop.constant = 270.0;
        c_slider_image_leading.constant = 160.0;
        
    } else if ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft) || (interfaceOrientation == UIInterfaceOrientationLandscapeRight)) {
        
    
        // Call Method To Update UI For Landscape Orientation
        self.navigationController.navigationBarHidden = YES;
        // Hide ColdSlider Component and Refrigerant Selector If Visible
//        if (self.coldSliderComponent.view.hidden == NO) {
//            [self showHideColdSliderComponent];
//        }
//        if (self.coldSliderRSelector.view.hidden == NO) {
//            [self hideColdSliderRefrigerantSelectorWithRefrigerant:nil];
//        }

        
        // ** Disable ViewDeck
        [[NSNotificationCenter defaultCenter] postNotificationName:kEnableDisableViewDeckToggleNotificationIdentifier object:@"NO"];
        
        c_backgroundTop.constant = 0.0;
        c_slider_image_leading.constant = self.view.frame.size.width / 2.0;
        
    }
    
    [self setupSliderView];
    
}


#pragma mark Cleanup

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidDisappear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];

    
    if (![self.refrigerantButton.titleLabel.text isEqualToString:kColdSliderRefrigerantSelectorButtonTitle]) {
        [prefs setObject:self.refrigerantButton.titleLabel.text forKey:kColdSliderSelectedRefrigirant];
    }
    [prefs setObject:[self selectedPressureUnit] forKey:kColdSliderSelectedPressureType];
    [prefs setDouble:[self getPressureValue] forKey:kColdSliderSelectedPressureValue];
    [prefs setDouble:[self getTemperatureValue] forKey:kColdSliderSelectedTempratureValue];
    [prefs setObject:self.coldSliderComponent.selectedTemperatureUnit forKey:kColdSliderSelectedTempratureUnit];
    
    // Setup Buttons If They Have Not Already Been Setup
    if (unitButton1.selected)
    {
        [prefs setObject:self.unitButton1.titleLabel.text forKey:kColdSliderSelectedPressureUnit];
    }
    else if(unitButton2.selected)
    {
        [prefs setObject:self.unitButton2.titleLabel.text forKey:kColdSliderSelectedPressureUnit];
    }
    
    [prefs synchronize];
    
}


- (void)dealloc {
    
    self.numPad = nil;
    self.unitList = nil;
    self.csCalculator = nil;
    self.numberFormatter = nil;
    self.coldSliderComponent = nil;
    self.coldSliderRSelector = nil;
    self.refrigerantNotSelectedAlert = nil;
    
}


@end
