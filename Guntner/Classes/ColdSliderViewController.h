//
//  ColdSliderViewController.h
//  Guntner
//
//  Created by Shiraz Omar on 8/6/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"
#import "GAITrackedViewController.h"


@interface ColdSliderViewController : GAITrackedViewController {
    
    __weak IBOutlet NSLayoutConstraint *c_backgroundTop;
    
    __weak IBOutlet NSLayoutConstraint *c_slider_image_leading;
}


@end
