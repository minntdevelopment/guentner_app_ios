//
//  GUNRestmanager.h
//  guentner
//
//  Created by Julian Dawo on 01.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GUNVersionControlResponse;
@class GUNLanguagesResponse;
@class GUNDynamicMenuResponse;
@class GUNDataManager;
@class GUNTutorialDetailResponse;

@interface GUNRestmanager : NSObject

-(void) getVersionControlOverWebService:(NSString*) webServiceURL completion:(void (^)(GUNVersionControlResponse*, NSError*))block;

-(id)initWithDataManager:(GUNDataManager *)dataManager;

-(void) getOneTranslationListOverWebService:(NSString*) webServiceURL andLanguageKey:(NSString *) languageKey completion:(void (^)(GUNLanguagesResponse*, NSError*))block;
-(void) getDynamicMenuOverWebService:(NSString*) webServiceURL completion:(void (^)(GUNDynamicMenuResponse*, NSError*))block;
-(void)getTutorialDetailsOverWebService:(NSString *)webServiceURL withID:(NSInteger) tutorialID completion:(void (^)(GUNTutorialDetailResponse *, NSError *))block;



@end
