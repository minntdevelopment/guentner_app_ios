//
// Created by Julian Dawo on 17.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PageItemController.h"


@interface GUNInsideTutorialExplanationViewController : PageItemController <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *wv_explanation;
@property (weak, nonatomic) IBOutlet UIView *v_errorBox;
@property (weak, nonatomic) IBOutlet UIView *v_textBox;
@property (weak, nonatomic) IBOutlet UILabel *lbl_noConnection;
@property (weak, nonatomic) IBOutlet UILabel *lbl_start;
@property (nonatomic, strong) NSString *contentUrl;
@end