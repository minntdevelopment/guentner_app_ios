//
// Created by Julian Dawo on 17.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>
#import "PageItemController.h"

@interface GUNInsightTutorialInteractivePageViewController : PageItemController

@property (nonatomic, weak) IBOutlet UILabel *lbl_displayFirstLine;
@property (nonatomic, weak) IBOutlet UILabel *lbl_displaySecondLine;

@property (nonatomic, weak) IBOutlet UILabel *lbl_displayCounterBtnUp;
@property (nonatomic, weak) IBOutlet UILabel *lbl_displayCounterBtnDown;
@property (nonatomic, weak) IBOutlet UILabel *lbl_displayCounterBtnLeft;
@property (nonatomic, weak) IBOutlet UILabel *lbl_displayCounterBtnRight;

@property (nonatomic, weak) IBOutlet UIImageView *iv_displayFirstLineIcon1;
@property (nonatomic, weak) IBOutlet UIImageView *iv_displayFirstLineIcon2;
@property (nonatomic, weak) IBOutlet UIImageView *iv_displayFirstLineIcon3;
@property (nonatomic, weak) IBOutlet UIImageView *iv_displayFirstLineIcon4;

@property (nonatomic, weak) IBOutlet UIImageView *iv_displaySecondLineIcon1;
@property (nonatomic, weak) IBOutlet UIImageView *iv_displaySecondLineIcon2;
@property (nonatomic, weak) IBOutlet UIImageView *iv_displaySecondLineIcon3;
@property (nonatomic, weak) IBOutlet UIImageView *iv_displaySecondLineIcon4;

@property (weak, nonatomic) IBOutlet UILabel *hint_text_control;

@property (nonatomic, weak) IBOutlet UIImageView *iv_displayUp;
@property (nonatomic, weak) IBOutlet UIImageView *iv_displayDown;
@property (nonatomic, weak) IBOutlet UIImageView *iv_displayLeft;
@property (nonatomic, weak) IBOutlet UIImageView *iv_displayRight;
@property (nonatomic, weak) IBOutlet UIImageView *iv_displayEnter;
@property (nonatomic, weak) IBOutlet UIImageView *iv_displayHome;

@property (weak, nonatomic) IBOutlet UIView *v_interfaceContainer;

@property (nonatomic, weak) IBOutlet UIView *v_step1Box;
@property (nonatomic, weak) IBOutlet UILabel *lbl_numberBox1;
@property (nonatomic, weak) IBOutlet UILabel *lbl_firstLineBox1;
@property (nonatomic, weak) IBOutlet UILabel *lbl_secondLineBox1;

@property (nonatomic, weak) IBOutlet UIView *v_step2Box;
@property (nonatomic, weak) IBOutlet UILabel *lbl_numberBox2;
@property (nonatomic, weak) IBOutlet UILabel *lbl_firstLineBox2;
@property (nonatomic, weak) IBOutlet UILabel *lbl_secondLineBox2;

@property (nonatomic, weak) IBOutlet UIView *v_step3Box;
@property (nonatomic, weak) IBOutlet UILabel *lbl_numberBox3;
@property (nonatomic, weak) IBOutlet UILabel *lbl_firstLineBox3;
@property (nonatomic, weak) IBOutlet UILabel *lbl_secondLineBox3;

@property (nonatomic, strong) NSArray *pageSteps;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *c_lastbox_bottom;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *c_v1box_height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *c_v2box_height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *c_v3box_height;


@end