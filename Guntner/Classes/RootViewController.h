//
//  RootViewController.h
//  Guntner
//
//  Created by Shiraz Omar on 8/5/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GUNLoadingViewController;


@interface RootViewController : UIViewController

-(void)showLoadingViewController:(GUNLoadingViewController*)loadingViewController;

-(void)dismissLoadingViewController;

-(void)showPopupViewController:(UIViewController *)popupViewController;

-(void)dismissPopupViewController:(UIViewController*)popupViewController;

//

@end
