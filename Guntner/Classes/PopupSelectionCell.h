//
//  PopupSelectionCell.h
//  guentner
//
//  Created by Julian Dawo on 08.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopupSelectionCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UILabel* lbl_selectableText;
@property (nonatomic, weak) IBOutlet UIImageView* iv_selectionIcon;


@end
