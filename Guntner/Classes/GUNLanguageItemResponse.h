//
// Created by Julian Dawo on 01.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GUNLanguageItemResponse : NSObject

@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSString *value;


@end
