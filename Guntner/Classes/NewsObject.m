//
//  NewsObject.m
//  guentner
//
//  Created by Balazs Auth on 02.12.18.
//  Copyright © 2018 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NewsObject.h"


@implementation NewsObject : NSObject {
}

-(instancetype) initWithId:(NSNumber* )articleId imageLink:(NSString*)imageLink  text:(NSString*)text title:(NSString*)title createdDate:(NSNumber*)createdDate {
        self = [super init];
        if (self) {
            _createdDate = createdDate;
            _imageLink = imageLink;
            _text = text;
            _title = title;
            _articleId = articleId;
        }
        return self;
    }

@end
