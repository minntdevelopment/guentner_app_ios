//
//  ExternalLinkViewController.h
//  guentner
//
//  Created by Julian Dawo on 05.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "GAITrackedViewController.h"

@interface ExternalLinkViewController : GAITrackedViewController <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *wv_externalLinkViewer;
@property (weak, nonatomic) IBOutlet UIView *v_errorContainer;
@property (weak, nonatomic) IBOutlet UIView *v_textbox;
@property (weak, nonatomic) IBOutlet UILabel *lbl_noConnection;
@property (weak, nonatomic) IBOutlet UILabel *lbl_start;
@property (nonatomic, strong) NSString *contentUrl;
@end
