//
//  MenuCell.m
//  guentner
//
//  Created by Balazs Auth on 28/09/15.
//  Copyright © 2015 Brandixi3. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) prepareForReuse {
    [self.menuicon setImage:nil];
}


@end
