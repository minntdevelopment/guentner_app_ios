//
//  ConverterViewController.m
//  Guntner
//
//  Created by Shiraz Omar on 8/5/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "ConverterViewController.h"
#import "Firebase.h"
#import "Converter.h"
#import "Configuration.h"
#import "CustomKeyboard.h"
#import "HorizontalPickerView.h"
#import "PopupListViewController.h"


@interface ConverterViewController () <HorizontalPickerViewDelegate, HorizontalPickerViewDataSource, PopupListDelegate, CustomKeyboardDelegate, UITextFieldDelegate> {
    
    NSArray *unitList;
    Converter *converter;
    
    CustomKeyboard *numPad;
    
    CGPoint originalOffset;
    
    IBOutlet UIButton *swapButton;
    IBOutlet UIButton *unitButton1;
    IBOutlet UIButton *unitButton2;
    IBOutlet UITextField *inputTextField;
    IBOutlet UITextField *outputTextField;
    
    PopupListViewController *conversionList;
    NSUserDefaults *prefs;
    
    IBOutlet HorizontalPickerView *horizontalPicker;
    
}


@property (nonatomic, strong) NSArray *unitList;
@property (nonatomic, strong) Converter *converter;

@property (nonatomic, strong) CustomKeyboard *numPad;

@property (nonatomic, weak) IBOutlet UIButton *swapButton;
@property (nonatomic, weak) IBOutlet UIButton *unitButton1;
@property (nonatomic, weak) IBOutlet UIButton *unitButton2;
@property (nonatomic, weak) IBOutlet UITextField *inputTextField;
@property (nonatomic, weak) IBOutlet UITextField *outputTextField;

@property (nonatomic, strong) PopupListViewController *conversionList;

@property (nonatomic, weak) IBOutlet HorizontalPickerView *horizontalPicker;


- (IBAction)unitButtonPressed:(id)sender;


@end


@implementation ConverterViewController


@synthesize numPad = _numPad;

@synthesize unitList = _unitList;
@synthesize converter = _converter;

@synthesize swapButton = _swapButton;
@synthesize unitButton1 = _unitButton1;
@synthesize unitButton2 = _unitButton2;
@synthesize inputTextField = _inputTextField;
@synthesize outputTextField = _outputTextField;

@synthesize conversionList = _conversionList;

@synthesize horizontalPicker = _horizontalPicker;


#pragma mark Setup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        
        // Custom initialization
        
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    prefs = [NSUserDefaults standardUserDefaults];
    
    // Show Navigation Bar
    self.navigationController.navigationBarHidden = NO;
    
    
    // Load Unit Data
    [self loadUnitData];
    
    
    // Setup Unit Buttons
    [self setupUnitButtons];
    
    
    // Setup Horizontal Picker
    [self setupHorizontalPicker];
    
    
    // Setup TextFields
    [self setupTextField:self.inputTextField];
    [self setupTextField:self.outputTextField];
    
    
    // Setup Custom Keyboard
    [self setupCustomKeyboard];
    

    
    
}


- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    // Send Google Analytics Information
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:kConverterViewControllerIdentifier];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    [FIRAnalytics logEventWithName:kGAIScreenName
                        parameters:@{
                                     kFIRParameterItemName:kConverterViewControllerIdentifier,
                                     kFIRParameterContentType:@"screen"
                                     }];
    
    // Scroll To Default Position
    [self.horizontalPicker scrollToElement:0 animated:YES];
    [self restoreDefaultState];
    
}
-(void) restoreDefaultState
{
    [self.horizontalPicker scrollToElement:[prefs integerForKey:kConverterSelectedUnit] animated:YES];
    self.inputTextField.text = [prefs objectForKey:kConverterSeletectedInputValue];
    
    if ([prefs objectForKey:kConverterSelectedfromValue]) {
        [self.unitButton1 setTitle:[prefs objectForKey:kConverterSelectedfromValue] forState:UIControlStateNormal];
    }
    if ([prefs objectForKey:kConverterSelectedToValue]) {
        [self.unitButton2 setTitle:[prefs objectForKey:kConverterSelectedToValue] forState:UIControlStateNormal];
    }
    [self convertInputAndDisplayResult];
}


#pragma mark Unit Data Methods

- (void)loadUnitData {
    
    // Initialize Converter
    if (!self.converter) {
        self.converter = [[Converter alloc] init];
    }
    
    // Get Data From Converter Object
    if (!self.unitList) {
        self.unitList = [self.converter unitList];
    }
    
}


#pragma mark PopupListViewController Methods

- (void)showPopupListControllerWithData:(NSArray *)popupList fromView:(id)view {
    
    @autoreleasepool {
        
        // Remove PopupList If It Exists
        if (self.conversionList) {
            
            // Remove Popup List View From Screen
            [self.conversionList.view removeFromSuperview];
            self.conversionList = nil;
            
        }
        
        // Setup PopupList Controller
        NSString *title;
        // Get Appropriate Title
        if (view == self.unitButton1) {
            
            title = kConverterPopupListFromTitle;
            
        } else {
            
            title = kConverterPopupListToTitle;
        }
        
        self.conversionList = [[PopupListViewController alloc] initWithTitle:title andListData:popupList];
        self.conversionList.delegate = self;
        
        [[[[UIApplication sharedApplication] delegate] window] addSubview:self.conversionList.view];
        
        // Release title String
        title = nil;
    }
    
}


#pragma mark PopupListViewController Delegate Methods

- (void)dismissPopupListWithSelectedItem:(NSString *)selectedItem {
    
    // Validate selectedItem String
    if (selectedItem) {

            [self.unitButton1.titleLabel setFont:[UIFont systemFontOfSize:16]];
            [self.unitButton2.titleLabel setFont:[UIFont systemFontOfSize:16]];

        
        // Determine If From/To Button Was Selected
        if ([self.conversionList.titleLabel.text isEqualToString:kConverterPopupListFromTitle]) {
            
       
            // Update Button Title With Selected Item Title
            [self.unitButton1 setTitle:selectedItem forState:UIControlStateNormal];
            
        } else if ([self.conversionList.titleLabel.text isEqualToString:kConverterPopupListToTitle]) {
            
            // Update Button Title With Selected Item Title
       
            [self.unitButton2 setTitle:selectedItem forState:UIControlStateNormal];
        }
    }
    
    
    // Convert Input Value and Update Output Label
    [self convertInputAndDisplayResult];
    
    
    // Remove Popup List View From Screen
    [self.conversionList.view removeFromSuperview];
    self.conversionList = nil;
    
}


#pragma mark Unit Button Methods

- (void)setupUnitButtons {
    
    // Setup Unit Button Background Images
    [self.swapButton setBackgroundImage:[[UIImage imageNamed:kGreyButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    
    [self.unitButton1 setBackgroundImage:[[UIImage imageNamed:kBlueButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    
    [self.unitButton2 setBackgroundImage:[[UIImage imageNamed:kConverterNumpadDarkGreyButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    
    [self.unitButton1.titleLabel setFont:[UIFont systemFontOfSize:16]];
    [self.unitButton2.titleLabel setFont:[UIFont systemFontOfSize:16]];
    
}

- (IBAction)unitButtonPressed:(id)sender {
    
    @autoreleasepool {
        
        // Show Conversion List From Selected Button
        if (sender == self.unitButton1) {
            
            [self showPopupListControllerWithData:[self.converter conversionListForUnit:[self.unitList objectAtIndex:[self.horizontalPicker currentSelectedIndex]]] fromView:self.unitButton1];
            
        } else {
            
            [self showPopupListControllerWithData:[self.converter conversionListForUnit:[self.unitList objectAtIndex:[self.horizontalPicker currentSelectedIndex]]] fromView:self.unitButton2];
            
        }
        
        
        // Convert Input Value and Update Output Label
        [self convertInputAndDisplayResult];
    }
    
}


- (IBAction)swapButtonPressed:(id)sender {
    
    // Swap From and To Unit Button Titles
    NSString *toUnit = [self.unitButton2 titleForState:UIControlStateNormal];
    NSString *fromUnit = [self.unitButton1 titleForState:UIControlStateNormal];
    
    [self.unitButton1 setTitle:toUnit forState:UIControlStateNormal];
    [self.unitButton2 setTitle:fromUnit forState:UIControlStateNormal];
    
    fromUnit = nil;
    toUnit = nil;
    
    
    // Convert Input Value and Update Output Label
    [self convertInputAndDisplayResult];
    
}


- (NSString *)fromUnit {
    
    return [self.unitButton1 titleForState:UIControlStateNormal];
    
}

- (NSString *)toUnit {
    
    return [self.unitButton2 titleForState:UIControlStateNormal];
    
}

#pragma mark Conversion Methods

- (void)convertInputAndDisplayResult {
    
    if (![self.inputTextField.text isEqualToString:@""]) {
        // Get Input Value
        double inputValue = [self getDoubleFromFormattedString:self.inputTextField.text];
        //double inputValue = [self.inputTextField.text doubleValue];

        // Convert
        double convertedValue = [self.converter convert:inputValue unit:[self.unitList objectAtIndex:[self.horizontalPicker currentSelectedIndex]] from:[self fromUnit] to:[self toUnit]];
        
        // Format and Display Result
        self.outputTextField.text = [self getFormattedStringForDouble:convertedValue];
    } else {
        
        // Set Output As Zero If Input Is Blank
        self.outputTextField.text = @"";
        
    }
    
}


- (NSString *)getFormattedStringForDouble:(double)doubleValue {
    
    // Format Double and Return As String
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setUsesSignificantDigits:YES];
    [numberFormatter setMinimumSignificantDigits:kMaxSignificantDigitsConverter];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumSignificantDigits:kMaxSignificantDigitsConverter];
    
    if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqual:kGermanLocale]) {
        [numberFormatter setDecimalSeparator:kGermanDecimalSeperator];
        [numberFormatter setGroupingSeparator:kGermanGroupingSeperator];
    } else {
        [numberFormatter setDecimalSeparator:kEnglishDecimalSeperator];
        [numberFormatter setGroupingSeparator:kEnglishGroupingSeperator];
    }
    return [numberFormatter stringFromNumber:[NSNumber numberWithDouble:doubleValue]];

}

- (double)getDoubleFromFormattedString:(NSString *)string {
    
    // Format Double and Return As String
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setUsesSignificantDigits:YES];
    [numberFormatter setMinimumSignificantDigits:kMaxSignificantDigitsConverter];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [numberFormatter setMaximumSignificantDigits:kMaxSignificantDigitsConverter];
    
    if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqual:kGermanLocale]) {
        [numberFormatter setDecimalSeparator:kGermanDecimalSeperator];
        [numberFormatter setGroupingSeparator:kGermanGroupingSeperator];
    } else {
        [numberFormatter setDecimalSeparator:kEnglishDecimalSeperator];
        [numberFormatter setGroupingSeparator:kEnglishGroupingSeperator];
    }
    return [[numberFormatter numberFromString:string] doubleValue];
    
}


#pragma mark HorizontalPicker Methods

- (void)setupHorizontalPicker {
    
    // Setup Horizontal Picker
    self.horizontalPicker.delegate = self;
    self.horizontalPicker.dataSource = self;
    self.horizontalPicker.elementFont = kConverterHorizontalPickerElementFont;
    [self.horizontalPicker setTextColor:kGuntnerColor005A37];
    [self.horizontalPicker setSelectedTextColor:kGuntnerColor005A37];
    
    
    // Scroll To Default Position
    [self.horizontalPicker scrollToElement:0 animated:NO];
    
}


#pragma mark HorizontalPicker Delegate Methods

- (void)horizontalPickerView:(HorizontalPickerView *)picker didSelectElementAtIndex:(NSInteger)index {
    
    // Update Button Titles Accordingly
    if ([self.unitList count]>0) {
        
    }
    
    NSArray *conversionUnitList = [self.converter conversionListForUnit:[self.unitList objectAtIndex:index]];
    if ([conversionUnitList count] > 0) {
        [self.unitButton1 setTitle:[conversionUnitList objectAtIndex:0] forState:UIControlStateNormal];
        [self.unitButton2 setTitle:[conversionUnitList objectAtIndex:1] forState:UIControlStateNormal];
    } else {
        [self.unitButton1 setTitle:@"" forState:UIControlStateNormal];
        [self.unitButton2 setTitle:@"" forState:UIControlStateNormal];
    }
    conversionUnitList = nil;
    
    
    // Convert Input Value and Update Output Label
    [self convertInputAndDisplayResult];
}

- (NSInteger)horizontalPickerView:(HorizontalPickerView *)picker widthForElementAtIndex:(NSInteger)index {
    
    return ([[self.unitList objectAtIndex:index] sizeWithFont:[picker elementFont]].width + kConverterHorizontalPickerElementSpacing);
    
}



#pragma mark HorizontalPicker DataSource Methods

- (NSInteger)numberOfElementsInHorizontalPickerView:(HorizontalPickerView *)picker {
    
    // Data Source Count
    return [self.unitList count];
    
}

- (NSString *)horizontalPickerView:(HorizontalPickerView *)picker titleForElementAtIndex:(NSInteger)index {
    
    // Get Title From Data Source
    return [self.unitList objectAtIndex:index];
    
}


#pragma mark TextField Mathods

- (void)setupTextField:(UITextField *)textField {
    
    // Set Self As TextField Delegate
    textField.delegate = self;
    
    // Set Input/Output Textfield Background Image and Enable Interaction For InputTextField
    if (textField == self.inputTextField) {
        // Input Background
        textField.background = [[UIImage imageNamed:kConverterInputTextFieldBackgroundImage] resizableImageWithCapInsets:kImageSizingEdgeInsets];
        
        // Enable User Interaction and Set Selected
        textField.userInteractionEnabled = YES;
        [textField becomeFirstResponder];
        
    } else {
        
        // Output Background
        textField.background = [[UIImage imageNamed:kConverterOutputTextFieldBackgroundImage] resizableImageWithCapInsets:kImageSizingEdgeInsets];
        
    }
    
    // Set Input Textfield Right View To Prevent Text From Sticking To Edge
    if ((textField.leftView == nil) && (textField.rightView == nil)) {
        UIView *bufferView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, textField.frame.size.height)];
        bufferView.backgroundColor = [UIColor clearColor];
        textField.leftView = bufferView;
        textField.leftViewMode = UITextFieldViewModeAlways;
        textField.rightView = bufferView;
        textField.rightViewMode = UITextFieldViewModeAlways;
        bufferView = nil;
    }
    
    
    // Replace Keyboard With Placeholder
    textField.inputView = [[UIView alloc] initWithFrame:CGRectZero];
    
}


#pragma mark UITextField Delegate Methods

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    // Convert Input Value and Update Output Label
    [self convertInputAndDisplayResult];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:kCalculationInputCharacterList] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return (([string isEqualToString:filtered])&&(newLength <= kMaxCalculationInputLength));
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    return YES;
    
}


#pragma mark CustomKeyboard Methods

- (void)setupCustomKeyboard {
    
    if (self.numPad == nil) {
        self.numPad = [[CustomKeyboard alloc] init]; //WithOrienation:[[UIDevice currentDevice] orientation]];
        CGPoint keyboardCenter = CGPointMake((self.view.bounds.size.width / 2.0), (self.view.bounds.size.height - (self.numPad.bounds.size.height / 2.0)));
        self.numPad.delegate = self;
        self.numPad.blackBorder.hidden = YES;
        self.numPad.targetTextField = self.inputTextField;
        [self.view addSubview:self.numPad];
        self.numPad.center = keyboardCenter;
        [self.view sendSubviewToBack:self.numPad];
    }
    
}


#pragma mark CustomKeyboard Delegate Methods

- (void)customKeyboardDidUpdateTextField {
    
    // Convert Input Value and Update Output Label
    [self convertInputAndDisplayResult];
    
}


#pragma mark Auto Rotation Methods

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


#pragma mark Cleanup

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}


- (void)dealloc {
    
    [prefs setObject:[self fromUnit] forKey:kConverterSelectedfromValue];
    [prefs setObject:[self toUnit] forKey:kConverterSelectedToValue];
    [prefs setInteger:[self.horizontalPicker currentSelectedIndex] forKey:kConverterSelectedUnit];
    [prefs setObject:self.inputTextField.text forKey:kConverterSeletectedInputValue];
    [prefs setObject:self.outputTextField.text forKey:kConverterSeletectdOutPutValue];
    
    [prefs synchronize];
    
    self.numPad = nil;
    self.unitList = nil;
    self.converter = nil;
    self.conversionList = nil;
    
}


@end
