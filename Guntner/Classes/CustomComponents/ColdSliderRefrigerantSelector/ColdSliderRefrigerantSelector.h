//
//  ColdSliderRefrigerantSelector.h
//  Guntner
//
//  Created by Shiraz Omar on 8/30/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import "ColdSliderCalculator.h"


@protocol ColdSliderRefrigerantSelectorDelegate;


@interface ColdSliderRefrigerantSelector : UIViewController {
    
    id <ColdSliderRefrigerantSelectorDelegate> delegate;
    
    NSString *selectedTemperatureUnit;
    NSNumberFormatter *numberFormatter;
    
    ColdSliderCalculator *csCalculator;
    
}


@property (nonatomic, weak) id <ColdSliderRefrigerantSelectorDelegate> delegate;

@property (nonatomic, weak) NSNumberFormatter *numberFormatter;
@property (nonatomic, weak) ColdSliderCalculator *csCalculator;
@property (nonatomic, strong) NSString *selectedTemperatureUnit;


- (void)updateDataLabelsForSelectedTemperature;


@end


@protocol ColdSliderRefrigerantSelectorDelegate <NSObject>


- (void)hideColdSliderRefrigerantSelectorWithRefrigerant:(NSString *)refrigerant;


@end