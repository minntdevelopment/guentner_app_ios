//
//  ColdSliderRefrigerantSelector.m
//  Guntner
//
//  Created by Shiraz Omar on 8/30/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "ColdSliderRefrigerantSelector.h"

#import "Configuration.h"


@interface ColdSliderRefrigerantSelector () <UITableViewDelegate, UITableViewDataSource> {
    
    IBOutlet UIButton *odp;
    IBOutlet UIButton *gwp;
    IBOutlet UIButton *oil;
    IBOutlet UIButton *tMin;
    IBOutlet UIButton *tMax;
    IBOutlet UIButton *tCrit;
    
    IBOutlet UILabel *odpLabel;
    IBOutlet UILabel *gwpLabel;
    IBOutlet UILabel *oilLabel;
    IBOutlet UILabel *tMinLabel;
    IBOutlet UILabel *tMaxLabel;
    IBOutlet UILabel *tCritLabel;
    
    IBOutlet UILabel *titleLabel;
    
    IBOutlet UIButton *okButton;
    
    IBOutlet UIView *contentView;
    
    NSDictionary *refrigerantData;
    
    IBOutlet UITableView *refrigerantList;
    
}


@property (nonatomic, weak) IBOutlet UIButton *odp;
@property (nonatomic, weak) IBOutlet UIButton *gwp;
@property (nonatomic, weak) IBOutlet UIButton *oil;
@property (nonatomic, weak) IBOutlet UIButton *tMin;
@property (nonatomic, weak) IBOutlet UIButton *tMax;
@property (nonatomic, weak) IBOutlet UIButton *tCrit;

@property (nonatomic, weak) IBOutlet UILabel *odpLabel;
@property (nonatomic, weak) IBOutlet UILabel *gwpLabel;
@property (nonatomic, weak) IBOutlet UILabel *oilLabel;
@property (nonatomic, weak) IBOutlet UILabel *tMinLabel;
@property (nonatomic, weak) IBOutlet UILabel *tMaxLabel;
@property (nonatomic, weak) IBOutlet UILabel *tCritLabel;

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

@property (nonatomic, weak) IBOutlet UIButton *okButton;

@property (nonatomic, weak) IBOutlet UIView *contentView;

@property (nonatomic, strong) NSDictionary *refrigerantData;

@property (nonatomic, weak) IBOutlet UITableView *refrigerantList;


- (IBAction)dismissSelf;
- (IBAction)okButtonPressed:(id)sender;
- (NSString *)getDecimalFormattedStringForDouble:(double)doubleValue;


@end


@implementation ColdSliderRefrigerantSelector


@synthesize delegate = _delegate;

@synthesize odp = _odp;
@synthesize gwp = _gwp;
@synthesize oil = _oil;
@synthesize tMin = _tMin;
@synthesize tMax = _tMax;
@synthesize tCrit = _tCrit;

@synthesize odpLabel = _odpLabel;
@synthesize gwpLabel = _gwpLabel;
@synthesize oilLabel = _oilLabel;
@synthesize tMinLabel = _tMinLabel;
@synthesize tMaxLabel = _tMaxLabel;
@synthesize tCritLabel = _tCritLabel;

@synthesize titleLabel = _titleLabel;

@synthesize okButton = _okButton;

@synthesize contentView = _contentView;

@synthesize csCalculator = _csCalculator;

@synthesize refrigerantData = _refrigerantData;

@synthesize refrigerantList = _refrigerantList;

@synthesize numberFormatter = _numberFormatter;

@synthesize selectedTemperatureUnit = _selectedTemperatureUnit;


#pragma mark Setup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // Setup UI
    [self setupUI];
    
    
    // Setup Refrigerant List
    [self setupRefrigerantList];
    
    
    // Set Saved Refrigerant To Selected
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if ([prefs objectForKey:kColdSliderSelectedRefrigirant]) {
        [self.refrigerantList selectRowAtIndexPath:[NSIndexPath indexPathForRow:[[[self.refrigerantData allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] indexOfObject:[prefs objectForKey:kColdSliderSelectedRefrigirant]] inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    
}

#pragma mark UserDefaults Methods

- (void)setValuesToUserDefaults:(NSMutableDictionary *)defaultValueToSave
{
    //To save
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:defaultValueToSave forKey:kColdSliderUserDefaultKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (NSMutableDictionary *)getValuesFromUserDefaults
{
    //To retrieve
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *favourites = [[standardUserDefaults objectForKey:kColdSliderUserDefaultKey]   mutableCopy];
    return favourites;
}




#pragma mark UI Setup Methods

- (void)setupUI {
    
   
    // Update Content View Position For iPhone
    if (!IS_IPHONE_5) {
        CGRect contentViewFrame = self.contentView.frame;
        contentViewFrame.origin.y -= 45;
        self.contentView.frame = contentViewFrame;
    }
    
    
    // Update Title For Localization
    self.titleLabel.text = kColdSliderRefrigerantSelectorButtonTitle;
    
    
    // Setup Refrigerant Data Buttons/Labels
    [self.odp setBackgroundImage:[[UIImage imageNamed:kWhiteButtonFlatImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    [self.gwp setBackgroundImage:[[UIImage imageNamed:kWhiteButtonFlatImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    [self.oil setBackgroundImage:[[UIImage imageNamed:kWhiteButtonFlatImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    [self.tMin setBackgroundImage:[[UIImage imageNamed:kWhiteButtonFlatImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    [self.tMax setBackgroundImage:[[UIImage imageNamed:kWhiteButtonFlatImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    [self.tCrit setBackgroundImage:[[UIImage imageNamed:kWhiteButtonFlatImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    [self.okButton setBackgroundImage:[[UIImage imageNamed:kBlueButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    
    
    
    // Hide Oil Labels
    self.oil.hidden = YES;
    self.oilLabel.hidden = YES;
    
    
    // Update Okay Button Title For Localization
    [self.okButton setTitle:kColdSliderRefrigerantSelectorOkButtonTitle forState:UIControlStateNormal];
    
    
    // Round ContentView's Corners
    [self roundViewsCorners:self.contentView];
    
}

- (void)roundViewsCorners:(UIView *)view {
    
    // Set View's Corner Radius To 5px To Round The Corners
    view.layer.cornerRadius = 5.0;
    view.layer.masksToBounds = YES;
    
    // Set OK Button Layer Mask So It Doesn't Override ContentView's Rounded Corners
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.okButton.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:(CGSize){5.0, 5.0}].CGPath;
    self.okButton.layer.mask = maskLayer;
    
}

- (void)updateDataLabelsForSelectedTemperature {
    // Update Refrigerant Data Labels With The Appropriate Values
    if ([self.selectedTemperatureUnit isEqualToString:kCelsiusCharacterString]) {
        self.tMinLabel.text = [NSString stringWithFormat:@"%@:", kColdSliderDataTempMin];
        self.tMaxLabel.text = [NSString stringWithFormat:@"%@:", kColdSliderDataTempMax];
        self.tCritLabel.text = [NSString stringWithFormat:@"%@:", kColdSliderDataTempCritical];
    } else {
        self.tMinLabel.text = [NSString stringWithFormat:@"%@:", kColdSliderDataTempMinF];
        self.tMaxLabel.text = [NSString stringWithFormat:@"%@:", kColdSliderDataTempMaxF];
        self.tCritLabel.text = [NSString stringWithFormat:@"%@:", kColdSliderDataTempCriticalF];
    }
    
    NSIndexPath *selectedIndexPath = [self.refrigerantList indexPathForSelectedRow];
    if (selectedIndexPath != nil) { // Selected Row Exists
        
        // Retreive Appropriate Cell
        UITableViewCell *cell = [self.refrigerantList cellForRowAtIndexPath:selectedIndexPath];
        
        NSDictionary *selectedRefrigerantData = [self.refrigerantData valueForKey:cell.textLabel.text];
        
        [self.odp setTitle:[self getDecimalFormattedStringForDouble:[[selectedRefrigerantData valueForKey:kColdSliderDataODP] doubleValue]] forState:UIControlStateNormal];
        [self.gwp setTitle:[self getDecimalFormattedStringForDouble:[[selectedRefrigerantData valueForKey:kColdSliderDataGWP] doubleValue]] forState:UIControlStateNormal];
       // [self.oil setTitle:[selectedRefrigerantData valueForKey:kColdSliderDataOIL] forState:UIControlStateNormal];
        [self.tMin setTitle:[self getDecimalFormattedStringForDouble:[self.csCalculator minTemperature:self.selectedTemperatureUnit forRefrigerant:cell.textLabel.text]] forState:UIControlStateNormal];
        [self.tMax setTitle:[self getDecimalFormattedStringForDouble:[self.csCalculator maxTemperature:self.selectedTemperatureUnit forRefrigerant:cell.textLabel.text]] forState:UIControlStateNormal];
        [self.tCrit setTitle:[self getDecimalFormattedStringForDouble:[self.csCalculator critTemperature:self.selectedTemperatureUnit forRefrigerant:cell.textLabel.text]] forState:UIControlStateNormal];
        
        // Release Objects
        selectedRefrigerantData = nil;
        cell = nil;
    }
    
    // Release Objects
    selectedIndexPath = nil;
}


#pragma mark Methods To Dismiss Self

- (IBAction)dismissSelf {
    
    if ([self.delegate respondsToSelector:@selector(hideColdSliderRefrigerantSelectorWithRefrigerant:)]) {
        [self.delegate hideColdSliderRefrigerantSelectorWithRefrigerant:nil];
    }
    
}


#pragma mark Delegate Methods

- (IBAction)okButtonPressed:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(hideColdSliderRefrigerantSelectorWithRefrigerant:)]) {
        [self.delegate hideColdSliderRefrigerantSelectorWithRefrigerant:[[self.refrigerantList cellForRowAtIndexPath:[self.refrigerantList indexPathForSelectedRow]] textLabel].text];
    }
    
}


#pragma mark RefrigerantList Methods

- (void)setupRefrigerantList {
    
    // Load Data
    // Load Data From Disk
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:kColdSliderDataFile ofType:kColdSliderDataFileType];
    self.refrigerantData = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    plistPath = nil;
    
    // Setup Refrigerant List
    self.refrigerantList.delegate = self;
    self.refrigerantList.dataSource = self;
    self.refrigerantList.allowsSelection = YES;
    
}


#pragma mark RefrigerantList Delegate Methods

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Set Cell As Selected
    [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    
    
    // Update Refrigerant Data Labels With The Appropriate Values
    [self updateDataLabelsForSelectedTemperature];
    
}


#pragma mark RefrigerantList DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Data Source Count
    return [self.refrigerantData count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        // Configure Cell Text Label
        cell.textLabel.adjustsFontSizeToFitWidth = YES;
        cell.textLabel.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.highlightedTextColor = [UIColor blackColor];
        cell.textLabel.font = kColdSliderComponentRefrigerantListCellFont;
        
        // Setup Selected Background View
        UIView *backgroundView = [[UIView alloc] initWithFrame:cell.frame];
        backgroundView.backgroundColor = kGuntnerColorCBDDEE;
        cell.selectedBackgroundView = backgroundView;
        backgroundView = nil;
        
        // Setup Custom Seperator For Cell
        UIView *seperator = [[UIView alloc] initWithFrame:CGRectMake(kColdSliderRefrigerantSelectorListSeperatorPadding, (tableView.rowHeight - kColdSliderRefrigerantSelectorListSeperatorHeight), (tableView.bounds.size.width - kColdSliderRefrigerantSelectorListSeperatorPadding), kColdSliderRefrigerantSelectorListSeperatorHeight)];
        seperator.backgroundColor = kGuntnerColorEEEEEE;
        seperator.tag = kColdSliderRefrigerantSelectorListSeperatorTag;
        [cell.contentView addSubview:seperator];
        seperator = nil;
        
    }
    
    
    // Cell Customization
    
    // Set Cell Text
    cell.textLabel.text = [NSString stringWithFormat:@"%@", [[[self.refrigerantData allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)] objectAtIndex:indexPath.row]];
    
    
    // Hide Seperator For Last Cell
    if (indexPath.row == ([self.refrigerantData count] - 1)) {
        for (UIView *seperator in cell.contentView.subviews) {
            if (seperator.tag == kColdSliderRefrigerantSelectorListSeperatorTag) {
                seperator.hidden = YES;
            }
        }
    }
    
    
    return cell;
    
}


#pragma mark Number Formatter Methods
- (NSString *)getDecimalFormattedStringForDouble:(double)doubleValue {
    
    // Set Number Formatter Format To Decimal
    [self.numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    // Format Double and Return As String
    return [self.numberFormatter stringFromNumber:[NSNumber numberWithDouble:doubleValue]];
    
    // Set Number Formatter Back To Scientific
    [self.numberFormatter setNumberStyle:NSNumberFormatterScientificStyle];
    
}


#pragma mark Cleanup

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}


- (void)dealloc {
    
    self.refrigerantData = nil;
    self.selectedTemperatureUnit = nil;
    
}

@end
