//
//  LoadingViewController.m
//  Guntner
//
//  Created by Shiraz Omar on 9/13/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "LoadingViewController.h"


@interface LoadingViewController () {
    
    //
    
}

//

@end


@implementation LoadingViewController


@synthesize loadingIndicator = _loadingIndicator;


#pragma mark Setup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    // Rounds Corners
    [self roundViewsCorners:self.view];
    
    
    // Start Animating
    [self.loadingIndicator startAnimating];
    
    
}

- (void)roundViewsCorners:(UIView *)view {
    
    // Set View's Corner Radius To 5px To Round The Corners
    view.layer.cornerRadius = 10.0;
    view.layer.masksToBounds = YES;
    
}


#pragma mark Cleanup

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    [self.loadingIndicator stopAnimating];
    
}


@end
