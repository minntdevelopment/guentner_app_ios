//
//  LoadingViewController.h
//  Guntner
//
//  Created by Shiraz Omar on 9/13/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


@interface LoadingViewController : UIViewController {
    
    IBOutlet UIActivityIndicatorView *loadingIndicator;
    
}


@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *loadingIndicator;


@end
