//
//  SideController.h
//  Guntner
//
//  Created by Shiraz Omar on 8/5/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "WrapController.h"

@interface SideController : WrapController

@property (nonatomic, assign) CGFloat constrainedSize;

- (id)initWithViewController:(UIViewController*)controller constrained:(CGFloat)constrainedSize;

- (void)shrinkSide;
- (void)shrinkSideAnimated:(BOOL)animated;

@end


// category on UIViewController to provide access to the sideController in the
// contained viewcontrollers, a la UINavigationController.
@interface UIViewController (SideController)

@property(nonatomic,readonly,retain) SideController *sideController;

@end
