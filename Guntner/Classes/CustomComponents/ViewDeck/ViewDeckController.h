//
//  ViewDeckController.h
//  Guntner
//
//  Created by Shiraz Omar on 8/5/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>


#if TARGET_OS_IPHONE && defined(__IPHONE_5_0) && (__IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_5_0) && __clang__ && (__clang_major__ >= 3)
#define _SDK_SUPPORTS_WEAK 1
#elif TARGET_OS_MAC && defined(__MAC_10_7) && (MAC_OS_X_VERSION_MIN_REQUIRED >= __MAC_10_7) && __clang__ && (__clang_major__ >= 3)
#define _SDK_SUPPORTS_WEAK 1
#else
#define _SDK_SUPPORTS_WEAK 0
#endif

#if _SDK_SUPPORTS_WEAK
#define ___weak        __weak
#define _weak_property weak
#else
#if __clang__ && (__clang_major__ >= 3)
#define ___weak __unsafe_unretained
#else
#define ___weak
#endif
#define _weak_property assign
#endif


@protocol ViewDeckControllerDelegate;

enum {
    ViewDeckLeftSide = 1,
    ViewDeckRightSide = 2,
    ViewDeckTopSide = 3,
    ViewDeckBottomSide = 4,
};
typedef UInt32 ViewDeckSide;

enum {
    ViewDeckHorizontalOrientation = 1,
    ViewDeckVerticalOrientation = 2
};
typedef UInt32 ViewDeckOffsetOrientation;

enum {
    ViewDeckNoPanning,              // no panning allowed
    ViewDeckFullViewPanning,        // the default: touch anywhere in the center view to drag the center view around
    ViewDeckNavigationBarPanning,   // panning only occurs when you start touching in the navigation bar (when the center controller is a UINavigationController with a visible navigation bar). Otherwise it will behave as ViewDeckNoPanning. 
    ViewDeckPanningViewPanning,      // panning only occurs when you start touching in a UIView set in panningView property
    ViewDeckDelegatePanning,         // allows panning with a delegate
    ViewDeckNavigationBarOrOpenCenterPanning,      //panning occurs when you start touching the navigation bar if the center controller is visible.  If the left or right controller is open, pannning occurs anywhere on the center controller, not just the navbar.
    ViewDeckAllViewsPanning,        // you can pan anywhere in the viewdeck (including sideviews)
};
typedef UInt32 ViewDeckPanningMode;


enum {
    ViewDeckCenterHiddenUserInteractive,         // the center view stays interactive
    ViewDeckCenterHiddenNotUserInteractive,      // the center view will become nonresponsive to useractions
    ViewDeckCenterHiddenNotUserInteractiveWithTapToClose, // the center view will become nonresponsive to useractions, but will allow the user to tap it so that it closes
    ViewDeckCenterHiddenNotUserInteractiveWithTapToCloseBouncing, // same as ViewDeckCenterHiddenNotUserInteractiveWithTapToClose, but closes the center view bouncing
};
typedef UInt32 ViewDeckCenterHiddenInteractivity;


enum {
    ViewDeckNavigationControllerContained,      // the center navigation controller will act as any other viewcontroller. Pushing and popping view controllers will be contained in the centerview.
    ViewDeckNavigationControllerIntegrated      // the center navigation controller will integrate with the viewdeck.
};
typedef UInt32 ViewDeckNavigationControllerBehavior;


enum {
    ViewDeckLedgeSizeMode, // when rotating, the ledge sizes are kept (side views are more/less visible)
    ViewDeckViewSizeMode  // when rotating, the size view sizes are kept (ledges change)
};
typedef UInt32 ViewDeckSizeMode;


enum {
    ViewDeckDelegateOnly, // call the delegate only
    ViewDeckDelegateAndSubControllers  // call the delegate and the subcontrollers
};
typedef UInt32 ViewDeckDelegateMode;

#define ViewDeckCenterHiddenCanTapToClose(interactivity) ((interactivity) == ViewDeckCenterHiddenNotUserInteractiveWithTapToClose || (interactivity) == ViewDeckCenterHiddenNotUserInteractiveWithTapToCloseBouncing)
#define ViewDeckCenterHiddenIsInteractive(interactivity) ((interactivity) == ViewDeckCenterHiddenUserInteractive)

extern NSString* NSStringFromViewDeckSide(ViewDeckSide side);
extern ViewDeckOffsetOrientation ViewDeckOffsetOrientationFromViewDeckSide(ViewDeckSide side);

@interface ViewDeckController : UIViewController {
@private    
    CGPoint _panOrigin;
    UInt32 _viewAppeared;
    BOOL _viewFirstAppeared;
    UInt32 _sideAppeared[6];
    CGFloat _ledge[5];
    UIViewController* _controllers[6];
    CGFloat _offset, _maxLedge;
    CGSize _preRotationSize, _preRotationCenterSize;
    BOOL _preRotationIsLandscape;
    ViewDeckOffsetOrientation _offsetOrientation;
    UIInterfaceOrientation _willAppearShouldArrangeViewsAfterRotation;
    CGPoint _willAppearOffset;
    NSMutableArray* _finishTransitionBlocks;
    int _disabledUserInteractions;
    BOOL _needsAddPannersIfAllPannersAreInactive;
}

typedef void (^ViewDeckControllerBlock) (ViewDeckController *controller, BOOL success);
typedef void (^ViewDeckControllerBounceBlock) (ViewDeckController *controller);

@property (nonatomic, _weak_property) ___weak id<ViewDeckControllerDelegate> delegate;
@property (nonatomic, assign) ViewDeckDelegateMode delegateMode;

@property (nonatomic, readonly, retain) NSArray* controllers;
@property (nonatomic, retain) IBOutlet UIViewController* centerController;
@property (nonatomic, retain) IBOutlet UIViewController* leftController;
@property (nonatomic, retain) IBOutlet UIViewController* rightController;
@property (nonatomic, retain) IBOutlet UIViewController* topController;
@property (nonatomic, retain) IBOutlet UIViewController* bottomController;
@property (nonatomic, readonly, assign) UIViewController* slidingController;

@property (nonatomic, retain) IBOutlet UIView* panningView;
@property (nonatomic, _weak_property) ___weak id<UIGestureRecognizerDelegate> panningGestureDelegate;
@property (nonatomic, assign, getter=isEnabled) BOOL enabled;
@property (nonatomic, assign, getter=isElastic) BOOL elastic;

@property (nonatomic, assign) CGFloat leftSize;
@property (nonatomic, assign, readonly) CGFloat leftViewSize;
@property (nonatomic, assign, readonly) CGFloat leftLedgeSize;
@property (nonatomic, assign) CGFloat rightSize;
@property (nonatomic, assign, readonly) CGFloat rightViewSize;
@property (nonatomic, assign, readonly) CGFloat rightLedgeSize;
@property (nonatomic, assign) CGFloat topSize;
@property (nonatomic, assign, readonly) CGFloat topViewSize;
@property (nonatomic, assign, readonly) CGFloat topLedgeSize;
@property (nonatomic, assign) CGFloat bottomSize;
@property (nonatomic, assign, readonly) CGFloat bottomViewSize;
@property (nonatomic, assign, readonly) CGFloat bottomLedgeSize;
@property (nonatomic, assign) CGFloat maxSize;
@property (nonatomic, assign) BOOL resizesCenterView;
@property (nonatomic, assign) ViewDeckPanningMode panningMode;
@property (nonatomic, assign) BOOL panningCancelsTouchesInView;
@property (nonatomic, assign) ViewDeckCenterHiddenInteractivity centerhiddenInteractivity;
@property (nonatomic, assign) ViewDeckNavigationControllerBehavior navigationControllerBehavior;
@property (nonatomic, assign) BOOL automaticallyUpdateTabBarItems;
@property (nonatomic, assign) ViewDeckSizeMode sizeMode;
@property (nonatomic, assign) CGFloat bounceDurationFactor; // capped between 0.01 and 0.99. defaults to 0.3. Set to 0 to have the old 1.4 behavior (equal time for long part and short part of bounce)
@property (nonatomic, assign) CGFloat bounceOpenSideDurationFactor; // Same as bounceDurationFactor, but if set, will give independent control of the bounce as the side opens fully (first half of the bounce)
@property (nonatomic, assign) CGFloat openSlideAnimationDuration;
@property (nonatomic, assign) CGFloat closeSlideAnimationDuration;
@property (nonatomic, assign) CGFloat parallaxAmount;

@property (nonatomic, strong) NSString *centerTapperAccessibilityLabel; // Voice over accessibility label for button to close side panel
@property (nonatomic, strong) NSString *centerTapperAccessibilityHint;  // Voice over accessibility hint for button to close side panel

- (id)initWithCenterViewController:(UIViewController*)centerController;
- (id)initWithCenterViewController:(UIViewController*)centerController leftViewController:(UIViewController*)leftController;
- (id)initWithCenterViewController:(UIViewController*)centerController rightViewController:(UIViewController*)rightController;
- (id)initWithCenterViewController:(UIViewController*)centerController leftViewController:(UIViewController*)leftController rightViewController:(UIViewController*)rightController;
- (id)initWithCenterViewController:(UIViewController*)centerController topViewController:(UIViewController*)topController;
- (id)initWithCenterViewController:(UIViewController*)centerController bottomViewController:(UIViewController*)bottomController;
- (id)initWithCenterViewController:(UIViewController*)centerController topViewController:(UIViewController*)topController bottomViewController:(UIViewController*)bottomController;
- (id)initWithCenterViewController:(UIViewController*)centerController leftViewController:(UIViewController*)leftController rightViewController:(UIViewController*)rightController topViewController:(UIViewController*)topController bottomViewController:(UIViewController*)bottomController;

- (void)setLeftSize:(CGFloat)leftSize completion:(void(^)(BOOL finished))completion;
- (void)setRightSize:(CGFloat)rightSize completion:(void(^)(BOOL finished))completion;
- (void)setTopSize:(CGFloat)leftSize completion:(void(^)(BOOL finished))completion;
- (void)setBottomSize:(CGFloat)rightSize completion:(void(^)(BOOL finished))completion;
- (void)setMaxSize:(CGFloat)maxSize completion:(void(^)(BOOL finished))completion;

- (BOOL)toggleLeftView;
- (BOOL)openLeftView;
- (BOOL)closeLeftView;
- (BOOL)toggleLeftViewAnimated:(BOOL)animated;
- (BOOL)toggleLeftViewAnimated:(BOOL)animated completion:(ViewDeckControllerBlock)completed;
- (BOOL)openLeftViewAnimated:(BOOL)animated;
- (BOOL)openLeftViewAnimated:(BOOL)animated completion:(ViewDeckControllerBlock)completed;
- (BOOL)openLeftViewBouncing:(ViewDeckControllerBounceBlock)bounced;
- (BOOL)openLeftViewBouncing:(ViewDeckControllerBounceBlock)bounced completion:(ViewDeckControllerBlock)completed;
- (BOOL)closeLeftViewAnimated:(BOOL)animated;
- (BOOL)closeLeftViewAnimated:(BOOL)animated completion:(ViewDeckControllerBlock)completed;
- (BOOL)closeLeftViewAnimated:(BOOL)animated duration:(NSTimeInterval)duration completion:(ViewDeckControllerBlock)completed;
- (BOOL)closeLeftViewBouncing:(ViewDeckControllerBounceBlock)bounced;
- (BOOL)closeLeftViewBouncing:(ViewDeckControllerBounceBlock)bounced completion:(ViewDeckControllerBlock)completed;

- (BOOL)toggleRightView;
- (BOOL)openRightView;
- (BOOL)closeRightView;
- (BOOL)toggleRightViewAnimated:(BOOL)animated;
- (BOOL)toggleRightViewAnimated:(BOOL)animated completion:(ViewDeckControllerBlock)completed;
- (BOOL)openRightViewAnimated:(BOOL)animated;
- (BOOL)openRightViewAnimated:(BOOL)animated completion:(ViewDeckControllerBlock)completed;
- (BOOL)openRightViewBouncing:(ViewDeckControllerBounceBlock)bounced;
- (BOOL)openRightViewBouncing:(ViewDeckControllerBounceBlock)bounced completion:(ViewDeckControllerBlock)completed;
- (BOOL)closeRightViewAnimated:(BOOL)animated;
- (BOOL)closeRightViewAnimated:(BOOL)animated completion:(ViewDeckControllerBlock)completed;
- (BOOL)closeRightViewAnimated:(BOOL)animated duration:(NSTimeInterval)duration completion:(ViewDeckControllerBlock)completed;
- (BOOL)closeRightViewBouncing:(ViewDeckControllerBounceBlock)bounced;
- (BOOL)closeRightViewBouncing:(ViewDeckControllerBounceBlock)bounced completion:(ViewDeckControllerBlock)completed;

- (BOOL)toggleTopView;
- (BOOL)openTopView;
- (BOOL)closeTopView;
- (BOOL)toggleTopViewAnimated:(BOOL)animated;
- (BOOL)toggleTopViewAnimated:(BOOL)animated completion:(ViewDeckControllerBlock)completed;
- (BOOL)openTopViewAnimated:(BOOL)animated;
- (BOOL)openTopViewAnimated:(BOOL)animated completion:(ViewDeckControllerBlock)completed;
- (BOOL)openTopViewBouncing:(ViewDeckControllerBounceBlock)bounced;
- (BOOL)openTopViewBouncing:(ViewDeckControllerBounceBlock)bounced completion:(ViewDeckControllerBlock)completed;
- (BOOL)closeTopViewAnimated:(BOOL)animated;
- (BOOL)closeTopViewAnimated:(BOOL)animated completion:(ViewDeckControllerBlock)completed;
- (BOOL)closeTopViewAnimated:(BOOL)animated duration:(NSTimeInterval)duration completion:(ViewDeckControllerBlock)completed;
- (BOOL)closeTopViewBouncing:(ViewDeckControllerBounceBlock)bounced;
- (BOOL)closeTopViewBouncing:(ViewDeckControllerBounceBlock)bounced completion:(ViewDeckControllerBlock)completed;

- (BOOL)toggleBottomView;
- (BOOL)openBottomView;
- (BOOL)closeBottomView;
- (BOOL)toggleBottomViewAnimated:(BOOL)animated;
- (BOOL)toggleBottomViewAnimated:(BOOL)animated completion:(ViewDeckControllerBlock)completed;
- (BOOL)openBottomViewAnimated:(BOOL)animated;
- (BOOL)openBottomViewAnimated:(BOOL)animated completion:(ViewDeckControllerBlock)completed;
- (BOOL)openBottomViewBouncing:(ViewDeckControllerBounceBlock)bounced;
- (BOOL)openBottomViewBouncing:(ViewDeckControllerBounceBlock)bounced completion:(ViewDeckControllerBlock)completed;
- (BOOL)closeBottomViewAnimated:(BOOL)animated;
- (BOOL)closeBottomViewAnimated:(BOOL)animated completion:(ViewDeckControllerBlock)completed;
- (BOOL)closeBottomViewAnimated:(BOOL)animated duration:(NSTimeInterval)duration completion:(ViewDeckControllerBlock)completed;
- (BOOL)closeBottomViewBouncing:(ViewDeckControllerBounceBlock)bounced;
- (BOOL)closeBottomViewBouncing:(ViewDeckControllerBounceBlock)bounced completion:(ViewDeckControllerBlock)completed;

- (BOOL)toggleOpenView;
- (BOOL)toggleOpenViewAnimated:(BOOL)animated;
- (BOOL)toggleOpenViewAnimated:(BOOL)animated completion:(ViewDeckControllerBlock)completed;

- (BOOL)closeOpenView;
- (BOOL)closeOpenViewAnimated:(BOOL)animated;
- (BOOL)closeOpenViewAnimated:(BOOL)animated completion:(ViewDeckControllerBlock)completed;
- (BOOL)closeOpenViewAnimated:(BOOL)animated duration:(NSTimeInterval)duration completion:(ViewDeckControllerBlock)completed;
- (BOOL)closeOpenViewBouncing:(ViewDeckControllerBounceBlock)bounced;
- (BOOL)closeOpenViewBouncing:(ViewDeckControllerBounceBlock)bounced completion:(ViewDeckControllerBlock)completed;

- (BOOL)previewBounceView:(ViewDeckSide)viewDeckSide;
- (BOOL)previewBounceView:(ViewDeckSide)viewDeckSide withCompletion:(ViewDeckControllerBlock)completed;
- (BOOL)previewBounceView:(ViewDeckSide)viewDeckSide toDistance:(CGFloat)distance duration:(NSTimeInterval)duration callDelegate:(BOOL)callDelegate completion:(ViewDeckControllerBlock)completed;
- (BOOL)previewBounceView:(ViewDeckSide)viewDeckSide toDistance:(CGFloat)distance duration:(NSTimeInterval)duration numberOfBounces:(CGFloat)numberOfBounces dampingFactor:(CGFloat)zeta callDelegate:(BOOL)callDelegate completion:(ViewDeckControllerBlock)completed;

- (BOOL)canRightViewPushViewControllerOverCenterController;
- (void)rightViewPushViewControllerOverCenterController:(UIViewController*)controller;

- (BOOL)isSideClosed:(ViewDeckSide)viewDeckSide;
- (BOOL)isSideOpen:(ViewDeckSide)viewDeckSide;
- (BOOL)isAnySideOpen;

- (CGFloat)statusBarHeight;

- (ViewDeckSide)sideForController:(UIViewController*)controller;

@end


// Delegate protocol

@protocol ViewDeckControllerDelegate <NSObject>

@optional
- (BOOL)viewDeckController:(ViewDeckController*)viewDeckController shouldPan:(UIPanGestureRecognizer*)panGestureRecognizer;

- (void)viewDeckController:(ViewDeckController*)viewDeckController applyShadow:(CALayer*)shadowLayer withBounds:(CGRect)rect;

- (void)viewDeckController:(ViewDeckController*)viewDeckController didChangeOffset:(CGFloat)offset orientation:(ViewDeckOffsetOrientation)orientation panning:(BOOL)panning;
- (void)viewDeckController:(ViewDeckController *)viewDeckController didBounceViewSide:(ViewDeckSide)viewDeckSide openingController:(UIViewController*)openingController;
- (void)viewDeckController:(ViewDeckController *)viewDeckController didBounceViewSide:(ViewDeckSide)viewDeckSide closingController:(UIViewController*)closingController;

- (BOOL)viewDeckController:(ViewDeckController*)viewDeckController shouldOpenViewSide:(ViewDeckSide)viewDeckSide;
- (void)viewDeckController:(ViewDeckController*)viewDeckController willOpenViewSide:(ViewDeckSide)viewDeckSide animated:(BOOL)animated;
- (void)viewDeckController:(ViewDeckController*)viewDeckController didOpenViewSide:(ViewDeckSide)viewDeckSide animated:(BOOL)animated;
- (BOOL)viewDeckController:(ViewDeckController*)viewDeckController shouldCloseViewSide:(ViewDeckSide)viewDeckSide animated:(BOOL)animated;
- (void)viewDeckController:(ViewDeckController*)viewDeckController willCloseViewSide:(ViewDeckSide)viewDeckSide animated:(BOOL)animated;
- (void)viewDeckController:(ViewDeckController*)viewDeckController didCloseViewSide:(ViewDeckSide)viewDeckSide animated:(BOOL)animated;
- (void)viewDeckController:(ViewDeckController*)viewDeckController didShowCenterViewFromSide:(ViewDeckSide)viewDeckSide animated:(BOOL)animated;

- (BOOL)viewDeckController:(ViewDeckController *)viewDeckController shouldPreviewBounceViewSide:(ViewDeckSide)viewDeckSide;
- (void)viewDeckController:(ViewDeckController *)viewDeckController willPreviewBounceViewSide:(ViewDeckSide)viewDeckSide animated:(BOOL)animated;
- (void)viewDeckController:(ViewDeckController *)viewDeckController didPreviewBounceViewSide:(ViewDeckSide)viewDeckSide animated:(BOOL)animated;

- (CGFloat)viewDeckController:(ViewDeckController*)viewDeckController changesLedge:(CGFloat)ledge forSide:(ViewDeckSide)viewDeckSide;

@end


// category on UIViewController to provide access to the viewDeckController in the 
// contained viewcontrollers, a la UINavigationController.
@interface UIViewController (UIViewDeckItem) 

@property(nonatomic,readonly,retain) ViewDeckController *viewDeckController; 

@end


