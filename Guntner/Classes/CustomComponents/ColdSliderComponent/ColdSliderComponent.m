//
//  ColdSliderComponentController.m
//  Guntner
//
//  Created by Shiraz Omar on 8/7/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "ColdSliderComponent.h"

#import "Configuration.h"


@interface ColdSliderComponent () <UITableViewDelegate, UITableViewDataSource> {
    
    IBOutlet UIButton *okButton;
    
    IBOutlet UIView *contentView;
    
    IBOutlet UILabel *titleLabel;
    
    IBOutlet UITableView *pressureList;
    
}


@property (nonatomic, weak) IBOutlet UIButton *okButton;

@property (nonatomic, weak) IBOutlet UIView *contentView;

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

@property (nonatomic, weak) IBOutlet UITableView *pressureList;


- (IBAction)hideSelf;
- (IBAction)temperatureButtonPressed:(id)sender;


@end


@implementation ColdSliderComponent


@synthesize delegate = _delegate;

@synthesize unitList = _unitList;

@synthesize okButton = _okButton;

@synthesize titleLabel = _titleLabel;

@synthesize contentView = _contentView;

@synthesize tempButton1 = _tempButton1;
@synthesize tempButton2 = _tempButton2;

@synthesize pressureList = _pressureList;


#pragma mark Setup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        // Custom initialization
        
    }
    
    return self;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    // Round ContentView's Corners
    [self roundViewsCorners:self.contentView];
    
    
    // Setup Buttons
    [self setupUI];
    
    
    // Setup PressureList
    [self setupPressureList];

    
}


- (void)viewWillAppear:(BOOL)animated {
    
    // Set bar As Selected Cell If Nothing Is Selected
    if (self.pressureList.indexPathForSelectedRow.row == 0) {
        [self.pressureList selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    
}


- (void)setupUI {
    
    // Update Title For Localization
    self.titleLabel.text = kColdSliderComponentButtonTitle;
    
    
    // Set Button Background Image
    [self.okButton setBackgroundImage:[[UIImage imageNamed:kBlueButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    [self.tempButton1 setBackgroundImage:[[UIImage imageNamed:kBlueButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    [self.tempButton2 setBackgroundImage:[[UIImage imageNamed:kBlueButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    
    
    // Update Okay Button Title For Localization
    [self.okButton setTitle:kColdSliderRefrigerantSelectorOkButtonTitle forState:UIControlStateNormal];
    
    
    // Set Image Insets To Display It On Right Side Of Title
    self.tempButton1.imageEdgeInsets = UIEdgeInsetsMake(0.0, 80.0, 0.0, 20.0);
    self.tempButton2.imageEdgeInsets = UIEdgeInsetsMake(0.0, 80.0, 0.0, 20.0);
    
    
    // Setup Buttons If They Have Not Already Been Setup
    if (!tempButton1.selected || !tempButton2.selected) {
        [self.tempButton1 setSelected:YES];
        [self.tempButton2 setSelected:NO];
    }
    
}


- (void)roundViewsCorners:(UIView *)view {
    
    // Set View's Corner Radius To 5px To Round The Corners
    view.layer.cornerRadius = 5.0;
    view.layer.masksToBounds = YES;
    
}


#pragma mark Temperature Button Methods

- (IBAction)temperatureButtonPressed:(id)sender {
    
    // Set Clicked Button As Selected
    if (sender == self.tempButton1) {
        [self.tempButton1 setSelected:YES];
        [self.tempButton2 setSelected:NO];
    } else {
        [self.tempButton1 setSelected:NO];
        [self.tempButton2 setSelected:YES];
    }
    
    
    // Update Parents TextField With New Suffix
    [self updateDelegateTextFieldWithNewSuffix];
    
}


#pragma mark Public Button Getters

- (NSString *)selectedTemperatureUnit {
    
    if (self.tempButton1.selected) {
        return [self.tempButton1 titleForState:UIControlStateNormal];
    } else {
        return [self.tempButton2 titleForState:UIControlStateNormal];
    }
    
}


#pragma mark Public Button Setters

- (void)updateSelectedTemperature:(NSString *)selectedTemperature {
    
    if ([[self.tempButton1 titleForState:UIControlStateNormal] isEqualToString:selectedTemperature]) {
        self.tempButton1.selected = YES;
        self.tempButton2.selected = NO;
    } else {
        self.tempButton1.selected = NO;
        self.tempButton2.selected = YES;
    }
    
}


#pragma mark Delegate Methods

- (IBAction)hideSelf {
    
    if ([self.delegate respondsToSelector:@selector(showHideColdSliderComponent)]) {
        [self.delegate showHideColdSliderComponent];//
    }
    
}

- (void)updateDelegateTextFieldWithNewSuffix {
    
    if([self.delegate respondsToSelector:@selector(updateTextFieldWithNewSuffix)]) {
		[self.delegate updateTextFieldWithNewSuffix];
	}
    
}

- (void)updateDelegatePressureUnit:(NSString *)pressureUnit {
    
    if([self.delegate respondsToSelector:@selector(updatePressureUnit:)]) {
		[self.delegate updatePressureUnit:pressureUnit];
	}
    
}


#pragma mark PressureList Methods

- (void)setupPressureList {
    
    // Setup Pressure List
    self.pressureList.delegate = self;
    self.pressureList.dataSource = self;
    self.pressureList.allowsSelection = YES;
    
}


#pragma mark PressureList Delegate Methods

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Retrive Appropriate Cell
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    
    // Set Cell As Selected
    [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    
    
    // Update Pressure Unit
    [self updateDelegatePressureUnit:cell.textLabel.text];
    
    // Update TextField Suffix To Display New Unit
    [self updateDelegateTextFieldWithNewSuffix];
    
    
    // Release Objects
    cell = nil;
    
}


#pragma mark PressureList DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Data Source Count
    return [self.unitList count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        // Configure Cell Text Label
        cell.textLabel.adjustsFontSizeToFitWidth = YES;
        cell.textLabel.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.highlightedTextColor = [UIColor blackColor];
        cell.textLabel.font = kColdSliderComponentPressureListCellFont;
        
        // Setup Selected Background View
        UIView *backgroundView = [[UIView alloc] initWithFrame:cell.frame];
        backgroundView.backgroundColor = kGuntnerColorCBDDEE;
        cell.selectedBackgroundView = backgroundView;
        backgroundView = nil;
        
        // Setup Custom Seperator For Cell
        UIView *seperator = [[UIView alloc] initWithFrame:CGRectMake(kColdSliderRefrigerantSelectorListSeperatorPadding, (tableView.rowHeight - kColdSliderRefrigerantSelectorListSeperatorHeight), (tableView.bounds.size.width - kColdSliderRefrigerantSelectorListSeperatorPadding), kColdSliderRefrigerantSelectorListSeperatorHeight)];
        seperator.backgroundColor = kGuntnerColorEEEEEE;
        seperator.tag = kColdSliderRefrigerantSelectorListSeperatorTag;
        [cell.contentView addSubview:seperator];
        seperator = nil;
        
    }
    
    
    // Cell Customization
    
    // Set Cell Text
    cell.textLabel.text = [NSString stringWithFormat:@"%@", [self.unitList objectAtIndex:indexPath.row]];
    
    
    // Hide Seperator For Last Cell
    if (indexPath.row == ([self.unitList count] - 1)) {
        
        for (UIView *seperator in [cell.contentView subviews]) {
            if (seperator.tag == kColdSliderRefrigerantSelectorListSeperatorTag) {
                seperator.hidden = YES;
            }
        }
    }
    
    
    
    return cell;
    
}


#pragma mark Cleanup

- (void)dealloc {

    self.unitList = nil;
    
}


@end
