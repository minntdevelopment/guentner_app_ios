//
//  ColdSliderComponentController.h
//  Guntner
//
//  Created by Shiraz Omar on 8/7/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


@protocol ColdSliderComponentDelegate;


@interface ColdSliderComponent : UIViewController {
    
    id <ColdSliderComponentDelegate> delegate;
    
    NSArray *unitList;
    
    IBOutlet UIButton *tempButton1;
    IBOutlet UIButton *tempButton2;
    
}


@property (nonatomic, weak) id <ColdSliderComponentDelegate> delegate;

@property (nonatomic, strong) NSArray *unitList;

@property (nonatomic, weak) IBOutlet UIButton *tempButton1;
@property (nonatomic, weak) IBOutlet UIButton *tempButton2;


- (NSString *)selectedTemperatureUnit;
- (void)updateSelectedTemperature:(NSString *)selectedTemperature;


@end



@protocol ColdSliderComponentDelegate <NSObject>


- (NSString *)selectedPressureUnit;
- (void)showHideColdSliderComponent;
- (void)updateTextFieldWithNewSuffix;
- (void)updatePressureUnit:(NSString *)unitName;


@end