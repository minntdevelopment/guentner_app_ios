//
//  PopupListViewController.h
//  Guntner
//
//  Created by Shiraz Omar on 8/31/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


@protocol PopupListDelegate;


@interface PopupListViewController : UIViewController {
    
    id <PopupListDelegate> delegate;
    
    NSArray *listData;
    IBOutlet UILabel *titleLabel;
    
}


@property (nonatomic, weak) id <PopupListDelegate> delegate;

@property (nonatomic, strong) NSArray *listData;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;


- (id)initWithTitle:(NSString *)title andListData:(NSArray *)data;


@end


@protocol PopupListDelegate <NSObject>


- (void)dismissPopupListWithSelectedItem:(NSString *)selectedItem;


@end
