//
//  PopupListViewController.m
//  Guntner
//
//  Created by Shiraz Omar on 8/31/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "PopupListViewController.h"
#import "Configuration.h"


@interface PopupListViewController () <UITableViewDelegate, UITableViewDataSource> {
    
    NSString *titleString;
    
    IBOutlet UIView *contentView;
    IBOutlet UITableView *listDisplay;
    
}

@property (nonatomic, weak) IBOutlet UIView *contentView;
@property (nonatomic, weak) IBOutlet UITableView *listDisplay;


- (IBAction)dismissPopupList;

@end


@implementation PopupListViewController


@synthesize delegate = _delegate;

@synthesize listData = _listData;
@synthesize titleLabel = _titleLabel;
@synthesize contentView = _contentView;
@synthesize listDisplay = _listDisplay;


#pragma mark Setup

- (id)initWithTitle:(NSString *)title andListData:(NSArray *)data {
    
    self = [super initWithNibName:kPopupListViewControllerXibName bundle:nil];
    if (self) {
        
        // Set Title
        titleString = title;
        
        // Set ListData
        self.listData = data;
    }
    
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    // Round ContentView's Corners
    [self roundViewsCorners:self.contentView];
    
    
    // Setup ListDisplay
    [self setupListDisplay];
    
    
    // Update TitleLabel With Title
    self.titleLabel.text = titleString;
}

- (void)roundViewsCorners:(UIView *)view {
    
    // Set View's Corner Radius To 5px To Round The Corners
    view.layer.cornerRadius = 5.0;
    view.layer.masksToBounds = YES;
    
    // Set List Display Layer Mask So It Doesn't Override ContentView's Rounded Corners
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:self.listDisplay.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:(CGSize){5.0, 5.0}].CGPath;
    self.listDisplay.layer.mask = maskLayer;
    
}


#pragma mark Title Setter

- (void)setPopupListTitle:(NSNotification *)notification {
    
    // Get Title From Notification and Set It To Title Label
    self.titleLabel.text = [notification object];
    
}


#pragma mark Dismiss Methods

- (IBAction)dismissPopupList {
    
    if ([self.delegate respondsToSelector:@selector(dismissPopupListWithSelectedItem:)]) {
        [UIView animateWithDuration:0.3 animations:^{
            self.view.alpha = 0.0;  // Animate Fade By Setting Alpha Over Time
        } completion: ^(BOOL finished) {
            // Call Delegate Method
            [self.delegate dismissPopupListWithSelectedItem:nil];
        }];
    }
    
}


#pragma mark Delegate Methods

- (void)dismissPopupListWithSelectedItem:(NSString *)selectedItem {
    
    if ([self.delegate respondsToSelector:@selector(dismissPopupListWithSelectedItem:)]) {
        [UIView animateWithDuration:0.3 animations:^{
            self.view.alpha = 0.0;  // Animate Fade By Setting Alpha Over Time
        } completion: ^(BOOL finished) {
            // Call Delegate Method
            [self.delegate dismissPopupListWithSelectedItem:[[self.listDisplay cellForRowAtIndexPath:[self.listDisplay indexPathForSelectedRow]] textLabel].text];
        }];
    }
}



#pragma mark List Display Methods

- (void)setupListDisplay {
    
    // Setup List Display
    self.listDisplay.delegate = self;
    self.listDisplay.dataSource = self;
    self.listDisplay.allowsSelection = YES;
    
}


#pragma mark RefrigerantList Delegate Methods

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Retrive Appropriate Cell
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    // Set Cell As Selected
    [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    
    
    // Dismiss Self With Selected Item
     [self dismissPopupListWithSelectedItem:cell.textLabel.text];
    
    
    // Release Objects
    cell = nil;
    
}


#pragma mark RefrigerantList DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Data Source Count
    return [self.listData count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        // Configure Cell Text Label
        cell.textLabel.adjustsFontSizeToFitWidth = YES;
        cell.textLabel.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.highlightedTextColor = [UIColor blackColor];
        cell.textLabel.font = kColdSliderComponentRefrigerantListCellFont;
        
        // Setup Selected Background View
        UIView *backgroundView = [[UIView alloc] initWithFrame:cell.frame];
        backgroundView.backgroundColor = kGuntnerColorCBDDEE;
        cell.selectedBackgroundView = backgroundView;
        backgroundView = nil;
        
        // Setup Custom Seperator For Cell
        UIView *seperator = [[UIView alloc] initWithFrame:CGRectMake(kColdSliderRefrigerantSelectorListSeperatorPadding, (tableView.rowHeight - kColdSliderRefrigerantSelectorListSeperatorHeight), (tableView.bounds.size.width - kColdSliderRefrigerantSelectorListSeperatorPadding), kColdSliderRefrigerantSelectorListSeperatorHeight)];
        seperator.backgroundColor = kGuntnerColorEEEEEE;
        [cell.contentView addSubview:seperator];
        seperator = nil;
        
    }

    // Cell Customization
    
    // Set Cell Text
    cell.textLabel.text = [NSString stringWithFormat:@"%@", [self.listData objectAtIndex:indexPath.row]];
    
    
    return cell;
    
}


#pragma mark Cleanup

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    
    self.listData = nil;
    
}


@end
