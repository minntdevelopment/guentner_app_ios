//
//  HorizontalPickerViewProtocol.h
//  Guntner
//
//  Created by Shiraz Omar on 8/7/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

@class HorizontalPickerView;

// ------------------------------------------------------------------
// HorizontalPickerView DataSource Protocol
@protocol HorizontalPickerViewDataSource <NSObject>
@required
// data source is responsible for reporting how many elements there are
- (NSInteger)numberOfElementsInHorizontalPickerView:(HorizontalPickerView *)picker;
@end


// ------------------------------------------------------------------
// HorizontalPickerView Delegate Protocol
@protocol HorizontalPickerViewDelegate <NSObject>

@optional
// delegate callback to notify delegate selected element has changed
- (void)horizontalPickerView:(HorizontalPickerView *)picker didSelectElementAtIndex:(NSInteger)index;

// one of these two methods must be defined
- (NSString *)horizontalPickerView:(HorizontalPickerView *)picker titleForElementAtIndex:(NSInteger)index;
- (UIView *)horizontalPickerView:(HorizontalPickerView *)picker viewForElementAtIndex:(NSInteger)index;
// any view returned from this must confirm to the HorizontalPickerElementState protocol

@required
// delegate is responsible for reporting the size of each element
- (NSInteger)horizontalPickerView:(HorizontalPickerView *)picker widthForElementAtIndex:(NSInteger)index;

@end

// ------------------------------------------------------------------
// HorizontalPickerElementState Protocol
@protocol HorizontalPickerElementState <NSObject>
@required
// element views should know how display themselves based on selected status
- (void)setSelectedElement:(BOOL)selected;
@end