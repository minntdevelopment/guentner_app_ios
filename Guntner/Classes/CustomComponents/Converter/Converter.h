//
//  Converter.h
//  Guntner
//
//  Created by Shiraz Omar on 8/14/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Converter : NSObject {
    
    //
    
}


- (NSArray *)unitList;
- (NSArray *)conversionListForUnit:(NSString *)unit;
- (double)convert:(double)input unit:(NSString *)unit from:(NSString *)from to:(NSString *)to;


@end
