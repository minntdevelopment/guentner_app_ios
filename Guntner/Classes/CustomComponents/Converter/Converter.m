//
//  Converter.m
//  Guntner
//
//  Created by Shiraz Omar on 8/14/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "Converter.h"

#import "Configuration.h"


@interface Converter () {
    
    NSDictionary *unitData;
    
}

@property (nonatomic, strong) NSDictionary *unitData;

@end


@implementation Converter


@synthesize unitData = _unitData;


#pragma mark Setup

- (id)init {
    
    self = [super init];
    
    if (self) {
        
        // Load Data From Disk
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:kConverterDataFile ofType:kConverterDataFileType];
        NSLog(@"plist Path : %@",plistPath);
        self.unitData = [NSDictionary dictionaryWithContentsOfFile:plistPath];
        
        NSMutableDictionary *tempDict = [NSMutableDictionary dictionary];
        for (NSString *key in self.unitData) {
            [tempDict setObject:[self.unitData objectForKey:key] forKey:[key substringFromIndex:3]];
        }
        self.unitData = [tempDict copy];
        tempDict = nil;
    }
    
    return self;
    
}


#pragma mark Public Getters

- (NSArray *)unitList {
    
    // Set UnitList With Ordered Array After Removing Ordering String
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:kConverterDataFile ofType:kConverterDataFileType];
    NSArray *orderedDataArray = [[[NSDictionary dictionaryWithContentsOfFile:plistPath] allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSMutableArray *tempArray = [NSMutableArray array];
    for (int i=0; i<orderedDataArray.count; i++) {
        [tempArray insertObject:[[orderedDataArray objectAtIndex:i] substringFromIndex:3] atIndex:i];
    }
    orderedDataArray = nil;
    plistPath = nil;
    
    return [tempArray copy];
    
}

- (NSArray *)conversionListForUnit:(NSString *)unit {
    
    NSMutableArray *allKeysForUnit = [NSMutableArray arrayWithArray:[[self.unitData valueForKey:unit] allKeys]];
    [allKeysForUnit removeObject:kConverterSI_UnitKey];
    return [allKeysForUnit sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
}


#pragma mark Convert Method

- (double)convert:(double)input unit:(NSString *)unit from:(NSString *)from to:(NSString *)to {
    
    // Retun Input If To and From Are The Same
    if ([to isEqualToString:from]) {
        
        return input;
        
    } else {
        
        double result = 0.0;
        
        @autoreleasepool {
            NSDictionary *tempConversionData = [self.unitData valueForKey:unit];
            
            if ([[tempConversionData allKeys] containsObject:from] && [[tempConversionData allKeys] containsObject:to]) {
                
                // Get Conversion Values From Dictionary
                NSString *toString = [tempConversionData valueForKey:to];
                NSString *fromString = [tempConversionData valueForKey:from];
                
                if (([toString rangeOfString:@"x" options:NSCaseInsensitiveSearch].location == NSNotFound) || ([fromString rangeOfString:@"x" options:NSCaseInsensitiveSearch].location == NSNotFound)) {
                    
                    // Simple Expression So Doesn't Contain Any Arguments
                    
                    // Evaluate String Using NSExpression
                    NSExpression *toMathEvaluator = [NSExpression expressionWithFormat:toString];
                    NSExpression *fromMathEvaluator = [NSExpression expressionWithFormat:fromString];
                    double SItoTo = [[toMathEvaluator expressionValueWithObject:nil context:nil] doubleValue];
                    double fromToSI = [[fromMathEvaluator expressionValueWithObject:nil context:nil] doubleValue];
                    toMathEvaluator = nil;
                    fromMathEvaluator = nil;
                    
                    // Convert input-value to to-value
                    result = ((input * fromToSI) / SItoTo);
                    
                } else {
                    
                    // Expression Contains Arguments
                    
                    // If from Not SI Unit Inverse The Equation
                    if (![from isEqualToString:[tempConversionData valueForKey:kConverterSI_UnitKey]]) {
                        
                        // Special Case For ℉ Since The Inverse Can Not Be Acieved By Inversing Just The Operators
                        if ([fromString isEqualToString:@"((x * (9.0 / 5.0)) + 32.0)"]) {
                            
                            fromString = @"((x - 32.0) * (5.0 / 9.0))";
                            
                        } else {
                            fromString = [fromString stringByReplacingOccurrencesOfString:@"+" withString:@"##"];
                            fromString = [fromString stringByReplacingOccurrencesOfString:@"-" withString:@"+"];
                            fromString = [fromString stringByReplacingOccurrencesOfString:@"##" withString:@"-"];
                            fromString = [fromString stringByReplacingOccurrencesOfString:@"*" withString:@"##"];
                            fromString = [fromString stringByReplacingOccurrencesOfString:@"/" withString:@"*"];
                            fromString = [fromString stringByReplacingOccurrencesOfString:@"##" withString:@"/"];
                        }
                    }
                    
                    // Replace Variable with Value Before Evaluating String Using NSExpression
                    fromString = [fromString stringByReplacingOccurrencesOfString:@"x" withString:[NSString stringWithFormat:@"%f", input]];
                    NSExpression *fromToSIMathEvaluator = [NSExpression expressionWithFormat:fromString];
                    double fromToSI = [[fromToSIMathEvaluator expressionValueWithObject:nil context:nil] doubleValue];
                    fromToSIMathEvaluator = nil;
                    
                    toString = [toString stringByReplacingOccurrencesOfString:@"x" withString:[NSString stringWithFormat:@"%f", fromToSI]];
                    NSExpression *SItoToMathEvaluator = [NSExpression expressionWithFormat:toString];
                    result = [[SItoToMathEvaluator expressionValueWithObject:nil context:nil] doubleValue];
                    SItoToMathEvaluator = nil;
                }
                
                toString = nil;
                fromString = nil;
            }
            
            tempConversionData = nil;
        }
        
        return result;
        
    }
    
}


#pragma mark Cleanup

- (void)dealloc {
    
    self.unitData = nil;
    
}


@end
