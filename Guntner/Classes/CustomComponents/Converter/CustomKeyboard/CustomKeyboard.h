//
//  CustomKeyboard.h
//  Guntner
//
//  Created by Shiraz Omar on 9/10/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol CustomKeyboardDelegate;


@interface CustomKeyboard : UIView {
    
    id <CustomKeyboardDelegate> delegate;
    
    IBOutlet UIView *blackBorder;
    
    UITextField *targetTextField;
    
}


@property (nonatomic, weak) id <CustomKeyboardDelegate> delegate;

@property (nonatomic, weak) IBOutlet UIView *blackBorder;

@property (nonatomic, weak) UITextField *targetTextField;


@end


@protocol CustomKeyboardDelegate <NSObject>


@optional
- (void)dismissCustomKeyboard;
- (void)customKeyboardDidUpdateTextField;


@end
