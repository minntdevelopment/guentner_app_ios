//
//  CustomKeyboard.m
//  Guntner
//
//  Created by Shiraz Omar on 9/10/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "CustomKeyboard.h"

#import "Configuration.h"


@interface CustomKeyboard () <UIInputViewAudioFeedback> {
    
    IBOutlet UIButton *numPadButton1;
    IBOutlet UIButton *numPadButton2;
    IBOutlet UIButton *numPadButton3;
    IBOutlet UIButton *numPadButton4;
    IBOutlet UIButton *numPadButton5;
    IBOutlet UIButton *numPadButton6;
    IBOutlet UIButton *numPadButton7;
    IBOutlet UIButton *numPadButton8;
    IBOutlet UIButton *numPadButton9;
    IBOutlet UIButton *numPadButton0;
    IBOutlet UIButton *numPadButtonMinus;
    IBOutlet UIButton *numPadButtonDelete;
    IBOutlet UIButton *numPadButtonDecimal;
    
}


@property (nonatomic,assign) id <UITextInput> inputDelegate;

@property (nonatomic, weak) IBOutlet UIButton *numPadButton1;
@property (nonatomic, weak) IBOutlet UIButton *numPadButton2;
@property (nonatomic, weak) IBOutlet UIButton *numPadButton3;
@property (nonatomic, weak) IBOutlet UIButton *numPadButton4;
@property (nonatomic, weak) IBOutlet UIButton *numPadButton5;
@property (nonatomic, weak) IBOutlet UIButton *numPadButton6;
@property (nonatomic, weak) IBOutlet UIButton *numPadButton7;
@property (nonatomic, weak) IBOutlet UIButton *numPadButton8;
@property (nonatomic, weak) IBOutlet UIButton *numPadButton9;
@property (nonatomic, weak) IBOutlet UIButton *numPadButton0;
@property (nonatomic, weak) IBOutlet UIButton *numPadButtonMinus;
@property (nonatomic, weak) IBOutlet UIButton *numPadButtonDelete;
@property (nonatomic, weak) IBOutlet UIButton *numPadButtonDecimal;


@end


@implementation CustomKeyboard


@synthesize delegate = _delegate;

@synthesize blackBorder = _blackBorder;

@synthesize targetTextField = _targetTextField;

@synthesize numPadButton1 = _numPadButton1;
@synthesize numPadButton2 = _numPadButton2;
@synthesize numPadButton3 = _numPadButton3;
@synthesize numPadButton4 = _numPadButton4;
@synthesize numPadButton5 = _numPadButton5;
@synthesize numPadButton6 = _numPadButton6;
@synthesize numPadButton7 = _numPadButton7;
@synthesize numPadButton8 = _numPadButton8;
@synthesize numPadButton9 = _numPadButton9;
@synthesize numPadButton0 = _numPadButton0;
@synthesize numPadButtonMinus = _numPadButtonMinus;
@synthesize numPadButtonDelete = _numPadButtonDelete;
@synthesize numPadButtonDecimal = _numPadButtonDecimal;


#pragma mark Setup


- (id)init{
    
	//UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
	CGRect frame;
    
	//if(UIDeviceOrientationIsLandscape(orientation)) {
    if (orientation != UIInterfaceOrientationPortrait) {
    
        frame = CGRectMake(0, 0, [[[UIApplication sharedApplication] delegate] window].bounds.size.height, 188);
    } else {
        if (IS_IPHONE_5) {
            frame = CGRectMake(0, 0, [[[UIApplication sharedApplication] delegate] window].bounds.size.width, 288);
        } else {
            frame = CGRectMake(0, 0, [[[UIApplication sharedApplication] delegate] window].bounds.size.width, 228);
        }
    }
	
	self = [super initWithFrame:frame];
	
	if (self) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:kCustomKeyboardXibName owner:self options:nil];
		[[nib objectAtIndex:0] setFrame:frame];
        self = [nib objectAtIndex:0];
        
        
        // Setup Custom Buttons
        [self setupUI];
    }
	
    
	return self;
}

#pragma mark Custom Button Methods

- (void)setupUI {
    
    // Setup Custom Button Background Images
    [self.numPadButton1 setBackgroundImage:[[UIImage imageNamed:kConverterNumpadGreyButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    [self.numPadButton2 setBackgroundImage:[[UIImage imageNamed:kConverterNumpadGreyButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    [self.numPadButton3 setBackgroundImage:[[UIImage imageNamed:kConverterNumpadGreyButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    [self.numPadButton4 setBackgroundImage:[[UIImage imageNamed:kConverterNumpadGreyButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    [self.numPadButton5 setBackgroundImage:[[UIImage imageNamed:kConverterNumpadGreyButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    [self.numPadButton6 setBackgroundImage:[[UIImage imageNamed:kConverterNumpadGreyButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    [self.numPadButton7 setBackgroundImage:[[UIImage imageNamed:kConverterNumpadGreyButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    [self.numPadButton8 setBackgroundImage:[[UIImage imageNamed:kConverterNumpadGreyButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    [self.numPadButton9 setBackgroundImage:[[UIImage imageNamed:kConverterNumpadGreyButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    [self.numPadButton0 setBackgroundImage:[[UIImage imageNamed:kConverterNumpadGreyButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    [self.numPadButtonMinus setBackgroundImage:[[UIImage imageNamed:kConverterNumpadDarkGreyButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    [self.numPadButtonDelete setBackgroundImage:[[UIImage imageNamed:kConverterNumpadDarkGreyButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    [self.numPadButtonDecimal setBackgroundImage:[[UIImage imageNamed:kConverterNumpadDarkGreyButtonImage] resizableImageWithCapInsets:kImageSizingEdgeInsets] forState:UIControlStateNormal];
    
    
    // Update Period Button Label For Appropriate Localization
    if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqual:kGermanLocale]) {
        [self.numPadButtonDecimal setTitle:kGermanDecimalSeperator forState:UIControlStateNormal];
    } else {
        [self.numPadButtonDecimal setTitle:kEnglishDecimalSeperator forState:UIControlStateNormal];
    }
    
    
    // Add Gesture Recognizer To Handle InputTextField Clear When Holding Delete Button
    if ([[self.numPadButtonDelete gestureRecognizers] count] == 0) {
        UILongPressGestureRecognizer *holdToClearGR = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(holdToClearGRHandler)];
        holdToClearGR.minimumPressDuration = 1.0;
        holdToClearGR.numberOfTouchesRequired = 1;
        [self.numPadButtonDelete addGestureRecognizer:holdToClearGR];
        holdToClearGR = nil;
    }
    
}

- (id <UITextInput>)inputDelegate {
    return self.targetTextField;
}

- (IBAction)buttonPressed:(UIButton *)numPadButton {
    
    // Get Button Title
    NSString *buttonTitle = [numPadButton titleForState:UIControlStateNormal];
    
    // Check If Title Is Blank - If So Consider It As Delete Button
    if (buttonTitle.length == 0) {
        
        // Remove Last Character From TextField
        // Send Delegate Delete Callback
        if ([self.inputDelegate conformsToProtocol:@protocol(UITextInput)]) {
            [self.inputDelegate deleteBackward];
        } else {
            if (self.targetTextField.text.length > 0) {
                self.targetTextField.text = [self.targetTextField.text stringByReplacingCharactersInRange:NSMakeRange((self.targetTextField.text.length - 1), 1) withString:@""];
            }
        }
        
    } else {
        
        // Make Sure User Doesn't Exceed Max Length
        if (self.targetTextField.text.length < kMaxCalculationInputLength) {
            
            // Don't Allow ./, If It Is First Character
            if ([buttonTitle isEqualToString:kEnglishDecimalSeperator] || [buttonTitle isEqualToString:kGermanDecimalSeperator]) {
                
                if ((self.targetTextField.text.length == 0) || ([self.targetTextField.text rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:kAllLocaleDecimalSeperatorCharacterSet]].location != NSNotFound)) {
                    
                    // Ignore
                    
                } else {
                    
                    // Insert Button Title To Textfield Text
                    [self.inputDelegate insertText:buttonTitle];
                    
                }
                
            } else if ([buttonTitle isEqualToString:kMinusSign]) {
                
                // Dont Allow User To Enter Minus Twice
                if ((self.targetTextField.text.length != 0) || ([self.targetTextField.text rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:kMinusSign]].location != NSNotFound)) {
                    
                    // Ignore
                    
                } else {
                    
                    // Insert Button Title To Textfield Text
                    [self.inputDelegate insertText:buttonTitle];
                    
                }
                
            } else {
                
                // Insert Button Title To Textfield Text
                [self.inputDelegate insertText:buttonTitle];
                
            }
            
        }
        
    }
    
    buttonTitle = nil;
    
    
    // Play Input Click
    [[UIDevice currentDevice] playInputClick];
    
    
    // Notify Delegate That TextField Has Been Updated
    if ([self.delegate respondsToSelector:@selector(customKeyboardDidUpdateTextField)]) {
        [self.delegate customKeyboardDidUpdateTextField];
    }
    
}

- (void)holdToClearGRHandler {
    
    // Clear Input Field
    self.targetTextField.text = @"";
    
    
    // Notify Delegate That TextField Has Been Updated
    if ([self.delegate respondsToSelector:@selector(customKeyboardDidUpdateTextField)]) {
        [self.delegate customKeyboardDidUpdateTextField];
    }
    
}

// Apple Method To Enable Input Clicks
- (BOOL)enableInputClicksWhenVisible {
    return YES;
}


#pragma mark Cleanup

- (void)dealloc {
    
    //
    
}


@end
