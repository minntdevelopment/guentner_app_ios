//
//  ColdSliderScale.h
//  Guntner
//
//  Created by Shiraz Omar on 8/22/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


@interface ColdSliderScale : UIView


- (UIImage *)coldSliderScaleImage;
- (id)initWithScaleData:(NSArray *)data tempUnit:(NSString *)temperatureUnit pressureUnit:(NSString *)pressureUnit refrigerant:(NSString *)refrigerant orientation:(UIInterfaceOrientation)orentation andFrame:(CGRect)frame;


@end
