//
//  ColdSliderScale.m
//  Guntner
//
//  Created by Shiraz Omar on 8/22/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "ColdSliderScale.h"


#define kGermanLocale @"de"
#define kGermanDecimalSeperator @","
#define kGermanGroupingSeperator @"."
#define kEnglishDecimalSeperator @"."
#define kEnglishGroupingSeperator @","

#define kScaleTypePressure @"Pressure"
#define kScaleTypeTemperature @"Temperature"

#define kLineWidth (kColdSliderScaleFrame.size.width / 320.0)

#define kEdgePadding 0.0
#define kMajorTickInterval 5
#define kMajorBigTickInterval 10
#define kCenterPadding (kColdSliderScaleFrame.size.height/40.0)
#define kMinorTickLength (kColdSliderScaleFrame.size.height/24.0)
#define kMajorTickLength (kColdSliderScaleFrame.size.height/12.0)
#define kMajorBigTickLength (kColdSliderScaleFrame.size.height/8.0)
#define kMaxSignificantDigits 4

#define kTempColor [UIColor redColor]
#define kPressureColor [UIColor blueColor]
#define kRefrigerantLabelColor [UIColor blackColor]

#define kAnnotationFontSize (kUnitLabelFontSize * 0.75)

#define kColdSliderScaleFrame CGRectMake(0, 0, 1280, 640)

#define kUnitLabelMultiplicationFactorP 0.0
#define kUnitLabelMultiplicationFactorL 0.6
#define kUnitLabelWidth (kColdSliderScaleFrame.size.width/4.0)
#define kUnitLabelHeight (kColdSliderScaleFrame.size.height/4.0)
#define kUnitLabelFontSize (kColdSliderScaleFrame.size.height/10.0)
#define kRefrigerantLabelFontSize (kColdSliderScaleFrame.size.height/9.0)


@interface ColdSliderScale () {
    
    NSArray *scaleData;
    
    UILabel *tempUnitLabel;
    UILabel *tempUnitLabel2;
    UILabel *pressureUnitLabel;
    UILabel *pressureUnitLabel2;
    UILabel *selectedRefrigerantLabel;
    
    NSNumberFormatter *numberFormatter;
    
}


@property (nonatomic, strong) NSArray *scaleData;

@property (nonatomic, strong) UILabel *tempUnitLabel;
@property (nonatomic, strong) UILabel *tempUnitLabel2;
@property (nonatomic, strong) UILabel *pressureUnitLabel;
@property (nonatomic, strong) UILabel *pressureUnitLabel2;
@property (nonatomic, strong) UILabel *selectedRefrigerantLabel;

@property (nonatomic, strong) NSNumberFormatter *numberFormatter;


@end


@implementation ColdSliderScale


@synthesize scaleData = _scaleData;

@synthesize tempUnitLabel = _tempUnitLabel;
@synthesize tempUnitLabel2 = _tempUnitLabel2;
@synthesize pressureUnitLabel = _pressureUnitLabel;
@synthesize pressureUnitLabel2 = _pressureUnitLabel2;
@synthesize selectedRefrigerantLabel = _selectedRefrigerantLabel;

@synthesize numberFormatter = _numberFormatter;


#pragma mark Setup

- (id)initWithScaleData:(NSArray *)data tempUnit:(NSString *)temperatureUnit pressureUnit:(NSString *)pressureUnit refrigerant:(NSString *)refrigerant orientation:(UIInterfaceOrientation)orentation andFrame:(CGRect)frame
{
    
    if (CGRectEqualToRect(frame, CGRectZero)) {
        frame = kColdSliderScaleFrame;
    }
    
    self = [super initWithFrame:frame];
    if (self) {
        
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        
        // Store data So It Can Be Used Later
        self.scaleData = data;
        
        // Set Label Multiplaction Factor For Orientation
        CGFloat unitLabelMultiplicationFactor;
        if ((orentation == UIInterfaceOrientationLandscapeLeft) || (orentation == UIInterfaceOrientationLandscapeRight)) {
            unitLabelMultiplicationFactor = kUnitLabelMultiplicationFactorL;
        } else {
            unitLabelMultiplicationFactor = kUnitLabelMultiplicationFactorP;
        }
        
        // Add Temp and Pressure Unit Labels
        self.tempUnitLabel = [[UILabel alloc] initWithFrame:CGRectMake(kEdgePadding, (self.bounds.size.height - (kUnitLabelHeight * (1.0 + unitLabelMultiplicationFactor))), kUnitLabelWidth, kUnitLabelHeight)];
        self.tempUnitLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:kUnitLabelFontSize];
        self.tempUnitLabel.backgroundColor = [UIColor clearColor];
        self.tempUnitLabel.textAlignment = NSTextAlignmentLeft;
        self.tempUnitLabel.adjustsFontSizeToFitWidth = YES;
        self.tempUnitLabel.minimumScaleFactor = 0.2;
        self.tempUnitLabel.textColor = kTempColor;
        self.tempUnitLabel.text = temperatureUnit;
        [self addSubview:self.tempUnitLabel];
        
        self.tempUnitLabel2 = [[UILabel alloc] initWithFrame:CGRectMake((self.bounds.size.width - (kUnitLabelWidth + kEdgePadding)), (self.bounds.size.height - (kUnitLabelHeight * (1.0 + unitLabelMultiplicationFactor))), kUnitLabelWidth, kUnitLabelHeight)];
        self.tempUnitLabel2.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:kUnitLabelFontSize];
        self.tempUnitLabel2.backgroundColor = [UIColor clearColor];
        self.tempUnitLabel2.textAlignment = NSTextAlignmentRight;
        self.tempUnitLabel2.adjustsFontSizeToFitWidth = YES;
        self.tempUnitLabel2.minimumScaleFactor = 0.2;
        self.tempUnitLabel2.textColor = kTempColor;
        self.tempUnitLabel2.text = temperatureUnit;
        [self addSubview:self.tempUnitLabel2];
        
        self.pressureUnitLabel = [[UILabel alloc] initWithFrame:CGRectMake((self.bounds.size.width - (kUnitLabelWidth + kEdgePadding)), (kUnitLabelHeight * unitLabelMultiplicationFactor), kUnitLabelWidth, kUnitLabelHeight)];
        self.pressureUnitLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:kUnitLabelFontSize];
        self.pressureUnitLabel.backgroundColor = [UIColor clearColor];
        self.pressureUnitLabel.textAlignment = NSTextAlignmentRight;
        self.pressureUnitLabel.adjustsFontSizeToFitWidth = YES;
        self.pressureUnitLabel.textColor = kPressureColor;
        self.pressureUnitLabel.minimumScaleFactor = 0.2;
        self.pressureUnitLabel.text = pressureUnit;
        [self addSubview:self.pressureUnitLabel];
        
        self.pressureUnitLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(kEdgePadding, (kUnitLabelHeight * unitLabelMultiplicationFactor), kUnitLabelWidth, kUnitLabelHeight)];
        self.pressureUnitLabel2.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:kUnitLabelFontSize];
        self.pressureUnitLabel2.backgroundColor = [UIColor clearColor];
        self.pressureUnitLabel2.textAlignment = NSTextAlignmentLeft;
        self.pressureUnitLabel2.adjustsFontSizeToFitWidth = YES;
        self.pressureUnitLabel2.textColor = kPressureColor;
        self.pressureUnitLabel2.minimumScaleFactor = 0.2;
        self.pressureUnitLabel2.text = pressureUnit;
        [self addSubview:self.pressureUnitLabel2];
        
        // Add Refrigerant Label In Landscape Mode
        if ((orentation == UIInterfaceOrientationLandscapeLeft) || (orentation == UIInterfaceOrientationLandscapeRight)) {
            self.selectedRefrigerantLabel = [[UILabel alloc] initWithFrame:CGRectMake(((self.bounds.size.width / 2.0) - (kUnitLabelWidth / 2.0)), (self.bounds.size.height - (kUnitLabelHeight * (1.0 + unitLabelMultiplicationFactor))), kUnitLabelWidth, kUnitLabelHeight)];
            self.selectedRefrigerantLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:kRefrigerantLabelFontSize];
            self.selectedRefrigerantLabel.backgroundColor = [UIColor clearColor];
            self.selectedRefrigerantLabel.textAlignment = NSTextAlignmentCenter;
            self.selectedRefrigerantLabel.adjustsFontSizeToFitWidth = YES;
            self.selectedRefrigerantLabel.minimumScaleFactor = 0.2;
            self.selectedRefrigerantLabel.textColor = kRefrigerantLabelColor;
            self.selectedRefrigerantLabel.text = refrigerant;
            [self addSubview:self.selectedRefrigerantLabel];
        }
        
        // Setup Number Formatter
        self.numberFormatter = [[NSNumberFormatter alloc] init];
        [self.numberFormatter setUsesSignificantDigits:YES];
        [self.numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
        [self.numberFormatter setMaximumSignificantDigits:kMaxSignificantDigits];
        if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqual:kGermanLocale]) {
            [self.numberFormatter setDecimalSeparator:kGermanDecimalSeperator];
            [self.numberFormatter setGroupingSeparator:kGermanGroupingSeperator];
        } else {
            [self.numberFormatter setDecimalSeparator:kEnglishDecimalSeperator];
            [self.numberFormatter setGroupingSeparator:kEnglishGroupingSeperator];
        }
        
    }
    return self;
}


#pragma mark Drawing Methods

- (void)drawRect:(CGRect)rect
{
    // Get Graphics Context
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context); // Save Graphics State
    
    // Draw Pressure Scale
    [self drawScale:kScaleTypePressure forContext:context];
    
    
    // Draw Temperature Scale
    [self drawScale:kScaleTypeTemperature forContext:context];
    
    
    CGContextRestoreGState(context); // Restore Graphics State
}


- (void)drawScale:(NSString *)scale forContext:(CGContextRef)context {
    
    // Get View Diemntions
    float width = self.bounds.size.width;
    float height = self.bounds.size.height;
    
    // Set Starting Position
    CGFloat horizontalPosition = kEdgePadding;
    CGFloat verticalPosition = (height / 2.0);
    
    // Calculate Spacing
    CGFloat totalNumberOfTicks = 100.00;
    CGFloat minorTickSpacing = ((width - (kEdgePadding * 2.0)) / totalNumberOfTicks);
    
    
    // Set Appropriate Color
    if ([scale isEqualToString:kScaleTypeTemperature]) {
        [kTempColor set];
    } else {
        [kPressureColor set];
    }
    
    // Set Line Width
    CGContextSetLineWidth(context, kLineWidth);
    
    
    // Use Counter To Keep Track of Retreived ScaleData Values
    int counter = 0;
    
    
    // Draw Ticks
    for (NSInteger currentTickNumber = 0; currentTickNumber < (totalNumberOfTicks + 1); currentTickNumber++) {
        
        // Determine Appropriate Start Point
        if ([scale isEqualToString:kScaleTypeTemperature]) {
            CGContextMoveToPoint(context, (horizontalPosition + (minorTickSpacing * currentTickNumber)), (verticalPosition + kCenterPadding));
        } else {
            CGContextMoveToPoint(context, (horizontalPosition + (minorTickSpacing * currentTickNumber)), (verticalPosition - kCenterPadding));
        }
        
        
        // Draw Ticks and Text
        if (currentTickNumber % kMajorTickInterval == 0) {
            
            if (currentTickNumber % kMajorBigTickInterval == 0) {
                
                // Draw Appropriate Major Big Tick
                CGPoint tickPoint;
                if ([scale isEqualToString:kScaleTypeTemperature]) {
                    tickPoint = CGPointMake((horizontalPosition + (minorTickSpacing * currentTickNumber)), (kMajorBigTickLength + (verticalPosition + kCenterPadding)));
                } else {
                    tickPoint = CGPointMake((horizontalPosition + (minorTickSpacing * currentTickNumber)), ((verticalPosition - kCenterPadding) - kMajorBigTickLength));
                }
                CGContextAddLineToPoint(context, tickPoint.x, tickPoint.y);
                
                // Draw Text
                if (currentTickNumber % 20 == 0) {
                    
                    CGFloat fontSize = kAnnotationFontSize;
                    CGFloat minFontSize = 5.0f;
                    
                    // Determine Appropriate Annotation Value
                    float annotationValue;
                    if ([scale isEqualToString:kScaleTypeTemperature]) {
                        annotationValue = [[[self.scaleData objectAtIndex:counter] objectAtIndex:0] doubleValue];
                    } else {
                        annotationValue = [[[self.scaleData objectAtIndex:counter] objectAtIndex:1] doubleValue];
                    }
                    counter++;
                    
                    UIFont *annotationFont = [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize];
                    
                    NSString *annotation = [NSString stringWithFormat:@"%@", [self.numberFormatter stringFromNumber:[NSNumber numberWithFloat:annotationValue]]];
                    
                    CGSize textSize = [annotation sizeWithFont:annotationFont];
                    
                    // Limit Annotation Width So Annotations Do Not Overlap Each Other
                    if (textSize.width > (minorTickSpacing * kMajorBigTickInterval)) {
                        textSize.width = (minorTickSpacing * kMajorBigTickInterval);
                    }
                    
                    // Adjust Vertical Position Appropriately
                    if ([scale isEqualToString:kScaleTypeTemperature]) {
                        tickPoint.y += 1.0;
                    } else {
                        tickPoint.y -= 1.0;
                        tickPoint.y -= textSize.height;
                    }
                    
                    if (!(currentTickNumber == 0)) {
                        if (currentTickNumber == totalNumberOfTicks) {
                            tickPoint.x -= textSize.width;
                        } else {
                            tickPoint.x -= (textSize.width / 2.0);
                        }
                    }
                    [annotation drawAtPoint:tickPoint forWidth:textSize.width withFont:annotationFont minFontSize:minFontSize actualFontSize:&fontSize lineBreakMode:NSLineBreakByCharWrapping baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
                    annotationFont = nil;
                    annotation = nil;
                }
                
            } else {
                
                // Draw Appropriate Major Tick
                if ([scale isEqualToString:kScaleTypeTemperature]) {
                    CGContextAddLineToPoint(context, (horizontalPosition + (minorTickSpacing * currentTickNumber)), (kMajorTickLength + (verticalPosition + kCenterPadding)));
                } else {
                    CGContextAddLineToPoint(context, (horizontalPosition + (minorTickSpacing * currentTickNumber)), ((verticalPosition - kCenterPadding) - kMajorTickLength));
                }
            }
            
        } else {
            
            // Draw Appropriate Minor Tick
            if ([scale isEqualToString:kScaleTypeTemperature]) {
                CGContextAddLineToPoint(context, (horizontalPosition + (minorTickSpacing * currentTickNumber)), (kMinorTickLength + (verticalPosition + kCenterPadding)));
            } else {
                CGContextAddLineToPoint(context, (horizontalPosition + (minorTickSpacing * currentTickNumber)), ((verticalPosition - kCenterPadding) - kMinorTickLength));
            }
        }
        
    }
    
    CGContextStrokePath(context);
    
}


- (UIImage *)coldSliderScaleImage {
    
    UIGraphicsBeginImageContext(self.bounds.size);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}


#pragma mark Cleanup

- (void)dealloc {
    
    self.scaleData = nil;
    
    self.tempUnitLabel = nil;
    self.tempUnitLabel2 = nil;
    self.pressureUnitLabel = nil;
    self.pressureUnitLabel2 = nil;
    self.selectedRefrigerantLabel = nil;
    
    self.numberFormatter = nil;
    
}


@end
