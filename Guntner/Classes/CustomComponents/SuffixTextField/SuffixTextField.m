//
//  SuffixTextField.m
//  Guntner
//
//  Created by Shiraz Omar on 8/15/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "SuffixTextField.h"


#define kSuffixTextLabelTag 35467756
#define kSuffixTextFieldSuffixFont [UIFont fontWithName:@"HelveticaNeue" size:16.0f]


@implementation SuffixTextField


@synthesize suffix = _suffix;


#pragma mark Setup

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    // Use Interface Builder User Defined Runtime Attributes to set
    // suffix and suffixColor in Interface Builder.
    if (!self.suffix) {
        [self setSuffix:@""];
    }
    
    if (!self.suffixColor) {
        [self setSuffixColor:[UIColor lightGrayColor]];
    }
    
    // Setup Suffix Text Label
    [self setupSuffixTextLabel];
    
}

- (id)initWithFrame:(CGRect)frame
{
    if( (self = [super initWithFrame:frame]) )
    {
        [self setSuffix:@""];
        [self setSuffixColor:[UIColor lightGrayColor]];
        
        // Setup Suffix Text Label
        [self setupSuffixTextLabel];
        
    }
    return self;
}


- (void)setupSuffixTextLabel {
    
    @autoreleasepool {
        // Create And Add Suffix Label
        UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 60, self.bounds.size.height)];
        UILabel *suffixTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, (rightView.bounds.size.width - 5), rightView.bounds.size.height)];
        suffixTextLabel.text = self.suffix;
        suffixTextLabel.minimumScaleFactor = 0.2;
        suffixTextLabel.tag = kSuffixTextLabelTag;
        suffixTextLabel.textColor = self.suffixColor;
        suffixTextLabel.adjustsFontSizeToFitWidth = YES;
        suffixTextLabel.textAlignment = NSTextAlignmentCenter;
        suffixTextLabel.backgroundColor = [UIColor clearColor];
        suffixTextLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        suffixTextLabel.font = kSuffixTextFieldSuffixFont;
        self.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.rightViewMode = UITextFieldViewModeUnlessEditing;
        [rightView addSubview:suffixTextLabel];
        self.rightView = rightView;
        
        suffixTextLabel = nil;
        rightView = nil;
        
        
        // Create and Add Blank UIView To Add Left Padding
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5.0, self.bounds.size.height)];
        paddingView.backgroundColor = [UIColor clearColor];
        self.leftViewMode = UITextFieldViewModeUnlessEditing;
        self.leftView = paddingView;
        paddingView = nil;
    }
    
}


#pragma mark Suffix Methods

- (void)setSuffix:(NSString *)suffix {
    
    @autoreleasepool {
        // Update TextField SuffixTextLabel
        for (UILabel *label in [self.rightView subviews]) {
            
            if (label.tag == kSuffixTextLabelTag) {
                
                // Update Label Text
                label.text = suffix;
            }
        }
    }
    
    // Standard Implementation
    if (_suffix == suffix) return;
    
    _suffix = nil;
    _suffix = [suffix copy];
    
}


- (void)setSuffixColor:(UIColor *)suffixColor {
    
    @autoreleasepool {
        // Update TextField SuffixTextLabel
        for (UILabel *label in [self.rightView subviews]) {
            
            if (label.tag == kSuffixTextLabelTag) {
                
                // Update Label Text Color
                label.textColor = suffixColor;
            }
        }
    }
    
    // Standard Implementation
    if (_suffixColor == suffixColor) return;
    
    _suffixColor = nil;
    _suffixColor = [suffixColor copy];
    
}


- (void)setSuffixLabelFont:(UIFont *)suffixFont {
    
    @autoreleasepool {
        // Update TextField SuffixTextLabel
        for (UILabel *label in [self.rightView subviews]) {
            
            if (label.tag == kSuffixTextLabelTag) {
                
                // Update Label Text Color
                label.font = suffixFont;
            }
        }
    }
}


#pragma mark Cleanup

- (void)dealloc
{
#if __has_feature(objc_arc)
    self.suffix = nil;
    self.suffixColor = nil;
#else
    [self.suffix release]; self.suffix = nil;
    [self.suffixColor release]; self.suffixColor = nil;
    [super dealloc];
#endif
}



@end
