//
//  SuffixTextField.h
//  Guntner
//
//  Created by Shiraz Omar on 8/15/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SuffixTextField : UITextField


@property (nonatomic, strong) NSString *suffix;
@property (nonatomic, strong) UIColor *suffixColor;


- (void)setSuffixLabelFont:(UIFont *)suffixFont;


@end
