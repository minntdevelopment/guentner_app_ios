//
//  ColdSliderCalculator.m
//  Guntner
//
//  Created by Shiraz Omar on 8/16/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "ColdSliderCalculator.h"

#import "Converter.h"
#import "Configuration.h"


@interface ColdSliderCalculator () {
    
    Converter *unitConverter;
    NSDictionary *refrigerantData;
    
}


@property (nonatomic, strong) Converter *unitConverter;
@property (nonatomic, strong) NSDictionary *refrigerantData;


@end


@implementation ColdSliderCalculator


@synthesize unitConverter = _unitConverter;
@synthesize refrigerantData = _refrigerantData;


#pragma mark Setup

- (id)init {
    
    self = [super init];
    
    if (self) {
        
        // Load Data From Disk
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:kColdSliderDataFile ofType:kColdSliderDataFileType];
        self.refrigerantData = [NSDictionary dictionaryWithContentsOfFile:plistPath];
        plistPath = nil;
        
        
        // Initialize Unit Converter
        self.unitConverter = [[Converter alloc] init];
        
    }
    
    return self;
    
}


#pragma mark Public Getters

- (NSArray *)refrigerantList {
    
    return [[self.refrigerantData allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
}

- (NSArray *)pressureUnitList {
    
    NSLog(@"HERE: %@",[self.unitConverter conversionListForUnit:kConverterUnitTypePressure]);
    return [self.unitConverter conversionListForUnit:kConverterUnitTypePressure];
    
}

- (NSString *)odpForRefrigerant:(NSString *)refrigerant {
    
    return [[self.refrigerantData valueForKey:refrigerant] valueForKey:kColdSliderDataODP];
    
}

- (NSString *)gwpForRefrigerant:(NSString *)refrigerant {
    
    return [[self.refrigerantData valueForKey:refrigerant] valueForKey:kColdSliderDataGWP];
    
}

- (NSString *)oilForRefrigerant:(NSString *)refrigerant {
    
    return [[self.refrigerantData valueForKey:refrigerant] valueForKey:kColdSliderDataOIL];
    
}

- (double)minTemperature:(NSString *)tempUnit forRefrigerant:(NSString *)refrigerant {
    
    return [self convertTemperature:[[self minTempInCelsiusForRefrigerant:refrigerant] doubleValue] fromUnit:kCelsiusCharacterString toUnit:tempUnit];
    
}

- (double)maxTemperature:(NSString *)tempUnit forRefrigerant:(NSString *)refrigerant {
    
    return [self convertTemperature:[[self maxTempInCelsiusForRefrigerant:refrigerant] doubleValue] fromUnit:kCelsiusCharacterString toUnit:tempUnit];
    
}

- (double)critTemperature:(NSString *)tempUnit forRefrigerant:(NSString *)refrigerant {
    
    return [self convertTemperature:[[self critTempInCelsiusForRefrigerant:refrigerant] doubleValue] fromUnit:kCelsiusCharacterString toUnit:tempUnit];
    
}

- (NSString *)minTempInCelsiusForRefrigerant:(NSString *)refrigerant {
    
    return [[self.refrigerantData valueForKey:refrigerant] valueForKey:kColdSliderDataTempMin];
    
}

- (NSString *)maxTempInCelsiusForRefrigerant:(NSString *)refrigerant {
    
    return [[self.refrigerantData valueForKey:refrigerant] valueForKey:kColdSliderDataTempMax];
    
}

- (NSString *)critTempInCelsiusForRefrigerant:(NSString *)refrigerant {
    
    return [[self.refrigerantData valueForKey:refrigerant] valueForKey:kColdSliderDataTempCritical];
    
}


#pragma mark Convert Methods

- (double)getGaugePressureForTemperature:(double)temperature temperatureUnit:(NSString *)temperatureUnit andRefrigerant:(NSString *)refrigerant {
    
    double result = 0.0;
    
    double convertedTemperature = [self.unitConverter convert:temperature unit:kConverterUnitTypeTemperature from:temperatureUnit to:kCelsiusCharacterString];
    result = [self getGaugePressureInBarForTemperatureInCelsius:convertedTemperature andRefrigerant:refrigerant];
    
    return result;
    
}

- (double)getGaugePressureInBarForTemperatureInCelsius:(double)temperature andRefrigerant:(NSString *)refrigerant {
    
    double result = 0.0;
    
    @autoreleasepool {
        
        // Get Relevant Data For Selected Refrigerant
        NSDictionary *refrigerantValues = [self.refrigerantData valueForKey:refrigerant];
        
        // Retreive Contacts For Refrigerant
        double c1 = [[refrigerantValues valueForKey:kColdSliderDataConstant1] doubleValue];
        double c2 = [[refrigerantValues valueForKey:kColdSliderDataConstant2] doubleValue];
        double c3 = [[refrigerantValues valueForKey:kColdSliderDataConstant3] doubleValue];
        double c4 = [[refrigerantValues valueForKey:kColdSliderDataConstant4] doubleValue];
        double c5 = [[refrigerantValues valueForKey:kColdSliderDataConstant5] doubleValue];
        double c6 = [[refrigerantValues valueForKey:kColdSliderDataConstant6] doubleValue];
        double c7 = [[refrigerantValues valueForKey:kColdSliderDataConstant7] doubleValue];
        double c8 = [[refrigerantValues valueForKey:kColdSliderDataConstant8] doubleValue];
        double c9 = [[refrigerantValues valueForKey:kColdSliderDataConstant9] doubleValue];
        double c10 = [[refrigerantValues valueForKey:kColdSliderDataConstant10] doubleValue];
        
        // Release Refrigerant Values Dictionary
        refrigerantValues = nil;
        
        // Workout Components Required To Do Necessary Calculations
        double tempInKelvin = (temperature + 273.15);
        double v = (tempInKelvin + (c9 / (tempInKelvin - c10)));
        
        double A = (pow(v, 2.0) + (c1 * v) + c2);
        double B = ((c3 * pow(v, 2.0)) + (c4 * v) + c5);
        double C = ((c6 * pow(v, 2.0)) + (c7 * v) + c8);
        
        double top = (2.0 * C);
        double bottom = ((B * -1.0) + sqrt((pow(B, 2.0) - (4.0 * A * C))));
        
        // Calculate Result
        result = ((10.0 * pow((top/bottom), 4.0)) - kAtmosphericPressure);
    }
    
    return result;
    
}

- (double)getTemperatureInCelsiusForGaugePressureInBar:(double)pressure andRefrigerant:(NSString *)refrigerant {
    
    double result = 0.0;
    
    @autoreleasepool {
        
        // Get Relevant Data For Selected Refrigerant
        NSDictionary *refrigerantValues = [self.refrigerantData valueForKey:refrigerant];
        
        // Retreive Contacts For Refrigerant
        double c1 = [[refrigerantValues valueForKey:kColdSliderDataConstant1] doubleValue];
        double c2 = [[refrigerantValues valueForKey:kColdSliderDataConstant2] doubleValue];
        double c3 = [[refrigerantValues valueForKey:kColdSliderDataConstant3] doubleValue];
        double c4 = [[refrigerantValues valueForKey:kColdSliderDataConstant4] doubleValue];
        double c5 = [[refrigerantValues valueForKey:kColdSliderDataConstant5] doubleValue];
        double c6 = [[refrigerantValues valueForKey:kColdSliderDataConstant6] doubleValue];
        double c7 = [[refrigerantValues valueForKey:kColdSliderDataConstant7] doubleValue];
        double c8 = [[refrigerantValues valueForKey:kColdSliderDataConstant8] doubleValue];
        double c9 = [[refrigerantValues valueForKey:kColdSliderDataConstant9] doubleValue];
        double c10 = [[refrigerantValues valueForKey:kColdSliderDataConstant10] doubleValue];
        
        // Release Refrigerant Values Dictionary
        refrigerantValues = nil;
        
        // Workout Components Required To Do Necessary Calculations
        double beta = pow(((pressure + kAtmosphericPressure) / 10.0), 0.25);
        
        double E = (pow(beta, 2.0) + (c3 * beta) + c6);
        double F = ((c1 * pow(beta, 2.0)) + (c4 * beta) + c7);
        double G = ((c2 * pow(beta, 2.0)) + (c5 * beta) + c8);
        
        double D = ((2.0 * G) / ((F * -1.0) - (sqrt((pow(F, 2.0) - (4.0 * E * G))))));
        
        double top = (c10 + D - sqrt((pow((c10 + D), 2.0) - (4.0 * (c9 + (c10 * D))))));
        double bottom = 2.0;
        
        // Calculate Temperature In Kelvin
        result = (top/bottom);
        
        // Calculate Result
        result -= 273.15;
    }
    
    return result;
    
}

- (double)getAbsolutePressureForTemperature:(double)temperature temperatureUnit:(NSString *)temperatureUnit andRefrigerant:(NSString *)refrigerant {
    
    double result = 0.0;
    
    double convertedTemperature = [self.unitConverter convert:temperature unit:kConverterUnitTypeTemperature from:temperatureUnit to:kCelsiusCharacterString];
    result = [self getGaugePressureInBarForTemperatureInCelsius:convertedTemperature andRefrigerant:refrigerant];
    result += kAtmosphericPressure;
    
    return result;
    
}

- (double)getPressure:(NSString *)pressureUnit forTemperature:(double)temperature temperatureUnit:(NSString *)temperatureUnit andRefrigerant:(NSString *)refrigerant {
    
    // Return 0 For Invalid Refrigerants
    if ([refrigerant isEqualToString:kColdSliderRefrigerantSelectorButtonTitle] || [refrigerant isEqualToString:@""] || (refrigerant == nil)) {
        return 0.0;
    }
    
    NSString *pressure = [[pressureUnit componentsSeparatedByString:@" "] objectAtIndex:0];
    
    if ([temperatureUnit isEqualToString:kCelsiusString]) {
        temperatureUnit = kCelsiusCharacterString;
    } else if ([temperatureUnit isEqualToString:kFahrenheitString]) {
        temperatureUnit = kFahrenheitCharacterString;
    }
    
    double result = 0.0;
    
    if ([pressureUnit hasSuffix:kGaugePressureSymbol]) {
        
        result = [self getGaugePressureForTemperature:temperature temperatureUnit:temperatureUnit andRefrigerant:refrigerant];
        
    } else {
        
        result = [self getAbsolutePressureForTemperature:temperature temperatureUnit:temperatureUnit andRefrigerant:refrigerant];
        
    }
    
    result = [self.unitConverter convert:result unit:kConverterUnitTypePressure from:kConverterPressureUnitBar to:pressure];
    
    pressure = nil;
    
    return result;
}

- (double)getTemperature:(NSString *)temperatureUnit forPressure:(double)pressure pressureUnit:(NSString *)pressureUnit andRefrigerant:(NSString *)refrigerant {
    
    // Return 0 For Invalid Refrigerants
    if ([refrigerant isEqualToString:kColdSliderRefrigerantSelectorButtonTitle] || [refrigerant isEqualToString:@""] || (refrigerant == nil)) {
        return 0.0;
    }
    
    if ([temperatureUnit isEqualToString:kCelsiusString]) {
        temperatureUnit = kCelsiusCharacterString;
    } else if ([temperatureUnit isEqualToString:kFahrenheitString]) {
        temperatureUnit = kFahrenheitCharacterString;
    }
    
    // Convert Pressure Unit To bar
    pressure = [self.unitConverter convert:pressure unit:kConverterUnitTypePressure from:[[pressureUnit componentsSeparatedByString:@" "] objectAtIndex:0] to:kConverterPressureUnitBar];
    
    // Change Atmospheric Pressure To Gauge
    if (![pressureUnit hasSuffix:kGaugePressureSymbol]) {
        
        pressure -= kAtmosphericPressure;
    }
    
    // Get Temperature
    double result = [self getTemperatureInCelsiusForGaugePressureInBar:pressure andRefrigerant:refrigerant];
    
    // Converted Temperature To Desired Unit
    result = [self.unitConverter convert:result unit:kConverterUnitTypeTemperature from:kCelsiusCharacterString to:temperatureUnit];
    
    return result;
}

- (double)convertTemperature:(double)temperature fromUnit:(NSString *)fromUnit toUnit:(NSString *)toUnit {
    
    // Convert To and From Temperature Unit Strings To Symbols
    if ([toUnit isEqualToString:kCelsiusString]) {
        toUnit = kCelsiusCharacterString;
    } else if ([toUnit isEqualToString:kFahrenheitString]) {
        toUnit = kFahrenheitCharacterString;
    }
    if ([fromUnit isEqualToString:kCelsiusString]) {
        fromUnit = kCelsiusCharacterString;
    } else if ([fromUnit isEqualToString:kFahrenheitString]) {
        fromUnit = kFahrenheitCharacterString;
    }
    
    double result = 0.0;
    
    result = [self.unitConverter convert:temperature unit:kConverterUnitTypeTemperature from:fromUnit to:toUnit];
    
    return result;
    
}


#pragma mark Cleanup

- (void)dealloc {
    
    self.unitConverter = nil;
    self.refrigerantData = nil;
    
}


@end
