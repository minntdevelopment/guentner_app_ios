//
//  ColdSliderCalculator.h
//  Guntner
//
//  Created by Shiraz Omar on 8/16/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ColdSliderCalculator : NSObject{
    
    //
    
}


- (NSArray *)refrigerantList;
- (NSArray *)pressureUnitList;
- (NSString *)odpForRefrigerant:(NSString *)refrigerant;
- (NSString *)gwpForRefrigerant:(NSString *)refrigerant;
- (NSString *)oilForRefrigerant:(NSString *)refrigerant;
- (NSString *)minTempInCelsiusForRefrigerant:(NSString *)refrigerant;
- (NSString *)maxTempInCelsiusForRefrigerant:(NSString *)refrigerant;
- (NSString *)critTempInCelsiusForRefrigerant:(NSString *)refrigerant;
- (double)minTemperature:(NSString *)tempUnit forRefrigerant:(NSString *)refrigerant;
- (double)maxTemperature:(NSString *)tempUnit forRefrigerant:(NSString *)refrigerant;
- (double)critTemperature:(NSString *)tempUnit forRefrigerant:(NSString *)refrigerant;
- (double)getPressure:(NSString *)pressureUnit forTemperature:(double)temperature temperatureUnit:(NSString *)temperatureUnit andRefrigerant:(NSString *)refrigerant;
- (double)getTemperature:(NSString *)temperatureUnit forPressure:(double)pressure pressureUnit:(NSString *)pressureUnit andRefrigerant:(NSString *)refrigerant;

- (double)convertTemperature:(double)temperature fromUnit:(NSString *)fromUnit toUnit:(NSString *)toUnit;


@end
