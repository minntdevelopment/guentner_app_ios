//
//  UINavigationController+AutoRotationCategory.h
//  Guntner
//
//  Created by Shiraz Omar on 9/2/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (AutoRotationCategory)


- (BOOL)shouldAutorotate;
- (NSUInteger)supportedInterfaceOrientations;
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation;


@end
