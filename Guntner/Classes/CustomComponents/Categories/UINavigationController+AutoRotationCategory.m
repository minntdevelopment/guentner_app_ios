//
//  UINavigationController+AutoRotationCategory.m
//  Guntner
//
//  Created by Shiraz Omar on 9/2/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "UINavigationController+AutoRotationCategory.h"


@implementation UINavigationController (AutoRotationCategory)

- (BOOL)shouldAutorotate
{
    return [[self.viewControllers lastObject] shouldAutorotate];
}

- (NSUInteger)supportedInterfaceOrientations
{
    return [[self.viewControllers lastObject] supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return [[self.viewControllers lastObject]  preferredInterfaceOrientationForPresentation];
}


@end
