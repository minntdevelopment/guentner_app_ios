//
//  RootViewController.m
//  Guntner
//
//  Created by Shiraz Omar on 8/5/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "RootViewController.h"

#import "Configuration.h"
#import "GUNLoadingViewController.h"
#import "AppDelegate.h"


@interface RootViewController () {

    GUNLoadingViewController* _loadingViewController;
    CGRect _contentViewControllerFrame;

    UIView* _loadingView;
    UIView* _popupView;
    
    NSMutableArray* _popupViewControllers;
}

//

@end


@implementation RootViewController {
    UIViewController *_activeViewController;
}


#pragma Setup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    ((AppDelegate *)[[UIApplication sharedApplication] delegate]).rootViewController = self;
    
    _popupViewControllers = [NSMutableArray array];
    
    // Add Notification Listener For Presenting Menu Item
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentSelectedMenuItem:) name:kMenuPresentSelectedItemIdentifier object:nil]; // Listen For Notifications Posted When Menu Item Is Pressed
    
    
    // Customize Navigation Bar By Setting NavBar Background Image and Title Font
    UIImage *navigationBarBackgroundImage = [[UIImage imageNamed:kNavigationBarImage] resizableImageWithCapInsets:kImageSizingEdgeInsets];
    [self.navigationController.navigationBar setBackgroundImage:navigationBarBackgroundImage forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBackgroundImage:navigationBarBackgroundImage forBarMetrics:UIBarMetricsDefaultPrompt];
    [self.navigationController.navigationBar setBackgroundImage:navigationBarBackgroundImage forBarMetrics:UIBarMetricsCompact];
    [self.navigationController.navigationBar setBackgroundImage:navigationBarBackgroundImage forBarMetrics:UIBarMetricsCompactPrompt];
    
    
//    navigationBarBackgroundImage = nil;
    
    
    // Add Swap Menu Button And Back Button Notification Listener
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(swapMenuButtonWithBackButton) name:kSwapMenuButtonWithBackButtonIdentifier object:nil];
    
    
    // Add Set Menu Button Notification Listener
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setLeftBarButtonItemWithMenuButton) name:kSetMenuButtonIdentifier object:nil];
    
    
//    // Present NewsfeedViewController As Initial View
//    [self presentViewControllerWithTitle:kNewsfeedButtonTitle animated:NO];
//    //[self showHideMenu:nil];

    //Loading and Popups
    CGRect screenRect = [[UIScreen mainScreen] applicationFrame];
    NSLog(@"Screen\n x: %f y: %f w: %f h: %f",screenRect.origin.x,screenRect.origin.y,screenRect.size.width,screenRect.size.height);
    _contentViewControllerFrame = CGRectMake(0.0f, 0.0f, screenRect.size.width, screenRect.size.height);



}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    [self becomeFirstResponder];

    if(!_popupView) { // first run
        UIApplication *application = [UIApplication sharedApplication];

        _popupView = [[UIView alloc] initWithFrame:application.keyWindow.bounds];
        _popupView.userInteractionEnabled = NO;
        _popupView.backgroundColor = color_clear;
        [application.keyWindow addSubview:_popupView];

        _loadingView = [[UIView alloc] initWithFrame:_contentViewControllerFrame];
        _loadingView.userInteractionEnabled = NO;
        _loadingView.backgroundColor = color_clear;
        [application.keyWindow addSubview:_loadingView];
    }
    
    // Present NewsfeedViewController As Initial View
    [self presentViewControllerWithTitle:kNewsfeedButtonTitle animated:NO];
    //[self showHideMenu:nil];

}


#pragma mark Menu Methods

- (void)presentSelectedMenuItem:(NSNotification *)itemNotification {
    
    // Get Title From Notification Object And Display Appropriate View Controller
    [self presentViewControllerWithTitle:[itemNotification object] animated:YES];
    
}

- (void)presentViewControllerWithTitle:(NSString *)title animated:(BOOL)animated {

    
    @autoreleasepool {
        
        
        // Pop Any Left Over Controllers
        if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0) {
            if (self.navigationController.topViewController != self) {
                [self.navigationController popToRootViewControllerAnimated:NO];
            }
        }
                       
        // Identify Controller Using Title and Present Accordingly
        UIViewController *viewController;
        
        if ([title isEqualToString:kNewsfeedButtonTitle]) {
            viewController = [self.storyboard instantiateViewControllerWithIdentifier:kNewsfeedViewControllerIdentifier];
        } else if ([title isEqualToString:kContactsButtonTitle]) {
            viewController = [self.storyboard instantiateViewControllerWithIdentifier:kContactsViewControllerIdentifier];
        } else if ([title isEqualToString:kMaterialRecommendationButtonTitle]) {
            viewController = [self.storyboard instantiateViewControllerWithIdentifier:kMaterialViewControllerIdentifier];
        } else if ([title isEqualToString:kConverterButtonTitle]) {
            viewController = [self.storyboard instantiateViewControllerWithIdentifier:kConverterViewControllerIdentifier];
        } else if ([title isEqualToString:kInformationButtonTitle]) {
            viewController = [self.storyboard instantiateViewControllerWithIdentifier:kInformationViewControllerIdentifier];
        } else if ([title isEqualToString:kColdSliderButtonTitle]) {
            viewController = [self.storyboard instantiateViewControllerWithIdentifier:kColdSliderViewControllerIdentifier];
        } else if ([title isEqualToString:kLiteratureHubButtonTitle]) {
            viewController = [self.storyboard instantiateViewControllerWithIdentifier:kLiteratureHubViewControllerIdentifier];
        } else if ([title isEqualToString:kTutorialButtonTitle]) {
            viewController = [[UIStoryboard storyboardWithName:@"Tutorials" bundle:nil] instantiateViewControllerWithIdentifier:kTutorialsViewControllerIdentifier];        } else {
            viewController = nil;
            NSLog(kRootViewControllerInvalidMenuError, title);
        }
    
        if (viewController != nil) {
            
            viewController.title = title;
            viewController.navigationItem.hidesBackButton = YES;
            _activeViewController = viewController;
            [self.navigationController pushViewController:viewController animated:animated];
            [self createAndSetLeftBarButtonItem:kMenuButtonIdentifier];
            [self showHideMenu:nil];
            
        }
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 && self.navigationController.viewControllers.count > 2) {
            NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: self.navigationController.viewControllers];
            [navigationArray removeObjectAtIndex: 1];
            self.navigationController.viewControllers = navigationArray;
        }
    }
    
}


- (void)setLeftBarButtonItemWithMenuButton {
    
    [self createAndSetLeftBarButtonItem:kMenuButtonIdentifier];
    
}


- (void)createAndSetLeftBarButtonItem:(NSString *)buttonTitle {
    
    // Release Existing Button
    self.navigationController.topViewController.navigationItem.leftBarButtonItem = nil;
    
    
    if ([buttonTitle isEqualToString:kMenuButtonIdentifier]) {
        
        // Create and Show Menu Button
        UIImage *buttonImage = [UIImage imageNamed:kMenuButtonImage];
        CGRect buttonFrame = kMenuButtonFrame;
        UIButton *menuButton = [[UIButton alloc] initWithFrame:buttonFrame];
        menuButton.contentMode = UIViewContentModeCenter;
        menuButton.autoresizingMask = UIViewAutoresizingNone;
        [menuButton setImage:buttonImage forState:UIControlStateNormal];
        [menuButton addTarget:self action:@selector(showHideMenu:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
        self.navigationController.topViewController.navigationItem.leftBarButtonItem = menuBarButton;
        menuBarButton = nil;
        buttonImage = nil;
        
    } else if ([buttonTitle isEqualToString:kBackButtonIdentifier]) {
        
        // Create and Show Back Button
        UIImage *buttonImage = [UIImage imageNamed:kBackButtonImage];
        CGRect buttonFrame = kBackButtonFrame;
        UIButton *backButton = [[UIButton alloc] initWithFrame:buttonFrame];
        [backButton setTitle:kBackButtonTitle forState:UIControlStateNormal];
        [backButton setTitleColor:kBackButtonTitleColor forState:UIControlStateNormal];
        [[backButton titleLabel] setFont:kBackButtonTitleFont];
        [backButton setImage:buttonImage forState:UIControlStateNormal];
        [backButton addTarget:self.navigationController.topViewController action:@selector(goBackToPreviousPageOnPhoneGapView) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        self.navigationController.topViewController.navigationItem.leftBarButtonItem = backBarButton;
        backBarButton = nil;
        buttonImage = nil;
        
    }
    
}


- (IBAction)showHideMenu:(id)sender {
    
    // Post Notification To Toggle Menu
    [[NSNotificationCenter defaultCenter] postNotificationName:kMenuToggleNotificationIdentifier object:self];
  
    
}


- (void)swapMenuButtonWithBackButton {
    
    // Create Back Button and Replace Menu Button
    if ([[(UIButton *)[self.navigationController.topViewController.navigationItem.leftBarButtonItem customView] titleForState:UIControlStateNormal] isEqualToString:kBackButtonTitle]) {
        [self createAndSetLeftBarButtonItem:kMenuButtonIdentifier];
        if (self.navigationController.topViewController.navigationItem.rightBarButtonItem) {
            self.navigationController.topViewController.navigationItem.rightBarButtonItem.customView.hidden = NO;
        }
    } else {
        
        [self createAndSetLeftBarButtonItem:kBackButtonIdentifier];
        if([self.navigationController.navigationBar.topItem.title isEqualToString:kContactsButtonTitle])
        {
            self.navigationController.navigationBar.topItem.title = kDetailsTitle;
            if (self.navigationController.topViewController.navigationItem.rightBarButtonItem) {
                self.navigationController.topViewController.navigationItem.rightBarButtonItem.customView.hidden = YES;
            }
        }
    }

}



#pragma mark LoadingViewController Handling

-(void)showLoadingViewController:(GUNLoadingViewController*)loadingViewController {
    if(_loadingViewController != nil) {
        [self dismissLoadingViewController];
    }
    _loadingViewController = loadingViewController;

    [loadingViewController.view setFrame:_contentViewControllerFrame];
    [loadingViewController willMoveToParentViewController:self];

    [_loadingView addSubview:loadingViewController.view];

    [self addChildViewController:loadingViewController];
    [loadingViewController didMoveToParentViewController:self];
    _loadingView.userInteractionEnabled = YES;
}

-(void)dismissLoadingViewController {
    _loadingView.userInteractionEnabled = NO;
    [_loadingViewController willMoveToParentViewController:nil];
    [_loadingViewController.view removeFromSuperview];
    [_loadingViewController removeFromParentViewController];
    [_loadingViewController didMoveToParentViewController:nil];
    _loadingViewController = nil;
}


-(void)showPopupViewController:(UIViewController *)popupViewController {
    [self showPopupViewController:popupViewController animated:NO];
}

-(void)showPopupViewController:(UIViewController*)popupViewController animated:(BOOL)animated {
    if([_popupViewControllers containsObject:popupViewController]) {
        [self dismissPopupViewController:popupViewController];
    }
    
    [_popupViewControllers addObject:popupViewController];
    
    CGRect popupViewControllerFrame = _contentViewControllerFrame;
    
    if(animated) {
        popupViewControllerFrame.origin.y = popupViewControllerFrame.size.height;
    }
    [popupViewController.view setFrame:popupViewControllerFrame];
    
    [popupViewController willMoveToParentViewController:self];
    
    [_popupView addSubview:popupViewController.view];
    if(animated) {
        [UIView animateWithDuration:0.6 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [popupViewController.view setFrame:_contentViewControllerFrame];
        } completion:^(BOOL finished) {
        }];
    }
    
    [self addChildViewController:popupViewController];
    [popupViewController didMoveToParentViewController:self];
    
    _popupView.userInteractionEnabled = YES;
}

-(void)dismissPopupViewController:(UIViewController*)popupViewController {
    [self dismissPopupViewController:popupViewController animated:NO];
}

-(void)dismissPopupViewController:(UIViewController *)popupViewController animated:(BOOL)animated {
    [self dismissPopupViewController:popupViewController animated:animated completion:nil];
}

-(void)dismissPopupViewController:(UIViewController *)popupViewController animated:(BOOL)animated completion:(void (^)())block {
    [popupViewController willMoveToParentViewController:nil];
    
    if(animated) {
        CGRect popupViewFrame = popupViewController.view.frame;
        popupViewFrame.origin.y = popupViewFrame.size.height;
        [UIView animateWithDuration:0.6 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [popupViewController.view setFrame:popupViewFrame];
        } completion:^(BOOL finished) {
            [popupViewController.view removeFromSuperview];
            if(block) {
                block();
            }
            [_popupViewControllers removeObject:popupViewController];
            if(_popupViewControllers.count == 0) {
                _popupView.userInteractionEnabled = NO;
            }
        }];
    } else {
        [popupViewController.view removeFromSuperview];
    }
    [popupViewController removeFromParentViewController];
    [popupViewController didMoveToParentViewController:nil];
    
    
    
    if(!animated) {
        if(block) {
            block();
        }
        [_popupViewControllers removeObject:popupViewController];
        if(_popupViewControllers.count == 0) {
            _popupView.userInteractionEnabled = NO;
        }
    }
}

#pragma mark Auto Rotation Methods

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


#pragma mark Cleanup

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSetMenuButtonIdentifier object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kMenuPresentSelectedItemIdentifier object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kSwapMenuButtonWithBackButtonIdentifier object:nil];
    
}


@end
