//
//  MaterialViewController.m
//  Guntner
//
//  Created by Shiraz Omar on 8/6/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "MaterialViewController.h"
#import "Firebase.h"
#import <Cordova/CDVViewController.h>

#import "Configuration.h"
#import "LoadingViewController.h"


@interface MaterialViewController () {
    
    LoadingViewController *loadingView;
    CDVViewController* phoneGapViewController;
    
}

@property (nonatomic, strong) LoadingViewController *loadingView;
@property (nonatomic, strong) CDVViewController* phoneGapViewController;

@end


@implementation MaterialViewController


@synthesize loadingView = _loadingView;
@synthesize phoneGapViewController = _phoneGapViewController;


#pragma mark Setup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        // Uncomment to override the CDVCommandDelegateImpl used
        // _commandDelegate = [[MainCommandDelegate alloc] initWithViewController:self];
        // Uncomment to override the CDVCommandQueue used
        // _commandQueue = [[MainCommandQueue alloc] initWithViewController:self];
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    // Show Navigation Bar
    self.navigationController.navigationBarHidden = NO;
    
    
    // Setup Notification Listeners
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showLoadingIndicator) name:kShowLoadingViewIdentifier object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeLoadingIndicator) name:kDismissLoadingViewIdentifier object:nil];
    
    
    // Setup PhoneGap/Cordova
    [self setupPhoneGap];
    
    
    [self copyFileToSDcard];
    
    // Show Loading Indicator
    //[self showLoadingIndicator];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    // Send Google Analytics Information
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:kMaterialViewControllerIdentifier];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    [FIRAnalytics logEventWithName:kGAIScreenName
                        parameters:@{
                                     kFIRParameterItemName:kMaterialViewControllerIdentifier,
                                     kFIRParameterContentType:@"screen"
                                     }];
}


- (void)copyFileToSDcard
{
	
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
	
    NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"MaterialRecommendation" ofType:@"json"];
	NSString *targetPath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"MaterialRecommendation.json"];
	
	// delete file before copying, makes updates easier
	if ( [fileManager fileExistsAtPath:targetPath] ) {
		[fileManager removeItemAtPath:targetPath error:&error];
		if ( error ) {
			NSLog(@"Error deleting MaterialRecommendation.json file: %@", error);
		}
	}
	[fileManager copyItemAtPath:resourcePath toPath:targetPath error:&error];
	
	if ( error ) {
		NSLog(@"Error copying MaterialRecommendation.json file: %@", error);
	}
	
	fileManager = nil;
	error = nil;
}


#pragma mark PhoneGap/Cordova Methods

- (void)setupPhoneGap {
    
    @autoreleasepool {
        
        // Setup PhoneGap/Cordova
        NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        
        [cookieStorage setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
        
        int cacheSizeDisk = kCacheSizeDisk; // 32MB
        int cacheSizeMemory = kCacheSizeMemory; // 8MB
        NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:kCacheDiskPath];
        [NSURLCache setSharedURLCache:sharedCache];
        
        
        self.phoneGapViewController = [CDVViewController new];
        self.phoneGapViewController.wwwFolderName = kHTML5Folder;
        self.phoneGapViewController.startPage = kHTML5MaterialRecommendationPage;
        
        self.phoneGapViewController.view.frame = self.view.bounds;
        [self.view addSubview:self.phoneGapViewController.view];
    }
    
}


- (void)goBackToPreviousPageOnPhoneGapView {
    
    // Call Javascript Function To Go To Previous Page
    [self.phoneGapViewController.webView stringByEvaluatingJavaScriptFromString:kGoBackJavaScriptFunction];
    
    // Replace Back Button With Menu Button
    [[NSNotificationCenter defaultCenter] postNotificationName:kSwapMenuButtonWithBackButtonIdentifier object:nil];
    
}


- (void)showLoadingIndicator {
    
    // Disable User Interaction On PhoneGap View
    [self.phoneGapViewController.view setUserInteractionEnabled:NO];
    
    // Show Loading Indicator
    if (self.loadingView == nil) {
        self.loadingView = [[LoadingViewController alloc] initWithNibName:kLoadingViewControllerXibName bundle:nil];
    }
    [self.loadingView.loadingIndicator startAnimating];
    [self.view addSubview:self.loadingView.view];
    self.loadingView.view.center = CGPointMake(self.view.center.x, (self.view.center.y - kLoadingViewVerticalOffset));
    
}

- (void)removeLoadingIndicator {
    
    // Enable User Interaction On PhoneGap View
    [self.phoneGapViewController.view setUserInteractionEnabled:YES];
    
    // Remove Loading Indicator
    if (self.loadingView != nil) {
        [self.loadingView.loadingIndicator stopAnimating];
        [self.loadingView.view removeFromSuperview];
    }
}


#pragma mark UIWebDelegate implementation

- (void)webViewDidFinishLoad:(UIWebView*)theWebView
{
    // White base color for background matches the native apps
    theWebView.backgroundColor = [UIColor whiteColor];
    
    
    return [self.phoneGapViewController webViewDidFinishLoad:theWebView];
}

/* Comment out the block below to over-ride */

/*
 
 - (void) webViewDidStartLoad:(UIWebView*)theWebView
 {
 return [super webViewDidStartLoad:theWebView];
 }
 
 - (void) webView:(UIWebView*)theWebView didFailLoadWithError:(NSError*)error
 {
 return [super webView:theWebView didFailLoadWithError:error];
 }
 
 - (BOOL) webView:(UIWebView*)theWebView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType
 {
 return [super webView:theWebView shouldStartLoadWithRequest:request navigationType:navigationType];
 }
 */


#pragma mark Auto Rotation Methods

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


#pragma mark Cleanup

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}


- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kShowLoadingViewIdentifier object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kDismissLoadingViewIdentifier object:nil];
    
    self.loadingView = nil;
    self.phoneGapViewController = nil;
    
}


@end
