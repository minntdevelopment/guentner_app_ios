//
// Created by Julian Dawo on 04.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "GUNLanguageTO.h"


@implementation GUNLanguageTO {

}
- (instancetype)initWithLangugeCode:(NSString *)code andTranslatedValue:(NSString *)value {
    self = [super init];

    if (self) {
        _langugageCode = code;
        _translatedValue = value;
    }
    return self;
}

@end