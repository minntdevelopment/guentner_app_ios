//
//  PrivacyViewController.m
//  Guntner
//
//  Created by Shiraz Omar on 10/10/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "PrivacyViewController.h"
#import "Firebase.h"
#import "KLSwitch.h"
#import "Configuration.h"


@interface PrivacyViewController () <UIScrollViewDelegate> {
    
    IBOutlet UITextView *contentView;
    IBOutlet UIScrollView *scrollView;
    
}

@property (nonatomic, weak) IBOutlet UITextView *contentView;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;

@end


@implementation PrivacyViewController


@synthesize scrollView = _scrollView;
@synthesize contentView = _contentView;


#pragma mark Setup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    // Show Navigation Bar
    self.navigationController.navigationBarHidden = NO;
    
    
    // Setup ScrollView and ContentView
    self.scrollView.delegate = self;
//    if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqual:kGermanLocale]) {
//        self.scrollView.contentSize = self.contentView.contentSize;
//        //contentViewFrame.size.height = 1054.0;
//    } else {
//        
//        //contentViewFrame.size.height = 844.0;
//    }
    //self.contentView.frame = contentViewFrame;
    
    
    // Round ContentView's Corners
    [self roundViewsCorners:self.contentView];
    
    
    // Create And Show Back Button
    [self createAndShowBackButton];
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    // Setup GoogleAnalytics On/Off Label and Switch
    [self setupAnalyticsLabelAndSwitch];
    
    // Send Google Analytics Information
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:kPrivacyViewControllerIdentifier];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    [FIRAnalytics logEventWithName:kGAIScreenName
                        parameters:@{
                                     kFIRParameterItemName:kPrivacyViewControllerIdentifier,
                                     kFIRParameterContentType:@"screen"
                                     }];
}

- (void)roundViewsCorners:(UIView *)view {
    
    // Set View's Corner Radius To 5px To Round The Corners
    view.layer.cornerRadius = 5.0;
    view.layer.masksToBounds = YES;
    
}


- (void)createAndShowBackButton {
    
    // Create and Show Back Button
    UIImage *buttonImage = [UIImage imageNamed:kBackButtonImage];
    CGRect buttonFrame = kBackButtonFrame;
    UIButton *backButton = [[UIButton alloc] initWithFrame:buttonFrame];
    [backButton setTitle:kBackButtonTitle forState:UIControlStateNormal];
    [backButton setTitleColor:kBackButtonTitleColor forState:UIControlStateNormal];
    [[backButton titleLabel] setFont:kBackButtonTitleFont];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(goToPreviousViewController) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationController.topViewController.navigationItem.leftBarButtonItem = backBarButton;
    backBarButton = nil;
    buttonImage = nil;
    
}


- (void)goToPreviousViewController {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (void)setupAnalyticsLabelAndSwitch {
    
    // Setup GoogleAnalytics On/Off Label and Switch
    KLSwitch *klSwitch = [[KLSwitch alloc] initWithFrame:CGRectMake(10.0, self.contentView.contentSize.height + 50.0, 50.0, 25.0) didChangeHandler:^(BOOL isOn) {
        //Do something useful with it here
        if (isOn) {
            [[GAI sharedInstance] trackerWithTrackingId:@""]; // Disable Analytics
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kGAnalyticsSwitchState]; // Save Switch State
        } else {
            [[GAI sharedInstance] trackerWithTrackingId:kGAnalyticsTrackingID]; // Enable Analytics
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kGAnalyticsSwitchState]; // Save Switch State
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
    }];
    [self.scrollView addSubview:klSwitch]; // Add It To Subview
    
    // Update Switch To Saved State
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kGAnalyticsSwitchState]) {
        klSwitch.on = YES;
    } else {
        klSwitch.on = NO;
    }
    
    // Create Label
    UILabel *switchTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, self.contentView.contentSize.height + 20.0, 240.0, 25.0)];
    switchTitleLabel.font = kInformationTableHeaderViewBoldFont;
    switchTitleLabel.textColor = kGuntnerColor005A37;
    switchTitleLabel.backgroundColor = [UIColor clearColor];
    switchTitleLabel.text = kGAnalyticsSwitchLabelTitle;
    [self.scrollView addSubview:switchTitleLabel];
    
    self.scrollView.contentSize = CGSizeMake(self.contentView.contentSize.width, self.contentView.contentSize.height + 100.0);
    
    [self.view updateConstraintsIfNeeded];
    [self.view layoutIfNeeded];
    // Position Them Appropriately
//    if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqual:kGermanLocale]) {
//        //self.scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, 1074.0);
//        klSwitch.center = CGPointMake(284.0, 1048.0);
//        switchTitleLabel.center = CGPointMake(140.0, 1048.0);
//    } else {
//        //self.scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, 904.0);
//        klSwitch.center = CGPointMake(284.0, 878.0);
//        switchTitleLabel.center = CGPointMake(140.0, 878.0);
//    }
    
}


#pragma mark Auto Rotation Methods

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


#pragma mark Cleanup

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    
    //
    
}


@end
