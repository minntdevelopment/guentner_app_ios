//
//  GUNTutorialManager.h
//  guentner
//
//  Created by Julian Dawo on 01.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GUNRestmanager.h"
#import "Configuration.h"

@class GUNDataManager;
@class GUNTutorialDetailResponse;
@class GUNContentTutorialResponse;

@interface GUNTutorialManager : NSObject

@property (nonatomic,strong) NSString *userDefaultLanguage;

-(instancetype) initWithRestManager:(GUNRestmanager*) restManager dataManager:(GUNDataManager *) dataManager;

-(void)downloadAllTutroialMenuDataIfNecessaryCompletion:(void (^)(NSError*))block;
-(void)downloadLangugeIfNotExistsWithLanguageKey:(NSString *)languageKey complition:(void (^)(NSError*))block;
-(NSArray *) getProductsForRegion:(ContenRegion )contentRegion;

- (NSArray *)getContentForProduct:(NSNumber *)productID;



-(ContenRegion) getActiveRegion;
-(void) setActiveRegion: (ContenRegion) acvtiveRegion;

-(NSArray *) getActiveLangues;
-(void) setActiveLangues:(NSArray *) selectedLanguages;
-(void)setActiveLanguageToPhoneLanguage;

- (NSString *)translationForKeyInActiveLanguage:(NSString *)key;
-(NSString *)nameForLanaguageOfLanguageCode:(NSString *) langCode;
- (NSArray *) languagesForRegion:(RegionCode) regionCode;


- (void)loadTutorialByID:(NSInteger)tutorialID completed:(void (^)(GUNTutorialDetailResponse *, NSError*))block;

-(BOOL) checkIfFeedbackForTutorialExists:(NSNumber *) tutorialID;
-(void) feedbackSentForTutorial:(NSNumber *) tutorialID withRating:(NSNumber *) rating;

-(GUNContentTutorialResponse *) nextTutorialForTutorialID:(NSNumber *) tutorialID andProduct:(NSNumber *)productID;

- (NSString *)valueForRegionCode:(ContenRegion)region;
@end
