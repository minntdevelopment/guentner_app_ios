//
//  MenuViewController.m
//  Guntner
//
//  Created by Shiraz Omar on 8/5/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "MenuViewController.h"

#import "Configuration.h"
#import "AppDelegate.h"
#import "ImpressumViewController.h"
#import "MenuCell.h"

static NSString* menuIconNames[] = {@"icn_news.png", @"icn_materialempfehlung.png", @"icn_kaelteschieber.png", @"icn_converter.png", @"icn_literatur_hub.png", @"G_Tutorial_Icon_34x34.png", @"icn_controls.png", @"icn_kontakte.png"};

@interface MenuViewController () {
    
//    IBOutlet UIButton *newsfeedButton;
//    IBOutlet UIButton *contactsButton;
//    IBOutlet UIButton *converterButton;
//    IBOutlet UIButton *coldSliderButton;
//    IBOutlet UIButton *literatureHubButton;
//    IBOutlet UIButton *materialRecommendationButton;
//    IBOutlet UIButton *controlsButton;
//    IBOutlet UIButton *tutorialButton;
    
//    IBOutlet UIImageView *newsfeedIcon;
//    IBOutlet UIImageView *contactsIcon;
//    IBOutlet UIImageView *converterIcon;
//    IBOutlet UIImageView *coldSliderIcon;
//    IBOutlet UIImageView *literatureHubIcon;
//    IBOutlet UIImageView *materialRecommendationIcon;
//    IBOutlet UIImageView *tutorialIcon;
    
    NSString* selectedTitle;
    NSMutableArray* menuTitles;
}


//@property (nonatomic, weak) IBOutlet UIButton *newsfeedButton;
//@property (nonatomic, weak) IBOutlet UIButton *contactsButton;
@property (nonatomic, weak) IBOutlet UIButton *impressumButton;
//@property (nonatomic, weak) IBOutlet UIButton *converterButton;
//@property (nonatomic, weak) IBOutlet UIButton *coldSliderButton;
//@property (nonatomic, weak) IBOutlet UIButton *literatureHubButton;
//@property (nonatomic, weak) IBOutlet UIButton *materialRecommendationButton;
//@property (nonatomic, weak) IBOutlet UIButton *controlsButton;
//@property (weak, nonatomic) IBOutlet UIButton *tutorialsButton;
//
//
//@property (nonatomic, weak) IBOutlet UIImageView *newsfeedIcon;
//@property (nonatomic, weak) IBOutlet UIImageView *contactsIcon;
//@property (nonatomic, weak) IBOutlet UIImageView *converterIcon;
//@property (nonatomic, weak) IBOutlet UIImageView *coldSliderIcon;
//@property (nonatomic, weak) IBOutlet UIImageView *literatureHubIcon;
//@property (nonatomic, weak) IBOutlet UIImageView *materialRecommendationIcon;
//@property (nonatomic, weak) IBOutlet UIImageView *controlsIcon;
//@property (weak, nonatomic) IBOutlet UIImageView *tutorialIcon;

@property (nonatomic, weak) IBOutlet UIImageView *newcontrolIcon;

- (IBAction)menuButtonPressed:(id)sender;
- (void) moveButton:(UIButton*) button withPixel:(int) pixel;

@end


@implementation MenuViewController


//@synthesize newsfeedIcon = _newsfeedIcon;
//@synthesize contactsIcon = _contactsIcon;
//@synthesize converterIcon = _converterIcon;
//@synthesize coldSliderIcon = _coldSliderIcon;
//@synthesize literatureHubIcon = _literatureHubIcon;
//@synthesize materialRecommendationIcon = _materialRecommendationIcon;
//
//@synthesize controlsButton = _controlsButton;
//@synthesize newsfeedButton = _newsfeedButton;
//@synthesize contactsButton = _contactsButton;
@synthesize impressumButton = _impressumButton;
//@synthesize converterButton = _converterButton;
//@synthesize coldSliderButton = _coldSliderButton;
//@synthesize literatureHubButton = _literatureHubButton;
//@synthesize materialRecommendationButton = _materialRecommendationButton;


#pragma mark Setup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    // Setup Swipe Gesture
    [self setupSwipeGesture];
    menuTitles = [NSMutableArray arrayWithObjects:  NSLocalizedString(@"Newsfeed", nil),
                                                    NSLocalizedString(@"Material Selection", nil),
                                                    NSLocalizedString(@"Cold Slider", nil),
                                                    NSLocalizedString(@"Converter", nil),
                                                    NSLocalizedString(@"Literature Hub", nil),
                                                    NSLocalizedString(@"Tutorials", nil),
                                                    NSLocalizedString(@"Controls", nil),
                                                    NSLocalizedString(@"Contacts", nil),
                                                    nil];
    
    
    // Setup and Load Button Icons
    //[self setupAndLoadButtonIcons];
    
    // Setup to fit to orientation
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:[UIDevice currentDevice]];
    
//    self.contactsButton.enabled = true;
//    self.materialRecommendationButton.enabled = true;
//    self.converterButton.enabled = true;
//    self.literatureHubButton.enabled = true;
//    self.newsfeedButton.enabled = true;
    self.impressumButton.hidden = false;
//    self.controlsButton.enabled = true;
//    self.coldSliderButton.enabled = true;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.newcontrolIcon.hidden = [[NSUserDefaults standardUserDefaults] boolForKey:kControlPressedKey];
    
}


- (void)setupSwipeGesture {
    
    // Add Swipe Gesture Recognizer To Toggle Menu When Swiping Inside Menu View
    UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(toggleMenu)];
    leftSwipe.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:leftSwipe];
    leftSwipe = nil;
    
}

-(void) moveView:(UIView*) view withPixel:(int) pixel {
    CGRect fr = view.frame;
    fr.origin.y += pixel;
    view.frame = fr;
}
/*
- (void)setupAndLoadButtonIcons {
    
    //repositioning of the buttons in case iPhone5 or newer device
    AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appdelegate.window.frame.size.height > 480.0) {
        CGFloat kSPoint = 30.0;
        CGFloat kSpace = 3.0;
        
        [self moveView:self.newsfeedButton withPixel:kSPoint];
        [self moveView:self.newsfeedIcon withPixel:kSPoint];
        
        [self moveView:self.materialRecommendationButton withPixel:kSPoint + kSpace];
        [self moveView:self.materialRecommendationIcon withPixel:kSPoint + kSpace];
        
        [self moveView:self.coldSliderButton withPixel:kSPoint + 2*kSpace];
        [self moveView:self.coldSliderIcon withPixel:kSPoint + 2*kSpace];
        
        [self moveView:self.converterButton withPixel:kSPoint + 3*kSpace];
        [self moveView:self.converterIcon withPixel:kSPoint + 3*kSpace];
        
        [self moveView:self.literatureHubButton withPixel:kSPoint + 4*kSpace];
        [self moveView:self.literatureHubIcon withPixel:kSPoint + 4*kSpace];
        
        [self moveView:self.tutorialsButton withPixel:kSPoint + 5*kSpace];
        [self moveView:self.tutorialIcon withPixel:kSPoint + 5*kSpace];
        
        [self moveView:self.controlsButton withPixel:kSPoint + 6*kSpace];
        [self moveView:self.controlsIcon withPixel:kSPoint + 6*kSpace];
        [self moveView:self.newcontrolIcon withPixel:kSPoint + 6*kSpace];
        
        [self moveView:self.contactsButton withPixel:kSPoint + 7*kSpace];
        [self moveView:self.contactsIcon withPixel:kSPoint + 7*kSpace];
    }
   
    
    @autoreleasepool {
        
        // Load Button Icon Images
        self.newsfeedIcon.image = kNewsfeedIconImage;
        self.contactsIcon.image = kContactsIconImage;
        self.converterIcon.image = kConverterIconImage;
        self.coldSliderIcon.image = kColdSliderIconImage;
        self.literatureHubIcon.image = kLiteratureHubIconImage;
        self.materialRecommendationIcon.image = kMaterialRecommendationIconImage;
        
        // Set Button Titles
        self.newsfeedButton.alpha = 1.0;
        self.newsfeedButton.titleLabel.minimumScaleFactor = 0.1;
        self.contactsButton.titleLabel.minimumScaleFactor = 0.1;
        self.impressumButton.titleLabel.minimumScaleFactor = 0.1;
        self.converterButton.titleLabel.minimumScaleFactor = 0.1;
        self.coldSliderButton.titleLabel.minimumScaleFactor = 0.1;
        self.literatureHubButton.titleLabel.minimumScaleFactor = 0.1;
        self.materialRecommendationButton.titleLabel.minimumScaleFactor = 0.1;
        self.controlsButton.titleLabel.minimumScaleFactor = 0.1;
        
        self.newsfeedButton.titleLabel.adjustsFontSizeToFitWidth = YES;
        self.contactsButton.titleLabel.adjustsFontSizeToFitWidth = YES;
        self.impressumButton.titleLabel.adjustsFontSizeToFitWidth = YES;
        self.converterButton.titleLabel.adjustsFontSizeToFitWidth = YES;
        self.coldSliderButton.titleLabel.adjustsFontSizeToFitWidth = YES;
        self.literatureHubButton.titleLabel.adjustsFontSizeToFitWidth = YES;
        self.materialRecommendationButton.titleLabel.adjustsFontSizeToFitWidth = YES;
        self.controlsButton.titleLabel.adjustsFontSizeToFitWidth = YES;
        
        [self.newsfeedButton setTitle:kNewsfeedButtonTitle forState:UIControlStateNormal];
        [self.contactsButton setTitle:kContactsButtonTitle forState:UIControlStateNormal];
        [self.converterButton setTitle:kConverterButtonTitle forState:UIControlStateNormal];
        [self.impressumButton setTitle:kInformationButtonTitle forState:UIControlStateNormal];
        [self.coldSliderButton setTitle:kColdSliderButtonTitle forState:UIControlStateNormal];
        [self.literatureHubButton setTitle:kLiteratureHubButtonTitle forState:UIControlStateNormal];
        [self.materialRecommendationButton setTitle:kMaterialRecommendationButtonTitle forState:UIControlStateNormal];
        [self.controlsButton setTitle:kControlsButtonTitle forState:UIControlStateNormal];
    }
}*/

- (void)roundUIImageView:(UIImageView *)imageView {
    
    imageView.layer.cornerRadius = (imageView.frame.size.width/2.0);
    imageView.layer.masksToBounds = YES;
    
}

#pragma mark - Change Orientation

- (void) orientationChanged:(NSNotification *)note
{
    /*
    UIDevice * device = note.object;
    switch(device.orientation)
    {
        case UIDeviceOrientationPortrait:
            self.contactsButton.enabled = true;
            self.materialRecommendationButton.enabled = true;
            self.converterButton.enabled = true;
            self.literatureHubButton.enabled = true;
            self.newsfeedButton.enabled = true;
            self.coldSliderButton.enabled = true;
            self.impressumButton.hidden = false;
            
            break;
            
        case UIDeviceOrientationLandscapeLeft:
            self.contactsButton.enabled = false;
            self.materialRecommendationButton.enabled = false;
            self.converterButton.enabled = false;
            self.literatureHubButton.enabled = false;
            self.newsfeedButton.enabled = false;
            self.coldSliderButton.enabled = false;
            self.impressumButton.hidden = true;
            [self toggleMenu];
            break;
            
        case UIDeviceOrientationLandscapeRight:
            self.contactsButton.enabled = false;
            self.materialRecommendationButton.enabled = false;
            self.converterButton.enabled = false;
            self.literatureHubButton.enabled = false;
            self.newsfeedButton.enabled = false;
            self.coldSliderButton.enabled = false;
            self.impressumButton.hidden = true;
            [self toggleMenu];
            break;
            
        default:
            break;
    };
     
    */
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Data Source Count
    return 8;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return tableView.frame.size.height / 8.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MenuCell *cell = (MenuCell *)[tableView dequeueReusableCellWithIdentifier:@"MenuCell"];
    if(cell == nil) {
        cell = [[MenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MenuCell"];
    }
    NSString* iconName = menuIconNames[indexPath.row];
    NSString* currentTitle = [menuTitles objectAtIndex:indexPath.row];
    [cell.imageView setImage:[UIImage imageNamed:iconName]];
    [cell.menubutton setTitle:currentTitle forState:UIControlStateNormal];
    
    if ([selectedTitle isEqualToString:[menuTitles objectAtIndex:indexPath.row]]) {
        cell.menubutton.alpha = 1.0;
    }
    else {
        cell.menubutton.alpha = kMenuButtonAlpha;
    }

	// no "new" items for now (2018-01)
//    if ([currentTitle isEqualToString:NSLocalizedString(@"Tutorials", nil)] /*|| [currentTitle isEqualToString:NSLocalizedString(@"Controls", nil)]*/) {
//        [cell.menuNewLbl setHidden:NO];
//    }
//    else {
        [cell.menuNewLbl setHidden:YES];
//    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedTitle = [menuTitles objectAtIndex:indexPath.row];
    [self menuButtonPressed:[menuTitles objectAtIndex:indexPath.row]];
}



#pragma mark Menu Methods

- (void) menuButtonPressed:(NSString*) menuTitle {
//    // Reset Alpha On All Button And Update Appropriate Button
//    self.newsfeedButton.alpha = kMenuButtonAlpha;
//    self.contactsButton.alpha = kMenuButtonAlpha;
//    self.converterButton.alpha = kMenuButtonAlpha;
//    self.coldSliderButton.alpha = kMenuButtonAlpha;
//    self.literatureHubButton.alpha = kMenuButtonAlpha;
//    self.materialRecommendationButton.alpha = kMenuButtonAlpha;
//    self.controlsButton.alpha = kMenuButtonAlpha;
//    self.tutorialsButton.alpha = kMenuButtonAlpha;
//    [(UIButton *)sender setAlpha:1.0];
    
    
    // control button
    if ([menuTitle isEqualToString:kControlsButtonTitle]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kControlPressedKey];
      
        self.newcontrolIcon.hidden = YES;
        
        //open the given application that has receiverurl
        NSURL *appURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@://", kControlApplicationName]];
        if ([[UIApplication sharedApplication] canOpenURL:appURL]) {
            [[UIApplication sharedApplication] openURL:appURL];
        } else {
            NSString *iTunesLink = [NSString stringWithFormat:@"https://itunes.apple.com/de/app/apple-store/id%@?mt=8", kControlApplicationId];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
        }
    }
    else {
    
        // Post Notification To Present Selected Menu Item
        [[NSNotificationCenter defaultCenter] postNotificationName:kMenuPresentSelectedItemIdentifier object:menuTitle];
    
    }
    
}


- (void)toggleMenu {
    
    // Post Notification To Toggle Menu
    [[NSNotificationCenter defaultCenter] postNotificationName:kMenuToggleNotificationIdentifier object:self];
    
}


#pragma mark Auto Rotation Methods

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {

    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


#pragma mark Cleanup

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc {
    
    //
    
}


- (IBAction)impressumPressed:(id)sender {
    [self menuButtonPressed:@"Information"];
}

@end
