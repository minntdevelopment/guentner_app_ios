//
//  GUNPopupHelper.m
//  guentner
//
//  Created by Julian Dawo on 07.08.15.
//

#import "GUNPopupHelper.h"
#import "AppDelegate.h"
#import "RootViewController.h"
#import "GUNPopupInfoViewController.h"
#import "GUNPopupSelectionViewController.h"

@implementation GUNPopupHelper


+ (GUNPopupInfoViewController*)showOneButtonStyleAlertViewWithTitle:(NSString *)title messageText:(NSString *)text okButtonBlock:(void (^)())okButtonBlock {
    AppDelegate* appDelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    RootViewController* rootViewController = appDelegate.rootViewController;
    GUNPopupInfoViewController* alertViewController = [[UIStoryboard storyboardWithName:@"Popup" bundle:nil] instantiateViewControllerWithIdentifier:@"GUNPopupInfoViewController"];
    [alertViewController setNoButtonStyleWithTitle:title text:text okButtonBlock:okButtonBlock];
    [rootViewController showPopupViewController:alertViewController];
    return alertViewController;
}

+ (GUNPopupSelectionViewController*)showSelectionWithTitle:(NSString*)title multiselection:(BOOL)multiselection selectableItems:(NSArray *)selectableItems okButtonBlock:(void (^)(NSArray*))okBlock {
    
    AppDelegate* appDelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    RootViewController* rootViewController = appDelegate.rootViewController;
    GUNPopupSelectionViewController* alertViewController = [[UIStoryboard storyboardWithName:@"Popup" bundle:nil] instantiateViewControllerWithIdentifier:@"GUNPopupSelectionViewController"];
    [alertViewController setSelectionWithTitle:title multiselection:multiselection selectableItems:selectableItems okButtonBlock:okBlock];

    [rootViewController showPopupViewController:alertViewController];
    return alertViewController;
    
}


@end
