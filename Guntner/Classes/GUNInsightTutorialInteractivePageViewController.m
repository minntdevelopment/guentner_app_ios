//
// Created by Julian Dawo on 17.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "GUNInsightTutorialInteractivePageViewController.h"
#import "GUNTutorialPageStepResponse.h"
#import "GUNTutorialStepActionResponse.h"
#import "UIView+RZViewActions.h"
#import <QuartzCore/QuartzCore.h>

#import "GUNTutorialManager.h"
#import "GUNRootManager.h"

@implementation GUNInsightTutorialInteractivePageViewController {
    CGFloat _actionDuration;
    RZViewAction *_actionTotalAnimations;
    UIView *_dummy;
    BOOL _stopAnimation;
    GUNTutorialManager *_tutorialManager;
    BOOL _animationIsRunning;
    NSArray* _allActionFlat;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _tutorialManager = [[GUNRootManager instance] tutorialManager];
    _actionDuration = 2.0f;
    _animationIsRunning = NO;
    [self setupSteps];
    
    _hint_text_control.text = [_tutorialManager translationForKeyInActiveLanguage:@"Use the buttons of your device"];
	
	_v_interfaceContainer.layer.shadowColor = color_black_090.CGColor;
	_v_interfaceContainer.layer.shadowOpacity = 0.6;
	_v_interfaceContainer.layer.shadowRadius = 3.0;
	_v_interfaceContainer.layer.shadowOffset = CGSizeMake(0, 3);
	_v_interfaceContainer.layer.masksToBounds = YES;
	_v_interfaceContainer.clipsToBounds = NO;
    
    _v_step1Box.layer.shadowColor = color_black_015.CGColor;
    _v_step1Box.layer.shadowOpacity = 1.0;
    _v_step1Box.layer.shadowRadius = 3.0;
    _v_step1Box.layer.shadowOffset = CGSizeMake(0, 3);
    _v_step1Box.layer.masksToBounds = YES;
    _v_step1Box.clipsToBounds = NO;
    
    _v_step2Box.layer.shadowColor = color_black_015.CGColor;
    _v_step2Box.layer.shadowOpacity = 1.0;
    _v_step2Box.layer.shadowRadius = 3.0;
    _v_step2Box.layer.shadowOffset = CGSizeMake(0, 3);
    _v_step2Box.layer.masksToBounds = YES;
    _v_step2Box.clipsToBounds = NO;
    
    _v_step3Box.layer.shadowColor = color_black_015.CGColor;
    _v_step3Box.layer.shadowOpacity = 1.0;
    _v_step3Box.layer.shadowRadius = 3.0;
    _v_step3Box.layer.shadowOffset = CGSizeMake(0, 3);
    _v_step3Box.layer.masksToBounds = YES;
    _v_step3Box.clipsToBounds = NO;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone && MAX([[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height) >= 568.0) { //iphone5 or higher {
        _c_lastbox_bottom.constant = 50.0;
        [self.view updateConstraintsIfNeeded];
    }
    else {
        _c_v1box_height.constant = 50.0;
        _c_v2box_height.constant = 50.0;
        _c_v3box_height.constant = 50.0;
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
   // init _allAction, combined in one array over all steps
    if (!_allActionFlat) {
        _allActionFlat = [self prepareActionArrayForPage];
    }
    
    //check that the disply is filled with the first action, if the steps are not running
    if (_allActionFlat.count > 0 && !_animationIsRunning ) {
        [self resetUIBeforeAnimationSatrts];
        GUNTutorialStepActionResponse* nextAction = [_allActionFlat firstObject];
        _lbl_displayFirstLine.text = [[[GUNRootManager instance] tutorialManager] translationForKeyInActiveLanguage:nextAction.displayStartLine1Text];
        
        [self activateDisplayIconsForLine:1 andArray:nextAction.displayStartLine1Icons];
        
        _lbl_displaySecondLine.text = [[[GUNRootManager instance] tutorialManager] translationForKeyInActiveLanguage:nextAction.displayStartLine2Text];
        [self activateDisplayIconsForLine:2 andArray:nextAction.displayStartLine2Icons];
        
        
    }
    // dummy view is for the animations, so that the duration works.
    _dummy = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f, 0.0f)];
    [self.view addSubview:_dummy];

    // after the display is visible and the paging is completed, the step animation starts with a delay
    [NSTimer scheduledTimerWithTimeInterval:0.5f
                                         target:self
                                       selector:@selector(_timerFired:)
                                       userInfo:nil
                                        repeats:NO];



}

-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    NSLog(@"Stop all animations");
    
    // when the nex page get visible the old animation stops, to restart it when the user comes back.
    _actionTotalAnimations = nil;
    _stopAnimation = YES;
    _animationIsRunning = NO;
    [_dummy.layer removeAllAnimations];
    
}


-(void) setupSteps {

     _v_step1Box.hidden = YES;
    _v_step2Box.hidden = YES;
    _v_step3Box.hidden = YES;
    
    for (int i = 0; i < _pageSteps.count; i++) {

        if (i == 0) {
            GUNTutorialPageStepResponse *response1 = _pageSteps[i];
            // setUp
            _v_step1Box.hidden = NO;
            
            _lbl_firstLineBox1.text = [self adapteStringWithUpperCase:[[_tutorialManager translationForKeyInActiveLanguage:response1.stepDescriptionLine1 ] uppercaseString]];
            _lbl_secondLineBox1.text = [_tutorialManager translationForKeyInActiveLanguage:response1.stepDescriptionLine2 ];

        } else if (i == 1) {
            GUNTutorialPageStepResponse *response2 = _pageSteps[i];
            // setUp
            _v_step2Box.hidden = NO;
            
            _lbl_firstLineBox2.text = [self adapteStringWithUpperCase:[[_tutorialManager translationForKeyInActiveLanguage:response2.stepDescriptionLine1 ] uppercaseString]];
            _lbl_secondLineBox2.text = [_tutorialManager translationForKeyInActiveLanguage:response2.stepDescriptionLine2 ];
            
        } else if (i == 2) {
            GUNTutorialPageStepResponse *response3 = _pageSteps[i];
            // setUp
            _v_step3Box.hidden = NO;
//            _lbl_firstLineBox3.text = [response3.stepDescriptionLine1 uppercaseString];
//            _lbl_secondLineBox3.text = [response3.stepDescriptionLine2 uppercaseString];
            
            _lbl_firstLineBox3.text = [self adapteStringWithUpperCase:[[_tutorialManager translationForKeyInActiveLanguage:response3.stepDescriptionLine1 ] uppercaseString]];
            _lbl_secondLineBox3.text = [_tutorialManager translationForKeyInActiveLanguage:response3.stepDescriptionLine2 ] ;
        }

        [self changeColor:color_black_090 ofTextInBox:i];
    }
}

-(void)changeColor:(UIColor *) color ofTextInBox:(NSUInteger) index {

    switch (index) {
        case 0:
            _lbl_numberBox1.textColor = color;
            _lbl_firstLineBox1.textColor = color;
            _lbl_secondLineBox1.textColor = color;
            break;
        case 1:
            _lbl_numberBox2.textColor = color;
            _lbl_firstLineBox2.textColor = color;
            _lbl_secondLineBox2.textColor = color;
            break;

        case 2:
            _lbl_numberBox3.textColor = color;
            _lbl_firstLineBox3.textColor = color;
            _lbl_secondLineBox3.textColor = color;
            break;
    }


}

///TODO: In Manager verschieben.
-(NSArray*) prepareActionArrayForPage {
    
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < _pageSteps.count; i++) {
        GUNTutorialPageStepResponse *stepResponse = _pageSteps[i];
        long counter = stepResponse.stepActions.count;
        for (id item in stepResponse.stepActions) {
            GUNTutorialStepActionResponse* curAction = item;
            curAction.displayButtonType = [stepResponse.actionsButtonType integerValue];
            curAction.activeStepIndex = i;

            if (stepResponse.showEqualActionCount) {
                curAction.actionCountText = [NSString stringWithFormat:@"%ldx",counter];
            } else {
                curAction.actionCountText = @"";
            }
        
            [result addObject:curAction];
        }
    }
    return result;
}

-(void)animateStepByStep:(GUNTutorialStepActionResponse *) currentAction missingActions:(NSArray *) missingActions {
   
    _dummy.alpha = 0.0;
    _dummy.hidden = YES;
    _animationIsRunning = YES;
    
    float firstStepDuration = 0.6f;
    float secondStepDuration = 0.5f;
    float thirdStepDuration = 0.5f;
    
    [self changeColor:color_red ofTextInBox:currentAction.activeStepIndex];
    [UIView animateWithDuration:0.0 delay:0.0 options:(UIViewAnimationOptionTransitionCrossDissolve| UIViewAnimationOptionAllowUserInteraction) animations:^{
       //start
        _dummy.alpha = 1.0;
    } completion:^(BOOL finished) {
        if (_stopAnimation) {
            return ;
        }
        NSLog(@"completed.");
        
        /* FIRST STEP IN ANIMATION*/
        // set display Text start and display icons
        NSString *newTextFirstLine = [[[GUNRootManager instance] tutorialManager] translationForKeyInActiveLanguage:currentAction.displayStartLine1Text];
        if (![newTextFirstLine isEqualToString:_lbl_displayFirstLine.text]) {
            _lbl_displayFirstLine.text =  newTextFirstLine;
        }
        
        NSString *newTextSecondLine = [[[GUNRootManager instance] tutorialManager] translationForKeyInActiveLanguage:currentAction.displayStartLine2Text];
        if (![newTextSecondLine isEqualToString:_lbl_displayFirstLine.text]) {
            _lbl_displaySecondLine.text = newTextSecondLine;
            
        }
   
        [self activateDisplayIconsForLine:1 andArray:currentAction.displayStartLine1Icons];
        
//        _lbl_displaySecondLine.text = [[[GUNRootManager instance] tutorialManager] translationForKeyInActiveLanguage:currentAction.displayStartLine2Text];
        [self activateDisplayIconsForLine:2 andArray:currentAction.displayStartLine2Icons];
        
        
        [self counter:currentAction.actionCountText forbutton:currentAction.displayButtonType];
        
        /* done - FIRST STEP IN ANIMATION*/
        
        
        [UIView animateWithDuration:firstStepDuration delay:0.0 options:(UIViewAnimationOptionTransitionCrossDissolve| UIViewAnimationOptionAllowUserInteraction) animations:^{
            NSLog(@"wating  after setting display Text");
            _dummy.alpha = 0.0;
            self.view.alpha = 1.0;
        } completion:^(BOOL finished) {
            
            // if user interupts the animation by going to another step, the animation gets interuppted
            if (_stopAnimation) {
                return ;
            }
            
            /* SECOND STEP IN ANIMATION*/
            //simulate the Button tap and show the ending text and icons
            
            [self simulateButtonTapFor:currentAction.displayButtonType push:YES];
           
            _lbl_displayFirstLine.text = [[[GUNRootManager instance] tutorialManager] translationForKeyInActiveLanguage:currentAction.displayEndLine1Text];
            [self activateDisplayIconsForLine:1 andArray:currentAction.displayEndLine1Icons];
            
            _lbl_displaySecondLine.text = [[[GUNRootManager instance] tutorialManager] translationForKeyInActiveLanguage:currentAction.displayEndLine2Text];
            [self activateDisplayIconsForLine:2 andArray:currentAction.displayEndLine2Icons];
            
            /* done - SECOND STEP IN ANIMATION*/
            
            [UIView animateWithDuration:secondStepDuration delay:0.0 options:(UIViewAnimationOptionTransitionCrossDissolve| UIViewAnimationOptionAllowUserInteraction) animations:^{
                
                self.view.alpha = 1.0;
                
                _dummy.alpha = 1.0;
            } completion:^(BOOL finished) {
                if (_stopAnimation) {
                    return ;
                }
                /* THIRD STEP IN ANIMATION*/
                
                //simulate the Button release
                [self simulateButtonTapFor:currentAction.displayButtonType push:NO];
//                _lbl_displayFirstLine.text = [[[GUNRootManager instance] tutorialManager] translationForKeyInActiveLanguage:currentAction.displayEndLine1Text];
//             
//                [self activateDisplayIconsForLine:1 andArray:currentAction.displayEndLine1Icons];
//
//                _lbl_displaySecondLine.text = [[[GUNRootManager instance] tutorialManager] translationForKeyInActiveLanguage:currentAction.displayEndLine2Text];
//                
//                [self activateDisplayIconsForLine:2 andArray:currentAction.displayEndLine2Icons];
                /* done - THIRD STEP IN ANIMATION*/
                [UIView animateWithDuration:thirdStepDuration delay:0.0 options:(UIViewAnimationOptionTransitionCrossDissolve| UIViewAnimationOptionAllowUserInteraction) animations:^{
                    self.view.alpha = 1.0;
                    
                    /* WAITING STEP IN ANIMATION*/
                    
                    NSLog(@"wating  after releasing the button and setting end text");
                    _dummy.alpha = 0.0;
                } completion:^(BOOL finished) {
                    
                    if (_stopAnimation) {
                        return ;
                    }
                    //unhighlight box
                    [self changeColor:[UIColor blackColor] ofTextInBox:currentAction.activeStepIndex];
                    
                    NSLog(@"Animation of single action step done.");
                    GUNTutorialStepActionResponse* nextAction;
                    NSMutableArray *newMissingArray;
                    if (missingActions.count > 0) {
                        nextAction = [missingActions firstObject];
                        newMissingArray = [[NSMutableArray alloc] initWithArray:missingActions];
                        [newMissingArray removeObjectAtIndex:0];
                        NSLog(@"Next Action is available");
                        [self animateStepByStep:nextAction missingActions:[newMissingArray copy]];
                    } else {
                        NSLog(@"Animation of all steps done.");
                        [self counter:@"" forbutton:enterControllerButtonType];
                        _animationIsRunning = NO;
                    }
                    
                }];
            }];
        }];
    }];
}

-(void) simulateButtonTapFor:(ControllerButtonType) controllerButtonType push:(BOOL) push {
    
    UIImageView *tmpButton;
    UIImage *activeImage;
    UIImage *inactiveImage;
    
    if (controllerButtonType == enterControllerButtonType) {
        tmpButton = _iv_displayEnter;
        activeImage = [UIImage imageNamed:@"G_Button_Enter_rot.png"];
        inactiveImage = [UIImage imageNamed:@"G_Button_Enter_blau.png"];
        
    } else if (controllerButtonType == arrowDownControllerButtonType) {
        tmpButton = _iv_displayDown;
        activeImage = [UIImage imageNamed:@"G_Button_PfeilU_rot.png"];
        inactiveImage = [UIImage imageNamed:@"G_Button_PfeilU_weiss.png"];
    } else if (controllerButtonType == arrowUpControllerButtonType) {
        tmpButton = _iv_displayUp;
        activeImage = [UIImage imageNamed:@"G_Button_PfeilO_rot.png"];
        inactiveImage = [UIImage imageNamed:@"G_Button_PfeilO_weiss.png"];
    } else if (controllerButtonType == arrowLeftControllerButtonType) {
        tmpButton = _iv_displayLeft;
        activeImage = [UIImage imageNamed:@"G_Button_PfeilLI_rot.png"];
        inactiveImage = [UIImage imageNamed:@"G_Button_PfeilLI_weiss.png"];
    } else if (controllerButtonType == arrowRightControllerButtonType) {
        tmpButton = _iv_displayRight;
        activeImage = [UIImage imageNamed:@"G_Button_PfeilRE_rot.png"];
        inactiveImage = [UIImage imageNamed:@"G_Button_PfeilRE_weiss.png"];
    } else if (controllerButtonType == homeControllerButtonType) {
        tmpButton = _iv_displayHome;
        activeImage = [UIImage imageNamed:@"G_Button_X_rot.png"];
        inactiveImage = [UIImage imageNamed:@"G_Button_X_weiss.png"];
    } else {
        tmpButton = _iv_displayEnter;
        activeImage = [UIImage imageNamed:@"G_Button_Pfeil_rot.png"];
        inactiveImage = [UIImage imageNamed:@"G_Button_Pfeil_weiss.png"];
    }
    
    
    if (push) {
        [tmpButton setImage:activeImage];
    } else {
        [tmpButton setImage:inactiveImage];
    }
    
}

-(void) counter:(NSString *)counterText forbutton:(ControllerButtonType) controllerButtonType {
   
    _lbl_displayCounterBtnDown.text = @"";
    _lbl_displayCounterBtnUp.text = @"";
    _lbl_displayCounterBtnLeft.text = @"";
    _lbl_displayCounterBtnRight.text = @"";
    
    
    if (controllerButtonType == arrowDownControllerButtonType) {
        _lbl_displayCounterBtnDown.text = counterText;
    } else if (controllerButtonType == arrowUpControllerButtonType) {
        _lbl_displayCounterBtnUp.text = counterText;
    } else if (controllerButtonType == arrowLeftControllerButtonType) {
        _lbl_displayCounterBtnLeft.text = counterText;
    } else if (controllerButtonType == arrowRightControllerButtonType) {
        _lbl_displayCounterBtnRight.text =counterText;
    }
}



-(void) activateDisplayIconsForLine:(int) lineNumber andArray:(NSArray *) displayIconsInLine {
    UIImageView *displayIcon1;
    UIImageView *displayIcon2;
    UIImageView *displayIcon3;
    UIImageView *displayIcon4;
    
    if (lineNumber == 1) {
        displayIcon1 = _iv_displayFirstLineIcon1;
        displayIcon2 = _iv_displayFirstLineIcon2;
        displayIcon3 = _iv_displayFirstLineIcon3;
        displayIcon4 = _iv_displayFirstLineIcon4;
    
    } else if (lineNumber == 2) {
        displayIcon1 = _iv_displaySecondLineIcon1;
        displayIcon2 = _iv_displaySecondLineIcon2;
        displayIcon3 = _iv_displaySecondLineIcon3;
        displayIcon4 = _iv_displaySecondLineIcon4;
    }
    
    displayIcon1.hidden = YES;
    displayIcon2.hidden = YES;
    displayIcon3.hidden = YES;
    displayIcon4.hidden = YES;
    
    for (int i = 0; i<displayIconsInLine.count; i++) {
        
        NSUInteger tmp = [((NSNumber *)[displayIconsInLine objectAtIndex: i]) integerValue];
        
        switch (i) {
            case 0:
                
                [displayIcon1 setImage:[self imageForIconType: tmp]];
                displayIcon1.hidden = NO;
                break;
            case 1:
                [displayIcon2 setImage:[self imageForIconType: tmp]];
                displayIcon2.hidden = NO;
                break;
            case 2:
                [displayIcon3 setImage:[self imageForIconType: tmp]];
                displayIcon3.hidden = NO;
                break;
            case 3:
                [displayIcon4 setImage:[self imageForIconType: tmp]];
                displayIcon4.hidden = NO;
                break;
                
            default:
                break;
        }
    }
    
    
}


-(void) resetUIBeforeAnimationSatrts {
    _lbl_displaySecondLine.text = @"";
    _lbl_displayFirstLine.text = @"";

    _iv_displaySecondLineIcon1.hidden = YES;
    _iv_displaySecondLineIcon2.hidden = YES;
    _iv_displaySecondLineIcon3.hidden = YES;
    _iv_displaySecondLineIcon4.hidden = YES;

    _iv_displayFirstLineIcon1.hidden = YES;
    _iv_displayFirstLineIcon2.hidden = YES;
    _iv_displayFirstLineIcon3.hidden = YES;
    _iv_displayFirstLineIcon4.hidden = YES;
    
    _lbl_displayCounterBtnDown.text = @"";
    _lbl_displayCounterBtnUp.text = @"";
    _lbl_displayCounterBtnLeft.text = @"";
    _lbl_displayCounterBtnRight.text = @"";

    
    [self changeColor:color_black_090 ofTextInBox:0];
    [self changeColor:color_black_090 ofTextInBox:1];
    [self changeColor:color_black_090 ofTextInBox:2];
}

///TODO: In Helper auslager oder Manager
-(UIImage *) imageForIconType:(NSUInteger) displayIcon {

    if (displayIcon == arrowAIcon) {
        return [UIImage imageNamed:@"G_Display_A.png"];
    } else if (displayIcon == arrowLeftIcon) {
        return [UIImage imageNamed:@"G_Display_links.png"];
    } else if (displayIcon == arrowRightIcon) {
        return [UIImage imageNamed:@"G_Display_rechts.png"];
    } else if (displayIcon == twoArrowsIcon) {
        return [UIImage imageNamed:@"G_Display_ouPfeil.png"];
    } else if (displayIcon == starIcon) {
        return [UIImage imageNamed:@"G_Display_Stern.png"];
    }  else if (displayIcon == blackArrowIcon) {
        return [UIImage imageNamed:@"G_Pfeil_runter_gross.png"];
    }  else if (displayIcon == enterIcon) {
        return [UIImage imageNamed:@"G_Display_Enter.png"];
    }
    
    return nil;
}

-(NSString *) adapteStringWithUpperCase:(NSString *)inputString {
   
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[0-9]{1,2}X " options:NSRegularExpressionCaseInsensitive error:&error];
    NSRange hit = [regex rangeOfFirstMatchInString:inputString options:NSMatchingReportProgress range:NSMakeRange(0, [inputString length])];
    NSInteger positionOfX = hit.location + hit.length - 2;
    if (error == nil && hit.length > 0) {
        NSString *result = [inputString stringByReplacingCharactersInRange:NSMakeRange(positionOfX, 1) withString:@"x"];
        return result;
    } else {
        return inputString;
    }
    
}


- (void)_timerFired:(NSTimer *)timer {
    
    NSArray *allActions = [self prepareActionArrayForPage];
    GUNTutorialStepActionResponse* nextAction;
    NSMutableArray *newMissingArray;
    
    if (allActions.count > 0 && !_animationIsRunning ) {
        nextAction = [allActions firstObject];
        newMissingArray = [[NSMutableArray alloc] initWithArray:allActions];
        [newMissingArray removeObjectAtIndex:0];
//        [self resetUIBeforeAnimationSatrts];
       
        
        [self animateStepByStep:nextAction missingActions:[newMissingArray copy]];
        _stopAnimation = NO;
    }
    
}

@end