//
//  NewsObject.h
//  guentner
//
//  Created by Balazs Auth on 02.12.18.
//  Copyright © 2018 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NewsObject : NSObject

@property (nonatomic, strong) NSNumber* createdDate;
@property (nonatomic, strong) NSString* imageLink;
@property (nonatomic, strong) NSString* text;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSNumber* articleId;
@property (assign) BOOL isLinkedin;


-(instancetype) initWithId:(NSNumber* )articleId imageLink:(NSString*)imageLink  text:(NSString*)text title:(NSString*)title createdDate:(NSNumber*)cretedDate;
@end

