//
//  GUNHelper.h
//  guentner
//
//  Created by Julian Dawo on 06.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GUNHelper : NSObject


/**
 *  Creates a new UUID and removes all '-' from the result
 *
 *  @return the new UUID
 */
+ (NSString*)generateUUID;
@end
