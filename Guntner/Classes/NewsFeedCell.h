//
//  NewsFeedCell.h
//  guentner
//
//  Created by Balazs Auth on 03.12.18.
//  Copyright © 2018 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsFeedCell : UITableViewCell

-(void) setupCellWithTitle:(NSString*) title text:(NSString*)text createdDate:(NSNumber*)createdDate rowNr:(NSUInteger) rowNr;
-(void) setLinkedinNews:(BOOL) isLinkedin;

@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;

@property (weak, nonatomic) IBOutlet UIImageView *iv_linkedin;
@property (weak, nonatomic) IBOutlet UITextView *textview_article;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *c_tv_height;
@property (weak, nonatomic) IBOutlet UIView *v_container;

@end
