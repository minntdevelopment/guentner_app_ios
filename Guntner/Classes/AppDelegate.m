//
//  AppDelegate.m
//  Guntner
//
//  Created by Shiraz Omar on 8/5/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

//#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "Firebase.h"
#import "AppDelegate.h"

#import "GAI.h"
#import "Configuration.h"
#import "ViewDeckController.h"
#import "Preferences.h"
//#import "TestFlight.h"
#import "RootViewController.h"

@interface AppDelegate () <ViewDeckControllerDelegate> {
    
    BOOL shouldToggleViewDeck;
    
    ViewDeckController *viewDeckController;

}


@end


@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    //[Fabric with:@[CrashlyticsKit]];

    // Remove Shadow From Navigation Bar
    [[UINavigationBar appearance]setShadowImage:[[UIImage alloc] init]];
    
    //[[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    // Set Navigation Bar Font
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:kNavigationBarTitleFontColor, UITextAttributeTextColor, kNavigationBarTitleFont, UITextAttributeFont, nil]];
    
    
    // TestFlight
    //[TestFlight takeOff:kTestFlightToken];

    //[TestFlight takeOff:kTestFlightToken];
    
    
    // Allow ViewDeck To Toggle
    shouldToggleViewDeck = YES;
    
    
    // Setup Google Analytics
    if ([[NSUserDefaults standardUserDefaults] boolForKey:kGAnalyticsSwitchState]) {
        // Turn Off Analytics
        [GAI sharedInstance].trackUncaughtExceptions = YES; // Automatically send uncaught exceptions to Google Analytics.
        [[GAI sharedInstance] trackerWithTrackingId:@""]; // Initialize Tracker
    } else {
        // Turn On Analytics
        [GAI sharedInstance].trackUncaughtExceptions = YES; // Automatically send uncaught exceptions to Google Analytics.
        [[GAI sharedInstance] trackerWithTrackingId:kGAnalyticsTrackingID]; // Initialize Tracker
    }
    
    
    // Override point for customization after application launch.
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
    
    // Setup Menu Controller Using ViewDeckController and Nib's From Storyboard
    viewDeckController =  [[ViewDeckController alloc] initWithCenterViewController:[storyboard instantiateViewControllerWithIdentifier:kNavigationController] leftViewController:[storyboard instantiateViewControllerWithIdentifier:kMenuViewController]];
    viewDeckController.delegate = self;
    CGFloat screenWidth = self.window.bounds.size.width;
    
    
    // Set ViewDeckPanning Mode
    viewDeckController.panningMode = ViewDeckDelegatePanning;
    
    // Adjust Menu Opening Size
    [viewDeckController setLeftSize:(screenWidth - kMenuViewWidth + kMenuViewPadding)]; // 60.00
    
    
    // Set ViewDeckController As RootViewController
    self.window.rootViewController = viewDeckController;
    [self.window makeKeyAndVisible];
    
    
    // Add Notification Listener For Toggle Menu
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(menuButtonPressed) name:kMenuToggleNotificationIdentifier object:nil]; // Listen For Notifications Posted When Menu Toggle Is Pressed
    
    // Add Notification Listener For Enable/Disable ViewDeck Toggle
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(toggleViewDeck:) name:kEnableDisableViewDeckToggleNotificationIdentifier object:nil];
    
    if([Preferences getPopupViewedFlag] == false)
    {
        [self incrementAppOpenCountBy1];
    }
    
    [FIRApp configure];
    return YES;
}

-(ViewDeckController *)getViewDeckController
{
    return viewDeckController;
}


-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    // marpinion://?username=123462&password=ziG2pina2TDzNIUM
//    NSDictionary* parsedQueryAsDictionary = [MARHelper parseQueryStringToDictionary:[url query]];
    
//    if ([parsedQueryAsDictionary objectForKey:@"pw"] != nil && [parsedQueryAsDictionary objectForKey:@"user"] != nil) {
//        DDLogDebug(@"Username:%@", [parsedQueryAsDictionary objectForKey:@"user"]);
//        DDLogDebug(@"Password:%@", [parsedQueryAsDictionary objectForKey:@"pw"]);
//        
//        MARRootViewController* rootViewController = self.rootViewController;
//        [rootViewController showLoginViewControllerAndForceLogoutWithPrefillUsername:[parsedQueryAsDictionary objectForKey:@"user"] password:[parsedQueryAsDictionary objectForKey:@"pw"]];
//        return YES;
//    }
    return YES;
}


#pragma mark Method To Round Corners
- (void)roundViewsCorners:(UIView *)view {
    
    // Set View's Corner Radius To 5px To Round The Corners
    //view.layer.cornerRadius = 5.0;
    view.layer.masksToBounds = YES;
    
    // Set OK Button Layer Mask So It Doesn't Override ContentView's Rounded Corners
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerTopLeft cornerRadii:(CGSize){5.0, 5.0}].CGPath;
    view.layer.mask = maskLayer;
    
}


#pragma mark Menu Show/Hide Methods

- (void)menuButtonPressed {
    
    [viewDeckController toggleLeftViewAnimated:YES];
    
    if ([viewDeckController isSideClosed:ViewDeckLeftSide]) {
        viewDeckController.centerController.view.layer.mask = nil;
        viewDeckController.centerController.view.layer.masksToBounds = NO;
        [[[(UINavigationController*)viewDeckController.centerController topViewController] view] setUserInteractionEnabled:YES];
        
    } else {
        // Round Center View Corners
        [[[(UINavigationController*)viewDeckController.centerController topViewController] view] setUserInteractionEnabled:NO];
        [self roundViewsCorners:viewDeckController.centerController.view];
    }
    
}

#pragma mark ViewDeckController Delegate Methods
- (BOOL)viewDeckController:(ViewDeckController *)viewDeckController shouldOpenViewSide:(ViewDeckSide)viewDeckSide {
    
    return shouldToggleViewDeck;
    
}

- (BOOL)viewDeckController:(ViewDeckController *)viewDeckController shouldPan:(UIPanGestureRecognizer*)panGestureRecognizer {
    
    CGPoint touchPoint = [panGestureRecognizer locationInView:panGestureRecognizer.view];
    if (touchPoint.x > (self.window.bounds.size.width/4.0)) {
        return NO;
    } else {
        return YES;
    }
    
}

- (void)viewDeckController:(ViewDeckController *)viewDController didOpenViewSide:(ViewDeckSide)viewDeckSide animated:(BOOL)animated {
    
    // Round Center View Corners
    [self roundViewsCorners:viewDeckController.centerController.view];
    [[[(UINavigationController*)viewDeckController.centerController topViewController] view] setUserInteractionEnabled:NO];
    
    [[[(UINavigationController*)viewDeckController.centerController topViewController] view] endEditing:YES];
}

- (void)viewDeckController:(ViewDeckController *)viewDController didCloseViewSide:(ViewDeckSide)viewDeckSide animated:(BOOL)animated {
    
    viewDeckController.centerController.view.layer.mask = nil;
    viewDeckController.centerController.view.layer.masksToBounds = NO;
    [[[(UINavigationController*)viewDeckController.centerController topViewController] view] setUserInteractionEnabled:YES];
}

#pragma mark Enable/Disable ViewDeck Methods
- (void)toggleViewDeck:(NSNotification *)itemNotification {
    
    // Update BOOL to Enable/Disable ViewDeck Toggle
    shouldToggleViewDeck = [[itemNotification object] boolValue];
    
}


#pragma mark Multitasking Methods

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kMenuToggleNotificationIdentifier object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kEnableDisableViewDeckToggleNotificationIdentifier object:nil];
}


#pragma mark PhoneGap Methods

// this happens while we are running ( in the background, or from within our own app )
// only valid if Guntner-Info.plist specifies a protocol to handle
- (BOOL)application:(UIApplication*)application handleOpenURL:(NSURL*)url
{
    if (!url) {
        return NO;
    }
    
    // calls into javascript global function 'handleOpenURL'
    //**NSString* jsString = [NSString stringWithFormat:@"handleOpenURL(\"%@\");", url];
    //**[self.phoneGapViewController.webView stringByEvaluatingJavaScriptFromString:jsString];
    
    // all plugins will get the notification, and their handlers will be called
    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:CDVPluginHandleOpenURLNotification object:url]];
    
    return YES;
}


// repost the localnotification using the default NSNotificationCenter so multiple plugins may respond
- (void)application:(UIApplication*)application didReceiveLocalNotification:(UILocalNotification*)notification
{
    // re-post ( broadcast )
    [[NSNotificationCenter defaultCenter] postNotificationName:CDVLocalNotification object:notification];
}

#pragma mark App Rating Count feature
- (void)incrementAppOpenCountBy1
{
    //Get App Open Count from preferences
    int currentOpenAppCount = [Preferences getAppOpenCount];
    
    //Increment this value as we started this app
    currentOpenAppCount++;
    
    //If the currentOpenAppCount Value equals a defined value
    if(currentOpenAppCount == kAppRatingCountTrigger)
    {
        [self showAppRatingAlertPopup];
        [Preferences setPopupViewedFlag:YES];
    }
    
    //Set updated App Open Count
    [Preferences setAppOpenCount:currentOpenAppCount];
}

- (void) showAppRatingAlertPopup
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"RateAppHeader", nil) message:NSLocalizedString(@"RateAppText", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"No", nil) otherButtonTitles:@"OK", nil];
    
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //0 = Tapped yes
    if(buttonIndex == 0)
    {
        //NSLog(@"[+] You clicked on NO");
    }
    else
    {
        //NSLog(@"[+] You clicked on OK");
        
        NSString *currentOSVersion = [[UIDevice currentDevice] systemVersion];
        NSString *templateReviewURL = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=727511951";
        NSString *templateReviewURLiOS7 = @"itms-apps://itunes.apple.com/app/id727511951";
        
        if([currentOSVersion hasPrefix:@"7."])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString: templateReviewURLiOS7]];
        }
        else
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString: templateReviewURL]];
        }
    }
}

@end


@implementation MainCommandDelegate

/* To override the methods, uncomment the line in the init function(s)
 in PhoneGapViewController's
 */

#pragma mark CDVCommandDelegate implementation

- (id)getCommandInstance:(NSString*)className
{
    return [super getCommandInstance:className];
}

/*
 NOTE: this will only inspect execute calls coming explicitly from native plugins,
 not the commandQueue (from JavaScript). To see execute calls from JavaScript, see
 MainCommandQueue below
 */
- (BOOL)execute:(CDVInvokedUrlCommand*)command
{
    return [super execute:command];
}

- (NSString*)pathForResource:(NSString*)resourcepath;
{
    return [super pathForResource:resourcepath];
}

@end


@implementation MainCommandQueue

/* To override, uncomment the line in the init function(s)
 in PhoneGapViewController's
 */
- (BOOL)execute:(CDVInvokedUrlCommand*)command
{
    return [super execute:command];
}

@end
