//
// Created by Julian Dawo on 08.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "SelectionItemTO.h"


@implementation SelectionItemTO {

}

- (instancetype)initWithKey:(NSObject *)key andTranslatedValue:(NSString *)translatedValue selectionState: (BOOL) selected {
    self = [super init];

    if (self) {
        _key = key;
        _translatedValue = translatedValue;
        _isSelected = selected;
    }
    return self;
}
@end