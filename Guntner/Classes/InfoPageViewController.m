//
//  InfoPageViewController.m
//  Guntner
//
//  Created by Shiraz Omar on 10/9/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "InfoPageViewController.h"

#import "Configuration.h"
#import "Firebase.h"

@interface InfoPageViewController () {
    
    IBOutlet UITextView *textView;
    IBOutlet UIImageView *imageView;
    
}

@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backButton;

@end


@implementation InfoPageViewController


@synthesize textView = _textView;
@synthesize imageView = _imageView;


#pragma mark Setup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    // Show Navigation Bar
    self.navigationController.navigationBarHidden = NO;
    
    
    // Set Title
    self.title = kInfoPageViewTitle;
    [self.backButton setTitle:kBackButtonTitle];
    [self.navBar.topItem setTitle:kInfoPageViewTitle];
    
    // Setup Back Button
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:kBackButtonFrame];
    [cancelButton setTitleColor:kBackButtonTitleColor forState:UIControlStateNormal];
    [[cancelButton titleLabel] setFont:kBackButtonTitleFont];
    [cancelButton setTitle:kInfoPageViewBackButtonTitle forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(dismissInfoPageView) forControlEvents:UIControlEventTouchUpInside];
    self.navigationController.topViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:cancelButton];
    
    
    // Populate View With Information
    [self populateView];
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    // Send Google Analytics Information
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:kInfoPageViewControllerIdentifier];
    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    [FIRAnalytics logEventWithName:kGAIScreenName
                        parameters:@{
                                     kFIRParameterItemName:kInfoPageViewControllerIdentifier,
                                     kFIRParameterContentType:@"screen"
                                     }];
    
}


#pragma mark ImageView and TextView Population Methods

- (void)populateView {
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:kInfoPageViewTextViewString];
    
    NSRange stringRange = NSMakeRange(0, string.length);
    NSRange firstBoldRange;
    NSRange secondBoldRange;
    
    if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqual:kGermanLocale]) {
        firstBoldRange = NSMakeRange(0, 20); // 20 characters, starting at index 0
        secondBoldRange = NSMakeRange(63, 17); // 17 characters, starting at index 63
    } else {
        firstBoldRange = NSMakeRange(0, 25); // 25 characters, starting at index 0
        secondBoldRange = NSMakeRange(80, 22); // 22 characters, starting at index 79
    }
    
    [string beginEditing];
    
    [string addAttribute:NSFontAttributeName
                   value:kInfoPageTextViewFont
                   range:stringRange];
    
    [string addAttribute:NSFontAttributeName
                   value:kInfoPageTextViewBoldFont
                   range:firstBoldRange];
    
    [string addAttribute:NSFontAttributeName
                   value:kInfoPageTextViewBoldFont
                   range:secondBoldRange];
    
    [string endEditing];
    
    
    self.textView.attributedText = string;
    string = nil;
    
}

- (void)dismissInfoPageView {
    
    CATransition *animation = [CATransition animation];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromBottom];
    [animation setDuration:0.5];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:
                                  kCAMediaTimingFunctionEaseInEaseOut]];
    
    [self.navigationController.view.layer addAnimation:animation forKey:@"slideUpDown"];
    
    [self.navigationController popViewControllerAnimated:NO];
    
    [CATransaction commit];
    
}


#pragma mark Auto Rotation Methods



- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


#pragma mark Cleanup

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)dismissVc:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dealloc {
    
    //
    
}


@end
