//
//  TutorialProductItemCell.h
//  guentner
//
//  Created by Julian Dawo on 04.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialProductItemCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_productName;

@end
