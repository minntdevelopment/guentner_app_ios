//
//  GUNLoadingViewController.m
//  guentner
//
//  Created by Julian Dawo on 06.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "GUNLoadingViewController.h"
#import "AppDelegate.h"
#import "GUNHelper.h"
#import "RootViewController.h"
#import "GUNRootManager.h"
#import "GUNTutorialManager.h"

@interface GUNLoadingViewController () {
    
        NSMutableArray* _handles;
        NSMutableDictionary* _handlesToLoadingTexts;
        BOOL _isShowing;
    
    
    
}

@end

@implementation GUNLoadingViewController

static GUNLoadingViewController *_currentInstance;
NSTimer *_timer;


+ (GUNLoadingViewController* ) instance {
    if(!_currentInstance) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Tutorials" bundle:nil];
        _currentInstance = [storyboard instantiateViewControllerWithIdentifier:@"GUNLoadingViewController"];
        [_currentInstance view];
    }
    
    return _currentInstance;
}

- (id)initWithCoder:(NSCoder*)coder {
    self = [super initWithCoder:coder];
    if (self) {
        _handles = [NSMutableArray array];
        _handlesToLoadingTexts = [NSMutableDictionary dictionary];
        self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    }
    
    return self;
}

-(void)dealloc {
    NSLog(@"MARLoadingViewController dealloced");
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.view.alpha = 0.0f;
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.25f
                                              target:self
                                            selector:@selector(_timerFired:)
                                            userInfo:nil
                                             repeats:NO];
    
    [_iv_logo setImage:[UIImage animatedImageNamed:@"G_LogoAni" duration:1.5f]];
}

+ (NSString*)pushLoadingViewController {
    return [self pushLoadingViewControllerWithText:defaultLabelText];
}

+ (NSString*)pushLoadingViewControllerWithText:(NSString*)loadingText {
    GUNLoadingViewController* sharedViewController = [self instance];
    return [sharedViewController pushLoadingViewControllerWithText:loadingText];
}

+ (void)popLoadingViewController:(NSString*)handle {
    GUNLoadingViewController* sharedViewController = [self instance];
    [sharedViewController popLoadingViewController:handle];
}

- (void)popLoadingViewController:(NSString*)handle {
    [_handles removeObject:handle];
    [_handlesToLoadingTexts removeObjectForKey:handle];
    [self updateLabel];
    if(_handles.count == 0) {
        RootViewController* activeRootViewController = [((AppDelegate *) [[UIApplication sharedApplication] delegate]) rootViewController];
        [activeRootViewController dismissLoadingViewController];
        _isShowing = NO;
        [_timer invalidate];
        _timer = nil;
        _currentInstance = nil;
    }
}

-(NSString*)pushLoadingViewControllerWithText:(NSString*)loadingText {
    if(loadingText.length == 0) {
        loadingText = defaultLabelText;
    }
    NSString* handle = [GUNHelper generateUUID];
    [_handles addObject:handle];
    [_handlesToLoadingTexts setValue:loadingText forKey:handle];

    // change text
    [self updateLabel];


    if(!_isShowing) {
        _isShowing = YES;
        RootViewController* activeRootViewController = [((AppDelegate *) [[UIApplication sharedApplication] delegate]) rootViewController];
        [activeRootViewController showLoadingViewController:self];
    }
    return handle;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateLabel {


    NSString* labelText = nil;
    NSString* handle = [_handles firstObject];
    if(handle) {
        labelText = _handlesToLoadingTexts[handle];
    }

    if(labelText.length == 0) {
        labelText = defaultLabelText;
    }
    [_lbl_staticLoadingText setText:defaultLabelText];
    
    [_lbl_text setText:[[[GUNRootManager instance] tutorialManager] translationForKeyInActiveLanguage:labelText]];
}

- (void)_timerFired:(NSTimer *)timer {
    NSLog(@"Now also show the UI");
    self.view.alpha = 1.0f;
    _timer = nil;
}

@end
