//
//  FeedbackViewController.m
//  Guntner
//
//  Created by Shiraz Omar on 10/10/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "FeedbackViewController.h"
#import "Firebase.h"
#import "Configuration.h"


@interface FeedbackViewController () <MFMailComposeViewControllerDelegate, UITextViewDelegate> {
    
    IBOutlet UITextView *messageBodyTextView;
    
}

@property (nonatomic, weak) IBOutlet UITextView *messageBodyTextView;

@end


@implementation FeedbackViewController


@synthesize messageBodyTextView = _messageBodyTextView;


#pragma mark Setup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Show Navigation Bar
    self.navigationController.navigationBarHidden = NO;
    
    
    // Add Keyboard Notification Listeners
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_keyboardWillShowNotification:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_keyboardWillHideNotification:) name:UIKeyboardWillHideNotification object:nil];
    
    
    
    // Make Self MessageBodyTextViewDelegate
    self.messageBodyTextView.delegate = self;
    
    
    // Create And Show Back Button
    [self createAndShowBackButton];
    
    
    // Create and Show Send Button
    [self createAndShowSendButton];
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    // Send Google Analytics Information
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:kFeedbackViewControllerIdentifier];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    [FIRAnalytics logEventWithName:kGAIScreenName
                        parameters:@{
                                     kFIRParameterItemName:kFeedbackViewControllerIdentifier,
                                     kFIRParameterContentType:@"screen"
                                     }];
}

- (void)createAndShowBackButton {
    
    // Create and Show Back Button
    UIImage *buttonImage = [UIImage imageNamed:kBackButtonImage];
    CGRect buttonFrame = kBackButtonFrame;
    UIButton *backButton = [[UIButton alloc] initWithFrame:buttonFrame];
    [backButton setTitle:kBackButtonTitle forState:UIControlStateNormal];
    [backButton setTitleColor:kBackButtonTitleColor forState:UIControlStateNormal];
    [[backButton titleLabel] setFont:kBackButtonTitleFont];
    [backButton setImage:buttonImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(goToPreviousViewController) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationController.topViewController.navigationItem.leftBarButtonItem = backBarButton;
    backBarButton = nil;
    buttonImage = nil;
    
}

- (void)createAndShowSendButton {
    
    // Create and Show Back Button
    UIButton *sendButton = [[UIButton alloc] initWithFrame:kBackButtonFrame];
    [sendButton setTitle:kSendButtonTitle forState:UIControlStateNormal];
    [sendButton setTitleColor:kBackButtonTitleColor forState:UIControlStateNormal];
    [[sendButton titleLabel] setFont:kBackButtonTitleFont];
    [sendButton addTarget:self action:@selector(showMailComposer) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *sendBarButton = [[UIBarButtonItem alloc] initWithCustomView:sendButton];
    self.navigationController.topViewController.navigationItem.rightBarButtonItem = sendBarButton;
    sendBarButton = nil;
    sendButton = nil;
    
}

- (void)createAndShowKeyboardDismissButton {
    
    // Add Dismiss Keyboard Bar Button
    self.navigationController.topViewController.navigationItem.rightBarButtonItem = nil;
    UIButton *dismissButton = [[UIButton alloc] initWithFrame:kBackButtonFrame];
    [dismissButton setTitle:kColdSliderDismissKeyboardButtonTitle forState:UIControlStateNormal];
    [dismissButton setTitleColor:kBackButtonTitleColor forState:UIControlStateNormal];
    [[dismissButton titleLabel] setFont:kBackButtonTitleFont];
    [dismissButton addTarget:self action:@selector(dismissKeyboard) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *dismissBarButton = [[UIBarButtonItem alloc] initWithCustomView:dismissButton];
    self.navigationController.topViewController.navigationItem.rightBarButtonItem = dismissBarButton;
    dismissBarButton = nil;
    dismissButton = nil;
    
}

- (void)goToPreviousViewController {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


#pragma mark TextView Delegate Methods

- (void)_keyboardWillShowNotification:(NSNotification*)notification
{    
    CGRect messageBodyFrame = self.messageBodyTextView.frame;
    if (IS_IPHONE_5) {
        messageBodyFrame.size.height -= ([notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height + 4.0);
    } else {
        messageBodyFrame.size.height -= ([notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height + 4.0);
    }
    self.messageBodyTextView.frame = messageBodyFrame;
}

- (void)_keyboardWillHideNotification:(NSNotification*)notification
{
    CGRect messageBodyFrame = self.messageBodyTextView.frame;
    if (IS_IPHONE_5) {
        messageBodyFrame.size.height += ([notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height + 4.0);
    } else {
        messageBodyFrame.size.height += ([notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height + 4.0);
    }
    self.messageBodyTextView.frame = messageBodyFrame;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    // Show Keyboard Done Button
    [self createAndShowKeyboardDismissButton];
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    NSRange typingRange = NSMakeRange(textView.text.length - 1, 1);
    [textView scrollRangeToVisible:typingRange];
    
    return YES;
    
}

- (void)dismissKeyboard {
    
    // Dismiss Keyboard
    [self.messageBodyTextView resignFirstResponder];
    
    // Remove Dismiss Button and Add Send Button
    self.navigationController.topViewController.navigationItem.rightBarButtonItem = nil;
    [self createAndShowSendButton];
}


#pragma mark Mail Composer Methods

- (void)showMailComposer {
    
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
        mailComposer.mailComposeDelegate = self;
        
        // Set Subject
        [mailComposer setSubject:@"iPhone App Feedback"];
        
        // Set Recipient Email
        NSArray *toRecipients = [NSArray arrayWithObject:@"app@guentner.de"];
        [mailComposer setToRecipients:toRecipients];
        toRecipients = nil;
        
        // Set Body
        [mailComposer setMessageBody:self.messageBodyTextView.text isHTML:NO];
        
        
        // Show Mail Composer
        [self presentViewController:mailComposer animated:YES completion:NULL];
    }
    
}


#pragma mark Mail Composer Delegate Methods

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark Auto Rotation Methods

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


#pragma mark Cleanup

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}


@end
