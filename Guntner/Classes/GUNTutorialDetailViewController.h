//
//  GUNTutorialDetailViewController.h
//  guentner
//
//  Created by Julian Dawo on 16.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GUNInsightTutorialFeedbackPageViewController.h"


@interface GUNTutorialDetailViewController : UIViewController <UIPageViewControllerDataSource,UIPageViewControllerDelegate,GUNFeedbackDelegate >




@property (weak, nonatomic) IBOutlet UIPageControl *pc_pageCount;
@property (readwrite, assign) NSInteger tutorialID;
@property (readwrite, assign) NSInteger productID;
@property (weak, nonatomic) IBOutlet UIView *v_containerForPages;
@property (weak, nonatomic) IBOutlet UIButton *btn_leftArrow;
@property (weak, nonatomic) IBOutlet UIButton *btn_rightArrow;
- (IBAction)tapRightButton:(id)sender;
- (IBAction)tapLeftButton:(id)sender;
- (IBAction)tapPageControl:(id)sender;

@end
