//
//  NewsFeedCell.m
//  guentner
//
//  Created by Balazs Auth on 03.12.18.
//  Copyright © 2018 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NewsFeedCell.h"

@implementation NewsFeedCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void) setupCellWithTitle:(NSString*) title text:(NSString*)text createdDate:(NSNumber*)createdDate rowNr:(NSUInteger) rowNr {
    
    if (rowNr > 0) {
        _c_tv_height.constant = 0;
        [_iv_linkedin setImage:[UIImage imageNamed:@"Icon_linkedin_small"]];
    }
    else {
        self.textview_article.scrollEnabled = false;
        self.textview_article.text = text;
        [self.textview_article sizeToFit];
        _c_tv_height.constant = self.textview_article.contentSize.height;
         [_iv_linkedin setImage:[UIImage imageNamed:@"Icon_linkedin_large"]];
        
    }
    
    _v_container.layer.cornerRadius = 5;
    _v_container.layer.masksToBounds = true;
    
    [self.lbl_title setText:title];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[createdDate longValue]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSString *strDate = [dateFormat  stringFromDate:date];
    [self.lbl_date setText:strDate];
    [self layoutIfNeeded];
}

-(void) setLinkedinNews:(BOOL) isLinkedin {
    [self.iv_linkedin setHidden:!isLinkedin];
}


@end
