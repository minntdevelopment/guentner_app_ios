//
//  TutorialItemCell.h
//  guentner
//
//  Created by Julian Dawo on 05.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialItemCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lbl_tutorialName;
@property (nonatomic, weak) IBOutlet UIView *v_availableLanguagesContainer;

@property (nonatomic, strong) NSArray *availableLanguages;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *c_verticalSpace;

-(void) setupViewForAvailableLanguages;
-(void) updateNamePosition;
@end
