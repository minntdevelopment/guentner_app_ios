//
//  CommunicationManager.m
//  guentner
//
//  Created by Balazs Auth on 30.11.18.
//  Copyright © 2018 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommunicationManager.h"
#import <CommonCrypto/CommonDigest.h>

@implementation CommunicationManager : NSObject

static CommunicationManager *_sharedSingleton;

+ (void)initialize
{
    static BOOL initialized = NO;
    if(!initialized)
    {
        initialized = YES;
        _sharedSingleton = [[CommunicationManager alloc] init];
    }
}


-(id)init
{
    if(self = [super init]){
    }
    return self;
}

+ (CommunicationManager* ) instance {
    return _sharedSingleton;
}

-(NSString*)urlEscapeString:(NSString *)unencodedString
{
    CFStringRef originalStringRef = (__bridge_retained CFStringRef)unencodedString;
    NSString *s = (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,originalStringRef, NULL, (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ", kCFStringEncodingUTF8);
    CFRelease(originalStringRef);
    return s;
}

+ (NSString *)MD5String:(NSString*) parameter  {
    const char *cStr = [parameter UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, (CC_LONG)strlen(cStr), result );
    
    return [NSString stringWithFormat:
            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

-(void)getLinkedinNewsWithCompletionHandler:(void (^)(NSArray* result))completionHandler {
    
    NSString *fullUrl = @"http://104.248.27.14:8080/api/v1/linkedinNews";
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    NSString *encodedUrlAsString = [fullUrl stringByAddingPercentEncodingWithAllowedCharacters:set];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:encodedUrlAsString]];

    NSString *authStr = @"minnt-test-user:Minnt2018guent";
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    NSString *authValue = [NSString stringWithFormat: @"Basic %@",[authData base64EncodedStringWithOptions:0]];
    [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (!error) {
            // Success
            if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                NSError *jsonError;
                NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                
                if (jsonError) {
                    // Error Parsing JSON
                    
                } else {
                    // Success Parsing JSON
                    // Log NSDictionary response:
                    completionHandler([jsonResponse objectForKey:@"linkedInNews"]);
                }
            }  else {
                //Web server is returning an error
                completionHandler(nil);
            }
        } else {
            // Fail
            NSLog(@"error : %@", error.description);
            completionHandler(nil);
        }
    }] resume];
    
}

-(void)getNews:(NSString*)language withContry:(NSString*)country andCompletionHandler:(void (^)(NSArray* result))completionHandler{
    
    long currentTime = [[NSDate date] timeIntervalSince1970] * 1000;
    NSString* timeValue = [NSString stringWithFormat:@"%ld", currentTime];
    NSMutableArray* paramkeys = [NSMutableArray arrayWithObjects:@"time", @"type", @"language", @"country", nil];
    NSString* urlStr = [NSString string];
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{paramkeys[0]: timeValue, paramkeys[1]: @"news", paramkeys[2]: language, paramkeys[3]: country}];
    
    for (NSString* key in paramkeys) {
        urlStr = [urlStr stringByAppendingString:key];
        urlStr = [urlStr stringByAppendingString:@"="];
        urlStr = [urlStr stringByAppendingString:[params valueForKey:key]];
        urlStr = [urlStr stringByAppendingString:@"&"];
    }
    urlStr = [urlStr substringToIndex:[urlStr length]-1];
    //generate token parameter
    urlStr = [urlStr stringByAppendingString:@"4c6fc3f0-08b6-11e3-8ffd-0800200c9a66"];
    NSString* parametersToken = [CommunicationManager MD5String:urlStr].lowercaseString;
    
    //add token parameter
    [paramkeys addObject:@"token"];
    [params setValue:parametersToken forKey:@"token"];
    
    NSMutableString* urlWithQuerystring = [[NSMutableString alloc] init];
    for (id keyString in paramkeys) {
        NSString *valueString = [params objectForKey:keyString];
        
        if ([urlWithQuerystring rangeOfString:@"?"].location == NSNotFound) {
            [urlWithQuerystring appendFormat:@"?%@=%@", [self urlEscapeString:keyString], [self urlEscapeString:valueString]];
        } else {
            [urlWithQuerystring appendFormat:@"&%@=%@", [self urlEscapeString:keyString], [self urlEscapeString:valueString]];
        }
    }
    
    NSString* fullUrl = @"http://www.guentner.com/api";
    fullUrl = [fullUrl stringByAppendingString:urlWithQuerystring];
    
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    NSString *encodedUrlAsString = [fullUrl stringByAddingPercentEncodingWithAllowedCharacters:set];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    [[session dataTaskWithURL:[NSURL URLWithString:encodedUrlAsString]
            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if (!error) {
                    // Success
                    if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                        NSError *jsonError;
                        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                        
                        if (jsonError) {
                            // Error Parsing JSON
                            
                        } else {
                            // Success Parsing JSON
                            // Log NSDictionary response:
                            completionHandler([jsonResponse objectForKey:@"news"]);
                        }
                    }  else {
                        //Web server is returning an error
                        completionHandler(nil);
                    }
                } else {
                    // Fail
                    NSLog(@"error : %@", error.description);
                    completionHandler(nil);
                }
            }] resume];
    
    
    

}


@end
