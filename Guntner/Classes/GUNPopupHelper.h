//
//  GUNPopupHelper.h
//  guentner
//
//  Created by Julian Dawo on 07.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GUNPopupInfoViewController, GUNPopupSelectionViewController;

@interface GUNPopupHelper : NSObject

+ (GUNPopupInfoViewController*)showOneButtonStyleAlertViewWithTitle:(NSString *)title messageText:(NSString *)text okButtonBlock:(void (^)())okButtonBlock;

+ (GUNPopupSelectionViewController*)showSelectionWithTitle:(NSString*)title multiselection:(BOOL)multiselection selectableItems:(NSArray *)selectableItems okButtonBlock:(void (^)(NSArray*))okBlock;
@end
