//
//  GECLiteViewController.h
//  Guntner
//
//  Created by Shiraz Omar on 8/6/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"
#import "GAITrackedViewController.h"


@interface NewsfeedViewController : UIViewController {
}

@property (nonatomic, readwrite, strong) NSArray* newsArray;
@property (weak, nonatomic) IBOutlet UITableView *tv_news;

@end
