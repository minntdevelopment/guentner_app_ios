//
//  NewsDetailViewController.h
//  guentner
//
//  Created by Balazs Auth on 03.12.18.
//  Copyright © 2018 Brandixi3. All rights reserved.
//

#import <MessageUI/MessageUI.h>
#import <UIKit/UIKit.h>
#import "NewsObject.h"

@interface NewsDetailViewController : UIViewController <MFMailComposeViewControllerDelegate> {
}
-(void) loadValues;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *c_tv_height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *c_contentview_height;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *c_iv_width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *c_lbl_title_height;

@property (strong, nonatomic) NewsObject *selectedNewsObject;
@property (weak, nonatomic) IBOutlet UIView* v_containter;
@property (weak, nonatomic) IBOutlet UIImageView *iv_article;
@property (weak, nonatomic) IBOutlet UILabel *lbl_date;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UITextView *tv_article;
@property (weak, nonatomic) IBOutlet UIButton *btn_share;

@end
