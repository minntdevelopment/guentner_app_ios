//
//  InformationViewController.m
//  Guntner
//
//  Created by Shiraz Omar on 10/10/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

#import "InformationViewController.h"
#import "Firebase.h"
#import "Configuration.h"


@interface InformationViewController () <UITableViewDelegate, UITableViewDataSource> {
    
    IBOutlet UITableView *infoTableView;
	
}

@property (nonatomic, weak) IBOutlet UITableView *infoTableView;

@end


@implementation InformationViewController


@synthesize infoTableView = _infoTableView;


#pragma mark Setup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Show Navigation Bar
    self.navigationController.navigationBarHidden = NO;
    
    
    // Set Title
    self.title = kInfoPageViewTitle;
    
    
    // Setup Info Table View
    [self setupTableView];
	
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    // Send Google Analytics Information
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:kInfoPageViewControllerIdentifier];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    [FIRAnalytics logEventWithName:kGAIScreenName
                        parameters:@{
                                     kFIRParameterItemName:kInfoPageViewControllerIdentifier,
                                     kFIRParameterContentType:@"screen"
                                     }];
    
    // DeSelect Any Selected Cells
    [self.infoTableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO];
    [self.infoTableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] animated:NO];
    [self.infoTableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1] animated:NO];
    [self.infoTableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1] animated:NO];
    
}


#pragma mark TableView Methods

- (void)setupTableView {
    
    // Setup Info Table View
    self.infoTableView.delegate = self;
    self.infoTableView.dataSource = self;
    
}


#pragma mark TableView Delegate Methods

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Retrive Appropriate Cell
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    
    // Set Cell As Selected
    //[tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    
    
    // Push Appropriate View Controller
    UIViewController *viewController = nil;
    if (indexPath.section == 0) {
        // Feedback
        if ([MFMailComposeViewController canSendMail]) {
            MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
            
            // Set Subject
            [mailComposer setSubject:@"iPhone App Feedback"];
            
            // Set Recipient Email
            NSArray *toRecipients = [NSArray arrayWithObject:@"app@guentner.de"];
            [mailComposer setToRecipients:toRecipients];
            toRecipients = nil;
            
            // Show Mail Composer
            [self presentViewController:mailComposer animated:YES completion:NULL];
        }
        //viewController = [self.storyboard instantiateViewControllerWithIdentifier:kFeedbackViewControllerIdentifier];
    } else {
        if (indexPath.row == 0) {
            // Privacy
            viewController = [self.storyboard instantiateViewControllerWithIdentifier:kPrivacyViewControllerIdentifier];
        } else if(indexPath.row == 1){
            // About Us
            viewController =  [[UIStoryboard storyboardWithName:@"Impressum" bundle:NULL]
                               instantiateViewControllerWithIdentifier:kImpressumViewControllerIdentifier];
            //[self.storyboard instantiateViewControllerWithIdentifier:kImpressumViewControllerIdentifier];
        }
        else if(indexPath.row == 2){
            // About Us
            viewController = [self.storyboard instantiateViewControllerWithIdentifier:kDisclaimerViewControllerIdentifier];
        }
        
    }
    if (viewController != nil) {
        viewController.title = cell.textLabel.text;
        [self.navigationController pushViewController:viewController animated:YES];
    }
    viewController = nil;
    
    
    // Release Objects
    cell = nil;
    
}


#pragma mark PressureList DataSource Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Retrun Appropriate Result
    if (section == 0) {
        return 1; // Other Section
    } else {
        return 3; // Legal Section
    }
    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return kInformationTableHeaderSection0Title; // Other Section
    } else {
        return kInformationTableHeaderSection1Title; // Legal Section
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *headerView = [UILabel new];
    headerView.backgroundColor = [UIColor clearColor];
    headerView.font = kInformationTableHeaderViewBoldFont;
    headerView.textColor = [UIColor darkGrayColor];
    if (section == 0) {
        headerView.text = [NSString stringWithFormat:@"     %@", kInformationTableHeaderSection0Title]; // Other Section
    } else {
        headerView.text = [NSString stringWithFormat:@"     %@", kInformationTableHeaderSection1Title]; // Legal Section
    }
    return headerView;
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
	if ( section == 1 ) {
		// get the version number
		NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
		NSString *versionText = [NSString stringWithFormat:@"Version %@", [info objectForKey:@"CFBundleVersion"]];
		
		// position the footer at the bottom of the table view
		CGFloat footerHeight = 30.0;
		CGFloat titlebarHeight = 45.0;
		CGFloat footerOffset = [UIScreen mainScreen].bounds.size.height - tableView.contentSize.height - titlebarHeight - ( footerHeight / 2 );
		CGRect rectFooter = CGRectMake(0, footerOffset, tableView.frame.size.width, footerHeight);
		
		UIView *footerView = [[UIView alloc] initWithFrame:rectFooter];
		UILabel *label = [[UILabel alloc] initWithFrame:footerView.frame];
		[label setText:versionText];
		[label setFont:[UIFont systemFontOfSize:11.0]];
		[label setTextColor:[UIColor grayColor]];
		[label setTextAlignment:NSTextAlignmentCenter];
		[footerView addSubview:label];
		
		return footerView;
	} else {
		return nil;
	}
	
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        // Configure Cell Text Label
        cell.textLabel.adjustsFontSizeToFitWidth = YES;
        cell.textLabel.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.highlightedTextColor = [UIColor blackColor];
        cell.textLabel.font = kColdSliderComponentPressureListCellFont;
        
        // Remove Rounded Corners From Standard Grouped Cell
        UIView *backgroundView = [UIView new];
        backgroundView.backgroundColor = [UIColor whiteColor];
        cell.backgroundView = backgroundView;
        backgroundView = nil;
        
        // Setup Selected Background View
        UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, (cell.frame.size.height - kColdSliderRefrigerantSelectorListSeperatorHeight))];
        selectedBackgroundView.backgroundColor = kGuntnerColorCBDDEE;
        cell.selectedBackgroundView = selectedBackgroundView;
        selectedBackgroundView = nil;
        
        // Setup Custom Seperator For Cell
        UIView *seperator = [[UIView alloc] initWithFrame:CGRectMake(kColdSliderRefrigerantSelectorListSeperatorPadding, (tableView.rowHeight - kColdSliderRefrigerantSelectorListSeperatorHeight), (tableView.bounds.size.width - kColdSliderRefrigerantSelectorListSeperatorPadding), kColdSliderRefrigerantSelectorListSeperatorHeight)];
        seperator.backgroundColor = kGuntnerColorEEEEEE;
        seperator.tag = kColdSliderRefrigerantSelectorListSeperatorTag;
        [cell.contentView addSubview:seperator];
        seperator = nil;
        
    }
    
    
    // Cell Customization
    
    // Set Cell Text
    if (indexPath.section == 0) {
        cell.textLabel.text = kInformationTableHeaderSection0Row0Title;
    } else {
        if (indexPath.row == 0) {
            cell.textLabel.text = kInformationTableHeaderSection1Row0Title;
        } else if(indexPath.row == 1){
            cell.textLabel.text = kInformationTableHeaderSection1Row1Title;
        } else if(indexPath.row == 2){
            cell.textLabel.text = kInformationTableHeaderSection1Row2Title;
        }
    }
    
    
    // Hide Seperator For Last Cell
    if (((indexPath.section == 0) && (indexPath.row == 0)) || ((indexPath.section == 1) && (indexPath.row == 1)) || ((indexPath.section == 1) && (indexPath.row == 2))) {
        for (UIView *seperator in cell.contentView.subviews) {
            if (seperator.tag == kColdSliderRefrigerantSelectorListSeperatorTag) {
                seperator.hidden = YES;
            }
        }
    }
    
    
    return cell;
    
}


#pragma mark Auto Rotation Methods

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}



#pragma mark Cleanup

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    
    //
    
}


@end
