//
//  TutorialGroupItemCell.m
//  guentner
//
//  Created by Julian Dawo on 05.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "TutorialGroupItemCell.h"
#import <QuartzCore/QuartzCore.h>
@implementation TutorialGroupItemCell {
    BOOL _isExtended;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void) expandGroup {
    
//    if (!_isExtended) {
        _isExtended = YES;
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        animation.fromValue = [[self.img_v_iconArrow.layer presentationLayer] valueForKeyPath:@"transform.rotation.z"];
        animation.toValue = [NSNumber numberWithFloat:M_PI];
        animation.duration = 0.3f;
        animation.fillMode = kCAFillModeForwards;
        animation.repeatCount = 0;
        animation.removedOnCompletion = NO;
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
        [self.img_v_iconArrow.layer addAnimation:animation forKey:@"transform.rotation.z"];
        
//    }
    
}
-(void) shrinkGroup {
    if (_isExtended) {
        _isExtended = NO;
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        animation.fromValue = [[self.img_v_iconArrow.layer presentationLayer] valueForKeyPath:@"transform.rotation.z"];
        animation.toValue = [NSNumber numberWithFloat: 2 * M_PI];
        animation.duration = 0.3f;
        animation.fillMode = kCAFillModeForwards;
        animation.repeatCount = 0;
        animation.removedOnCompletion = NO;
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
        [self.img_v_iconArrow.layer addAnimation:animation forKey:@"transform.rotation.z"];
    }
    
}

@end
