//
//  TutorialItemCell.m
//  guentner
//
//  Created by Julian Dawo on 05.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "TutorialItemCell.h"
#import <QuartzCore/QuartzCore.h>
#import "Configuration.h"

@implementation TutorialItemCell

- (void)awakeFromNib {
    // Initialization code

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setupViewForAvailableLanguages {
    CGFloat currentX = 0.0f;
    
    
    for (UIView *subview in [_v_availableLanguagesContainer subviews]) {
        [subview removeFromSuperview];
    
    }
    
    
    for (id item in _availableLanguages) {
        NSString* languageItem = (NSString *)item;
        UILabel* lbl_tmpLanguage = [[UILabel alloc] initWithFrame:CGRectMake(currentX, 5.0f, 20.0f, 20.0f)];
        lbl_tmpLanguage.font = [UIFont fontWithName:@"HelveticaNeue" size:7.0f];
        lbl_tmpLanguage.text = languageItem;
        lbl_tmpLanguage.textAlignment = NSTextAlignmentCenter;
        
        lbl_tmpLanguage.layer.borderColor = color_dark_blue.CGColor;
        lbl_tmpLanguage.layer.borderWidth = 0.7f;
        lbl_tmpLanguage.layer.cornerRadius = 11.0f;
        
        [_v_availableLanguagesContainer addSubview:lbl_tmpLanguage];
        currentX += 25.0f;
    }
    
    if (_availableLanguages.count > 0) {
        _c_verticalSpace.constant = 8.0f;
        //_lbl_tutorialName.frame = CGRectMake(15.0f, 8.0f, 278.0f, 21.0f);
    } else {
        _c_verticalSpace.constant = 19.0f;
        //_lbl_tutorialName.frame = CGRectMake(15.0f, 19.0f, 278.0f, 21.0f);
    }
    [self layoutIfNeeded];
}

-(void) updateNamePosition {
    _c_verticalSpace.constant = 19.0f;
    //_lbl_tutorialName.frame = CGRectMake(15.0f, 19.0f, 278.0f, 21.0f);

    [self layoutIfNeeded];
}
@end
