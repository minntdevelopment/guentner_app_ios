//
// Created by Julian Dawo on 01.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "GUNVersionControlResponse.h"


@implementation GUNVersionControlResponse {

}
- (id)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];

    _content_version = (NSNumber *)[dictionary valueForKey:@"content_version"];

    return self;

}
@end