//
// Created by Julian Dawo on 04.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GUNLanguageTO : NSObject

@property (nonatomic, strong) NSString *langugageCode;
@property (nonatomic, strong) NSString *translatedValue;

-(instancetype) initWithLangugeCode:(NSString *) code andTranslatedValue:(NSString *) value;
@end