//
//  TutorialsViewController.m
//  guentner
//
//  Created by Julian Dawo on 03.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "TutorialsViewController.h"
#import "GUNTutorialManager.h"
#import "GUNRootManager.h"
#import "GUNProductResponse.h"
#import "TutorialProductItemCell.h"
#import "TutorialListViewController.h"
#import "GUNLoadingViewController.h"

#import "GUNPopupHelper.h"
#import "SelectionItemTO.h"
#import "GUNLanguageTO.h"

#import <QuartzCore/QuartzCore.h>

@implementation TutorialsViewController {
    GUNTutorialManager *_tutorialManager;
    NSArray *_products;
    NSMutableArray *_filteredProducts;
    BOOL _menuOpen;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _tutorialManager = [GUNRootManager instance].tutorialManager;
    _tv_productList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _tv_productList.alwaysBounceVertical = NO;
    
    _btn_selectLanguages.layer.cornerRadius = 5.0;
    _btn_selectLanguages.layer.shadowColor = color_dark_gray.CGColor;
    _btn_selectLanguages.layer.shadowOffset = CGSizeMake(0, 1.0);
    _btn_selectLanguages.layer.shadowOpacity = 1.0;
    _btn_selectLanguages.layer.shadowRadius = 0.0;
    
    _btn_selectRegion.layer.cornerRadius = 5.0;
    _btn_selectRegion.layer.shadowColor = color_dark_gray.CGColor;
    _btn_selectRegion.layer.shadowOffset = CGSizeMake(0, 1.0);
    _btn_selectRegion.layer.shadowOpacity = 1.0;
    _btn_selectRegion.layer.shadowRadius = 0.0;
    
    _txt_search.layer.cornerRadius = 5.0;
    _txt_search.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    
//    [self showContent];
    
    _menuOpen = NO;
    //show loading indicator
    NSString* loading = [GUNLoadingViewController pushLoadingViewController];
    
    [_tutorialManager downloadAllTutroialMenuDataIfNecessaryCompletion:^(NSError *error) {
        if (error == nil) {
            [self loadProductsForRegion:[_tutorialManager getActiveRegion]];
        } else {
            NSString *titleText = [_tutorialManager translationForKeyInActiveLanguage:@"Connection error"];
            NSString *messageText = [_tutorialManager translationForKeyInActiveLanguage:@"An error occured while downloading the new menu content"];
            [GUNPopupHelper showOneButtonStyleAlertViewWithTitle:titleText messageText:messageText okButtonBlock:^{
                [self showErrorMessage];
            }];
            
        }
        [GUNLoadingViewController popLoadingViewController:loading];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self loadProductsForRegion:[_tutorialManager getActiveRegion]];
    [self.view endEditing:YES];
}



- (void)loadProductsForRegion:(ContenRegion) contentRegion {

    // now translations are available
    _txt_search.text = @"";
    
    _txt_search.placeholder = [_tutorialManager translationForKeyInActiveLanguage:@"Search "];
    if (!contentRegion) {

        [_btn_selectRegion setTitle:[_tutorialManager translationForKeyInActiveLanguage:@"Region"] forState:UIControlStateNormal];
        [_btn_selectRegion setTitle:[_tutorialManager translationForKeyInActiveLanguage:@"Region"] forState:UIControlStateHighlighted];

        _lbl_languageButtonTitle.text = [_tutorialManager translationForKeyInActiveLanguage:@"Language"];
        [self showFirstSelectRegion];
        
    } else {
        NSString *regionText =[_tutorialManager valueForRegionCode:[_tutorialManager getActiveRegion]];

        [_btn_selectRegion setTitle:[_tutorialManager translationForKeyInActiveLanguage:regionText] forState:UIControlStateNormal];
        [_btn_selectRegion setTitle:[_tutorialManager translationForKeyInActiveLanguage:regionText] forState:UIControlStateHighlighted];

        if ([_tutorialManager getActiveLangues].count > 0) {
            NSString *languageText = [_tutorialManager nameForLanaguageOfLanguageCode:[_tutorialManager getActiveLangues][0]];
            
            _lbl_languageButtonTitle.text = [_tutorialManager translationForKeyInActiveLanguage:languageText];
        } else {
            
            _lbl_languageButtonTitle.text = [_tutorialManager translationForKeyInActiveLanguage:@"Language"];
        }



        _products = [_tutorialManager getProductsForRegion:contentRegion];
        _filteredProducts = [[NSMutableArray alloc] initWithArray:_products];
        for (GUNProductResponse *tmpProduct in _filteredProducts) {
            NSLog(@"Product: %@", [_tutorialManager translationForKeyInActiveLanguage:tmpProduct.productName]);
            NSLog(@"Content: %@",[_tutorialManager getContentForProduct:tmpProduct.productID]);
        }

        if ([_filteredProducts count] > 0) {
            [self showContent];
            
        } else if (_products == nil) {
            [self showErrorMessage];
        } else {
            [self showNoResult];
        }

    }
  
}

- (void)showNoResult {

    NSString *translationNoResults = [_tutorialManager translationForKeyInActiveLanguage:@"No tutorial found for this search term."];
    _lbl_error_search.text = translationNoResults;
    [_v_noResult setHidden:NO];
    [_v_selectRegionFirst setHidden:YES];
    [_tv_productList setHidden:YES];
    [_v_error setHidden:YES];

}


- (void)showErrorMessage {
    NSString *translationConnection = [_tutorialManager translationForKeyInActiveLanguage:@"Connect to the Internet to download content."];
    _lbl_error_connection.text = translationConnection;
    _btn_selectLanguages.enabled = YES;
    _txt_search.enabled = NO;
    _txt_search.alpha = 0.4f;
    [_v_error setHidden:NO];
    [_tv_productList setHidden:YES];
    [_v_selectRegionFirst setHidden:YES];
    [_v_noResult setHidden:YES];
}

-(void) showFirstSelectRegion {
    NSString *translationRegion = [_tutorialManager translationForKeyInActiveLanguage:@"Select Region first"];
    _lbl_error_region.text = translationRegion;
    _btn_selectLanguages.enabled = NO;
    _txt_search.enabled = NO;
    _txt_search.alpha = 0.4f;
    [_v_selectRegionFirst setHidden:NO];
    [_tv_productList setHidden:YES];
    [_v_error setHidden:YES];
    [_v_noResult setHidden:YES];
}

-(void) showContent {
    _btn_selectLanguages.enabled = YES;
    _txt_search.enabled = YES;
    _txt_search.alpha = 1.0f;
    [self.tv_productList reloadData];
    [_tv_productList setHidden:NO];
    [_v_error setHidden:YES];
    [_v_selectRegionFirst setHidden:YES];
    [_v_noResult setHidden:YES];
}




- (IBAction)tapBtnSelectRegion:(id)sender {
    [self.view endEditing:YES];

    ContenRegion activeRegion = [_tutorialManager getActiveRegion];

    NSMutableArray *selectableItems = [[NSMutableArray alloc] init];
    NSNumber *activeKey = [NSNumber numberWithInteger:activeRegion];
    [selectableItems addObject:[[SelectionItemTO alloc] initWithKey:@20001 andTranslatedValue:
                                [_tutorialManager translationForKeyInActiveLanguage:@"Asia"] selectionState:NO]];
    [selectableItems addObject:[[SelectionItemTO alloc] initWithKey:@20002 andTranslatedValue:
                                [_tutorialManager translationForKeyInActiveLanguage:@"Europe"] selectionState:NO] ];
    [selectableItems addObject:[[SelectionItemTO alloc] initWithKey:@20003 andTranslatedValue:
                                [_tutorialManager translationForKeyInActiveLanguage:@"America"] selectionState:NO] ];

    for (SelectionItemTO *item in selectableItems) {

        if ([item.key isEqual:activeKey]) {
        item.isSelected = YES;
    }

    }
    NSString* titleText = [_tutorialManager translationForKeyInActiveLanguage:@"Region selection"];
    [GUNPopupHelper showSelectionWithTitle:[_tutorialManager translationForKeyInActiveLanguage:titleText] multiselection:NO selectableItems:selectableItems okButtonBlock:^(NSArray *array) {
        for (id item in array) {
           
            NSLog(@"%@", ((SelectionItemTO *)item).translatedValue);
            SelectionItemTO *selectedKey =  ((SelectionItemTO *)item);
            NSNumber *noSelectedKey = (NSNumber *) selectedKey.key;
            [_tutorialManager setActiveRegion:[noSelectedKey integerValue]];
            [self loadProductsForRegion:[noSelectedKey integerValue]];
        
            NSString* activeLanguage;
            NSArray* activeLanguages = [_tutorialManager getActiveLangues];
            if (![activeLanguages count] == 0 ) {
                activeLanguage = activeLanguages[0];
                NSArray* languagesForRegion = [_tutorialManager languagesForRegion:[noSelectedKey integerValue]];
                bool languageForRegionFound = NO;
                for (GUNLanguageTO *languageTO in languagesForRegion) {
                    if ([languageTO.langugageCode isEqual:activeLanguage]) {
                        languageForRegionFound = YES;
                        _lbl_languageButtonTitle.text = [_tutorialManager translationForKeyInActiveLanguage:languageTO.translatedValue];
                    }
                }
                if (!languageForRegionFound) {
                    [_tutorialManager setActiveLanguageToPhoneLanguage];
                    _lbl_languageButtonTitle.text = [_tutorialManager translationForKeyInActiveLanguage:@"Language"];
                }
            }
        }
    }];
    
    
}

- (IBAction)tapBtnSelectLanguage:(id)sender {
    [self.view endEditing:YES];
    
    NSSortDescriptor *sortAlphabetically = [NSSortDescriptor sortDescriptorWithKey:@"translatedValue" ascending:YES];
    
    NSArray* languageTOArray =[_tutorialManager languagesForRegion:[_tutorialManager getActiveRegion]];
   
    
    NSArray* activeLanguages = [_tutorialManager getActiveLangues];

    NSMutableArray *selectableItems = [[NSMutableArray alloc] init];


    for ( GUNLanguageTO *item in languageTOArray) {

        BOOL preSelect = NO;
        if ([activeLanguages containsObject:[item.langugageCode uppercaseString]] || [activeLanguages containsObject:[item.langugageCode lowercaseString]]) {
            preSelect = YES;
        }
        NSString *translatedValue = [_tutorialManager translationForKeyInActiveLanguage:item.translatedValue];
        [selectableItems addObject:[[SelectionItemTO alloc] initWithKey:item.langugageCode andTranslatedValue:translatedValue selectionState:preSelect]];

    }
    NSMutableArray* sortedSelectableItems = [NSMutableArray arrayWithArray:[selectableItems sortedArrayUsingDescriptors:@[sortAlphabetically]]];
    
    //Only show languages that are available in tutoriallist
    
    NSData* languageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"availableLanguagesInTutorials"];
    NSMutableSet* availableLanguages = [NSKeyedUnarchiver unarchiveObjectWithData: languageData];
    NSMutableArray* languagesToBeDeleted = [[NSMutableArray alloc]init];
    
    for (int i = 0 ; i < sortedSelectableItems.count; i++) {
        if (![availableLanguages containsObject:[sortedSelectableItems[i] key]]) {
            [languagesToBeDeleted addObject:sortedSelectableItems[i]];
        }
    }
    [sortedSelectableItems removeObjectsInArray:languagesToBeDeleted];
    
    
    // special value all
    NSString *translatedValue = [_tutorialManager translationForKeyInActiveLanguage:@"All"];
    [sortedSelectableItems insertObject:[[SelectionItemTO alloc] initWithKey:@"all" andTranslatedValue:translatedValue selectionState:NO] atIndex:0];
    
    NSString* titleText = [_tutorialManager translationForKeyInActiveLanguage:@"Select Language"];
    [GUNPopupHelper showSelectionWithTitle:titleText multiselection:NO selectableItems:sortedSelectableItems okButtonBlock:^(NSArray *array) {
        
         NSMutableArray *result = [[NSMutableArray alloc] init];
        NSObject* activeKey;
        for (SelectionItemTO* item in array) {

            //special rule for selection of all --> all means no selection.
            if ([((NSString *)item.key) isEqualToString:@"all"]) {
                [result removeAllObjects];
               // _btn_selectLanguages.titleLabel.text = item.translatedValue;
                
                
                _lbl_languageButtonTitle.text = [_tutorialManager translationForKeyInActiveLanguage:item.translatedValue];
                activeKey = item.key;
            } else {
                [result addObject:item.key];
                //_btn_selectLanguages.titleLabel.text = item.translatedValue;
                _lbl_languageButtonTitle.text = [_tutorialManager translationForKeyInActiveLanguage:item.translatedValue];
                activeKey = item.key;
            }

        }
        [_tutorialManager setActiveLangues:result];
        
        NSString* firstItem;
        if (result.count > 0) {
            firstItem = result[0];
            
        } else {
            firstItem = _tutorialManager.userDefaultLanguage;
            
        }
        
        NSString* handler = [GUNLoadingViewController pushLoadingViewController];
        [_tutorialManager downloadLangugeIfNotExistsWithLanguageKey:firstItem complition:^(NSError *error) {
            [GUNLoadingViewController popLoadingViewController:handler];
            
            if (error != nil) {
                NSString *titleText = [_tutorialManager translationForKeyInActiveLanguage:@"Connection error"];
                NSString *messageText = [_tutorialManager translationForKeyInActiveLanguage:@"An error occured while downloading the new menu content"];
                [GUNPopupHelper showOneButtonStyleAlertViewWithTitle:titleText messageText:messageText okButtonBlock:^{
                    [self showErrorMessage];
                }];
            }
            NSString* activeLanguageTranslated = [_tutorialManager translationForKeyInActiveLanguage:[_tutorialManager nameForLanaguageOfLanguageCode:(NSString*)activeKey]];
            _lbl_languageButtonTitle.text = activeLanguageTranslated;
            
            NSString *regionText =[_tutorialManager valueForRegionCode:[_tutorialManager getActiveRegion]];
            
            [_btn_selectRegion setTitle:[_tutorialManager translationForKeyInActiveLanguage:regionText] forState:UIControlStateNormal];
            [_btn_selectRegion setTitle:[_tutorialManager translationForKeyInActiveLanguage:regionText] forState:UIControlStateHighlighted];
            _txt_search.placeholder = [_tutorialManager translationForKeyInActiveLanguage:@"Search "];
            
        }];
        
        
    }];
}

- (IBAction)editingCangedTxtfSerarch:(id)sender {
    if (_txt_search.text.length > 0) {
        
        _filteredProducts = [[NSMutableArray alloc] init];
        
        for (id item in _products) {
            GUNProductResponse* currentProduct = (GUNProductResponse *) item;
            
            //ignore the case, so make both lower case and remove whitespace
            NSString *textfieldValueInLowerCase = [[_txt_search.text lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@""];
            NSString *productNameTranslatetedInLowerCase = [[[_tutorialManager translationForKeyInActiveLanguage:currentProduct.productName ] lowercaseString] stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            if ([productNameTranslatetedInLowerCase containsString:textfieldValueInLowerCase]) {
                [_filteredProducts addObject:currentProduct];
            }
        }
        
        if (_filteredProducts.count == 0) {
            [self showNoResult];
        } else {
            [self showContent];
        }
    } else {
        // user is searching for an empty string
        _filteredProducts = [[NSMutableArray alloc] initWithArray:_products];
        [self showContent];
    }
    
    
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_filteredProducts count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.row < _filteredProducts.count) {
        static NSString *productCell = @"TutorialProductItemCell";

        TutorialProductItemCell *cell = (TutorialProductItemCell *) [_tv_productList dequeueReusableCellWithIdentifier:productCell];



        GUNProductResponse *currentProduct = (GUNProductResponse *)[_filteredProducts objectAtIndex:indexPath.row];

        cell.lbl_productName.text = [_tutorialManager translationForKeyInActiveLanguage:currentProduct.productName];

        return cell;
    }

    return nil;
}

-(void) scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GUNProductResponse* currentProduct = (GUNProductResponse *)[_filteredProducts objectAtIndex:indexPath.row];

    TutorialListViewController *tutorialListViewController = (TutorialListViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"TutorialListViewController"];

    if (tutorialListViewController != nil) {
       
        NSString* translatedProductName = [_tutorialManager translationForKeyInActiveLanguage:currentProduct.productName];
        tutorialListViewController.product = currentProduct;
        tutorialListViewController.title = translatedProductName;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

        [self.navigationController pushViewController:tutorialListViewController animated:YES];
    }

    

}



-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    [self.view endEditing:YES];
    return YES;
}


#pragma mark Auto Rotation Methods

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}




@end
