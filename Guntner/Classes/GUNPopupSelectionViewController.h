//
//  GUNPopupSelectionViewController.h
//  guentner
//
//  Created by Julian Dawo on 08.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GUNPopupSelectionViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UIButton* btn_ok;
@property (nonatomic, weak) IBOutlet UILabel* lbl_title;

@property (weak, nonatomic) IBOutlet UITableView *tv_selectionTableView;

@property (weak, nonatomic) IBOutlet UIView *v_popupBox;
@property (weak, nonatomic) IBOutlet UIView *v_popupHederBox;

- (IBAction)tapOkBtn:(id)sender;

-(void)setSelectionWithTitle:(NSString*)title multiselection:(BOOL)multiselection selectableItems:(NSArray *)selectableItems okButtonBlock:(void (^)(NSArray*)) okblock;


@end
