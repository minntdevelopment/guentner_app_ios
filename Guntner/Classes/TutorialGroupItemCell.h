//
//  TutorialGroupItemCell.h
//  guentner
//
//  Created by Julian Dawo on 05.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialGroupItemCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lbl_groupName;
@property (nonatomic, weak) IBOutlet UIImageView *img_v_iconArrow;

-(void) expandGroup;
-(void) shrinkGroup;

@end
