//
//  GUNPopupInfoViewController.h
//  guentner
//
//  Created by Julian Dawo on 07.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GUNPopupInfoViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UIButton *btn_ok;
@property (weak, nonatomic) IBOutlet UIView *v_popupBox;
@property (weak, nonatomic) IBOutlet UIView *v_popupHederBox;
@property (weak, nonatomic) IBOutlet UILabel *lbl_message;

- (IBAction)tapOkBtn:(id)sender;

-(void)setNoButtonStyleWithTitle:(NSString*)title text:(NSString*)text okButtonBlock:(void (^)())okButtonBlock;

@end
