//
//  GUNTutorialDetailViewController.m
//  guentner
//
//  Created by Julian Dawo on 16.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "GUNTutorialDetailViewController.h"
#import "GUNTutorialManager.h"
#import "GUNRootManager.h"
#import "GUNPopupHelper.h"
#import "GUNLoadingViewController.h"
#import "GUNTutorialDetailResponse.h"
#import "GUNInsideTutorialExplanationViewController.h"
#import "GUNTutorialDetailPageResponse.h"
#import "GUNInsightTutorialInteractivePageViewController.h"
#import "GUNInsightTutorialPasswordPageViewController.h"
#import "GUNInsightTutorialFeedbackPageViewController.h"
#import "GUNInsightTutorialHintViewController.h"
#import "GUNTutorialPageViewController.h"
#import "PageItemController.h"
#import "GUNContentTutorialResponse.h"

@interface GUNTutorialDetailViewController ()

@end

@implementation GUNTutorialDetailViewController {
    GUNTutorialManager *_tutorialManager;
    GUNTutorialDetailResponse *_tutorialDetails;
    NSArray *_viewControllerArray;
    GUNTutorialPageViewController *_pageViewController;
    NSUInteger _currentIndex;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    UIImage *navigationBarBackgroundImage = [[UIImage imageNamed:kNavigationBarImage] resizableImageWithCapInsets:kImageSizingEdgeInsets];
//    [self.navigationController.navigationBar setBackgroundImage:navigationBarBackgroundImage forBarMetrics:UIBarMetricsDefault];
    
    
    _tutorialManager = [[GUNRootManager instance] tutorialManager];
    _btn_leftArrow.hidden = YES;
    _btn_rightArrow.hidden = YES;
    
    [self loadTutorial:self.tutorialID];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone && MAX([[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height) < 568.0) { //iphone4
        [self.pc_pageCount setHidden:YES];
    }
    else {
    
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [_pc_pageCount setPageIndicatorTintColor: color_light_gray];
    [_pc_pageCount setCurrentPageIndicatorTintColor: color_gray];

}

-(void) loadTutorial:(NSInteger) activeTutorialID {
    NSString *loadingHandler = [GUNLoadingViewController pushLoadingViewControllerWithText:@"Loading Tutorial..."];
    [_tutorialManager loadTutorialByID:activeTutorialID completed:^(GUNTutorialDetailResponse *response, NSError *error) {

        if (!error) {
            _tutorialDetails = response;

            _btn_rightArrow.hidden = NO;
            [self createPageViewController];
            
            [GUNLoadingViewController popLoadingViewController:loadingHandler];
        } else {
            [GUNLoadingViewController popLoadingViewController:loadingHandler];
            

            NSString *titleText = [_tutorialManager translationForKeyInActiveLanguage:@"Error"];
            NSString *messageText = [_tutorialManager translationForKeyInActiveLanguage:@"The selected tutorial does not exist."];
            [GUNPopupHelper showOneButtonStyleAlertViewWithTitle:titleText messageText:messageText okButtonBlock:^{
                NSLog(@"Tutorial does not exists.");
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }


    }];
}

///TODO: refactoring move to manager, mal sehen ob das gut ist da auch UI Komponenten
-(NSArray *) prepareContent:(GUNTutorialDetailResponse *) tutorialDetails {

    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSUInteger currIndex = 0;
    // first add the web view, for the explanation
    if (tutorialDetails.explainationURL.length > 0) {
        GUNInsideTutorialExplanationViewController* insideTutorialExplanationViewController = (GUNInsideTutorialExplanationViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"GUNInsideTutorialExplanationViewController"];
        insideTutorialExplanationViewController.contentUrl = tutorialDetails.explainationURL;
        insideTutorialExplanationViewController.itemIndex = currIndex;
        currIndex++;
        [result addObject:insideTutorialExplanationViewController];
    }

    // for each tutorial step add an page
    for (id item in tutorialDetails.tutorialPages) {

        if ([item isKindOfClass:[GUNTutorialDetailPageResponse class]]) {
            GUNTutorialDetailPageResponse *pageResponse = (GUNTutorialDetailPageResponse *) item;

            if ([pageResponse.pageType integerValue] == interactivePageType) {

                GUNInsightTutorialInteractivePageViewController *insightTutorialInteractivePageViewController;
                insightTutorialInteractivePageViewController = (GUNInsightTutorialInteractivePageViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"GUNInsightTutorialInteractivePageViewController"];
                insightTutorialInteractivePageViewController.pageSteps = pageResponse.steps;
                insightTutorialInteractivePageViewController.itemIndex = currIndex;
                currIndex++;
                [result addObject:insightTutorialInteractivePageViewController];


            } else if ([pageResponse.pageType integerValue] == passwordPageType) {

                GUNInsightTutorialPasswordPageViewController *insightTutorialPasswordPageViewController;
                insightTutorialPasswordPageViewController = (GUNInsightTutorialPasswordPageViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"GUNInsightTutorialPasswordPageViewController"];
                insightTutorialPasswordPageViewController.itemIndex = currIndex;
                currIndex++;
                [result addObject:insightTutorialPasswordPageViewController];

            } else if ([pageResponse.pageType integerValue] == hintPageType) {
                
                GUNInsightTutorialHintViewController *insightTutorialHintViewController;
                insightTutorialHintViewController = (GUNInsightTutorialHintViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"GUNInsightTutorialHintViewController"];
                insightTutorialHintViewController.itemIndex = currIndex;
                NSString *translatedHintText = [_tutorialManager translationForKeyInActiveLanguage:pageResponse.hintPageExplanation];
                insightTutorialHintViewController.hintText = [_tutorialManager translationForKeyInActiveLanguage:translatedHintText];
                currIndex++;
                [result addObject:insightTutorialHintViewController];
                
            }


        }

    }

    // finally add the feedback view for the tutorial

    GUNInsightTutorialFeedbackPageViewController *insightTutorialFeebackPageViewController;
    insightTutorialFeebackPageViewController = (GUNInsightTutorialFeedbackPageViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"GUNInsightTutorialFeedbackPageViewController"];
    insightTutorialFeebackPageViewController.itemIndex = currIndex;
    insightTutorialFeebackPageViewController.delegate = self;
    insightTutorialFeebackPageViewController.feedbackText = tutorialDetails.feedbackText;
    insightTutorialFeebackPageViewController.feedbackSent = [_tutorialManager checkIfFeedbackForTutorialExists:@(_tutorialID)];

    GUNContentTutorialResponse *nextTutorial = [_tutorialManager nextTutorialForTutorialID:@(_tutorialID) andProduct:@(_productID)];
    insightTutorialFeebackPageViewController.nextTutorial = nextTutorial.contentName;
    
    
    [result addObject:insightTutorialFeebackPageViewController];


    return result;
}


- (void) createPageViewController
{

    
    if (_pageViewController) {
       
        _viewControllerArray = [self prepareContent:_tutorialDetails];
        if([_viewControllerArray count])
        {
            _currentIndex = 0;
            NSArray *startingViewControllers = @[[self getViewControllerForIndex:0]];
            [_pageViewController setViewControllers: startingViewControllers
                                     direction: UIPageViewControllerNavigationDirectionForward
                                      animated: NO
                                    completion: nil];
            
            _btn_leftArrow.hidden = YES;
            _btn_rightArrow.hidden = NO;
            _pc_pageCount.currentPage = _currentIndex;
        }
        return;
    }
    
    GUNTutorialPageViewController *pageController = [self.storyboard instantiateViewControllerWithIdentifier: @"GUNTutorialPageViewController"];
    pageController.dataSource = self;
    pageController.delegate = self;
    
   //pageController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 20.0);
    
    
    _viewControllerArray = [self prepareContent:_tutorialDetails];
    if([_viewControllerArray count])
    {
        _currentIndex = 0;
        NSArray *startingViewControllers = @[[self getViewControllerForIndex:0]];
        [pageController setViewControllers: startingViewControllers
                                 direction: UIPageViewControllerNavigationDirectionForward
                                  animated: NO
                                completion: nil];
    }

    _pageViewController = pageController;
    [self addChildViewController: _pageViewController];
    [_v_containerForPages addSubview: _pageViewController.view];
    [_v_containerForPages updateConstraintsIfNeeded];
    
    [_pageViewController didMoveToParentViewController: self];
}


#pragma mark - pageViewController data source
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    PageItemController *itemController = (PageItemController *) viewController;

    if (itemController.itemIndex > 0)
    {
        _currentIndex--;
        return [self getViewControllerForIndex:itemController.itemIndex-1];

    }

    return nil;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    PageItemController *itemController = (PageItemController *) viewController;

    if (itemController.itemIndex+1 < [_viewControllerArray count])
    {
        _currentIndex++;
        return [self getViewControllerForIndex:itemController.itemIndex+1];
    }

    return nil;
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    
    _pc_pageCount.numberOfPages = [_viewControllerArray count];
    return [_viewControllerArray count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    return _currentIndex;
}



- (void)pageViewController:(UIPageViewController *)viewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    if (!completed){return;}
    
    // Find index of current page
    PageItemController *currentViewController = (PageItemController *)[_pageViewController.viewControllers lastObject];
    NSUInteger indexOfCurrentPage = currentViewController.itemIndex;
    _pc_pageCount.currentPage = indexOfCurrentPage;
    
    //hide the left Button if first item
    if (indexOfCurrentPage == 0) {
        _btn_leftArrow.hidden = YES;
    } else {
        _btn_leftArrow.hidden = NO;
    }
    // hide the right Button if last item
    if (indexOfCurrentPage == _viewControllerArray.count - 1) {
        _btn_rightArrow.hidden = YES;
    } else {
        _btn_rightArrow.hidden = NO;
    }
    
}

- (void)changePage:(UIPageViewControllerNavigationDirection)direction {
    NSUInteger pageIndex = ((PageItemController *) [_pageViewController.viewControllers objectAtIndex:0]).itemIndex;
    
    if (direction == UIPageViewControllerNavigationDirectionForward) {
        pageIndex++;
    }
    else {
        pageIndex--;
    }
    if (pageIndex >= [_viewControllerArray count]) {
        return;
    }
    PageItemController *viewController = [self getViewControllerForIndex:pageIndex];
    
    if (viewController == nil) {
        return;
    }
    __weak typeof(_pc_pageCount) weakPcPageCount = _pc_pageCount;
    __weak typeof(_btn_leftArrow) weakBtn_leftArrow = _btn_leftArrow;
    __weak typeof(_btn_rightArrow) weakBtn_rightArrow = _btn_rightArrow;
    [_pageViewController setViewControllers:@[viewController]
                                  direction:direction
                                   animated:YES
                                 completion:^(BOOL finished) {
                                     weakPcPageCount.currentPage = pageIndex;
                                     
                                     //hide the left Button if first item
                                     if (pageIndex == 0) {
                                         weakBtn_leftArrow.hidden = YES;
                                     } else {
                                         weakBtn_leftArrow.hidden = NO;
                                     }
                                     // hide the right Button if last item
                                     if (pageIndex == _viewControllerArray.count - 1) {
                                         weakBtn_rightArrow.hidden = YES;
                                     } else {
                                         weakBtn_rightArrow.hidden = NO;
                                     }
                                 }];
}

-(PageItemController *)getViewControllerForIndex:(NSUInteger) index {
    PageItemController *viewController = [_viewControllerArray objectAtIndex:index];

    return viewController;
    
}

- (IBAction)tapRightButton:(id)sender {
    
    NSLog(@"tap right Button");
    [self changePage:UIPageViewControllerNavigationDirectionForward];
    
}

- (IBAction)tapLeftButton:(id)sender {
    
    NSLog(@"tap left Button");
    [self changePage:UIPageViewControllerNavigationDirectionReverse];
    
}

- (IBAction)tapPageControl:(id)sender {
    
    UIPageControl *pager=sender;
    int page = pager.currentPage;
    NSLog(@"tap change page control to new index: %i",page);
    PageItemController *currentViewController = (PageItemController *)[_pageViewController.viewControllers lastObject];
    NSUInteger indexOfCurrentPage = currentViewController.itemIndex;
    
    if (page < indexOfCurrentPage) {
        
        [self changePage:UIPageViewControllerNavigationDirectionReverse];
    } else if (page > indexOfCurrentPage) {
        
        [self changePage:UIPageViewControllerNavigationDirectionForward];
    }

}

- (void)userWantsToStartTheNextTutorialInRow {
    
    GUNContentTutorialResponse *nextTutorial = [_tutorialManager nextTutorialForTutorialID:@(_tutorialID) andProduct:@(_productID)];

    if (nextTutorial) {
        
        
        NSString* translatedTutorialName = [_tutorialManager translationForKeyInActiveLanguage:nextTutorial.contentName];
        
        self.tutorialID = [nextTutorial.tutorialId integerValue];
        [self loadTutorial:nextTutorial.tutorialId.integerValue];
        self.title = translatedTutorialName;
        
        
    }



}

- (void)userSendFeedback:(NSNumber *)rating {
    
    [_tutorialManager feedbackSentForTutorial:@(_tutorialID) withRating:rating];

}


#pragma mark Auto Rotation Methods

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


@end
