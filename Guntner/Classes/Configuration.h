//
//  Configuration.h
//  Guntner
//
//  Created by Shiraz Omar on 8/5/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//


// Device Type Check
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )


// Configuration File Created To Hold Common Definitions For The Guntner App

// Fixtures for Testing
#define USE_FIXTURES 0

//Colors

#define color_clear [UIColor clearColor]
#define color_dark_gray [UIColor colorWithRed:238.0f/255.0f green:238.0f/255.0f blue:238.0f/255.0f alpha:1.0]
#define color_light_blue [UIColor colorWithRed:231.0f/255.0f green:237.0f/255.0f blue:250.0f/255.0f alpha:1.0]
#define color_dark_blue [UIColor colorWithRed:0.0f/255.0f green:99.0f/255.0f blue:175.0f/255.0f alpha:1.0]
#define color_dark_blue_020 [UIColor colorWithRed:0.0f/255.0f green:99.0f/255.0f blue:175.0f/255.0f alpha:0.2]
#define color_black_090 [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.9]
#define color_black_015 [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.15]
#define color_red [UIColor colorWithRed:213.0f/255.0f green:17.0f/255.0f blue:48.0f/255.0f alpha:1.0]

#define color_gray [UIColor colorWithRed:138.0f/255.0f green:138.0f/255.0f blue:138.0f/255.0f alpha:1.0]
#define color_light_gray [UIColor colorWithRed:208.0f/255.0f green:208.0f/255.0f blue:208.0f/255.0f alpha:1.0]



// TestFlightDefinitions
#define kTestFlightToken @"6d58e37d-e366-4546-86aa-4ae7513373f2"


// Google Analytics
#define kGAnalyticsTrackingID @"UA-35025796-3"  // TEST ID: UA-44652261-1 / RELEASE ID: UA-35025796-3


// Xib Names / Identifiers
#define kMainStoryboard @"MainStoryboard"
#define kCustomKeyboardXibName @"CustomKeyboard"
#define kMenuViewController @"MenuViewController"
#define kRootViewController @"RootViewController"
#define kNavigationController @"NavigationViewController"
#define kColdSliderComponentXibName @"ColdSliderComponent"
#define kLoadingViewControllerXibName @"LoadingViewController"
#define kPopupListViewControllerXibName @"PopupListViewController"
#define kColdSliderRefrigerantSelectorXibName @"ColdSliderRefrigerantSelector"

#define kPrivacyViewControllerIdentifier @"PrivacyViewController"
#define kFeedbackViewControllerIdentifier @"FeedbackViewController"
#define kInfoPageViewControllerIdentifier @"InfoPageViewController"
#define kNewsfeedViewControllerIdentifier @"NewsfeedViewController"
#define kContactsViewControllerIdentifier @"ContactsViewController"
#define kMaterialViewControllerIdentifier @"MaterialViewController"
#define kImpressumViewControllerIdentifier @"ImpressumViewController"
#define kConverterViewControllerIdentifier @"ConverterViewController"
#define kColdSliderViewControllerIdentifier @"ColdSliderViewController"
#define kInformationViewControllerIdentifier @"InformationViewController"
#define kLiteratureHubViewControllerIdentifier @"LiteratureHubViewController"
#define kDisclaimerViewControllerIdentifier @"DisclaimerViewController"


#define kTutorialsViewControllerIdentifier @"TutorialsViewController"





// Locale Definitions
#define kMinusSign @"-"
#define kGermanLocale @"de"
#define kGermanDecimalSeperator @","
#define kGermanGroupingSeperator @"."
#define kEnglishDecimalSeperator @"."
#define kEnglishGroupingSeperator @","
#define kAllLocaleDecimalSeperatorCharacterSet @".,"



// General Definitions
#define kGaugePressureSymbol @"(g)"
#define kAtmosphericPressureSymbol @"(a)"



// UIColor Definitions
#define kGuntnerColor005A37 [UIColor colorWithRed:(0.0/255.0) green:(87.0/255.0) blue:(163.0/255.0) alpha:1.0]
#define kGuntnerColorCBDDEE [UIColor colorWithRed:(0.0/255.0) green:(87.0/255.0) blue:(163.0/255.0) alpha:0.2]
#define kGuntnerColorD6002C [UIColor colorWithRed:(214.0/255.0) green:(0.0/255.0) blue:(44.0/255.0) alpha:1.0]
#define kGuntnerColorFCE5EA [UIColor colorWithRed:(252.0/255.0) green:(229.0/255.0) blue:(234.0/255.0) alpha:1.0]
#define kGuntnerColorEEEEEE [UIColor colorWithRed:(238.0/255.0) green:(238.0/255.0) blue:(238.0/255.0) alpha:1.0]



// Image Sizing Inset Definitions
#define kImageSizingEdgeInsets UIEdgeInsetsMake(6, 6, 6, 6)



// Image Files
#define kGreyButtonImage @"btn_grey_standart"
#define kBlueButtonImage @"btn_blue_standart"
#define kGreyButtonFlatImage @"btn_grey_hover"
#define kWhiteButtonFlatImage @"btn_white_hover"
#define kConverterInputTextFieldBackgroundImage @"bkg_input_converter"
#define kConverterNumpadGreyButtonImage @"bkg_numpad_converter_standart"
#define kConverterNumpadDarkGreyButtonImage @"bkg_numpad_converter_special_standart"
#define kConverterOutputTextFieldBackgroundImage @"bkg_numpad_converter_special_hover"
#define kColdSliderPressureTextFieldBackgroundImage @"bkg_input_kaelteschieber_blue_standart"
#define kColdSliderTemperatureTextFieldBackgroundImage @"bkg_input_kaelteschieber_red_standart"
#define kColdSliderStateChangeUnSelected @"icn_unselected.png"
#define kColdSliderStateChangeSelected @"icn_selected.png"


// MenuView
#define kMenuViewWidth 280.0
#define kMenuViewPadding 10.0

#define kMenuButtonAlpha 0.6

#define kMenuToggleNotificationIdentifier @"showHideMenu"
#define kMenuPresentSelectedItemIdentifier @"presentSelectedMenuItem"

#define kNewsfeedIconImage [UIImage imageNamed:@"icn_news"]
#define kContactsIconImage [UIImage imageNamed:@"icn_kontakte"]
#define kConverterIconImage [UIImage imageNamed:@"icn_converter"]
#define kColdSliderIconImage [UIImage imageNamed:@"icn_kaelteschieber"]
#define kLiteratureHubIconImage [UIImage imageNamed:@"icn_literatur_hub"]
#define kMaterialRecommendationIconImage [UIImage imageNamed:@"icn_materialempfehlung"]
#define kControlsIconImage [UIImage imageNamed:@"icn_controls"]

#define kNewsfeedButtonTitle NSLocalizedString(@"Newsfeed", nil)
#define kContactsButtonTitle NSLocalizedString(@"Contacts", nil)
#define kImpressumButtonTitle NSLocalizedString(@"About Us", nil)
#define kConverterButtonTitle NSLocalizedString(@"Converter", nil)
#define kColdSliderButtonTitle NSLocalizedString(@"Cold Slider", nil)
#define kLiteratureHubButtonTitle NSLocalizedString(@"Literature Hub", nil)
#define kInformationButtonTitle NSLocalizedString(@"InfoPageViewTitle", nil)
#define kMaterialRecommendationButtonTitle NSLocalizedString(@"Material Selection", nil)


#define kTutorialButtonTitle NSLocalizedString(@"Tutorials", nil)


#define kControlsButtonTitle NSLocalizedString(@"Controls", nil)
#define kControlPressedKey NSLocalizedString(@"controlPressed", nil)

#define kEnableDisableViewDeckToggleNotificationIdentifier @"EnableDisableVewDeckToggle"

#define kControlApplicationName @"guentner-app"
#define kControlApplicationId @"983534525"

// RootViewController
#define kBackButtonTitle NSLocalizedString(@"Back", nil)
#define kBackButtonTitleColor [UIColor whiteColor]
#define kBackButtonTitleFont [UIFont fontWithName:@"HelveticaNeue-Light" size:17.0f]

#define kBackButtonImage @"btn_back"
#define kMenuButtonImage @"icn_navigation"
#define kMenuButtonIdentifier @"MenuButton"
#define kBackButtonIdentifier @"BackButton"
#define kMenuButtonFrame CGRectMake(0, 0, 44, 44)
#define kBackButtonFrame CGRectMake(0, 0, 70, 44)

#define kRootViewControllerInvalidMenuError NSLocalizedString(@"Invalid Menu Item: %@", nil)



// NavigationController
#define kNavigationBarImage @"bkg_nav_bar.png"
#define kDetailsTitle NSLocalizedString(@"Details", nil)
#define kNavigationBarTitleFontColor [UIColor whiteColor]
#define kSetMenuButtonIdentifier @"SetNavigationBarMenuButton"
#define kNavigationBarTitleFont [UIFont fontWithName:@"HelveticaNeue" size:20.0]
#define kSwapMenuButtonWithBackButtonIdentifier @"SwapNavigationBarMenuButtonWithBackButton"



// LiteratureHubViewController
#define kSharePDFButtonFrame CGRectMake(0, 0, 25, 16)
#define kShowSharePDFButtonImage @"icn_navbar_share_mail.png"
#define kShowSharePDFButtonIdentifier @"ShowSharePDFButton"



// PageNavigation Plugin
#define kCDVPageNavigationResultTypeSuccess @"success"
#define kCDVPageNavigationResultTypeShowSharePDF @"showSharePDF"
#define kCDVPageNavigationBackButtonError NSLocalizedString(@"Error Showing Back Button", nil)



// LoadingView Plugin
#define kLoadingViewVerticalOffset 60.0
#define kShowLoadingViewIdentifier @"ShowLoadingViewFromCordovaView"
#define kDismissLoadingViewIdentifier @"DismissLoadingViewFromCordovaView"



// Calculation Specific Definitions
#define kAtmosphericPressure 1.01325
#define kMaxCalculationInputLength 10
#define kMaxFractionDigitsColdSlider 2
#define kMaxSignificantDigitsConverter 5
#define kMaxSignificantDigitsColdSlider 7
#define kCalculationInputCharacterList @"-1234567890.,"



// ConverterViewController
#define kConverterPopupListToTitle NSLocalizedString(@"Convert To", nil)
#define kConverterPopupListFromTitle NSLocalizedString(@"Convert From", nil)

#define kConverterHorizontalPickerElementSpacing 50.0

#define kConverterHorizontalPickerElementFont [UIFont fontWithName:@"HelveticaNeue-Medium" size:18.0]

#define kConverterSeletectedInputValue @"converterSelectedInputValue"
#define kConverterSeletectdOutPutValue @"converterSelectedOutputvalue"
#define kConverterSelectedfromValue @"converterSelectedFromValue"
#define kConverterSelectedToValue @"converterSelectedToValue"
#define kConverterSelectedUnit @"conveterSelectedUnit"


// ColdSliderViewController
#define kCelsiusString @"Celsius"
#define kFahrenheitString @"Fahrenheit"

#define kCelsiusCharacterString @"℃"
#define kFahrenheitCharacterString @"℉"

#define kColdSliderSliderPositionLeft 0.0

#define kColdSliderUserDefaultKey @"ColdSliderUserDefault"
#define kColdSliderSelectedRefrigirant @"selectedRefrigirant"
#define kColdSliderSelectedPressureType @"selectedPressureType"
#define kColdSliderSelectedPressureValue @"pressureValue"
#define kColdSliderSelectedTempratureValue @"tempratureValue"
#define kColdSliderSelectedTempratureUnit @"selectedTempUnit"
#define kColdSliderSelectedUnitType @"selectedUnitType"
#define kColdSliderSelectedPressureUnit @"pressureUnit"




#define kColdSliderUnitButtonImageInsets UIEdgeInsetsMake(0.0, 105.0, 0.0, 5.0)

#define kColdSliderComponentButtonTitle NSLocalizedString(@"Unit", nil)
#define kColdSliderComponentButtonInsets UIEdgeInsetsMake(0.0, 10.0, 0.0, 10.0)
#define kColdSliderComponentPressureListCellFont [UIFont fontWithName:@"HelveticaNeue" size:14.0f]

#define kColdSliderComponentRefrigerantListCellFont [UIFont fontWithName:@"HelveticaNeue" size:14.0f]

#define kColdSliderRefrigerantButtonInsets UIEdgeInsetsMake(0.0, 10.0, 0.0, 10.0)

#define kColdSliderDismissKeyboardButtonTitle NSLocalizedString(@"DismissKeyboardButtonTitle", nil)

#define kColdSliderRefrigerantSelectorButtonTitle NSLocalizedString(@"Refrigerant", nil)

#define kColdSliderInvalidPressureAlertTitle NSLocalizedString(@"Invalid Pressure", nil)
#define kColdSliderInvalidPressureAlertMessage NSLocalizedString(@"The Pressure Entered Falls Outside The Range For The Selected Refrigerant", nil)
#define kColdSliderInvalidTemperatureAlertTitle NSLocalizedString(@"Invalid Temperature", nil)
#define kColdSliderInvalidTemperatureAlertMessage NSLocalizedString(@"The Temperature Entered Falls Outside The Range For The Selected Refrigerant", nil)

#define kColdSliderRefrigerantNotSelectedAlertTitle NSLocalizedString(@"Refrigerant Not Selected", nil)
#define kColdSliderRefrigerantNotSelectedAlertMessage NSLocalizedString(@"Please Select Refrigerant Before Attempting To Change Temperature", nil)



// InfoPageViewController Definitions
#define kInfoPageViewTitle NSLocalizedString(@"InfoPageViewTitle", nil)
#define kInfoPageViewTextViewString NSLocalizedString(@"InfoPageViewTextViewString", nil)
#define kInfoPageViewBackButtonTitle NSLocalizedString(@"InfoPageViewBackButtonTitle", nil)
#define kInfoPageTextViewFont [UIFont fontWithName:@"HelveticaNeue" size:18.0]
#define kInfoPageTextViewBoldFont [UIFont fontWithName:@"HelveticaNeue-Bold" size:18.0]



// InformationViewController Definitions
#define kGAnalyticsSwitchState @"GAnalyticsSwitchState"
#define kSendButtonTitle NSLocalizedString(@"Send", nil)
#define kInformationTableHeaderSection1Row1Title NSLocalizedString(@"About Us", nil)
#define kGAnalyticsSwitchLabelTitle NSLocalizedString(@"GAnalyticsSwitchLabelTitle", nil)
#define kInformationTableHeaderViewBoldFont [UIFont fontWithName:@"HelveticaNeue-Bold" size:12.0]
#define kInformationTableHeaderSection0Title NSLocalizedString(@"InformationTableHeaderSection0Title", nil)
#define kInformationTableHeaderSection1Title NSLocalizedString(@"InformationTableHeaderSection1Title", nil)
#define kInformationTableHeaderSection0Row0Title NSLocalizedString(@"InformationTableHeaderSection0Row0Title", nil)
#define kInformationTableHeaderSection1Row0Title NSLocalizedString(@"InformationTableHeaderSection1Row0Title", nil)
#define kInformationTableHeaderSection1Row2Title NSLocalizedString(@"InformationTableHeaderSection1Row2Title", nil)



// ColdSliderCalculator
#define kColdSliderDataFileType @"plist"
#define kColdSliderDataFile @"ColdSliderData"

// ColdSliderData Definitions
#define kColdSliderDataConstant1 @"c1"
#define kColdSliderDataConstant2 @"c2"
#define kColdSliderDataConstant3 @"c3"
#define kColdSliderDataConstant4 @"c4"
#define kColdSliderDataConstant5 @"c5"
#define kColdSliderDataConstant6 @"c6"
#define kColdSliderDataConstant7 @"c7"
#define kColdSliderDataConstant8 @"c8"
#define kColdSliderDataConstant9 @"c9"
#define kColdSliderDataConstant10 @"c10"

#define kColdSliderDataOIL @"OIL"
#define kColdSliderDataODP @"ODP"
#define kColdSliderDataGWP @"GWP"
#define kColdSliderDataTempMin @"tMin (℃)"
#define kColdSliderDataTempMax @"tMax (℃)"
#define kColdSliderDataTempCritical @"tCrit (℃)"
#define kColdSliderDataTempMinF @"tMin (℉)"
#define kColdSliderDataTempMaxF @"tMax (℉)"
#define kColdSliderDataTempCriticalF @"tCrit (℉)"


// ColdSliderRefrigerantSelector
#define kColdSliderRefrigerantSelectorListSeperatorHeight 1.0
#define kColdSliderRefrigerantSelectorListSeperatorPadding 10.0
#define kColdSliderRefrigerantSelectorListSeperatorTag 45645645
#define kColdSliderRefrigerantSelectorOkButtonTitle NSLocalizedString(@"Okay", nil)



// Converter
#define kConverterPressureUnitBar @"bar"
#define kConverterSI_UnitKey @"SI_Unit"
#define kConverterDataFileType @"plist"
#define kConverterDataFile NSLocalizedString(@"ConverterData", nil)
#define kConverterUnitTypePressure NSLocalizedString(@"Pressure", nil)
#define kConverterUnitTypeTemperature NSLocalizedString(@"Temperature", nil)



// Alert View Definitions
#define kAlertViewCancelButtonTitle NSLocalizedString(@"OK", nil)



// iPhone Keyboard Offset
#define kKeyboardOffsetForOlderiPhone 80.0



// PhoneGap / Cordova
#define kNewsfeedShareIconImage @"icn_sort"
#define kContactsFilterIconImage @"icn_filter"

#define kHTML5Folder @"HTML5"
#define kHTML5IndexPage @"index.html"
#define kHTML5ContactsPage @"Contacts.html"
#define kHTML5NewsFeedPage @"NewsFeed.html"
#define kHTML5LiteratureHubPage @"LiteratureHub.html"
#define kHTML5MaterialRecommendationPage @"MaterialRecommendation.html"

#define kContactsJSONFileName @"contacts.json"
#define kNewsFeedJSONFileName @"newsfeed.json"

#define kCacheDiskPath @"nsurlcache"
#define kCacheSizeMemory (8 * 1024 * 1024) // 8MB
#define kCacheSizeDisk (32 * 1024 * 1024) // 32MB

#define kSharePDFJavaScriptFunction @"sharePDF()"
#define kGoBackJavaScriptFunction @"ChangePageToBack()"
#define kShowFilterJavaScriptFunction @"ShowFilterPopup()"
#define kGoBackJavaScriptFunctionForPDF @"window.history.back()"

// App Rating Feature
#define kAppRatingCountTrigger 7


// Enums
typedef enum
{
    interactiveTutorialContentType = 10001,
    externalExplanationContentType = 10002,
    contentGroupContentType = 10003
} ContentType;

typedef enum
{
    asiaContenRegion = 20001,
    europeContenRegion = 20002,
    americaContenRegion = 20003
} ContenRegion;

typedef enum
{
    interactivePageType = 30001,
    passwordPageType = 30002,
    hintPageType = 30003,
} PageType;

typedef enum
{
    arrowUpIcon = 40001,
    arrowDownIcon = 40002,
    arrowLeftIcon = 40003,
    arrowRightIcon = 40004,
    twoArrowsIcon = 40005,
    starIcon = 40006,
    blackArrowIcon = 40007,
    arrowAIcon = 40008,
    enterIcon = 40009
} DisplayIcon;

typedef enum
{
    arrowUpControllerButtonType = 50001,
    arrowDownControllerButtonType = 50002,
    arrowLeftControllerButtonType = 50003,
    arrowRightControllerButtonType = 50004,
    enterControllerButtonType = 50005,
    homeControllerButtonType = 50006,
} ControllerButtonType;

