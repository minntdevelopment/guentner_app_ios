//
//  ContactsViewController.m
//  Guntner
//
//  Created by Shiraz Omar on 8/6/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "ContactsViewController.h"

#import <Cordova/CDVViewController.h>

#import "Configuration.h"
#import "LoadingViewController.h"
#import "Firebase.h"

@interface ContactsViewController () {
    
    LoadingViewController *loadingView;
    CDVViewController* phoneGapViewController;
}

@property (nonatomic, strong) LoadingViewController *loadingView;
@property (nonatomic, strong) CDVViewController *phoneGapViewController;

@end


@implementation ContactsViewController


@synthesize loadingView = _loadingView;
@synthesize phoneGapViewController = _phoneGapViewController;


#pragma mark Setup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        // Uncomment to override the CDVCommandDelegateImpl used
        // _commandDelegate = [[MainCommandDelegate alloc] initWithViewController:self];
        // Uncomment to override the CDVCommandQueue used
        // _commandQueue = [[MainCommandQueue alloc] initWithViewController:self];
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    
    // Show Navigation Bar
    self.navigationController.navigationBarHidden = NO;
    
    
    // Setup Notification Listeners
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showLoadingIndicator) name:kShowLoadingViewIdentifier object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeLoadingIndicator) name:kDismissLoadingViewIdentifier object:nil];
    
    
    // Method To Check JSON File Age and Delete Once Every 7 Days So It Downloads New Data
    [self deleteJSONFileThatIsMoreThanAWeekOld];
    
    
    // Setup PhoneGap/Cordova
    [self setupPhoneGap];
    
    
    // Show Loading Indicator
    [self showLoadingIndicator];

}


- (void)viewWillAppear:(BOOL)animated {
    
    [self initializeRightBarButtonItem];
}


- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    // Send Google Analytics Information
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:kContactsViewControllerIdentifier];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    [FIRAnalytics logEventWithName:kGAIScreenName
                        parameters:@{
                                     kFIRParameterItemName:kContactsViewControllerIdentifier,
                                     kFIRParameterContentType:@"screen"
                                     }];
}


- (void)initializeRightBarButtonItem
{
    // Add Filter Button To Navigation Bar
    
    // Release Filter Button Button
    if (!self.navigationController.topViewController.navigationItem.rightBarButtonItem) {
        // Create and Show Share Button
        UIImage *buttonImage = [UIImage imageNamed:kContactsFilterIconImage];
        UIButton *filterButton = [[UIButton alloc] initWithFrame:kMenuButtonFrame];
        [filterButton setImage:buttonImage forState:UIControlStateNormal];
        [filterButton addTarget:self action:@selector(sendFilterCallToPhoneGap) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *filterBarButton = [[UIBarButtonItem alloc] initWithCustomView:filterButton];
        self.navigationController.topViewController.navigationItem.rightBarButtonItem = filterBarButton;
        filterBarButton = nil;
        filterButton = nil;
        buttonImage = nil;
    }
}


#pragma mark PhoneGap/Cordova Methods

- (void)setupPhoneGap {
    
    @autoreleasepool {
        
        // Setup PhoneGap/Cordova
        NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
        
        [cookieStorage setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
        
        int cacheSizeDisk = kCacheSizeDisk; // 32MB
        int cacheSizeMemory = kCacheSizeMemory; // 8MB
        NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:kCacheDiskPath];
        [NSURLCache setSharedURLCache:sharedCache];
        
        
        self.phoneGapViewController = [CDVViewController new];
        self.phoneGapViewController.wwwFolderName = kHTML5Folder;
        self.phoneGapViewController.startPage = kHTML5ContactsPage;
        
        self.phoneGapViewController.view.frame = self.view.bounds;
        [self.view addSubview:self.phoneGapViewController.view];
    }
    
}


- (void)goBackToPreviousPageOnPhoneGapView {
    
    // Call Javascript Function To Go To Previous Page
    [self.phoneGapViewController.webView stringByEvaluatingJavaScriptFromString:kGoBackJavaScriptFunction];
    
    // Replace Back Button With Menu Button
    [[NSNotificationCenter defaultCenter] postNotificationName:kSwapMenuButtonWithBackButtonIdentifier object:nil];
 
    [[self navigationItem] setTitle:kContactsButtonTitle];
    
}


- (void)sendFilterCallToPhoneGap {
    
    // Call Javascript Function To Show Filter Contacts Popup
    [self.phoneGapViewController.webView stringByEvaluatingJavaScriptFromString:kShowFilterJavaScriptFunction];
    
}


- (void)deleteJSONFileThatIsMoreThanAWeekOld {
    
    NSString *jsonFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:kContactsJSONFileName];
    
    // Check If File Exists
    if([[NSFileManager defaultManager] fileExistsAtPath:jsonFilePath]){
        // Get File Modification Date
        NSDate *fileDate =[[[NSFileManager defaultManager] attributesOfItemAtPath:jsonFilePath error:nil] objectForKey:NSFileModificationDate];
        
        //if ([[NSDate dateWithTimeInterval:(60*60*24*7) sinceDate:fileDate] timeIntervalSinceNow] >= 0) { // For Testing
        if ([[NSDate dateWithTimeInterval:(60*60*24*7) sinceDate:fileDate] timeIntervalSinceNow] <= 0) {
            // If File Is More Than A Week Old, Delete It
            [[NSFileManager defaultManager] removeItemAtPath:jsonFilePath error:nil];
        }
        
        // Release Objects
        jsonFilePath = nil;
        fileDate = nil;
    }
    
}


- (void)showLoadingIndicator {
    
    // Disable User Interaction On PhoneGap View
    [self.phoneGapViewController.view setUserInteractionEnabled:NO];
    
    // Show Loading Indicator
    if (self.loadingView == nil) {
        self.loadingView = [[LoadingViewController alloc] initWithNibName:kLoadingViewControllerXibName bundle:nil];
    }
    [self.loadingView.loadingIndicator startAnimating];
    [self.view addSubview:self.loadingView.view];
    self.loadingView.view.center = CGPointMake(self.view.center.x, (self.view.center.y - kLoadingViewVerticalOffset));
    
}

- (void)removeLoadingIndicator {
    
    // Enable User Interaction On PhoneGap View
    [self.phoneGapViewController.view setUserInteractionEnabled:YES];
    
    // Remove Loading Indicator
    if (self.loadingView != nil) {
        [self.loadingView.loadingIndicator stopAnimating];
        [self.loadingView.view removeFromSuperview];
    }
}


#pragma mark UIWebDelegate implementation

- (void)webViewDidFinishLoad:(UIWebView*)theWebView
{
    // White base color for background matches the native apps
    theWebView.backgroundColor = [UIColor whiteColor];
    
    
    return [self.phoneGapViewController webViewDidFinishLoad:theWebView];
}

/* Comment out the block below to over-ride */

/*
 
 - (void) webViewDidStartLoad:(UIWebView*)theWebView
 {
 return [super webViewDidStartLoad:theWebView];
 }
 
 - (void) webView:(UIWebView*)theWebView didFailLoadWithError:(NSError*)error
 {
 return [super webView:theWebView didFailLoadWithError:error];
 }
 
 - (BOOL) webView:(UIWebView*)theWebView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType
 {
 return [super webView:theWebView shouldStartLoadWithRequest:request navigationType:navigationType];
 }
 */


#pragma mark Auto Rotation Methods

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


#pragma mark Cleanup

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}


- (void)viewWillDisappear:(BOOL)animated {
    
    // Release Share Button Button
    if (self.navigationController.topViewController.navigationItem.rightBarButtonItem) {
        self.navigationController.topViewController.navigationItem.rightBarButtonItem = nil;
    }
    
}


- (void)dealloc {
    

    [[NSNotificationCenter defaultCenter] removeObserver:self name:kShowLoadingViewIdentifier object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kDismissLoadingViewIdentifier object:nil];
    
    self.loadingView = nil;
    self.phoneGapViewController = nil;
    
}


@end
