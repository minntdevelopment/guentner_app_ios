//
// Created by Julian Dawo on 17.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PageItemController.h"
#import "GUNBlueButton.h"

@class GUNInsightTutorialFeedbackPageViewController;

@protocol GUNFeedbackDelegate <NSObject>;

-(void) userWantsToStartTheNextTutorialInRow;
-(void) userSendFeedback:(NSNumber *) rating;

@end

@interface GUNInsightTutorialFeedbackPageViewController : PageItemController

@property (nonatomic, assign) id<GUNFeedbackDelegate>  delegate;
@property (nonatomic, strong) NSString *feedbackText;
@property (nonatomic) BOOL feedbackSent;
@property (nonatomic, weak) IBOutlet UILabel *lbl_feddbackMessage;
@property (nonatomic, weak) IBOutlet UILabel *lbl_feddbackQuestion;
@property (nonatomic, weak) IBOutlet UIView *v_ratingContainer;
@property (nonatomic, weak) IBOutlet UIButton *btn_rating1;
@property (nonatomic, weak) IBOutlet UIButton *btn_rating2;
@property (nonatomic, weak) IBOutlet UIButton *btn_rating3;
@property (nonatomic, weak) IBOutlet UIButton *btn_rating4;
@property (nonatomic, weak) IBOutlet UIButton *btn_rating5;
@property (nonatomic, weak) IBOutlet UILabel *lbl_ratingWorst;
@property (nonatomic, weak) IBOutlet UILabel *lbl_ratingBest;
@property (nonatomic, weak) IBOutlet GUNBlueButton *btn_sendRating;
@property (nonatomic, weak) IBOutlet UILabel *lbl_titleNextTutorial;
@property (nonatomic, weak) IBOutlet UIButton *btn_jumpToNextTutorial;
@property (nonatomic, weak) IBOutlet UIImageView *iv_nextBtnIcon;
@property (weak, nonatomic) IBOutlet UILabel *lbl_titleJumpToNextTutorial;
@property (weak, nonatomic) IBOutlet UILabel *lbl_feedbackSuccess;

@property(nonatomic, copy) NSString *nextTutorial;

- (IBAction)rating1ButtonPressed:(id)sender;
- (IBAction)rating2ButtonPressed:(id)sender;
- (IBAction)rating3ButtonPressed:(id)sender;
- (IBAction)rating4ButtonPressed:(id)sender;
- (IBAction)rating5ButtonPressed:(id)sender;

- (IBAction)sendRatingButtonPressed:(id)sender;
- (IBAction)jumpToNextButtonPressed:(id)sender;

@end