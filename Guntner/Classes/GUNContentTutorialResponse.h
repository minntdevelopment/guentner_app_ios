//
// Created by Julian Dawo on 03.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GUNProductContentResponse.h"


@interface GUNContentTutorialResponse : GUNProductContentResponse

@property (nonatomic, strong) NSNumber *tutorialId;
@end