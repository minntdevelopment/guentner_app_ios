//
//  NewsDetailviewController.m
//  guentner
//
//  Created by Balazs Auth on 03.12.18.
//  Copyright © 2018 Brandixi3. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "NewsDetailViewController.h"

@implementation NewsDetailViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.v_containter.layer.cornerRadius = 8.0;
    self.v_containter.layer.masksToBounds = true;
    
    if (self.selectedNewsObject.imageLink != nil) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                       ^{
                           NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.selectedNewsObject.imageLink]];
                           dispatch_sync(dispatch_get_main_queue(), ^{
                               UIImage* image = [UIImage imageWithData:imageData];
                               self.iv_article.image = image;
                               [self loadValues];
                           });
                       });
        
       
    }
    else {
        [self loadValues];
    }
}

-(void) loadValues {
    
    [self.lbl_title setText: self.selectedNewsObject.title];
    CGFloat fixedWidth = _lbl_title.frame.size.width;
    CGSize newSize = [_lbl_title sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    self.c_lbl_title_height.constant = newSize.height;
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[self.selectedNewsObject.createdDate longValue]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSString *strDate = [dateFormat  stringFromDate:date];
    [self.lbl_date setText:strDate];
    
    self.c_iv_width.constant = _v_containter.frame.size.width - 16.0;
    [self.tv_article setText:self.selectedNewsObject.text];
    CGFloat fixedTVWidth = self.tv_article.frame.size.width;
    CGSize newTVSize = [self.tv_article sizeThatFits:CGSizeMake(fixedTVWidth, MAXFLOAT)];
    self.c_tv_height.constant = newTVSize.height;
    [self.tv_article layoutIfNeeded];
    self.c_contentview_height.constant = _btn_share.frame.origin.y + _btn_share.frame.size.height + 40;
    [self.view layoutIfNeeded];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (IBAction)btnShareTapped:(id)sender {
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
        [mailController setMailComposeDelegate:self];
        [mailController setSubject:self.selectedNewsObject.title];
        [mailController setMessageBody:self.selectedNewsObject.title isHTML:NO];
        [self presentViewController:mailController animated:YES completion:nil];
    }
}


#pragma mark Mail delegate Methods

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark Auto Rotation Methods

- (BOOL)shouldAutorotate {
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}


#pragma mark Cleanup

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

@end
