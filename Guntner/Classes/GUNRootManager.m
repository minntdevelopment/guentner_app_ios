//
//  GUNRootManager.m
//  guentner
//
//  Created by Julian Dawo on 01.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "GUNRootManager.h"
#import "GUNDataManager.h"

@implementation GUNRootManager

static GUNRootManager *_sharedSingleton;


+ (void)initialize
{
    static BOOL initialized = NO;
    if(!initialized)
    {
        initialized = YES;
        _sharedSingleton = [[GUNRootManager alloc] init];
    }
}


-(id)init
{
    if(self = [super init])
    {

        //Data
        _dataManager = [[GUNDataManager alloc] init];

        // Rest
        _restManager = [[GUNRestmanager alloc] initWithDataManager:_dataManager];
        
        // Tutorials
        _tutorialManager = [[GUNTutorialManager alloc] initWithRestManager:_restManager dataManager:_dataManager];
        
        //Reachability
        _reachabilityManager = [[GUNReachabilityManager alloc] init];
    }
    return self;
}


+ (GUNRootManager* ) instance {
    return _sharedSingleton;
}

@end
