//
// Created by Julian Dawo on 03.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GUNProductResponse : NSObject

@property (nonatomic, strong) NSNumber *productID;
@property (nonatomic, strong) NSString *productName;

@property (nonatomic, strong) NSArray *content;
@end