//
// Created by Julian Dawo on 15.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GUNTutorialDetailResponse : NSObject

@property (nonatomic, strong) NSNumber *tutorialID;
@property (nonatomic, strong) NSString *explainationURL;
@property (nonatomic, strong) NSString *feedbackText;
@property (nonatomic, strong) NSArray *tutorialPages;

@end