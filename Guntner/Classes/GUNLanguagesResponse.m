//
// Created by Julian Dawo on 01.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "GUNLanguagesResponse.h"
#import "GUNLanguageItemResponse.h"


@implementation GUNLanguagesResponse {

}

- (id)initWithDictionary:(NSDictionary *)dictionary {
    self = [super init];

    NSMutableArray* result = [[NSMutableArray alloc] init];
    NSArray *allTranslationItems = dictionary[@"translations"];
    if ([allTranslationItems isKindOfClass:[NSArray class]]) {

        for (NSDictionary *tmpDictionary in allTranslationItems) {
            GUNLanguageItemResponse *languageItem = [[GUNLanguageItemResponse alloc] init];

            languageItem.key = [tmpDictionary objectForKey:@"key"];
            languageItem.value = [tmpDictionary objectForKey:@"value"];


            [result addObject:languageItem];
        }
    }

    _translations = result;
    _languageKey = [dictionary valueForKey:@"language_key"];

    return self;

}

-(NSString *) findTranslationForKey:(NSString *) key {

    for (id languageItem in _translations) {

        if ([languageItem isKindOfClass:[GUNLanguageItemResponse class]]) {

            GUNLanguageItemResponse *tmpLanguageItem = (GUNLanguageItemResponse *) languageItem;
            if ([tmpLanguageItem.key isEqualToString:key]) {
                return tmpLanguageItem.value;
            }
        }
    }

    return nil;
}
@end