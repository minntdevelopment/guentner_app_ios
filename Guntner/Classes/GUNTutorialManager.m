//
//  GUNTutorialManager.m
//  guentner
//
//  Created by Julian Dawo on 01.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//
#import "Firebase.h"
#import "GUNTutorialManager.h"
#import "GUNDataManager.h"
#import "GUNRootManager.h"
#import "GUNVersionControlResponse.h"
#import "GUNDynamicMenuResponse.h"
#import "GUNLanguagesResponse.h"
#import "GUNProductResponse.h"
#import "GUNLanguageItemResponse.h"
#import "GUNLanguageTO.h"
#import "GUNTutorialDetailResponse.h"

#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "GAITracker.h"
#import "GUNProductContentResponse.h"
#import "GUNContentTutorialResponse.h"

#import "GUNContentGroupResponse.h"


@implementation GUNTutorialManager {
  
    GUNRestmanager* _restManager;
    GUNDataManager* _dataManager;
    GUNDynamicMenuResponse *_activeDynmaicMenuResponse;
    GUNLanguagesResponse *_activeTranslationResponse;
    ContenRegion _activeRegion;
    NSArray *_selectedLanguages;
    

    BOOL _newDataAvailable;

    
}
NSString *const ERROR_DOMAIN = @"com.everywhere.guentner";

-(instancetype) initWithRestManager:(GUNRestmanager*) restManager dataManager:(GUNDataManager *) dataManager {
    self = [super init];
    
    if (self) {
        _restManager = restManager;
        _dataManager = dataManager;
        _userDefaultLanguage = [[[NSLocale preferredLanguages] objectAtIndex:0] substringToIndex:2];
        _activeTranslationResponse = [self getActiveLanguageTranslationObject];
        
    }
    return self;
}

-(void)downloadAllTutroialMenuDataIfNecessaryCompletion:(void (^)(NSError*))block {

    NSNumber *currentVersion = [_dataManager getVersionControlNumber];

//    if ([[GUNRootManager instance].reachabilityManager isOnline]) {
        
        [_restManager getVersionControlOverWebService:@"version/" completion:^(GUNVersionControlResponse *response, NSError *error) {

            if (error) {
                if ([_dataManager getDynamicMenuDictionary]) {
                    _activeDynmaicMenuResponse = [_dataManager getDynamicMenuFromDictionary:[_dataManager getDynamicMenuDictionary]];


                    _activeTranslationResponse = [self getActiveLanguageTranslationObject];

                    // offline Data available.
                    block(nil);
                } else {
                    block([self offlineErrorNoData]);
                }
            } else {



                if ([currentVersion longLongValue] != [response.content_version longLongValue] || [[[NSLocale preferredLanguages] objectAtIndex:0] substringToIndex:2] != _userDefaultLanguage) {
                    // we need to download all necessary Data again
                    _newDataAvailable = YES;

                    //download dynamic Menu
                    [_restManager getDynamicMenuOverWebService:@"menu/" completion:^(GUNDynamicMenuResponse *dynamicMenuResponse, NSError *error) {
                        if (error) {
                            block(error);
                        } else {
                           _activeDynmaicMenuResponse = dynamicMenuResponse;
                            [self saveAllLanguagesAvailableInMenuResponse:dynamicMenuResponse];

                            // download language File for default language

                            NSLog(@"Users Default Message : %@", _userDefaultLanguage);
                            [_restManager getOneTranslationListOverWebService:@"languages/language" andLanguageKey:_userDefaultLanguage completion:^(GUNLanguagesResponse *languagesResponse, NSError *error) {
                                if (error) {
                                    block(error);
                                } else {
                                    _activeTranslationResponse = languagesResponse;
                                    [_dataManager saveVersionControlPersistent:response.content_version];
                                    block(nil);
                                }
                            }];


                        }
                    }];


                } else {

                    _newDataAvailable = NO;
                    _activeDynmaicMenuResponse = [_dataManager getDynamicMenuFromDictionary:[_dataManager getDynamicMenuDictionary]];
                    

                    _activeTranslationResponse = [self getActiveLanguageTranslationObject];
                    
                    block(nil);

                }
            }
        }];
//    } else {
//        // check if offline Data is available.
//        if ([_dataManager getDynamicMenuDictionary]) {
//            _activeDynmaicMenuResponse = [_dataManager getDynamicMenuFromDictionary:[_dataManager getDynamicMenuDictionary]];
//
//
//
//            // offline Data available.
//            block(nil);
//        } else {
//            block([self offlineErrorNoData]);
//        }
//    }
}

-(void)saveAllLanguagesAvailableInMenuResponse:(GUNDynamicMenuResponse*)menuResponse{
    NSMutableSet* uniqueAvailableLanguages = [[NSMutableSet alloc]init];
    for (int i = 0; i < menuResponse.products.count; i++) {
        GUNProductResponse* productResponse = menuResponse.products[i];
        for (GUNProductContentResponse* contentResponse in productResponse.content) {
            for (int j = 0; j < contentResponse.availableLanguages.count; j++) {
                [uniqueAvailableLanguages addObject:[contentResponse.availableLanguages[j] lowercaseString]];
            }
        }
    }
    NSData *languagesData = [NSKeyedArchiver archivedDataWithRootObject:uniqueAvailableLanguages];
    
    [[NSUserDefaults standardUserDefaults] setObject:languagesData forKey:@"availableLanguagesInTutorials"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)downloadLangugeIfNotExistsWithLanguageKey:(NSString *)languageKey complition:(void (^)(NSError*))block {
    
    NSString* languageInLowerCase = [languageKey lowercaseString];
    
    if ([_dataManager getLanguageDictionaryForLanguageKey:languageInLowerCase] ) {
        // language already exists
        NSLog(@"Language already exists, no download necassery.");
        
        //addde this to always set activeLanguageResponse
        NSDictionary *languageDictionary = [_dataManager getLanguageDictionaryForLanguageKey:languageInLowerCase];
        _activeTranslationResponse = [_dataManager getLanguageFromDictionary:languageDictionary];
        
        block(nil);
    } else {

        [_restManager getOneTranslationListOverWebService:@"languages/language" andLanguageKey:languageInLowerCase completion:^(GUNLanguagesResponse *response, NSError *error) {
            if (error) {
                block(error);
            } else {
                //added this to save version persistent
                _activeTranslationResponse = response;
                block(nil);
            }
        }];


    }


}

- (NSArray *)getProductsForRegion:(ContenRegion)contentRegion {

    if (nil == _activeDynmaicMenuResponse) {
        return nil;
    }
    NSMutableArray *resultForRegion = [[NSMutableArray alloc] init];
    NSArray *productsOfSelectedRegion;

    // check which region
    if (contentRegion == asiaContenRegion) {
       productsOfSelectedRegion = _activeDynmaicMenuResponse.productsAsia;
    } else if (contentRegion == europeContenRegion) {
        productsOfSelectedRegion = _activeDynmaicMenuResponse.productsEurope;
    } else if (contentRegion == americaContenRegion) {
        productsOfSelectedRegion = _activeDynmaicMenuResponse.productsAmerica;
    }

    // find products for selected region
    for (id item in _activeDynmaicMenuResponse.products) {

        if ([item isKindOfClass:[GUNProductResponse class]]) {
            GUNProductResponse *tmpProduct = (GUNProductResponse *) item;

            if ([productsOfSelectedRegion containsObject:tmpProduct.productID]) {
                [resultForRegion addObject:tmpProduct];
            }
        }
    }

    return resultForRegion;
}

-(NSArray *) getContentForProduct:(NSNumber *)productID {

    GUNProductResponse *searchedProduct;
    //first find the product in the product list
    for (id item in _activeDynmaicMenuResponse.products) {

        if ([item isKindOfClass:[GUNProductResponse class]]) {
            GUNProductResponse *tmpProduct = (GUNProductResponse *) item;

            if ([tmpProduct.productID isEqualToNumber:productID]) {
                searchedProduct = tmpProduct;
                break;
            }
        }
    }

    // get the content of the searched product
    if (searchedProduct) {
        return searchedProduct.content;
    } else {
        return nil;
    }


}

-(NSString *) translationForKeyInActiveLanguage: (NSString *) key {
    
    if (nil == _activeTranslationResponse) {
        _activeTranslationResponse = [self getActiveLanguageTranslationObject];
    }
    
    for (id keyItem in _activeTranslationResponse.translations) {

        if ([keyItem isKindOfClass:[GUNLanguageItemResponse class]]) {
            GUNLanguageItemResponse *languageItem = (GUNLanguageItemResponse *) keyItem;

            if ([languageItem.key.lowercaseString isEqualToString:key.lowercaseString]) {
                return languageItem.value;
            }
        }
    }

    // if key not found, use the key also as value.
    return key;

}

- (ContenRegion)getActiveRegion {
    if (!_activeRegion) {
        _activeRegion = [_dataManager getSelectedRegionForTutorials];
    }
    return _activeRegion;
}

- (void)setActiveRegion:(ContenRegion)acvtiveRegion {
    _activeRegion = acvtiveRegion;
    [_dataManager saveSelectedRegionForTutorialsPersistent:acvtiveRegion];
}

- (NSArray *)getActiveLangues {
    if (!_selectedLanguages) {
        _selectedLanguages = [_dataManager getSelectedLanguagesForTutorials];
    }
    return _selectedLanguages;
}

-(void)setActiveLanguageToPhoneLanguage{
    _activeTranslationResponse = [_dataManager getLanguageFromDictionary:[_dataManager getLanguageDictionaryForLanguageKey:_userDefaultLanguage]];
}

- (void)setActiveLangues:(NSArray *)selectedLanguages {
    
    _selectedLanguages = selectedLanguages;
    [_dataManager saveSelectedLanguagesForTutorialPersistent:selectedLanguages];
    
   
}


-(NSString *)nameForLanaguageOfLanguageCode:(NSString *) langCode {

    NSString *languageCode = [langCode lowercaseString];

    if ([languageCode isEqualToString:@"en"]) {
        return @"English";
    } else if ([languageCode isEqualToString:@"cn"]) {
        return @"Chinese";
    } else if ([languageCode isEqualToString:@"bg"]) {
        return @"Bulgarian";
    } else if ([languageCode isEqualToString:@"hr"]) {
        return @"Croatian";
    } else if ([languageCode isEqualToString:@"cz"]) {
        return @"Czech";
    } else if ([languageCode isEqualToString:@"dk"]) {
        return @"Danish";
    } else if ([languageCode isEqualToString:@"nl"]) {
        return @"Dutch";
    } else if ([languageCode isEqualToString:@"fr"]) {
        return @"French";
    } else if ([languageCode isEqualToString:@"de"]) {
        return @"German";
    } else if ([languageCode isEqualToString:@"hu"]) {
        return @"Hungarian";
    } else if ([languageCode isEqualToString:@"it"]) {
        return @"Italian";
    } else if ([languageCode isEqualToString:@"no"]) {
        return @"Norwegian";
    } else if ([languageCode isEqualToString:@"pl"]) {
        return @"Polish";
    } else if ([languageCode isEqualToString:@"ro"]) {
        return @"Romanian";
    } else if ([languageCode isEqualToString:@"ru"]) {
        return @"Russian";
    } else if ([languageCode isEqualToString:@"sl"]) {
        return @"Slovenian";
    } else if ([languageCode isEqualToString:@"es"]) {
        return @"Spanish";
    } else if ([languageCode isEqualToString:@"se"]) {
        return @"Swedish";
    } else if ([languageCode isEqualToString:@"pt"]) {
        return @"Portuguese";
    }

    return langCode;
}

- (NSArray *)languagesForRegion:(RegionCode)regionCode {

    NSMutableArray *result = [[NSMutableArray alloc] init];
    if (regionCode == asiaContenRegion) {
        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"en" andTranslatedValue:@"English"]];
        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"cn" andTranslatedValue:@"Chinese"]];
    } else if (regionCode == europeContenRegion) {

        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"bg" andTranslatedValue:@"Bulgarian"]];
        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"hr" andTranslatedValue:@"Croatian"]];
        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"cz" andTranslatedValue:@"Czech"]];
        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"dk" andTranslatedValue:@"Danish"]];
        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"nl" andTranslatedValue:@"Dutch"]];
        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"en" andTranslatedValue:@"English"]];
        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"fr" andTranslatedValue:@"French"]];
        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"de" andTranslatedValue:@"German"]];
        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"hu" andTranslatedValue:@"Hungarian"]];
        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"it" andTranslatedValue:@"Italian"]];
        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"no" andTranslatedValue:@"Norwegian"]];
        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"pl" andTranslatedValue:@"Polish"]];
        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"ro" andTranslatedValue:@"Romanian"]];
        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"ru" andTranslatedValue:@"Russian"]];
        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"sl" andTranslatedValue:@"Slovenian"]];
        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"es" andTranslatedValue:@"Spanish"]];
        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"se" andTranslatedValue:@"Swedish"]];
    }  else if (regionCode == americaContenRegion) {

        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"en" andTranslatedValue:@"English"]];
        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"pt" andTranslatedValue:@"Portuguese"]];
        [result addObject:[[GUNLanguageTO alloc] initWithLangugeCode:@"es" andTranslatedValue:@"Spanish"]];
    }
    

    return result;
}

- (void)loadTutorialByID:(NSInteger)tutorialID completed:(void (^)(GUNTutorialDetailResponse *, NSError*))block {

    GUNTutorialDetailResponse *result;


    if (!_newDataAvailable ) {

        //first check if tutorial is already offline available
        result = [_dataManager getTutorialDetailsFromDictionary:[_dataManager getTutorialByID:tutorialID]];

        if (result != nil && result.tutorialID != nil) {
            block(result,nil);

        } else {
            // not offline available load from server
            [_restManager getTutorialDetailsOverWebService:@"tutorialDetails/" withID:tutorialID completion:^(GUNTutorialDetailResponse *response, NSError *error) {
                if (!error) {
                    block(response,nil);
                    
                } else {
                    block(nil,error);
                }
            }];

        }

    } else {
        // load new data since they are updated

        [_restManager getTutorialDetailsOverWebService:@"tutorialDetails/" withID: tutorialID completion:^(GUNTutorialDetailResponse *response, NSError *error) {
            if (!error) {
                block(response,nil);
                // save also local
            } else {

                // downloading new data failed, check if offline data is available
                GUNTutorialDetailResponse *localResult = [_dataManager getTutorialDetailsFromDictionary:[_dataManager getTutorialByID:tutorialID]];

                if (localResult) {
                    block(localResult,nil);
                } else {
                    block(nil,error);
                }


            }
        }];


    }



}

-(BOOL) checkIfFeedbackForTutorialExists:(NSNumber *) tutorialID {

    NSArray *tutorialsWithFeedback = [_dataManager getAllTutorialsWithFeedback];

    for (NSNumber *tutorial in tutorialsWithFeedback) {
        if ([tutorial isEqualToNumber:tutorialID]) {

            return YES;
        }
    }

    return NO;
}

-(void) feedbackSentForTutorial:(NSNumber *) tutorialID withRating:(NSNumber *) rating {
    [_dataManager addTutorialToFeedbackList:tutorialID];
//    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    NSString* labelString = [NSString stringWithFormat:@"tutorial-ID: %@ - rating: %@",[tutorialID stringValue],[rating stringValue]];
//    [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"tutorial_Feedback"     // Event category (required)
//                                                          action:@"send_feedback"  // Event action (required)
//                                                           label:labelString          // Event label
//                                                           value:nil] build]];    // Event value
    
    [FIRAnalytics logEventWithName:@"tutorial_Feedback"
                        parameters:@{
                                     kFIRParameterItemID:@"tutorial_Feedback",
                                     kFIRParameterItemName:labelString,
                                     kFIRParameterContentType:@"send_feedback"
                                     }];
    NSLog(@"Feedback sent.");
}


-(GUNContentTutorialResponse *) nextTutorialForTutorialID:(NSNumber *) tutorialID andProduct:(NSNumber *)productID {
    //find tutorial in products
    BOOL inGroupFound = NO;
    
    //go trough all products in active menu, e.g. GMM EC
    for (GUNProductResponse *product in _activeDynmaicMenuResponse.products) {
        
        // check if current product's productID equals ID of current product
        if (product.productID.integerValue == productID.integerValue) {
            
            //go trough products content, e.g. Erstinbetriebnahme
        for (int i = 0; i < [product.content count]; i++) {


            GUNProductContentResponse *content = product.content[i];
            
            //1. CASE: check if content's contenttype is groupContentType
            if ([content.contentType integerValue] == contentGroupContentType) {
                GUNContentGroupResponse *groupContent = (GUNContentGroupResponse *) content;
                
                //if contenttype is group, go through contents group items
                for (int j = 0; j < [groupContent.groupItems count]; j++) {
                    
                     GUNProductContentResponse *innerGroupContent = groupContent.groupItems[j];
                    
                    // if groupitem has contenttype Interactive...
                    if ([innerGroupContent.contentType integerValue] == interactiveTutorialContentType) {
                        GUNContentTutorialResponse *tutorialContent = (GUNContentTutorialResponse *) innerGroupContent;
                        
                        //... and it's tutorial ID equals tutorialID parameter...
                        if (tutorialContent.tutorialId.integerValue == tutorialID.integerValue || inGroupFound) {
                            int k = j + 1;
                            if (inGroupFound) {
                                k = j;
                            }
                            GUNProductContentResponse *innerNextContent;
                            
                            //then search it's next innergroup item
                            //if it's not an InteractiveTutorial, go to next one
                            while (k < [groupContent.groupItems count] && [innerNextContent.contentType integerValue] != interactiveTutorialContentType) {
                                
                                innerNextContent = groupContent.groupItems[k];
                                k++;
                            }
                            
                            //if it is a interactive tutorial, that's the tutorial we wanted to find
                            if ([innerNextContent.contentType integerValue] == interactiveTutorialContentType) {
                                
                                GUNContentTutorialResponse *innertutorialContent = (GUNContentTutorialResponse *) innerNextContent;
                                
                                //return found tutorial
                                return innertutorialContent;
                            }
                            //set flag that we found Tutorial in Group
                            inGroupFound = YES;
                        }
                    }
                    
                }
                
            }
            
            
            //2. CASE: check if content's contenttype is interactiveTutorialType
            if ([content.contentType integerValue] == interactiveTutorialContentType) {
                GUNContentTutorialResponse *tutorialContent = (GUNContentTutorialResponse *) content;
                //if tutorialContent ID equals tutorialID parameter or inGroupFound is true
                if (tutorialContent.tutorialId.integerValue == tutorialID.integerValue || inGroupFound ) {
                    
                    int y = i + 1;
                    if (inGroupFound) {
                        y = i;
                        inGroupFound = NO;
                    }
                    
                    
                    
                    GUNProductContentResponse *nextContent;
                    
                    //if nextContent is not an interactiveTutorial, go to next one
                    while (y < [product.content count] && [nextContent.contentType integerValue] != interactiveTutorialContentType) {

                        nextContent = product.content[y];
                        y++;
                    }

                    if ([nextContent.contentType integerValue] == interactiveTutorialContentType) {

                        GUNContentTutorialResponse *tutorialContent = (GUNContentTutorialResponse *) nextContent;
                        return tutorialContent;
                    }

                }
            }

            }
        }
    }

    return nil;
}


-(GUNLanguagesResponse *) getActiveLanguageTranslationObject {
    
    GUNLanguagesResponse* result;
    
    //check if the user already selected a language.
    if ([self getActiveLangues].count > 0) {
        // there is an existing langunge
        NSString* firstSelectedLanguage = [[self getActiveLangues] firstObject];
       result = [_dataManager getLanguageFromDictionary:[_dataManager getLanguageDictionaryForLanguageKey:firstSelectedLanguage]];
    } else {
        // there is no active langunage, so use the device language
      result = [_dataManager getLanguageFromDictionary:[_dataManager getLanguageDictionaryForLanguageKey:_userDefaultLanguage]];
    }
    return result;
}

#pragma mark - helping for tutorials

-(NSError *)offlineErrorNoData {
    NSString *errorDescription = @"The selected Link is not existing.";
    NSMutableDictionary* details = [NSMutableDictionary dictionary];
    [details setValue:errorDescription forKey:NSLocalizedDescriptionKey];

    return [NSError errorWithDomain:ERROR_DOMAIN code:101 userInfo:details];
}


- (NSString *)valueForRegionCode:(ContenRegion)region {
    switch (region) {
        case 20001:
            return @"Asien";

        case 20002:
            return @"Europe";

        case 20003:
            return @"America";

        default:
            return nil;
    }
}
@end
