//
//  GUNInsightTutorialHintViewController.h
//  guentner
//
//  Created by Julian Dawo on 06.09.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import "PageItemController.h"

@interface GUNInsightTutorialHintViewController : PageItemController


@property (nonatomic,weak) IBOutlet UIView *v_textBox;
@property (nonatomic,strong) NSString *hintText;
@property (weak, nonatomic) IBOutlet UITextView *txt_v_hintText;

@end
