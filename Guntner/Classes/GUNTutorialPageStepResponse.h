//
// Created by Julian Dawo on 16.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GUNTutorialPageStepResponse : NSObject


@property (nonatomic, strong) NSString *stepDescriptionLine1;
@property (nonatomic, strong) NSString *stepDescriptionLine2;
@property BOOL showEqualActionCount;
@property (nonatomic, strong) NSNumber *actionsButtonType;

@property (nonatomic, strong) NSArray *stepActions;

@end