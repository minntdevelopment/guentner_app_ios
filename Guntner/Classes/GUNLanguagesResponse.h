//
// Created by Julian Dawo on 01.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GUNLanguagesResponse : NSObject

@property (nonatomic, strong) NSString *languageKey;
@property (nonatomic, strong) NSArray *translations;

//- (id)initWithDictionary:(NSDictionary *)dictionary;

-(NSString *) findTranslationForKey:(NSString *) key;

@end