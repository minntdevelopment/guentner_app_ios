//
// Created by Julian Dawo on 08.08.15.
// Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SelectionItemTO : NSObject

@property (nonatomic, strong) NSObject *key;
@property (nonatomic, strong) NSString *translatedValue;
@property BOOL isSelected;

- (instancetype)initWithKey:(NSObject *)key andTranslatedValue:(NSString *)translatedValue selectionState: (BOOL) selected;

@end