//
//  GUNRootManager.h
//  guentner
//
//  Created by Julian Dawo on 01.08.15.
//  Copyright (c) 2015 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GUNReachabilityManager.h"
#import "GUNRestmanager.h"
#import "GUNTutorialManager.h"

@class GUNDataManager;

/*!
 * Singlton Class that contains all Manager Classes.
 * The Manger Classes contains the business logic of the application
 *
 */

@interface GUNRootManager : NSObject




/// MenuManager (read-only) the instance of the menu manager.
@property (strong,nonatomic,readonly) GUNTutorialManager* tutorialManager;

/// RestManager (read-only) the instance of the rest manager.
@property (strong,nonatomic,readonly) GUNRestmanager* restManager;

/// ReachabilityManager (read-only) the instance of reachability manager.
@property (strong,nonatomic,readonly) GUNReachabilityManager* reachabilityManager;

/// DatayManager (read-only) the instance of reachability manager.
@property (strong,nonatomic,readonly) GUNDataManager* dataManager;

/*!
 *  Get the singleton
 *
 *  @return The shared instance of BAYRootManager.
 */
+ (GUNRootManager* )instance;

@end
