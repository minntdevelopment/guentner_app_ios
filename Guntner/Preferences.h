//
//  Preferences.h
//  guentner
//
//  Created by Wassim Ben Hamadou on 20.05.14.
//  Copyright (c) 2014 Brandixi3. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PREF_APP_USE_COUNT @"PREF_APP_USE_COUNT"
#define APP_RATING_POPUP_VIEWED @"APP_RATING_POPUP_VIEWED"

@interface Preferences : NSObject

+(int)getAppOpenCount;
+(void)setAppOpenCount:(int)value;
+(bool)getPopupViewedFlag;
+(void)setPopupViewedFlag:(bool)value;
+(void)setUserPreference:(id)value forKey:(NSString*)key;
+(id)getUserPreference:(NSString*)forKey;

@end
