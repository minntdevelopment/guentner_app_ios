/**
 * Class for handling data on the material selection page
 */

function MaterialData(data) {
	this.products = [];
	this.categories = [];
	this.applicationCases = [];
	
	this.currentIndoorOutdoorName = "indoor";
	this.currentProductId = 1;
	this.currentCategoryId = 1;
	this.currentAppCaseId = 1;
	
	// list of icons; array index equals category id -> no first element
	this.CategoryImageList = [
		false,
		"img/MaterialCategory/obst@2x.png",
		"img/MaterialCategory/fleisch@2x.png",
		"img/MaterialCategory/fisch@2x.png",
		"img/MaterialCategory/molkereiprodukte@2x.png",
		"img/MaterialCategory/getraenke@2x.png",
		"img/MaterialCategory/gastronomie_icon_blue.png", // Gastro/Catering
		"img/MaterialCategory/teigwaren@2x.png",
		"img/MaterialCategory/gehoelze@2x.png",
		"img/MaterialCategory/innenstadt_icon.png", // Innenstadt
		"img/MaterialCategory/traktor_icon_blue.png", // Landwirtschaft
		"img/MaterialCategory/industrie@2x.png",
		"img/MaterialCategory/kuestennaehe@2x.png",
		"img/MaterialCategory/seeluft@2x.png",
		"img/MaterialCategory/zement@2x.png",
		"img/MaterialCategory/muellverbrennung@2x.png",
		
		"img/MaterialCategory/obst@2x.png",
		"img/MaterialCategory/fleisch@2x.png",
		"img/MaterialCategory/fisch@2x.png",
		"img/MaterialCategory/molkereiprodukte@2x.png",
		"img/MaterialCategory/getraenke@2x.png",
		"img/MaterialCategory/gastronomie_icon_blue.png", // Gastro/Catering
		"img/MaterialCategory/teigwaren@2x.png",
		"img/MaterialCategory/gehoelze@2x.png",
		"img/MaterialCategory/innenstadt_icon.png", // Innenstadt
		"img/MaterialCategory/traktor_icon_blue.png", // Landwirtschaft
		"img/MaterialCategory/industrie@2x.png",
		"img/MaterialCategory/kuestennaehe@2x.png",
		"img/MaterialCategory/seeluft@2x.png",
		"img/MaterialCategory/zement@2x.png",
		"img/MaterialCategory/muellverbrennung@2x.png"
	];
	
	this.initialize(data);
}

// read data from the given json object
MaterialData.prototype.initialize = function( data ) {
	var me = this;
	me.products = [];
	data.forEach( function( product ) {
		me.products.push({
			id: product.id,
			typeName: product.typeName,
			name: product.name
		});
		product.categories.forEach( function( category ) {
			me.categories.push({
				id: category.id,
				name: category.name,
				productId: product.id
			});
			category.applicationCases.forEach( function( applicationCase ) {
				me.applicationCases.push({
					id: applicationCase.id,
					productId: product.id,
					categoryId: category.id,
					name: applicationCase.name,
					tube_min: applicationCase.tube_min,
					tube_max: applicationCase.tube_max,
					fin_min: applicationCase.fin_min,
					fin_max: applicationCase.fin_max,
					coating: applicationCase.coating,
					frontplate_min: applicationCase.frontplate_min,
					frontplate_max: applicationCase.frontplate_max,
					casing_min: applicationCase.casing_min,
					casing_max: applicationCase.casing_max
				});
			});
		});
	});
	
	// set event listeners for page transitions; each time the corresponding view gets displayed,
	// reload the contents
	// $(document).on("pagebeforeshow", "#Third-page", function () {
};

// get all products
MaterialData.prototype.getProducts = function() {
	return this.products;
};

// get a product by its id
MaterialData.prototype.getProduct = function( id ) {
	for ( var i = 0; i < this.products.length; i++ )
		if ( this.products[ i ].id == id )
			return this.products[i];
	return false;
};

// get a category by its id
MaterialData.prototype.getCategory = function( categoryId ) {
	for ( var i = 0; i < this.categories.length; i++ )
		if ( this.categories[ i ].id == categoryId )
			return this.categories[i];
	return false;
};

// get a list of categories for the given product
MaterialData.prototype.getCategoriesForProduct = function( product ) {
	var result = [];
	this.categories.forEach( function( category ) {
		if ( category.productId == product.id ) {
			result.push( category );
		}
	});
	return result;
};

// get a list of application cases for the given category
MaterialData.prototype.getApplicationCasesForCategory = function( category ) {
	var result = [];
	this.applicationCases.forEach( function( applicationCase ) {
		if ( applicationCase.categoryId == category.id && applicationCase.productId == category.productId ) {
			result.push( applicationCase );
		}
	});
	return result;
};

MaterialData.prototype.getApplicationCase = function( appCaseId ) {
	for ( var i = 0; i < this.applicationCases.length; i++ )
		if ( this.applicationCases[i].id == appCaseId )
			return this.applicationCases[i];
	return false;
};

/**
 						RENDER METHODS
 **/

/// render the first set of buttons (indoor/outdoor)
MaterialData.prototype.renderIndoorOutdoorButtons = function(typeName) {
	var me = this,
		btnIndoor = $('#btnIn'),
		btnOutdoor = $('#btnOut');
	
	if ( typeof( typeName ) == "undefined" ) {
		// init
		btnIndoor.click(function (e) {
			me.currentIndoorOutdoorName = "indoor";
			// me.slideTo( me.currentProductId == 4 ? 2 : 1 );
			me.slideTo(1);
		});
		btnOutdoor.click(function (e) {
			me.currentIndoorOutdoorName = "outdoor";
			// me.slideTo( me.currentProductId == 3 ? 2 : 1 );
			me.slideTo(1);
		});
		typeName = "indoor";
	}
	
	me.currentIndoorOutdoorName = typeName;
	
	btnIndoor.find(".button_text").text(jQuery.i18n.prop('indoor'));
	btnOutdoor.find(".button_text").text(jQuery.i18n.prop('outdoor'));
	
	if ( typeName == "indoor" ) {
		btnIndoor[0].classList.remove("deselected");
		btnIndoor[0].classList.add("selected");
		btnOutdoor[0].classList.remove("selected");
		btnOutdoor[0].classList.add("deselected");
	} else {
		btnIndoor[0].classList.remove("selected");
		btnIndoor[0].classList.add("deselected");
		btnOutdoor[0].classList.remove("deselected");
		btnOutdoor[0].classList.add("selected");
	}
};

/// render the slider to switch between product groups
MaterialData.prototype.renderSliders = function(activeOptionIndex) {
	var me = this,
		slider1 = $("#slider1"),
		slider2 = $("#slider2");
	
	var slider1Text = me.currentIndoorOutdoorName == "indoor" ? me.products[0].name : me.products[1].name;
	var slider2Text = me.currentIndoorOutdoorName == "indoor" ? me.products[2].name : me.products[3].name;
	slider1.find(".content").text(jQuery.i18n.prop(slider1Text));
	slider2.find(".content").text(jQuery.i18n.prop(slider2Text));
	
	// if initial call (i.e. no option index): initialize the slider
	if ( typeof( activeOptionIndex ) == "undefined" ) {
		
		slider2.css("left", "1000px");
		var sliderBackground = $('#slider_background');
		sliderBackground.hammer().on("swipeleft", function () {
			me.slideTo(2);
		});
		sliderBackground.hammer().on("swiperight", function () {
			me.slideTo(1);
		});
		sliderBackground.click(function () {
			// trigger a slide action
			switch ( me.currentProductId ) {
				case 1:
				case 2:
					me.slideTo(2);
					break;
				case 3:
				case 4:
					me.slideTo(1);
					break;
			}
		});
		
		activeOptionIndex = 1;
	}
	
	if ( activeOptionIndex == 1 ) {
		$('.sliderBackgroundLeftIndicator').hide();
		$('.sliderBackgroundRightIndicator').show();
	} else {
		$('.sliderBackgroundLeftIndicator').show();
		$('.sliderBackgroundRightIndicator').hide();
	}
};

/// slide to the given slider index (1 or 2)
MaterialData.prototype.slideTo = function( optionIndex ) {
	var me = this;
	
	if ( optionIndex == 2 ) {
		$('.sliderBackgroundLeftIndicator').show();
		$('.sliderBackgroundRightIndicator').hide();
		$('#slider1').animate({
			left: (-1 * $(document).width())
		}, 300, function () {
			// Animation complete.
			me.renderIndoorOutdoorButtons(me.currentIndoorOutdoorName);
			me.renderSliders( 2 );
			me.renderCategoryList( me.currentIndoorOutdoorName == "indoor" ? 3 : 4 );
		});
		$('#slider2').animate({
			left: "0px"
		}, 300, function () {
			// Animation complete.
		});
	} else {
		$('.sliderBackgroundLeftIndicator').hide();
		$('.sliderBackgroundRightIndicator').show();
		$('#slider1').animate({
			left: "0px"
		}, 300, function () {
			// Animation complete.
			me.renderIndoorOutdoorButtons(me.currentIndoorOutdoorName);
			me.renderSliders( 1 );
			me.renderCategoryList( me.currentIndoorOutdoorName == "indoor" ? 1 : 2 );
		});
		$('#slider2').animate({
			left: ($(document).width())
		}, 300, function () {
			// Animation complete.
		});
	}
};

/// render the list of categories
MaterialData.prototype.renderCategoryList = function( productId ) {
	var me = this,
		product = me.getProduct(productId),
		categoryList = me.getCategoriesForProduct(product),
		targetNode = $('.categoryCollection');
	
	me.currentProductId = productId;
	
	targetNode.empty();
	
	categoryList.forEach( function( category ) {
		var icon = me.CategoryImageList[category.id],
			name = jQuery.i18n.prop(category.name),
			container = document.createElement("div"),
			imageContainer = document.createElement("div"),
			labelContainer = document.createElement("div");
		container.appendChild(imageContainer);
		container.appendChild(labelContainer);
		targetNode.append(container);
		
		container.setAttribute("id", "id" + category.id);
		container.classList.add("mr_ml");
		imageContainer.classList.add("mr_ml_img");
		labelContainer.classList.add("mr_ml_t");
		
		$(imageContainer).css("background-image", "url(" + icon + ")");
		$(labelContainer).html(name);
		
		// targetNode.append("<center><div class='mr_ml' id = 'id" + category.id + "'><div class='mr_ml_img'><img src='" + icon + "'  /></div><div class='mr_ml_t'>" + name + "</div></div></center>");
		$( "#id" + category.id ).click( function() {
			me.renderApplicationCaseList(category.id);
		});
	});

	// append invisible elements to make flexbox behave like a grid
	if(targetNode.children().count % 3 != 0) {
		var left = 3 - (targetNode.children().length % 3);
		for(var i = 0; i < left; i++) {
			targetNode.append('<div class="mr_ml dummy"><div class="mr_ml_img"><img /></div><div class="mr_ml_t">dummy</div></div>');
		}
	}
};

/// render the list of application cases (overview)
MaterialData.prototype.renderApplicationCaseList = function( categoryId ) {
	var me = this,
		category = me.getCategory(categoryId),
		appCases = me.getApplicationCasesForCategory(category),
		$target = $("#prof-list");
	
	$target.empty();
	
	$("#CategoryType").html(jQuery.i18n.prop(category.name));
	$("#CategoryImage").html('<img src="' + me.CategoryImageList[category.id] + '"/>');
	
	appCases.forEach( function( appCase ) {
		var li = '<li><a href="#" id="' + appCase.id + '" class="info-go">' + jQuery.i18n.prop(appCase.name) + '</a> <div class="sendPageListAccessory"></div></li>';
		$target.append(li);
		$target.unbind("click").on("click", ".info-go", function(e) {
			e.preventDefault();
			var appCase = me.getApplicationCase(this.id);
			
			me.renderApplicationCaseDetail(appCase);
		});
	});

	$.mobile.changePage("#Second-page");
	$target.listview();
	$target.listview("refresh");
};

/// render the details for the given application case
MaterialData.prototype.renderApplicationCaseDetail = function( appCase ) {
	var me = this,
		category = me.getCategory(appCase.categoryId),
		product = me.getProduct(appCase.productId);
	
	$("#CategoryType_ThirdPage").html( jQuery.i18n.prop(category.name) );
	$("#AppCaseName_Thirdpage").html( jQuery.i18n.prop(appCase.name) );
	$("#CateImageThirdPage").html( '<img src="' + me.CategoryImageList[category.id] + '"/>' );
	
	// inline function declaration, welcome to 2017
	function createLabel( text, hasMinMax ) {
		return text + ( hasMinMax ? '<span class="footnote"> (*1)</span>' : "" );
	}
	function createContent(min, max) {
		if ( max ) {
			return '<div class="min"><div class="label">(' + jQuery.i18n.prop("min") + ')</div> <div class="value">' + jQuery.i18n.prop(min) + '</div></div><div class="max"><div class="label">(' + jQuery.i18n.prop("max") + ')</div> <div class="value">' + jQuery.i18n.prop(max) + '</div></div>'
		} else {
			return jQuery.i18n.prop(min)
		}
	}
	
	$("#Tube_Title").html( createLabel( jQuery.i18n.prop("rohr"), !!appCase.tube_max ) );
	$("#Tube").html( createContent( appCase.tube_min, appCase.tube_max ) );
	
	$("#Fin_Title").html( createLabel( jQuery.i18n.prop("lamelle"), !!appCase.fin_max ) );
	$("#Fin").html( createContent( appCase.fin_min, appCase.fin_max ) );
	
	$("#FrontPlate_Title").html( createLabel( jQuery.i18n.prop("stirnbleche"), !!appCase.frontplate_max ) );
	$("#FrontPlate").html( createContent( appCase.frontplate_min, appCase.frontplate_max ) );
	
	$("#Casing_Title").html( createLabel( jQuery.i18n.prop("gehaeusewanne"), !!appCase.casing_max ) );
	$("#Casing").html( createContent( appCase.casing_min, appCase.casing_max ) );
	
	if ( appCase.coating ) {
		$("#Coating_Title").parent().show();
		$("#Coating_Title").html( jQuery.i18n.prop("blockbeschichtung") + '<span class="footnote"> (*2)</span>' );
		$("#Coating").html( jQuery.i18n.prop(appCase.coating) );
	} else {
		$("#Coating_Title").parent().hide();
	}
	
	// legend
	var legendMinMax = $("#Legend_MinMax");
	if ( appCase.tube_max || appCase.frontplate_max || appCase.casing_max ) {
		legendMinMax.show();
		legendMinMax.find("h2").html( "(*1) " + jQuery.i18n.prop("hinweis") );
		legendMinMax.find("p").html([
			'<strong>' + jQuery.i18n.prop("min") + ': </strong>',
			jQuery.i18n.prop('note_min'),
			'<br />',
			'<strong>' + jQuery.i18n.prop("max") + ': </strong>',
			jQuery.i18n.prop("note_max")
		]);
	} else {
		legendMinMax.hide();
	}
	
	var legendCoating = $("#Legend_Coating");
	if ( appCase.coating ) {
		legendCoating.show();
		legendCoating.find("h2").html( "(*2) " + jQuery.i18n.prop("hinweis") );
		legendCoating.find("p").html( jQuery.i18n.prop("note_coil_defender") );
	} else {
		legendCoating.hide();
	}
	
	$.mobile.changePage("#Third-page");
};
