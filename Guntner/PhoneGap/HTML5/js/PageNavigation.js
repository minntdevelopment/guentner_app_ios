var PageNavigation = {
    
    showBackButton: function (success, fail, resultType) {
        return Cordova.exec(
                            success,                // Success Callback
                            fail,                   // Error/Fail Callback
                            "PageNavigation",       // Plugin Name - config.xml
                            "showBackButton",       // Method Name
                            [resultType]            // Arguments
                            );
    }
};