/**
 * Created by Uchitha on 3/10/14.
 */
var LanguageSetter = {

    //
};

LanguageSetter.getLanguageValue = function (success, fail, resultType) {
    return Cordova.exec(
        success,                // Success Callback
        fail,                   // Error/Fail Callback
        "LanguageSetter",       // Plugin Name - config.xml
        "getLanguageValue",     // Method Name
        [resultType]            // Arguments
    );
}