var LoadingView = {
    
    //
    
};


LoadingView.showLoadingView = function (success, fail, resultType) {
    return Cordova.exec(
                        success,                // Success Callback
                        fail,                   // Error/Fail Callback
                        "LoadingView",          // Plugin Name - config.xml
                        "showLoadingView",      // Method Name
                        [resultType]            // Arguments
                        );
}

LoadingView.dismissLoadingView = function (success, fail, resultType) {
    return Cordova.exec(
                        success,                // Success Callback
                        fail,                   // Error/Fail Callback
                        "LoadingView",          // Plugin Name - config.xml
                        "dismissLoadingView",   // Method Name
                        [resultType]            // Arguments
                        );
}