$(function(){
  var currencies = [
    { value: 'Refrigerator 1', data: '<a href="http://www.education.gov.yk.ca/pdf/pdf-test.pdf">Data Sheet</a>' },
    { value: 'Refrigerator 2', data: '<a href="http://www.education.gov.yk.ca/pdf/pdf-test.pdf">Data Sheet</a>'  },
    { value: 'Refrigerator 3', data: 'DZD' },
    { value: 'Refrigerator 4', data: 'EUR' },
    { value: 'Refrigerator 5', data: '<a href="http://www.education.gov.yk.ca/pdf/pdf-test.pdf">Data Sheet</a>'  },
    { value: 'Refrigerator 6', data: 'XCD' },
    { value: 'Refrigerator 7', data: 'BSD' },
    { value: 'Refrigerator 8', data: 'BHD' },
    { value: 'Refrigerator 9', data: 'BDT' },
    { value: 'Refrigerator 10', data: '<a href="http://www.education.gov.yk.ca/pdf/pdf-test.pdf">Data Sheet</a>'  },
  ];
  
  // setup autocomplete function pulling from currencies[] array
  $('#autocomplete').autocomplete({
    lookup: currencies,
    onSelect: function (suggestion) {
      var thehtml =  suggestion.value + '   ' + suggestion.data;
      $('#outputcontent').html(thehtml);
    }
  });
  
  
  function openUrl(url)
  {
    window.open(url,'_system');
  }
  

});