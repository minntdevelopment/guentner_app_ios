//
//  LanguageSetter.h
//  guentner
//
//  Created by Uchitha Amarasinghe on 3/10/14.
//  Copyright (c) 2014 Brandixi3. All rights reserved.
//

#import <Cordova/CDVPlugin.h>

@interface LanguageSetter : CDVPlugin


- (void) getLanguageValue:(CDVInvokedUrlCommand *)command;

@end
