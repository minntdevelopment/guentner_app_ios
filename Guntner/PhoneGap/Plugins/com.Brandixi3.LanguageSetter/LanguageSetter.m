//
//  LanguageSetter.m
//  guentner
//
//  Created by Uchitha Amarasinghe on 3/10/14.
//  Copyright (c) 2014 Brandixi3. All rights reserved.
//

#import "LanguageSetter.h"

@implementation LanguageSetter

- (void) getLanguageValue:(CDVInvokedUrlCommand *)command
{
    
    CDVPluginResult* pluginResult = nil;
    NSString* localizedKey = [command.arguments objectAtIndex:0];
    
    
    if (localizedKey != nil && [localizedKey length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:NSLocalizedString(localizedKey, nil)];

    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

}



@end
