//
//  LoadingView.h
//  Guntner
//
//  Created by Shiraz Omar on 9/13/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import <Cordova/CDVPlugin.h>

@interface LoadingView : CDVPlugin


- (void)showLoadingView:(CDVInvokedUrlCommand *)command;
- (void)dismissLoadingView:(CDVInvokedUrlCommand *)command;


@end
