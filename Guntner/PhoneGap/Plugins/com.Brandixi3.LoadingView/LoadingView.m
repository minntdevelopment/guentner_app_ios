//
//  LoadingView.m
//  Guntner
//
//  Created by Shiraz Omar on 9/13/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "LoadingView.h"

#import "Configuration.h"

@implementation LoadingView


- (void)showLoadingView:(CDVInvokedUrlCommand *)command {
    
    // Post Notification To Dismiss Loading View
    [[NSNotificationCenter defaultCenter] postNotificationName:kShowLoadingViewIdentifier object:nil];
}

- (void)dismissLoadingView:(CDVInvokedUrlCommand *)command {
    
    // Post Notification To Dismiss Loading View
    [[NSNotificationCenter defaultCenter] postNotificationName:kDismissLoadingViewIdentifier object:nil];
}


@end
