//
//  PageNavigation.m
//  Guntner
//
//  Created by Shiraz Omar on 8/20/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import "PageNavigation.h"

#import "Configuration.h"

@implementation PageNavigation


- (void)showBackButton:(CDVInvokedUrlCommand *)command {
    
    NSString *resultType = [command.arguments objectAtIndex:0];
    
    if ( [resultType isEqualToString:kCDVPageNavigationResultTypeSuccess] ) {
        // Post Notification To Show Back Button
        [[NSNotificationCenter defaultCenter] postNotificationName:kSwapMenuButtonWithBackButtonIdentifier object:nil];
    } else if ( [resultType isEqualToString:kCDVPageNavigationResultTypeShowSharePDF] ) {
        // Post Notification To Show PDF Share Button
        [[NSNotificationCenter defaultCenter] postNotificationName:kShowSharePDFButtonIdentifier object:nil];
    }
    else {
        NSLog(kCDVPageNavigationBackButtonError);
    }
}


@end
