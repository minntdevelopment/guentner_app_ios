//
//  PageNavigation.h
//  Guntner
//
//  Created by Shiraz Omar on 8/20/13.
//  Copyright (c) 2013 Brandixi3. All rights reserved.
//

#import <Cordova/CDVPlugin.h>

@interface PageNavigation : CDVPlugin


- (void)showBackButton:(CDVInvokedUrlCommand *)command;


@end
